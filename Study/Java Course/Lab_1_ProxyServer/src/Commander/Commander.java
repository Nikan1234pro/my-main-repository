package Commander;
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.StringTokenizer;
import Logger.*;
import Server.Server;
import javax.swing.*;

public class Commander {
    private JTextField in;
    private JTextArea out;
    private Server server = null;

    public Commander(JTextField in, JTextArea out) {
        this.in = in;
        this.out = out;
    }

    public void command(){
        StringTokenizer st = new StringTokenizer(in.getText(), " ");
        in.removeAll();
        if (st.countTokens() == 0)
            return;
        out.append("server:~$ ");
        String command = st.nextToken();
        in.setText("");
        switch (command) {
            case "start":
                start(st);
                return;
            case "close":
                close(st);
                return;
            case "restart":
                restart(st);
                return;
            case "setlog":
                setLog(st);
                return;
            case "unsetlog":
                unsetLog(st);
                return;
            case "help":
                help(st);
                return;
        }
        out.append("Wrong command!\n");
    }

    private void start(StringTokenizer st){
        if (st.countTokens() != 0){
            out.append("Wrong command! Terminated...\n");
            return;
        }
        if (server != null) {
            out.append("Server have already started!\n");
            return;
        }
        try{
            server = new Server(new ServerSocket(1234, 0, InetAddress.getByName("localhost")));
            out.append("Server started!\n");
        } catch (Exception ex) {
            out.append("Couldn't open server! Terminated...\n");
        }
    }


    private void close(StringTokenizer st){
        if (st.countTokens() == 0) {
            if (server == null){
                out.append("Server have already closed!\n");
                return;
            }
            out.append("Closing server...\n");
            server.close();
            server.interrupt();
            server = null;
            return;

        }
        out.append("Too much parameters\n");;
    }

    private void restart(StringTokenizer st){
        if (server != null)
            close(st);
        start(st);
    }

    private void setLog(StringTokenizer st){
        if (st.countTokens() == 0)
            out.append("Too few parameters\n");
        else if (st.countTokens() > 1)
            out.append("Too many parameters\n");
        else {
            String fileName = st.nextToken();
            Logger.getInstance().setLogFile(fileName);
            out.append(fileName + " added for logs\n");
        }
    }

    private void unsetLog(StringTokenizer st){
        if (st.countTokens() !=0){
            out.append("Too many parameters\n");
            return;
        }

    }

    private void help(StringTokenizer st){
        char[] buf = new char[256];
        if (st.countTokens() == 0){
            try (FileReader file = new FileReader(new File("help.txt"))){
                int length = 0;
                out.append("\n");
                while ((length = file.read(buf, 0, 256)) != -1){
                    out.append(new String(Arrays.copyOf(buf, length)));
                }
                out.append("\n");
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        else
            out.append("Too many parameters\n");
    }
}
