package Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.net.Socket;

public class Logger {

    private static long MAX_FILE_SIZE = 256 * 1024 * 1024; //256 MB
    private static Writer writer;
    private static File file;
    private static volatile Logger instance;

    private Logger() {
        writer = null;
        file = null;
    }


    private void checkSize() {
        try {
            if (file.length() > MAX_FILE_SIZE){
                writer.close();
                String name = file.getName();
                file.mkdir();
                file = new File(name);
                writer = new FileWriter(file);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    public static Logger getInstance() {
        Logger localInstance = instance;
        if (localInstance == null) {
            synchronized (Logger.class) {
                localInstance = instance;
                if (localInstance == null)
                    instance = localInstance = new Logger();
            }
        }
        return localInstance;
    }

    public void setLogFile(String name) {
        try {
            file = new File(name);
            writer = new FileWriter(name);
        }
        catch(Exception ex)
        {
            System.out.println("Couldn't open log file\n");
            writer = null;
        }
    }

    public synchronized void connection(Socket client, Socket server) {
        if (writer == null)
            return;
        if (client.isClosed() || server.isClosed())
            return;
        try {
            writer.write("CONNECT\n");
            writer.write("Client: " + client.getLocalAddress().toString() + ":" + client.getPort() + '\n');
            writer.write("Server: " + server.getInetAddress().toString() + ":" + server.getPort() + "\n");
            writer.write("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
            writer.flush();
            checkSize();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public synchronized void readData(Socket from, Socket to, byte[] buffer)
    {
        try {
            String packet = new String(buffer);
            writer.write("DATA STREAM \n");
            writer.write("From: " + from.getInetAddress() + '\n');
            writer.write("To: " + to.getInetAddress() + '\n');
            writer.write(packet);
            writer.write("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
            writer.flush();
            checkSize();
        }
        catch(Exception ex) {
        }
    }

    public synchronized void close(Socket client, Socket server){
        if (writer == null)
            return;

        try {

            writer.write("CLOSE\n");
            writer.write("Client: " + client.getInetAddress().toString() + ":" + client.getPort() + '\n');
            writer.write("Server: " + server.getInetAddress().toString() + ":" + server.getPort());
            writer.write("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
            writer.flush();
            checkSize();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public synchronized void reset(){
        if (file != null){
            try {
                writer.flush();
                writer.close();
                writer = null;
                file = null;
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
