package Socks;
import java.net.*;

public interface Socks {
    static final int size = 64;
    void greeting(Socket client);
    Socket processCommand(Socket client);
    Socket connect(Socket client);
    void sendSuccess(Socket client);
    void sendFailure(Socket client);
}
