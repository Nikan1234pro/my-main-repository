package Socks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;

public class Socks5 implements Socks {
    private InetAddress ip;
    private int port;
    private int ipLen;
    private byte addrType;
    private byte OK = 0x00;
    private byte BAD = 0x01;

    private int getPort(byte[] bytes) {
        return ((Byte.toUnsignedInt(bytes[0]) << Byte.SIZE) | Byte.toUnsignedInt(bytes[1]));
    }

    public Socket processCommand(Socket client) {
        if (client.isClosed())
            return null;
        try {
            InputStream is = client.getInputStream();
            if (is.read() != 0x05)
                return null;
            int command = is.read();
            if (command == 0x01)
                return connect(client);
        }
        catch(SocketException ex) {
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void greeting(Socket client) {
        if (client.isClosed())
            return;
        try {
            InputStream is = client.getInputStream();
            OutputStream os = client.getOutputStream();
            int count = is.read();
            byte[] buffer = new byte[count];
            while(count > 0){
                int n = is.read(buffer);
                count -= n;
                for (int i = 0; i < n; i++)
                    if (buffer[i] == 0x00){
                        byte[] answer = {(byte)0x05, (byte)0x00};
                        os.write(answer);
                        return;
                    }
            }
            byte[] answer = {(byte)0x05, (byte)0xFF};
            os.write(answer);
        }
        catch(Exception ex) {
        }
    }

    public Socket connect(Socket client) {
        Socket server = null;
        ByteBuffer buffer = ByteBuffer.allocate(size);
        int length = 0;
        if (client.isClosed())
            return null;
        try {
            InputStream is = client.getInputStream();
            if (is.available() >= 0)
                length = is.read(buffer.array(), 0, size);
            else
                return null;

            addrType = buffer.get(1);

            switch (addrType){
                case 0x01: ipLen = 4;
                    break;

                case 0x04: ipLen = 16;
                    break;

                default: return null;
            }
            byte[] address = new byte[ipLen];
            buffer.position(2);
            buffer.get(address, 0, ipLen);

            ip = InetAddress.getByAddress(address);

            port = getPort(new byte[]{buffer.get(length - 2), buffer.get(length - 1)});
            server = new Socket(ip, port);
        }
        catch(SocketException ex){
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return server;
    }

    private void send(Socket client, byte Ans) {
        if (client == null)
            return;
        if (client.isClosed())
            return;
        try {
            OutputStream os = client.getOutputStream();
            int length = 6 + ipLen;

            byte[] buffer = new byte[length];
            buffer[0] = 0x05;
            buffer[1] = Ans;
            buffer[2] = 0x00;
            buffer[3] = addrType;
            for (int i = 0; i < ipLen; i++)
                buffer[i + 4] = ip.getAddress()[i];

            buffer[length - 2] = (byte)(port >> 8);
            buffer[length - 1] = (byte)(port & 0xFF);

            os.write(buffer);
        }
        catch (SocketException ex) {
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }

    }

    public void sendSuccess(Socket client){
        send(client, OK);
    }


    public void sendFailure(Socket client) {
        send(client, BAD);
    }
}
