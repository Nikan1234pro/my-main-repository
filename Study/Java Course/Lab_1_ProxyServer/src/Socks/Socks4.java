package Socks;
import java.io.*;

import java.net.*;
import java.nio.ByteBuffer;

public class Socks4 implements Socks {
    private final byte ipLen = 4;
    private final byte OK = 0x5a;
    private final byte BAD = 0x5b;

    private int getPort(byte[] bytes) {
        return ((Byte.toUnsignedInt(bytes[0]) << Byte.SIZE) | Byte.toUnsignedInt(bytes[1]));
    }

    public Socket processCommand(Socket client) {
        if (client.isClosed())
            return null;

        try {
            InputStream is = client.getInputStream();
            int command = is.read();
            switch (command){
                case 0x01: return connect(client);
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void greeting(Socket client) {
        /*Nothing*/}

    public Socket connect(Socket client) {
        Socket server = null;
        ByteBuffer buffer = ByteBuffer.allocate(size);
        if (client.isClosed())
            return null;

        try {
            InputStream is = client.getInputStream();
            int len = is.read(buffer.array(), 0, size);

            int port = getPort(new byte[]{buffer.get(0), buffer.get(1)});

            buffer.position(2);
            byte[] addr = new byte[ipLen];
            buffer.get(addr, 0, ipLen);

            InetAddress ip = InetAddress.getByAddress(addr);
            server = new Socket(ip, port);

            while (buffer.get(len - 1) != 0x00) {
                buffer.clear();
                buffer.position(0);
                len = is.read(buffer.array(), 0, size);
            }
        }
        catch(ConnectException ex) {
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return server;
    }

    private void send(Socket client, byte Ans) {
        if (client.isClosed())
            return;
        byte[] buffer = new byte[8];
        buffer[1] = Ans;
        try {
            OutputStream os = client.getOutputStream();
            os.write(buffer, 0, 8);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendSuccess(Socket client) {
        send(client, OK);
    }

    public void sendFailure(Socket client) {
        send(client, BAD);
    }
}
