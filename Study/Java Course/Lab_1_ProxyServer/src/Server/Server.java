package Server;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

public class Server extends Thread {
    private ServerSocket server = null;

    public Server(ServerSocket server) {
        this.server = server;
        start();
    }

    public void close() {
        try {
            server.close();
        }
        catch(Exception ex){
        }
    }

    public void run() {
        ArrayList<Connection> connections = new ArrayList<>();
        try {
            while (true) {
                Socket client = server.accept();
                if (client != null)
                    connections.add(new Connection(client));

                for (Connection connection : connections)
                    if (!connection.isAlive())
                        connection.close();

                Iterator<Connection> it = connections.iterator();
                while (it.hasNext()) {
                    Connection connection = it.next();
                    if (connection.isClosed())
                    {
                        connection.join();
                        it.remove();
                    }
                }
            }

        }
        catch(Exception ex) {
            for (Connection connection : connections) {
                connection.close();
            }
        }
        finally {
            try {
                server.close();
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
