package Server;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import Logger.*;

public class DataStream extends Thread{
    Socket from = null;
    Socket to = null;

    public DataStream(Socket from, Socket to) throws Exception {
        super("");
        this.from = from;
        this.to = to;
    }

    public void run(){
        byte[] buffer = new byte[64 * 1024];

        try {
            while(true){
                int len = from.getInputStream().read(buffer);
                if (len == -1){
                    from.close();
                    to.close();
                    return;
                }
                Logger.getInstance().readData(from, to, Arrays.copyOfRange(buffer, 0, len));

                to.getOutputStream().write(buffer, 0, len);
            }
        }
        catch(SocketException ex){
        }
        catch(IOException ex){
        }
        finally{
            try {
                from.close();
                to.close();
            }
            catch (IOException ex) {
            }
        }
    }

    public void close(){
        try{
            from.close();
            to.close();
        }
        catch (Exception ex){
        }
    }
}
