package Server;

import java.io.*;
import java.net.Socket;
import Socks.*;

public class Proxy {
    private Socks socks = null;

    public Proxy() {
    }

    public Socket work(Socket client) throws IOException {
        InputStream is = client.getInputStream();

        int version = is.read();
        if (version == 4)
            socks = new Socks4();
        else if (version == 5)
            socks = new Socks5();
        else
            return null;
        socks.greeting(client);

        return socks.processCommand(client);
    }

    public void sendSuccess(Socket client) {
        socks.sendSuccess(client);
    }

    public void sendFailure(Socket client) {
        if (client != null)
            socks.sendFailure(client);
    }
}
