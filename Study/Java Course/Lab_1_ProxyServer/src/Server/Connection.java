package Server;

import Logger.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Connection extends Thread{
    private Socket client = null;
    private Socket server = null;
    private boolean is_closed = false;
    private DataStream fromClient = null;
    private DataStream fromServer = null;

    public Connection(Socket socket){
        this.client = socket;
        start();
    }

    public void run(){
        if (client.isClosed())
            return;
        try(InputStream is = client.getInputStream();
            OutputStream os = client.getOutputStream()){
            Proxy proxy = new Proxy();

            server = proxy.work(client);
            if (server == null){
                try{
                    Logger.getInstance().connection(client, server);
                    proxy.sendFailure(client);
                }
                catch(NullPointerException ex){
                }
                return;
            }
            proxy.sendSuccess(client);
            Logger.getInstance().connection(client, server);

            fromClient = new DataStream(client, server);
            fromServer = new DataStream(server, client);
            fromClient.start();
            fromServer.start();

            fromServer.join();
            Logger.getInstance().close(client, server);
        }
        catch(InterruptedException ex){
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void close(){
        try{
            fromClient.close();
            fromServer.close();
        }
        catch(Exception ex){
        }
        finally{
            is_closed = true;
        }
    }

    public boolean isClosed(){
        return is_closed;
    }
}
