package threads;

public class ResumeDemo implements Runnable {
    private boolean suspendFlag;
    private Thread t;
    public ResumeDemo() {
        suspendFlag = false;
        t = new Thread(this, "RESUME_THREAD");
        t.start();
    }

    public void run() {
        try {
            for (int i = 0; i < 20; i++) {
                System.out.println(t.getName() + ":" + i);
                Thread.sleep(200);
                synchronized (this) {
                    while (suspendFlag)
                        wait();
                }
            }
        }
        catch (InterruptedException ex) {
            System.out.println("Thread was interrupted!");
        }
    }

    synchronized public void mySuspend() {
        suspendFlag = true;
    }

    synchronized public void myResume() {
        suspendFlag = false;
        notify();
    }
}
