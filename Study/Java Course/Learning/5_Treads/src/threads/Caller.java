package threads;

public class Caller {

    synchronized public void show(String msg) {
        System.out.print("[" + msg);
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException e){}

        System.out.println("]");
    }
}
