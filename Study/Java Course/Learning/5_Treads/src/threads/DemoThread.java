package threads;

public class DemoThread extends Thread {
    String message;
    static Caller caller = new Caller();
    public DemoThread(String msg) {
        message = msg;
        start();
    }

    public void run() {
        synchronized (caller) {
            caller.show(message);
        }
    }
}
