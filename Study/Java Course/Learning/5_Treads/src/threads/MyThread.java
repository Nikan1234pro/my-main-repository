package threads;

public class MyThread implements Runnable {
    Thread t;

    public MyThread() {
        t = new Thread(this);
        t.start();
    }

    public void run() {
        try {
            for (int i = 0; i < 3; i++) {
                System.out.println(t.getName());
                Thread.sleep(1000);
            }
        }
        catch(InterruptedException ex) {
        }
    }
}
