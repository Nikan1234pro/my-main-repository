import threads.DemoThread;
import threads.MyThread;
import threads.OtherThread;
import threads.ResumeDemo;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        MyThread thread = new MyThread();

        Thread other = new OtherThread();

        try {
            other.join();
        }
        catch(InterruptedException ex){
        }
        System.out.println("Here");

        //////////////////////////////////////////////////////////////////

        DemoThread d1 = new DemoThread("Hello");
        DemoThread d2 = new DemoThread("My name is");
        DemoThread d3 = new DemoThread("Nikita");

        try {
            d1.join();
            d2.join();
            d3.join();
        }
        catch (InterruptedException ex) {
            /* Nothing */
        }

        //////////////////////////////////////////////////////////////////

        try {
            ResumeDemo rDemo = new ResumeDemo();
            Thread.sleep(1000);
            rDemo.mySuspend();
            System.out.println("Thread was suspended!");
            Thread.sleep(1000);
            rDemo.myResume();
        }
        catch (InterruptedException e) {
            System.out.println("Something wrong!");
        }
    }
}
