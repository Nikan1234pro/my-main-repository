import java.io.*;

public class Main {

    public static void main(String[] args) {
        File file = ...
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            ...
        }
            finally {
                if (in != null) {
                    in.close();
                }
            }
        }

    }
}
