public interface TestNumber<T extends Number> {
    boolean test(T e);
}
