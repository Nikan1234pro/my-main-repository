public class Main {
    public static void main(String[] args) {
        FunctionalInterface fInt = new FunctionalInterface() {
            @Override
            public double getDouble() {
                return 0;
            }
        };

        System.out.println(fInt.getDouble());

        FunctionalInterface other = () -> 0.0;
        System.out.println(other.getDouble());


        TestNumber<Integer> iTest = (i) -> i % 2 == 0;
        System.out.println(iTest.test(9));

        NumericFunc factorial = (n) -> {
            int res = 1;
            for (int i = n; i > 0; i--)
                res *= i;
            return res;
        };

        System.out.println(factorial.func(6));
    }


    public static <T> void print(T[] arr) {
        for (var i : arr)
            System.out.print(i + " ");
        System.out.println();
    }
}

interface NumericFunc {
    int func(int n);
}
