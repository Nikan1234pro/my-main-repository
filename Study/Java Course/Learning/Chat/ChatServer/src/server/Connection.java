package server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Connection extends Thread {
    private Socket client;
    private static final int BUF_SIZE = 1024;
    private static final List<Socket> connections = new ArrayList<>();

    public Connection(Socket client) {
        this.client = client;
        connections.add(client);
        start();
    }

    public void run() {
        try {
            while (true) {
                byte[] buffer = new byte[BUF_SIZE];
                int length;
                System.out.println(connections.size());
                if ((length = client.getInputStream().read(buffer, 0, BUF_SIZE)) == -1) {
                    synchronized (connections) {
                        connections.remove(client);
                    }
                    return;
                }
                for (Socket user : connections)
                    user.getOutputStream().write(buffer, 0, length);
            }
        }
        catch (IOException ex) {
            connections.remove(client);
        }
    }
}
