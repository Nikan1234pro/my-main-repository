package server;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;


public class Server extends Thread {
    ServerSocket server;
    File file;

    public Server() {
        try {
            file = new File("dialog");
            server = new ServerSocket(1234, 0, InetAddress.getByName("192.168.1.135"));
        }
        catch (Exception e) {
            System.out.println("Can't start server!");
            return;
        }
        start();
    }

    public void close() {
        try {
            server.close();
        }
        catch (IOException e) {
        }
    }

    public void run() {
        try {
            while (true) {
                Socket socket = server.accept();
                System.out.println("Connection was created!");
                socket.getOutputStream().write("Welcome to Chat!\n".getBytes());
                new Connection(socket);
            }
        }
        catch (IOException e) {
            System.out.println("Server closed!");
        }
    }
}
