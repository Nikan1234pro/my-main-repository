import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        try {
            Socket server = new Socket(InetAddress.getByName("192.168.1.135"), 1234);
            new Channel(System.in, server.getOutputStream());
            new Channel(server.getInputStream(), System.out);
        }
        catch (IOException e) {
            System.out.println("Can't connect to ChatServer");
        }
    }
}
