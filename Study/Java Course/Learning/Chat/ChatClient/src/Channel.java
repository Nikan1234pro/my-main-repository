import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Channel extends Thread {
    private InputStream from;
    private OutputStream to;
    private static final int BUF_SIZE = 1024;
    public Channel(InputStream from, OutputStream to) {
        this.from = from;
        this.to = to;
        start();
    }

    public void run() {
        byte[] buffer = new byte[BUF_SIZE];
        try {
            while (true) {
                int length;
                if ((length = from.read(buffer, 0, BUF_SIZE)) == -1) {
                    from.close();
                    to.close();
                    return;
                }
                to.write(buffer,0 ,length);
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
