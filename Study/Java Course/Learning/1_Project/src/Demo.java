import jdk.jfr.Experimental;

public class Demo {
    public final int i = 5;         //this is constant
    public static int count = 0;    //static field

    private int priv = 5;
    double d = 1245.45;             //package

    Demo() {
        ++count;
        priv = 16;
    }

    protected  void finalize() {                //NEVER use this method
        System.out.println("Garbage collector working");
    }

    boolean equals(Demo other) {
        return d == other.d;
    }
}
