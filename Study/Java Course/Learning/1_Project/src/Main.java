/**
 * При создании объекта мы получаем ссылку на него, т.е. переменную, содержащую адрес в памяти созданного объекта
 * В функции объекты передаются всегда ПО ССЫЛКЕ, стало быть всегда можно изменить содержимое
 * Обычные переменные передаются ПО ЗНАЧЕНИЮ
 *
 * Кто чистит память? Garbage collector, так что ручками чистить память не надо
 */
public class Main {

    public static void main(String[] args) {
        Demo demo = new Demo();
        System.out.println(Demo.count);     //demo.count - OK too
        demo.d = 228;
        //demo.i = 5;                       Error - variable i is final
        demo.d = 14.88;                     //OK, same packet

        int temp = 0;
        foo(temp);
        System.out.println("temp = " + temp);

        //for (int i = 0; i < 1_000_000; i++) //if you want to see, that garbage collector is working
        //    new Demo();

        show(12, 4543, 42, 9843, 472);

        boolean result = demo.equals(demo);

        Runnable thread = new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello");
            }
        };
        thread.run();

        System.out.println(result);
    }

    private static void show(int ... args) {
        for (int i = 0; i < args.length; i++)
            System.out.printf("%d ", args[i]);
        System.out.println();
    }

    private static void foo(int i) {
        i += 10;
    }
}
