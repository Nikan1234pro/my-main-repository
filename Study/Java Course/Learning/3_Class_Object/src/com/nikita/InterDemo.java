package com.nikita;

public interface InterDemo {
    int TEST = 6;

    void doSomething();
    default String say() {
        return "Interface default func realization!";
    }

    static int staticMethod(){
        return 0;
    }
}
