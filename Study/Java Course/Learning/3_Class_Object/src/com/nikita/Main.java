package com.nikita;

import com.test.A;
import com.test.B;

public class Main {

    public static void main(String[] args) {
        Object obj = new ObjectDemo();


        A a = new A();
        a.foo();

        InterDemo inter = new ImplementInterface();
        inter.doSomething();
        System.out.println(inter.say());

        System.out.println(InterDemo.staticMethod());

        System.out.println("Hello World!");
    }
}
