package com.nikita;

public class ObjectDemo {
    private int i;
    public double d;

    public ObjectDemo() {
    }

    public ObjectDemo(int i, double d) {
        this.i = i;
        this.d = d;
    }

    @Override
    public String toString() {
        return "HELLO =D";
    }

    @Override
    public ObjectDemo clone() {
        return new ObjectDemo(i, d);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
