import Test.Base;
import Test.Derv;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Main demo = new Main();
        try {
            demo.test(9);
            demo.foo();
        }
        catch (Exception e) {
            System.out.println("MAIN: Exception: " + e + " was caught");
        }
        System.out.println("Hello World!");

        MyException ex = new MyException("LOOOOOOOOOOOOOOOOOOOL");
        System.out.println(ex);

        Base base = new Derv();
        Derv derv = new Derv();
        base.say();
        derv.say();

    }

    public void test(int i) throws IOException {
        try {
            if (i == 10)
                throw new IOException();
        }
        catch (IOException e) {
            e.printStackTrace();
            return;             //Finally still implements!
        }
        finally {
            System.out.println("Finally");
        }

        System.out.println("I am here");
    }

    public void foo() {
        throw new RuntimeException(); //This exception shouldn't be in throws declaration
    }
}
