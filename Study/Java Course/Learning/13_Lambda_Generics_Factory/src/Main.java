import enterprise.demos.factory.*;

public class Main {
    public static void main(String[] args) {
        Factory<String, Base, Integer> factory = new Factory<>();
        factory.add("Base", Base::new);
        factory.add("Derv1", Derv1::new);
        factory.showList();

        try {
            factory.create("Base", 10).say();
            factory.create("Derv1", 5).say();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}