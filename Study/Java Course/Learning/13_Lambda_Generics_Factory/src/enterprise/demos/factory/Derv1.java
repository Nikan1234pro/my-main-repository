package enterprise.demos.factory;

public class Derv1 extends Base {

    public Derv1(int i) {
        super(i);
    }

    public void say() {
        System.out.println("Derv1: field = " + i);
    }
}
