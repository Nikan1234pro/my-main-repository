package enterprise.demos.factory;

public interface Creator <T, V> {
    T create(V val);
}
