package enterprise.demos.factory;

public class Base {
    protected int i;

    public Base(int i) {
        this.i = i;
    }

    public void say() {
        System.out.println("Base: field = " + i);
    }
}
