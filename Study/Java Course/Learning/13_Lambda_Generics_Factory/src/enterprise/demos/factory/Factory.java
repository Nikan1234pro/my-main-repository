package enterprise.demos.factory;

import java.util.HashMap;
import java.util.Map;

public class Factory <Key extends Comparable<Key>, BaseType, Param>{
    private Map<Key, Creator<? extends BaseType, Param>> factory = new HashMap<>();

    public void add(Key key, Creator<? extends BaseType, Param> creator) {
        factory.put(key, creator);
    }

    public BaseType create(Key key, Param value) throws ClassNotFoundException {
        Creator<? extends BaseType, Param> creator = factory.get(key);
        if (creator == null)
            throw new ClassNotFoundException();
        return creator.create(value);
    }

    public void showList() {
        factory.forEach((key, creator) -> System.out.println(key));
    }
}
