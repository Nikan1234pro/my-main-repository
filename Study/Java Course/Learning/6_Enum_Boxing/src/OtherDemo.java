public enum OtherDemo {
    Nikita(1000), Max(400), Dima(228);

    private int iq;

    OtherDemo(int i) {
        System.out.println("Constructor");
        iq = i;
    }

    int getVal() {
        return iq;
    }
}
