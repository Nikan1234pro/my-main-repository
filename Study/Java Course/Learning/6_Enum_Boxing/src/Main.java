import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        EnumDemo ed;

        ed = EnumDemo.TEST_1;
        ed = EnumDemo.TEST_2;

        switch (ed) {
            case TEST_1:
                System.out.println("TEST_1");
            case TEST_2:
                System.out.println("TEST_2");
        }

        EnumDemo[] eArray = EnumDemo.values();
        for (var e : eArray)
            System.out.println(e);

        System.out.println(ed);

        OtherDemo Neket = OtherDemo.Nikita;
        OtherDemo Max = OtherDemo.Max;
        System.out.println(Max.compareTo(Neket));
        System.out.println(OtherDemo.Dima.ordinal());

        Boolean boolBox = true;     //auto boxing
        //Boolean boolBox2 = new Boolean(false);    //deprecated
        Integer integer = 1234;
        integer += 5;                               //auto unboxing to int -> value + 5 -> auto boxing to Integer
        System.out.println(integer );
        System.out.println(integer.doubleValue());  //returns (double)int


        //System.out.println(intReference.intValue());    //Нельзя, надо проинициализировать!
        Integer intReference = 5;
        Integer test = ++intReference;
        System.out.println("test = " + (test == intReference));
        System.out.println(intReference);

        TEST ii = new TEST();
        kek(ii);
        System.out.println(ii.k);


        Integer i = 1000;
        //kek(i++);
        System.out.println(foo(i) == i);
        System.out.println(i);
    }

    public static Integer foo(Integer i) {
        Integer tmp = i++;
        System.out.println(tmp);
        System.out.println(i);
        System.out.println("check = " + (tmp == i));
        return i;
    }

    private static void kek(TEST i) {
        i.k = 8;
    }
}
