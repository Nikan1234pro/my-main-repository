package com.nikita.genericsdemo;

public interface InterGeneric<T extends Comparable<T>> {
    T max();
    T min();
}
