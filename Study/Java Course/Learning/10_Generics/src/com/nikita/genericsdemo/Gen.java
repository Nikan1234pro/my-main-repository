package com.nikita.genericsdemo;

public class Gen<T> {
    private T obj;

    public Gen() {
    }

    public Gen(T o) {
        obj = o;
    }

    public T get() {
        return obj;
    }

    public void showInfo() {
        System.out.println(obj.getClass().getName());
    }
}
