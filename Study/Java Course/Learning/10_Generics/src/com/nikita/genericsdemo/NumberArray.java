package com.nikita.genericsdemo;

public class NumberArray<T extends Number> {
    private T[] array;

    public NumberArray(T[] a) {
        array = a;
    }

    public double average() {
        double sum = 0;
        for (var elem : array)
            sum += elem.doubleValue();

        return sum / array.length;
    }

    public boolean sameArgs(NumberArray<?> other) {
        return (average() == other.average());
    }

    public void test(NumberArray<? extends Integer> iArray) {
    }
}
