package com.nikita.genericsdemo;

public class ImplementInterGeneric<T extends Comparable<T>> implements InterGeneric<T> {
    private T[] array;

    public ImplementInterGeneric(T[] a) {
        array = a;
    }

    public T max() {
        T max = array[0];
        for (T elem : array) {
            if (elem.compareTo(max) > 0)
                max = elem;
        }
        return max;
    }

    public T min() {
        T min = array[0];
        for (T elem : array) {
            if (elem.compareTo(min) < 0)
                min = elem;
        }
        return min;
    }
}
