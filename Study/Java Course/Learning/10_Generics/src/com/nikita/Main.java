package com.nikita;

import com.nikita.genericsdemo.Gen;
import com.nikita.genericsdemo.Handler;
import com.nikita.genericsdemo.NumberArray;
import com.nikita.instancedemo.A;
import com.nikita.instancedemo.B;
import com.nikita.instancedemo.C;

public class Main {

    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();

        //about instanceof
        System.out.println(test(a));
        System.out.println(test(b));
        System.out.println(test(c));
        System.out.println(test(new Main()));

        //generics

        Gen<Integer> gen = new Gen<Integer>(5);  //if Gen<> gen = new Gen<Integer>(5); T = Object!!! (or simply Gen)
        gen.showInfo();

        System.out.println("test = " + error(gen));

        int k = gen.get();
        System.out.println("k = " + k);


        String str = "aadfghj";

        Integer[] arr1 = { 1, 2, 5, 6 };
        NumberArray<Integer> iArr = new NumberArray<>(arr1);
        System.out.println(iArr.average());

        Float[] arr2 = { 1.0f, 2.0f, 5.0f, 6.0f };
        NumberArray<Float> fArr = new NumberArray<>(arr2);
        System.out.println(fArr.average());

        System.out.println(fArr.sameArgs(iArr));

        Integer[] array = { 13, 5, 83, 9, 54, 32 };
        System.out.println(Handler.contains(9, array));
        System.out.println(Handler.contains(10, array));
    }

    public static boolean test(Object obj) {
        return (obj instanceof A);
    }

    public static boolean error(Object other) {
        return (other instanceof Gen<?>);
    }

}
