package enterprise.demos.lambda;

public class Main {
    public String field = "Hello";
    public static void main(String[] args) {
        new LambdaDemo().demo();
    }

}

class LambdaDemo {
    private int value = 0xFF;   //255
    public static double PI = 3.14;
    private Main object = new Main();

    public void demo() {
        int n = 0;
        Lambda lambda = () -> {
            this.object.field = "Goodbye";      //доступен this
            value++;    //ок
            PI = 0;     //можно и со статической
            int k = n + 9;
            //++n; нельзя изменять значение.
        };

        lambda.test();
        //n++; если раскомментировать, то строка int k = n + 9; станет не правильной, т.к. тогда n - не завершенная
        System.out.println(object.field);   //Kill me pls
        System.out.println(value);          //256
        System.out.println(PI);             //0.0

        Lambda newLambda = LambdaDemo::truth;       //ссылка на статический метод, можно задать лямбду так
        newLambda.test();
        System.out.println(PI);                     //PI снова равно 3.14

        LambdaDemo ld = new LambdaDemo();
        Lambda other = ld::lie;                     //сслыка на обычный метод, работает через объект
        other.test();
        System.out.println(ld.value);               //Значение изменилось, работает так же
    }

    void lie() {
        value = 4;
    }

    static void truth() {
        PI = 3.14;
    }
}
