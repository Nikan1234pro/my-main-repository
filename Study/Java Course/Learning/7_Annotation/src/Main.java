import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

public class Main {

    @Anno(str = "lol", val =12)
    public static void main(String[] args) {
        Main m = new Main();
        Class<?> c = m.getClass();
        try {
            Method meth = c.getMethod("main", String[].class);
            Anno anno = meth.getAnnotation(Anno.class);
            System.out.println(anno.str());
            System.out.println(anno.val());
        }
        catch (NoSuchMethodException e) {
            System.out.println("No such method");
        }

        new Demo2().testAnno();
    }
}

@Retention(RetentionPolicy.RUNTIME)
@interface Anno {
    String str();
    int val();
}
