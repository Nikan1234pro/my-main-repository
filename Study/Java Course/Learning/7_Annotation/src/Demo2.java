import java.lang.annotation.*;

@HELLO(answer = "Class")
@Anno(str = "Demo2", val = 0xFF)
public class Demo2 {

    @HELLO
    public void testAnno() {
        try {
            Annotation[] annotations = Demo2.class.getAnnotations();
            for (var anno : annotations)
                System.out.println(anno);

            annotations = Demo2.class.getMethod("testAnno").getAnnotations();
            for (var anno : annotations)
                System.out.println(anno);
        }
        catch (NoSuchMethodException e) {
            System.out.println("ERROR");
        }
    }
}

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface HELLO {
    String answer() default "Outstanding move";
}


