package main;

import main.A;

public class B extends A {
    int j = 6;

    public void test () {
        //super.test(); - OK
        System.out.println("B test");
    }

    public int foo() {
        return 1;
    }
}
