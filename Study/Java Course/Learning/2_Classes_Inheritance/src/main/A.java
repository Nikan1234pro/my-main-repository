package main;

public class A {
    protected int i;            //Couldn't be private if we want to use it in derived class

    public void test() {
        System.out.println("A test");
    }
}

class K {

}
