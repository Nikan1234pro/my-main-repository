public class Outer {
    public int a;
    private double d;

    private class Inner {
        private int f;
        void foo() {
            a++;    //can
            d *= 10;
            boo();      //OK
        }
    }

    public static class Other {
        public int stat;
        Other() {
           //a++;   //Error, static context
        }
    }

    void boo() {
        //f++;  //error
        Inner in = new Inner();
        in.f = 5;               //OK, with private field
    }
}
