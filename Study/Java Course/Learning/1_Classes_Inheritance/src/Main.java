/**
 * Нельзя создать вложенный класс извне,
 * в данном примере создать Inner вне класса Outer нельзя
 *
 * Можно создать класс внутри любого блока
 */

public class Main {

    static int a;
    static int b;

    static void args(int... args) {
        System.out.println("More arguments");
    }

    static void args(int arg) {
        System.out.println("1 argument");
    }

    static {
        System.out.println("Static block");
        a = 5;
        b = a * 100;

    }

    public static void main(String[] args) {
        class Test {
            void say() {
                System.out.println("Dorofff");
            }
        }
        Test test = new Test();
        test.say();

        String str = "ABCDEFGHIJKLMNOP";
        System.out.println(str.charAt(0));

        args(12);
        args(12, 45);

        System.out.println("Hello World!");
    }
}
