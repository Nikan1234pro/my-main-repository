package Socks;

import Server.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class Socks5 implements Socks{

    private int getPort(byte[] bytes) {
        return ((Byte.toUnsignedInt(bytes[0]) << Byte.SIZE) | Byte.toUnsignedInt(bytes[1]));
    }

    public void greeting(SelectionKey key, Attachment attachment) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();

        if (channel.read(attachment.recvBuffer) <= 0)
            throw new IOException();

        if (attachment.recvBuffer.position() < 2)
            return;
        if (attachment.recvBuffer.position() < 2 + attachment.recvBuffer.get(1))
            return;

        int i;
        for (i = 2; i < attachment.recvBuffer.position(); i++)
            if (attachment.recvBuffer.get(i) == 0x00) {
                break;
            }

        if (i == attachment.recvBuffer.position())
            attachment.sendBuffer.put(new byte[]{(byte)0x05, (byte)0xFF});
        else
            attachment.sendBuffer.put(new byte[]{(byte)0x05, (byte)0x00});
        attachment.recvBuffer.clear();
        attachment.sendBuffer.flip();
        key.interestOps(SelectionKey.OP_WRITE);
        attachment.state = States.Connect;
    }


    public void createConnection(SelectionKey key, Attachment attachment) throws IOException{
        SocketChannel channel = (SocketChannel) key.channel();
        if (channel.read(attachment.recvBuffer) <= 0)
            throw new IOException();
        if (attachment.recvBuffer.get(0) != 0x05 || attachment.recvBuffer.get(1) != 0x01) {
            throw new IOException();
        }
        int addressType = attachment.recvBuffer.get(3);
        int ipLength;
        switch (addressType) {
            case 0x01:
                ipLength = 4;
                break;
            case 0x04:
                ipLength = 16;
                break;
            default:
                throw new IOException();
        }
        byte[] address = new byte[ipLength];

        attachment.recvBuffer.position(4);
        attachment.recvBuffer.get(address, 0, ipLength);
        InetAddress ip = InetAddress.getByAddress(address);
        int port = getPort(new byte[]{attachment.recvBuffer.get(), attachment.recvBuffer.get()});

        SocketChannel serverChannel = SocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.connect(new InetSocketAddress(ip, port));
        SelectionKey serverKey = serverChannel.register(key.selector(), SelectionKey.OP_CONNECT);

        attachment.recvBuffer.clear();
        attachment.pair = serverKey;

        Attachment serverAttachment = new Attachment();
        serverKey.attach(serverAttachment);

        serverAttachment.pair = key;
        key.interestOps(0);
        attachment.state = States.Ready;
    }

    private byte[] create(SelectionKey key, byte Ans) {
        Socket socket = ((SocketChannel) key.channel()).socket();
        socket.getInetAddress();
        socket.getPort();

        byte[] ip = socket.getInetAddress().getAddress();
        int length = 6 + ip.length;

        System.out.println(socket.getInetAddress().toString());
        System.out.println(socket.getPort());

        byte[] buffer = new byte[length];
        buffer[0] = 0x05;
        buffer[1] = Ans;
        buffer[2] = 0x00;
        buffer[3] = (ip.length == 4) ? (byte) 0x01 : (byte) 0x04;
        for (int i = 0; i < ip.length; i++)
            buffer[i + 4] = ip[i];
        buffer[length - 2] = (byte) (socket.getPort() >> 8);
        buffer[length - 1] = (byte) (socket.getPort() & 0xFF);
        return buffer;
    }


    public byte[] good(SelectionKey key) {
        return create(key, (byte) 0x00);
    }
}