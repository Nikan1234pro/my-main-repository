package Socks;

import Server.Attachment;
import Server.States;

import java.io.*;

import java.net.*;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class Socks4 implements Socks {
    private final byte ipLen = 4;

    private int getPort(byte[] bytes) {
        return ((Byte.toUnsignedInt(bytes[0]) << Byte.SIZE) | Byte.toUnsignedInt(bytes[1]));
    }

    public void greeting(SelectionKey key, Attachment attachment) {
        attachment.state = States.Connect;
    }

    public void createConnection(SelectionKey key, Attachment attachment) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        if (channel.read(attachment.recvBuffer) <= 0)
            throw new IOException();
        if (attachment.recvBuffer.get(attachment.recvBuffer.position() - 1) != 0x00)
            throw new IOException();
        if (attachment.recvBuffer.get(0) != 0x04 || attachment.recvBuffer.get(1) != 0x01 || attachment.recvBuffer.position() + 1 < 8)
            throw new IOException();

        int port = getPort(new byte[]{attachment.recvBuffer.get(2), attachment.recvBuffer.get(3)});
        attachment.recvBuffer.position(4);
        byte[] address = new byte[ipLen];
        attachment.recvBuffer.get(address, 0, ipLen);
        InetAddress ip = InetAddress.getByAddress(address);

        SocketChannel serverChannel = SocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.connect(new InetSocketAddress(ip, port));
        SelectionKey serverKey = serverChannel.register(key.selector(), SelectionKey.OP_CONNECT);

        attachment.recvBuffer.clear();
        attachment.pair = serverKey;

        Attachment serverAttachment = new Attachment();
        serverAttachment.state = States.Ready;
        serverKey.attach(serverAttachment);

        serverAttachment.pair = key;
        key.interestOps(0);
        attachment.state = States.Ready;
    }

    public byte[] good(SelectionKey key) {
        return new byte[]{0x00, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    }

}