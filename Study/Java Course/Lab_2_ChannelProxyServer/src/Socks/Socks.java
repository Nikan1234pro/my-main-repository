package Socks;

import Server.Attachment;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public interface Socks {
    void createConnection(SelectionKey key, Attachment attachment) throws IOException;
    void greeting(SelectionKey key, Attachment attachment) throws IOException;
    byte[] good(SelectionKey key);
}
