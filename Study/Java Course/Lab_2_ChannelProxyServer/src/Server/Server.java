package Server;

import Socks.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;


public class Server extends Thread {
    private Socks socks = null;
    private Selector selector = null;
    private String host;
    private int port;

    public Server(String host, int port, Socks socks){
        this.host = host;
        this.port = port;
        this.socks = socks;
        start();
    }

    public void run(){
        if (socks == null)
            return;
        try {
            selector = Selector.open();
            ServerSocketChannel serverChannel = ServerSocketChannel.open();
            serverChannel.bind(new InetSocketAddress(host, port));
            serverChannel.configureBlocking(false);
            SelectionKey k = serverChannel.register(selector, SelectionKey.OP_ACCEPT);

            while(selector.select() != -1 && !isInterrupted()) {
                System.out.println(selector.keys().size());
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while(iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    try {
                        if (key.isValid() && key.isAcceptable())
                            access(key);
                        if (key.isValid() && key.isConnectable())
                            connect(key);
                        if (key.isValid() && key.isReadable())
                            read(key);
                        if (key.isValid() && key.isWritable())
                            write(key);
                    }
                    catch (IOException ioex) {
                        close(key);
                    }
                }
                selector.selectedKeys().clear();
            }
            selector.close();
            serverChannel.close();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void access(SelectionKey key) throws IOException {
        SocketChannel newChannel = ((ServerSocketChannel) key.channel()).accept();
        newChannel.configureBlocking(false);
        newChannel.register(key.selector(), SelectionKey.OP_READ);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        Attachment attachment = (Attachment) key.attachment();
        if (attachment == null) {
            attachment = new Attachment();
            attachment.recvBuffer = ByteBuffer.allocate(Attachment.BUF_SIZE);
            attachment.sendBuffer = ByteBuffer.allocate(Attachment.BUF_SIZE);
            attachment.state = States.Greeting;
            key.attach(attachment);
            return;
        }
        if (attachment.state == States.Greeting) {
            socks.greeting(key, attachment);
            return;
        }
        if (attachment.state == States.Connect) {
            socks.createConnection(key, attachment);
            return;
        }
        if (channel.read(attachment.recvBuffer) <= 0) {
            close(key);
            return;
        }
        attachment.pair.interestOps(attachment.pair.interestOps() | SelectionKey.OP_WRITE);
        key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);
        attachment.recvBuffer.flip();
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        Attachment attachment = (Attachment) key.attachment();
        if (channel.write(attachment.sendBuffer) <= 0) {
            close(key);
            return;
        }
        if (attachment.sendBuffer.remaining() == 0) {
            attachment.sendBuffer.clear();
            if (attachment.pair != null)
                attachment.pair.interestOps(attachment.pair.interestOps() | SelectionKey.OP_READ);
            key.interestOps(SelectionKey.OP_READ);
        }
    }

    private void connect(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        Attachment attachment = (Attachment) key.attachment();
        channel.finishConnect();

        attachment.sendBuffer = ((Attachment) attachment.pair.attachment()).recvBuffer;
        attachment.recvBuffer = ((Attachment) attachment.pair.attachment()).sendBuffer;

        attachment.recvBuffer.put(socks.good(key)).flip();
        attachment.pair.interestOps(SelectionKey.OP_WRITE);
        key.interestOps(0);
    }

    private void close(SelectionKey key) {
        try {
            key.cancel();
            key.channel().close();
            SelectionKey pair = ((Attachment) key.attachment()).pair;
            if (pair == null)
                return;

            ((Attachment) pair.attachment()).pair = null;
            if (pair.isWritable())
                ((Attachment) pair.attachment()).sendBuffer.flip();
            pair.interestOps(SelectionKey.OP_WRITE);
        }
        catch (Exception ex) {
        }
    }
}
