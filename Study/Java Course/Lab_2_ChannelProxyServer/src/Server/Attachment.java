package Server;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;


public class Attachment {
    public ByteBuffer recvBuffer;
    public ByteBuffer sendBuffer;
    public SelectionKey pair;
    public States state;
    public static final int BUF_SIZE = 1024;

    public  Attachment() {
    }
}
