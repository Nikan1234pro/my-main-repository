package UI;

import Commander.*;

import java.awt.*;
import javax.swing.*;


public class UserInterface extends JFrame {
    private Commander commander;

    private UserInterface() {
        setTitle("Proxy-Server");
        setSize(600, 380);
        setResizable(false);
        setBackground(Color.BLACK);
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension dim = kit.getScreenSize();
        setLocation(dim.width / 2 - 300, dim.height / 2 - 220);

        final JPanel southPanel = new JPanel();
        final JTextArea textArea = new JTextArea(8, 20);
        final JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setEditable(false);
        textArea.setBackground(Color.BLACK);
        textArea.setForeground(Color.WHITE);
        textArea.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        southPanel.add(scrollPane);
        scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        add(scrollPane);

        final JPanel northPanel = new JPanel();
        northPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        JTextField textField = new JTextField();
        textField.setBackground(Color.BLACK);
        textField.setForeground(Color.WHITE);
        textArea.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        textField.setSize(400, 90);
        textField.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        textField.setCaretColor(Color.GREEN);

        northPanel.setLayout(new GridLayout(1,1));
        northPanel.add(textField);
        add(northPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        textField.requestFocusInWindow();

        commander = new Commander(textField, textArea);
        textField.addActionListener(e -> commander.command());
    }


    public static void main(String[] args)  {
        new UserInterface();
    }
}
