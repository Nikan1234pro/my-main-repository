--Добавить купленный билет
CREATE OR REPLACE PROCEDURE BUY_TICKET (
    passenger_last_name         IN VARCHAR,
    passenger_first_name        IN VARCHAR,
    passenger_second_name       IN VARCHAR,
    has_baggage                 IN CHAR,
    passenger_age               IN NUMBER,
    passenger_gender            IN CHAR,   
    flight_number               IN INTEGER,
    operation_date              IN VARCHAR,
    ticket_id                   OUT VARCHAR
) AS
   n INTEGER;
BEGIN
    --Создаем идентификатор для билета
    SELECT COUNT(*) + 1
    INTO n FROM TICKETS T WHERE T.flight_number = flight_number;
    ticket_id := TO_CHAR(flight_number) || '-' || TO_CHAR(n);
   
    --Добавляем в таблицу
    INSERT INTO TICKETS VALUES (
        ticket_id, 
        passenger_last_name,
        passenger_first_name,
        passenger_second_name,
        has_baggage,
        passenger_age,
        passenger_gender,
        flight_number,
        'Y',
        TO_DATE(operation_date, 'YYYY-MM-DD HH24:MI:SS')
    );
END BUY_TICKET;
/



-- Добавить забронированный билет
CREATE OR REPLACE PROCEDURE RESERVE_TICKET(
    passenger_last_name         IN VARCHAR,
    passenger_first_name        IN VARCHAR,
    passenger_second_name       IN VARCHAR,
    has_baggage                 IN CHAR,
    passenger_age               IN NUMBER,
    passenger_gender            IN CHAR,   
    flight_number               IN INTEGER,
    operation_date              IN VARCHAR,
    ticket_id                   OUT VARCHAR
) AS
   n INTEGER;
BEGIN
    --Создаем идентификатор для билета
    SELECT COUNT(*) + 1
    INTO n FROM TICKETS T WHERE T.flight_number = flight_number;
    ticket_id := TO_CHAR(flight_number) || '-' || TO_CHAR(n);
   
    --Добавляем в таблицу
    INSERT INTO TICKETS VALUES (
        ticket_id, 
        passenger_last_name,
        passenger_first_name,
        passenger_second_name,
        has_baggage,
        passenger_age,
        passenger_gender,
        flight_number,
        'N',
        TO_DATE(operation_date, 'YYYY-MM-DD HH24:MI:SS')
    );
END RESERVE_TICKET;
/



--Возвращем билет
CREATE OR REPLACE PROCEDURE RETURN_TICKET (
    ticket                      IN VARCHAR,
    operation_date              IN VARCHAR
) AS 
    return_date DATE;
    flight  INTEGER;
    duration INTEGER;
    flight_date_ DATE;
    n INTEGER;
BEGIN
    return_date := TO_DATE(operation_date, 'YYYY-MM-DD HH24:MI:SS');

    --Проверяем, есть ли такой билет в базе
    SELECT COUNT(*)
    INTO n FROM TICKETS T
    WHERE T.ticket_id = ticket;
    IF n <> 1 THEN
        raise_application_error
        (-20202, 'No tickets found');
    END IF;
 
    --Получаем номер рейса
    SELECT flight_number, flight_date
    INTO flight, flight_date_
    FROM TICKETS INNER JOIN FLIGHTS USING(flight_number)
    WHERE ticket_id = ticket;
    
    --Проверяем, можем ли мы его сдать
    --Можем, если делаем это на начала рейса (с учетом задержек)
    SELECT COUNT(*)
    INTO n FROM DELAYS D
    WHERE D.flight_number = flight;

    --Есть задержка, прибавляем ее ко времени отправления
    IF n = 1 THEN
        SELECT delay_duration
        INTO duration FROM DELAYS D
        WHERE D.flight_number = flight;

        IF (return_date > flight_date_ + duration / 24) THEN
            raise_application_error
            (-20201, 'You cant return a ticket for this flight');
        END IF;
    END IF;

    --Задержки нет - рейс по расписанию
    IF n = 0 THEN 
        IF (return_date > flight_date_ ) THEN
            raise_application_error
            (-20201, 'You cant return a ticket for this flight');
        END IF;
    END IF;

    DELETE FROM TICKETS T WHERE T.ticket_id = ticket;
END RETURN_TICKET;
/



--Оплачиваем забронированный билет
CREATE OR REPLACE PROCEDURE PURCHASE_TICKET (
    ticket          IN VARCHAR,
    purchase_date   IN VARCHAR
) AS
    n INTEGER;
BEGIN
    --Ищем неоплаченный билет с таким номером
    SELECT COUNT(*)
    INTO n
    FROM TICKETS T
    WHERE T.ticket_id = ticket
    AND T.purchased = 'N';

    --Если такого билета нет
    if n <> 1 THEN
        raise_application_error(-20202, 'No tickets found');
    END IF;

    --Обновляем, в том числе дату операции
    UPDATE TICKETS T
    SET T.purchased = 'Y',
        T.operation_date = TO_DATE(purchase_date, 'YYYY-MM-DD HH24:MI:SS')
    WHERE T.ticket_id = ticket;
    
END PURCHASE_TICKET;
/



-- Нельзя купить билет на отмененный рейс.
-- На рейс можно купить билет до отправиления
-- Если рейс задерживается - до та до времени отправления
-- плюс задержка  
CREATE OR REPLACE TRIGGER TICKET_DATE_CHECK 
BEFORE INSERT
ON TICKETS
FOR EACH ROW 
DECLARE 
    duration INTEGER;
    flight_date DATE;
    old_date DATE;
    n INTEGER;
    w INTEGER;
    c INTEGER;
BEGIN
    --Для вставки через INSERT чтобы задать правильный номер билета
    IF :new.ticket_id IS NULL THEN
        SELECT COUNT(*) + 1
        INTO w FROM TICKETS T WHERE T.flight_number = :new.flight_number;
        :new.ticket_id := TO_CHAR(:new.flight_number) || '-' || TO_CHAR(w);
        RETURN;
    END IF;

    SELECT flight_date
    INTO flight_date FROM FLIGHTS
    WHERE flight_number = :new.flight_number;

    --Проверяем, что рейс не отменен
    SELECT COUNT(*)
    INTO n FROM CANCELLATIONS
    WHERE flight_number = :new.flight_number;
 
    IF n > 0 THEN raise_application_error
        (-20200, 'You cant buy a ticket for a cancelled flight');
    END IF;

    --Проверяем, что операция выполнена до отправления рейса
    SELECT COUNT(*)
    INTO n FROM DELAYS
    WHERE flight_number = :new.flight_number;

    --Рейс был задрежан
    IF n = 1 THEN
        SELECT delay_duration
        INTO duration FROM DELAYS
        WHERE flight_number = :new.flight_number;

        IF (:new.operation_date > flight_date + duration / 24) THEN
            raise_application_error
            (-20201, 'You cant buy a ticket for this flight');
        END IF;
    END IF;
 
    --Рейс отправляется по расписанию
    IF n = 0 THEN 
        IF (:new.operation_date > flight_date) THEN
            raise_application_error
            (-20201, 'You cant buy a ticket for this flight');
        END IF;
    END IF;
END;
/


CREATE OR REPLACE TRIGGER TICKET_DATE_CHECK_UPDATE
BEFORE UPDATE
ON TICKETS
FOR EACH ROW 
DECLARE 
    duration INTEGER;
    flight_date DATE;
    old_date DATE;
    n INTEGER;
    w INTEGER;
    c INTEGER;
BEGIN
    IF :old.operation_date > :new.operation_date THEN
        raise_application_error(-20203, 'WRONG TIME');
    END IF;


    --Для вставки через INSERT чтобы задать правильный номер билета
    IF :new.ticket_id IS NULL THEN
        SELECT COUNT(*) + 1
        INTO w FROM TICKETS T WHERE T.flight_number = :new.flight_number;
        :new.ticket_id := TO_CHAR(:new.flight_number) || '-' || TO_CHAR(w);
        RETURN;
    END IF;

    SELECT flight_date
    INTO flight_date FROM FLIGHTS
    WHERE flight_number = :new.flight_number;

    --Проверяем, что рейс не отменен
    SELECT COUNT(*)
    INTO n FROM CANCELLATIONS
    WHERE flight_number = :new.flight_number;
 
    IF n > 0 THEN raise_application_error
        (-20200, 'You cant buy a ticket for a cancelled flight');
    END IF;

    --Проверяем, что операция выполнена до отправления рейса
    SELECT COUNT(*)
    INTO n FROM DELAYS
    WHERE flight_number = :new.flight_number;

    --Рейс был задрежан
    IF n = 1 THEN
        SELECT delay_duration
        INTO duration FROM DELAYS
        WHERE flight_number = :new.flight_number;

        IF (:new.operation_date > flight_date + duration / 24) THEN
            raise_application_error
            (-20201, 'You cant buy a ticket for this flight');
        END IF;
    END IF;
 
    --Рейс отправляется по расписанию
    IF n = 0 THEN 
        IF (:new.operation_date > flight_date) THEN
            raise_application_error
            (-20201, 'You cant buy a ticket for this flight');
        END IF;
    END IF;
END;
/