BEGIN
    ADD_MANAGER(
        1001, 'Долгов', 'Никита', 'Юрьевич', 
        '2003-02-01',
        '1980-10-25', 'M', 0, 120000.00, '123456');

    ADD_MANAGER(
        1002, 'Петров', 'Евгений', 'Сергеевич', 
        '2002-04-15',
        '1976-04-09', 'M', 0, 80000.00, 'Hello');

    ADD_MANAGER(
        1003, 'Малаева', 'Екатерина', 'Андреевна', 
        '2012-07-08',
        '1979-04-19', 'F', 0, 15000.00, 'LOL1234');
END;
/

INSERT INTO DEPARTMENTS VALUES(7000, 'Отдел логистики');
INSERT INTO DEPARTMENTS VALUES(8000, 'Отдел ремонта');
INSERT INTO DEPARTMENTS VALUES(9000, 'Отдел обслуживания');

INSERT INTO MANAGEMENT VALUES(7000, 1001);
INSERT INTO MANAGEMENT VALUES(8000, 1001);
INSERT INTO MANAGEMENT VALUES(9000, 1002); 


--Первые 4 цифры id - номер бригады, 
--следующие 3 - номер команды в этой бригаде
INSERT INTO TEAMS VALUES(0, 'Команда диспетчеров', 7000);
INSERT INTO TEAMS VALUES(0, 'Локомотивная бригада', 8000);
INSERT INTO TEAMS VALUES(0, 'Кассиры', 9000);

BEGIN
    ADD_DISPATCHER(10001, 'Евтушенко', 'Василий', 'Андреевич',
    '2003-02-01', '1941-08-03', 'M', 3, 48000.00, 7000001, 'Vasya1976');
    
    ADD_CASHIER(20001, 'Маркова', 'Ирина', 'Петровна',
    '2012-09-26', '1982-07-19', 'F', 0, 19000.00, 8000001, 'Itanium228');

    ADD_DRIVER(30001, 'Лусников', 'Василий', 'Олегович',
    '2007-04-12', '1979-11-06', 'M', 1, 28500.00, 8000001, '123hihi321');
END;
/


INSERT INTO LOCOMOTIVES VALUES(3456, 8000001, TO_DATE('1999-05-01', 'YYYY-MM-DD'));
INSERT INTO LOCOMOTIVES VALUES(8247, 8000001, TO_DATE('2002-04-28', 'YYYY-MM-DD'));


INSERT INTO STATIONS VALUES(1, 'Москва');
INSERT INTO STATIONS VALUES(2, 'Омск');
INSERT INTO STATIONS VALUES(3, 'Новосибирск');
INSERT INTO STATIONS VALUES(4, 'Барнаул');
INSERT INTO STATIONS VALUES(5, 'Севастополь');
INSERT INTO STATIONS VALUES(6, 'Берлин');
INSERT INTO STATIONS VALUES(7, 'Минск');
INSERT INTO STATIONS VALUES(8, 'Варшава'); 

INSERT INTO ROUTE_TYPES VALUES('Внутренний');
INSERT INTO ROUTE_TYPES VALUES('Международный');
INSERT INTO ROUTE_TYPES VALUES('Туристический');
INSERT INTO ROUTE_TYPES VALUES('Специальный');

INSERT INTO ROUTES VALUES(101, 1, 2, 'Внутренний');
INSERT INTO PATHS VALUES(0, 1, 3, 72, 101);
INSERT INTO PATHS VALUES(0, 3, 2, 10, 101);

INSERT INTO ROUTES VALUES(201, 4, 6, 'Международный');
INSERT INTO PATHS VALUES(0, 4, 3, 5, 201);
INSERT INTO PATHS VALUES(0, 3, 1, 64, 201);
INSERT INTO PATHS VALUES(0, 1, 6, 120, 201);

INSERT INTO ROUTES VALUES(301, 3, 5, 'Туристический');
INSERT INTO PATHS VALUES(0, 3, 1, 80, 301);
INSERT INTO PATHS VALUES(0, 1, 5, 48, 301);

INSERT INTO ROUTES VALUES(401, 3, 4, 'Специальный');
INSERT INTO PATHS VALUES(0, 3, 4, 5, 401);

INSERT INTO FLIGHTS VALUES(
    1, TO_DATE('2020-04-11 13:00', 'YYYY-MM-DD HH24:MI'), 
    3456, 201, 'Скорый', 4500.00);

INSERT INTO FLIGHTS VALUES(
    2, TO_DATE('2020-04-15 18:45', 'YYYY-MM-DD HH24:MI'), 
    8247, 101, 'Пассажирский', 2900.50);

INSERT INTO TICKETS VALUES(
    '', 'Навальный', 'Алексей', 'Сергеевич', 'Y', 34, 'M', 1, 'Y', 
    TO_DATE('2020-04-11 12:45:00', 'YYYY-MM-DD HH24:MI:SS'));







