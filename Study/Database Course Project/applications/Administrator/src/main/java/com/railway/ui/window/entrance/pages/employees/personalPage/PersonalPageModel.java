package com.railway.ui.window.entrance.pages.employees.personalPage;

import com.railway.database.DatabaseController;
import com.railway.database.tables.employee.EmployeeDomains;
import com.railway.database.tables.employee.EmployeeMatchers;
import com.railway.ui.window.common.entity.Employee;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.jooq.impl.DSL.*;

public class PersonalPageModel {

    private static PreparedStatement statement;

    static {
        try {
            String sql = select().from(EmployeeDomains.DETAILS_TABLE_NAME)
                    .where(new EmployeeMatchers.MatchById().getCondition())
                    .toString();
            statement = DatabaseController.getInstance().getConnection().prepareStatement(sql);
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public PersonalPageModel() {
    }

    public Employee getEmployeeInfo(int employeeId) {
        final int EMPLOYEE_ID_POS = 1;
        try {

            statement.setInt(EMPLOYEE_ID_POS, employeeId);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                return new Employee(
                        result.getInt(EmployeeDomains.EMPLOYEE_ID),
                        result.getString(EmployeeDomains.EMPLOYEE_LAST_NAME),
                        result.getString(EmployeeDomains.EMPLOYEE_FIRST_NAME),
                        result.getString(EmployeeDomains.EMPLOYEE_SECOND_NAME),
                        result.getDate(EmployeeDomains.EMPLOYEE_BIRTH_DATE).toLocalDate(),
                        result.getDate(EmployeeDomains.EMPLOYEE_HIRE_DATE).toLocalDate(),
                        result.getString(EmployeeDomains.EMPLOYEE_SPECIALITY),
                        result.getDouble(EmployeeDomains.EMPLOYEE_SALARY),
                        result.getString(EmployeeDomains.EMPLOYEE_GENDER),
                        result.getInt(EmployeeDomains.EMPLOYEE_CHILD_COUNT)
                );
            }
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }
}
