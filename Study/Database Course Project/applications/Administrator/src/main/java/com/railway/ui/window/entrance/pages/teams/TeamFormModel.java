package com.railway.ui.window.entrance.pages.teams;

import com.railway.database.DatabaseController;
import com.railway.database.tables.Errors;
import com.railway.database.tables.teams.TeamDomains;
import com.railway.ui.window.common.entity.Team;
import com.railway.ui.window.common.entity.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TeamFormModel {
    private Department department;
    private static final String selectSql = "SELECT * FROM TEAMS " +
                                            "WHERE department_id = ? " +
                                            "ORDER BY team_id";

    private static final String insertSql = "INSERT INTO TEAMS " +
                                            "VALUES(?, ?, ?)";

    private static final String deleteSql = "DELETE FROM TEAMS " +
                                            "WHERE team_id = ?";

    private static PreparedStatement selectStatement;
    private static PreparedStatement insertStatement;
    private static PreparedStatement deleteStatement;

    static {
        Connection connection = DatabaseController.getInstance().getConnection();
        try {
            selectStatement = connection.prepareStatement(selectSql);
            insertStatement = connection.prepareStatement(insertSql);
            deleteStatement = connection.prepareStatement(deleteSql);
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public TeamFormModel(Department department) {
        this.department = department;
    }

    public List<Team> getTeamList() {
        final int DEPARTMENT_ID_POS = 1;
        try {
            List<Team> teams = new LinkedList<>();
            selectStatement.setInt(DEPARTMENT_ID_POS, department.getDepartmentId());
            ResultSet result = selectStatement.executeQuery();
            while (result.next()) {
                teams.add(new Team(
                        result.getInt(TeamDomains.TEAM_ID),
                        result.getString(TeamDomains.TEAM_TYPE),
                        department
                ));
            }
            return teams;
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
        return new LinkedList<>();
    }

    public int addTeam(Team team) {
        final int TEAM_ID_POSITION = 1;
        final int TEAM_TYPE_POSITION = 2;
        final int DEPARTMENT_ID_POSITION = 3;

        try {
            insertStatement.setInt(TEAM_ID_POSITION, team.getTeamId());
            insertStatement.setString(TEAM_TYPE_POSITION, team.getTeamType());
            insertStatement.setInt(DEPARTMENT_ID_POSITION, team.getDepartment().getDepartmentId());
            insertStatement.executeQuery();
        }
        catch (final SQLException e) {
            return e.getErrorCode();
        }
        return Errors.QUERY_SUCCESS;
    }


    public int deleteTeam(int id) {
        final int TEAM_ID_POSITION = 1;

        try {
            deleteStatement.setInt(TEAM_ID_POSITION, id);
            int rows = deleteStatement.executeUpdate();
            if (rows == 0)
                return Errors.NO_DATA_FOUND;
            return Errors.QUERY_SUCCESS;
        }
        catch (final SQLException e) {
            return e.getErrorCode();
        }
    }
}
