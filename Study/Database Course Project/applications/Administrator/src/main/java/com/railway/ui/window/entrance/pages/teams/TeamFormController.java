package com.railway.ui.window.entrance.pages.teams;

import com.railway.database.tables.Errors;
import com.railway.ui.base.windowManager.WindowController;
import com.railway.ui.window.common.entity.Manager;
import com.railway.ui.window.common.entity.Team;
import com.railway.ui.window.common.entity.Department;
import com.railway.ui.window.common.fieldContollers.NumberFieldController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ResourceBundle;

public class TeamFormController extends WindowController implements Initializable {
    private TeamFormModel model;
    private Department department;

    @FXML
    private Label logoLabel;

    @FXML
    private TableView<Team> teamsTable;

    @FXML
    private Label managerId;

    @FXML
    private Label managerLastName;

    @FXML
    private Label managerFirstName;

    @FXML
    private Label managerSecondName;

    @FXML
    private Label managerAge;

    @FXML
    private Label managerChildCount;

    @FXML
    private Label managerHireDate;

    @FXML
    private Label managerSalary;

    @FXML
    private TextField teamIdField;

    @FXML
    private TextField teamTypeField;

    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Label resultLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initTable();
        initControllers();
    }

    public void setDepartment(Department department) {
        this.model = new TeamFormModel(department);
        this.department = department;
        updateView();
    }

    private void updateView() {
        final String datePattern = "dd-MM-yyyy";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(datePattern);

        if (department == null)
            return;

        logoLabel.setText("Department "
                + department.getDepartmentId() + " "
                + department.getDepartmentName());

        Manager manager = department.getManager();
        if (manager == null) {
            managerId.setText("Manager not found");
            return;
        }

        managerId.setText("ID: " + manager.getId());
        managerLastName.setText(manager.getLastName());
        managerFirstName.setText(manager.getFirstName());
        managerSecondName.setText(manager.getSecondName());

        LocalDate now = LocalDate.now();
        long age = manager.getBirthDate().until(now, ChronoUnit.YEARS);

        managerAge.setText("Age: " + age);
        managerHireDate.setText("Hire Date: " + manager.getHireDate().format(format));
        managerChildCount.setText("Child count: " + manager.getChildCount());
        managerSalary.setText("Salary: " + manager.getSalary());


        teamsTable.setItems(FXCollections.observableList(model.getTeamList()));

        teamIdField.setText("");
        teamTypeField.setText("");

        new NumberFieldController(teamIdField);
    }

    private void initControllers() {
        addButton.setOnMouseClicked(e -> {
            String type = teamTypeField.getText();
            if (type.isEmpty()) {
                resultLabel.setText("SPECIFY TEAM TYPE");
                return;
            }
            int result = model.addTeam(new Team(0, type, department));
            if (result == Errors.QUERY_SUCCESS) {
                resultLabel.setText("TEAM ADDED SUCCESSFULLY");
            } else {
                resultLabel.setText("UNKNOWN ERROR " + result);
            }
            updateView();
        });


        deleteButton.setOnMouseClicked(e -> {
            String text = teamIdField.getText();
            if (text.isEmpty()) {
                resultLabel.setText("SPECIFY TEAM ID");
                return;
            }
            int id = Integer.parseInt(text);
            int result = model.deleteTeam(id);
            switch (result) {
                case Errors.QUERY_SUCCESS:
                    resultLabel.setText("TEAM DELETED SUCCESSFULLY");
                    break;
                case Errors.NO_DATA_FOUND:
                    resultLabel.setText("TEAM NOT FOUND");
                    break;
                default:
                    resultLabel.setText("UNKNOWN ERROR " + result);
            }
            updateView();
        });
    }

    private void initTable() {
        final double ID_COLUMN_RATIO     = 0.2;
        final double NAME_COLUMN_RATIO   = 0.8;

        TableColumn<Team, Integer> teamId = new TableColumn<>("TEAM ID");
        teamId.setCellValueFactory(new PropertyValueFactory<>("teamId"));
        teamId.prefWidthProperty().bind(teamsTable.widthProperty().multiply(ID_COLUMN_RATIO));

        TableColumn<Team, String> teamType = new TableColumn<>("TYPE");
        teamType.setCellValueFactory(new PropertyValueFactory<>("teamType"));
        teamType.prefWidthProperty().bind(teamsTable.widthProperty().multiply(NAME_COLUMN_RATIO));

        teamsTable.getColumns().add(teamId);
        teamsTable.getColumns().add(teamType);

        teamsTable.setOnMousePressed(e -> {
            Team team = teamsTable.getSelectionModel().getSelectedItem();
            teamIdField.setText(Integer.toString(team.getTeamId()));
            teamTypeField.setText(team.getTeamType());
        });
    }

    @FXML
    void onPrevButtonClicked() {
        getWindowManager().prevScene();
    }
}
