package com.railway.ui.window.common.entity;


public class Department {
    private int departmentId;
    private String departmentName;
    private Driver manager;

    public Department(int id, String name, Driver manager) {
        this.departmentId = id;
        this.departmentName = name;
        this.manager = manager;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public Driver getManager() {
        return manager;
    }
}
