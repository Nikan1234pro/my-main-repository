package com.railway;

import com.railway.ui.base.windowManager.WindowManager;
import javafx.application.Application;
import javafx.stage.Stage;

public class ClientApplication extends Application {
    public static final int DEFAULT_WIDTH = 1280;
    public static final int DEFAULT_HEIGHT = 720;

    private static WindowManager manager;

    public static WindowManager getWindowManager() {
        return manager;
    }

    @Override
    public void start(Stage primaryStage) {
        final String authForm = "/auth/auth.fxml";

        manager = new WindowManager(primaryStage, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        manager.showScene(manager.loadScene(authForm));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
