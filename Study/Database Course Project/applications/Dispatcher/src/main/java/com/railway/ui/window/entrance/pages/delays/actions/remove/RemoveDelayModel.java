package com.railway.ui.window.entrance.pages.delays.actions.remove;


import com.railway.database.DatabaseController;
import com.railway.database.tables.Errors;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RemoveDelayModel {
    private static final String sql =
            "DELETE FROM DELAYS WHERE flight_number = ?";

    private static PreparedStatement statement;

    static {
        try {
            statement = DatabaseController.getInstance().getConnection().prepareStatement(sql);
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public int removeDelay(int flightNumber) {
        try {
            statement.setInt(1, flightNumber);
            int rows = statement.executeUpdate();
            if (rows == 0)
                return Errors.NO_DATA_FOUND;
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
            return e.getErrorCode();
        }
        return Errors.QUERY_SUCCESS;
    }

}
