package com.railway.ui.window.entrance.pages.delays.actions.add;

import com.railway.database.DatabaseController;
import com.railway.database.tables.Errors;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddDelayModel {
    private static final String sql = "INSERT INTO DELAYS VALUES(0, ?, ?, ?)";
    private static PreparedStatement statement;

    static {
        try {
            statement = DatabaseController.getInstance().getConnection().prepareStatement(sql);
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public int addDelay(int flightId, int duration, String cause) {
        final int ID = 1;
        final int CAUSE = 2;
        final int DURATION = 3;

        try {
            statement.setInt(ID, flightId);
            statement.setInt(DURATION, duration);
            statement.setString(CAUSE, cause);
            int rows = statement.executeUpdate();
            if (rows != 1) {
                return Errors.SAME_DATA_ALREADY_EXISTS;
            }
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
            return e.getErrorCode();
        }
        return Errors.QUERY_SUCCESS;
    }

}
