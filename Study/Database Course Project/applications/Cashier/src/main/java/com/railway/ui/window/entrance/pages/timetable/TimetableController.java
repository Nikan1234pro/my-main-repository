package com.railway.ui.window.entrance.pages.timetable;

import com.railway.database.tables.flights.FlightDomains;
import com.railway.database.tables.flights.FlightMatchers;
import com.railway.ui.base.windowManager.WindowController;
import com.railway.ui.base.windowManager.WindowManager;
import com.railway.ui.base.windowManager.WindowManagerInterface;
import com.railway.ui.window.common.fieldContollers.NumberFieldController;
import com.railway.ui.window.common.stations.StationListController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.jooq.Condition;

import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import static org.jooq.impl.DSL.*;

public class TimetableController extends WindowController implements Initializable {
    TimetableModel model = new TimetableModel();

    @FXML
    private ScrollPane board;

    @FXML
    private TextField flightNumberField;

    @FXML
    private ComboBox<String> stationFrom;

    @FXML
    private ComboBox<String> stationTo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        new StationListController(stationFrom);
        new StationListController(stationTo);
        new NumberFieldController(flightNumberField);

        board.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        stationFrom.getSelectionModel().selectedItemProperty()
                .addListener((options, oldValue, newValue) -> onFiltersSelected());
        stationTo.getSelectionModel().selectedItemProperty()
                .addListener((options, oldValue, newValue) -> onFiltersSelected());
        flightNumberField.setOnKeyTyped(u -> onFiltersSelected());
    }

    public void updateTimetable() {
        final String fxmlFile = "/entrance/pages/timetable/flightInfo.fxml";
        final WindowManagerInterface manager = getWindowManager();
        final double SPACING = 10;

        VBox layout = new VBox();
        layout.setSpacing(SPACING);

        board.setContent(layout);
        for (var info : model.getTimetable()) {
            FlightInfoController controller = manager.loadScene(fxmlFile ,this);
            AnchorPane node = (AnchorPane) controller.getView();
            node.prefWidthProperty().bind(board.widthProperty());

            layout.getChildren().add(node);
            controller.setModel(info);
            controller.updateView();
        }
    }


    private void onFiltersSelected() {
        LinkedList<Condition> filters = new LinkedList<>();

        String flightNumberText = flightNumberField.getText();
        if (!flightNumberText.isEmpty()) {
            filters.add(condition(new FlightMatchers
                    .MatchByFlightIdBeginning()
                    .bind(flightNumberText)
                    .getCondition()));
        }
        if (stationFrom.getSelectionModel().getSelectedIndex() > 0) {
            filters.add(field(FlightDomains.Timetable.START_STATION_NAME).eq(stationFrom.getValue()));
        }
        if (stationTo.getSelectionModel().getSelectedIndex() > 0) {
            filters.add(field(FlightDomains.Timetable.FINISH_STATION_NAME).eq(stationTo.getValue()));
        }
        model.setFilters(filters);
        updateTimetable();
    }
}
