package com.railway.ui.window.entrance.pages.cancellations.actions.add;

import com.railway.database.DatabaseController;
import com.railway.database.tables.Errors;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddCancellationModel {
    private static final String sql = "INSERT INTO CANCELLATIONS VALUES(0, ?, ?)";
    private static PreparedStatement statement;

    static {
        try {
            statement = DatabaseController.getInstance().getConnection().prepareStatement(sql);
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public int addCancellation(int flightId, String cause) {
        final int ID = 1;
        final int CAUSE = 2;

        try {
            statement.setInt(ID, flightId);
            statement.setString(CAUSE, cause);
            int rows = statement.executeUpdate();
            if (rows != 1) {
                return Errors.SAME_DATA_ALREADY_EXISTS;
            }
        }
        catch (final SQLException e) {
            System.err.println(e.getMessage());
            return e.getErrorCode();
        }
        return Errors.QUERY_SUCCESS;
    }

}
