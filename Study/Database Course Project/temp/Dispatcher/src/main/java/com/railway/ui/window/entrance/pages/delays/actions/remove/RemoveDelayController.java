package com.railway.ui.window.entrance.pages.delays.actions.remove;

import com.railway.database.tables.Errors;
import com.railway.ui.base.windowManager.WindowController;
import com.railway.ui.window.common.fieldContollers.IntegerFieldController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class RemoveDelayController extends WindowController implements Initializable {
    private RemoveDelayModel model = new RemoveDelayModel();

    @FXML
    private TextField flightField;

    @FXML
    private Button removeButton;

    @FXML
    private Label resultLabel;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new IntegerFieldController(flightField);

        removeButton.setOnMouseClicked(e -> {
            String text = flightField.getText();
            if (text.isEmpty()) {
                resultLabel.setText("Specify flight number");
                return;
            }
            int result = model.removeDelay(Integer.parseInt(text));
            resultLabel.setText("RESULT: " + Errors.toString(result));
        });
    }

    @FXML
    void onPrevButtonClicked() {
        getWindowManager().prevScene();
    }
}
