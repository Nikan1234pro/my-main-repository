CREATE OR REPLACE TRIGGER LOCOMOTIVE_TEAM_CHECK
BEFORE INSERT OR UPDATE ON LOCOMOTIVES
FOR EACH ROW
DECLARE
    drivers_count  INTEGER;
    repairman_count INTEGER;
BEGIN 
    IF :new.team_id IS NULL THEN
        RETURN;
    END IF;

    SELECT count(*)
    INTO drivers_count
    FROM DRIVER 
    WHERE team_id = :new.team_id;

    SELECT count(*)
    INTO repairman_count
    FROM REPAIRMAN 
    WHERE team_id = :new.team_id;

    IF repairman_count = 0 OR drivers_count = 0 THEN 
        raise_application_error(
                    -20219, 'Locomotive team must have driver and repairman');
    END IF;
END;
/


-- REPAIR
DROP SEQUENCE REPAIR_SEQUENCE;
CREATE SEQUENCE REPAIR_SEQUENCE minvalue 0;

CREATE OR REPLACE TRIGGER REPAIR_AUTOINCR
BEFORE INSERT
   ON REPAIR
   FOR EACH ROW
DECLARE
    repairmen_count  INTEGER;
BEGIN
  -- Проверим, что у локомотива есть ремонтник
  SELECT COUNT(*)
  INTO repairmen_count 
  FROM LOCOMOTIVES INNER JOIN REPAIRMAN USING(team_id) 
  WHERE locomotive_id = :new.locomotive_id;
  
  If repairmen_count = 0 THEN 
    raise_application_error(-20223, 'Locomotive requires repairman');
  END IF;

     :new.repair_id := REPAIR_SEQUENCE.NextVal;
END REPAIR_AUTOINCR;
/