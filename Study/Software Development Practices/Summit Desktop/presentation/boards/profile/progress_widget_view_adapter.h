#ifndef PROGRESS_WIDGET_VIEW_ADAPTER_H
#define PROGRESS_WIDGET_VIEW_ADAPTER_H

#include "model/profile/user_progress.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

class ProgressWidgetViewAdapter
    : public WidgetsViewerAdapter<UserProgressList> {

public:
  nonstd::optional<WidgetData>
  createWidget(const UserProgressList &progress) const override;
};
} // namespace summit

#endif // PROGRESS_WIDGET_VIEW_ADAPTER_H
