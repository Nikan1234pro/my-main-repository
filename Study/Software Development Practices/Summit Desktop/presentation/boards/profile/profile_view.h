#ifndef USER_SCREEN_H
#define USER_SCREEN_H

#include "domain/profile/load_profile_usecase.h"
#include "domain/profile/load_progress_usecase.h"
#include "presentation/boards/board_view.h"
#include "profile_widget_view_adapter.h"
#include "progress_widget_view_adapter.h"

#include <QDialog>

namespace summit {

template <> struct WidgetAdapterTraits<UserProfilePointer> {
  using type = ProfileWidgetViewAdapter;
};

template <> struct WidgetAdapterTraits<UserProgressList> {
  using type = ProgressWidgetViewAdapter;
};

class ProfileView : public BoardView {
  Q_OBJECT

  static constexpr QPoint PROFILE_WIDGET_POS{0, 1};
  static constexpr QPoint PROGRESS_WIDGET_POS{0, 1};

public:
  static constexpr const char *TITLE = "#Профиль";

  explicit ProfileView(ViewManager *view_manager);
  ~ProfileView();

private:
  void onShowAction() override;

  std::shared_ptr<LoadProfileUseCase> load_profile_usecase;
  std::shared_ptr<LoadProgressUseCase> load_progress_usecase;

  ImagePointer default_progress_image;
  ViewManager *view_manager;
};

} // namespace summit

#endif // USER_SCREEN_H
