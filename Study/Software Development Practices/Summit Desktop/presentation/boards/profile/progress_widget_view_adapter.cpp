#include "progress_widget_view_adapter.h"

#include "constants/presentation/widget_constants.h"
#include "presentation/common/widget/widget_utilities.h"

namespace summit {

nonstd::optional<ProgressWidgetViewAdapter::WidgetData>
ProgressWidgetViewAdapter::createWidget(
    const UserProgressList &progress) const {

  constexpr QSize widget_size{2, 1};
  constexpr auto widget_type = WidgetType::HORIZONTAL;

  Widget *widget =
      new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS, "Прогресс");
  widget->setBaseSize(WIDGET_BASE_SIZE[WidgetType::HORIZONTAL]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);
  return WidgetData{widget, widget_size};
}

} // namespace summit
