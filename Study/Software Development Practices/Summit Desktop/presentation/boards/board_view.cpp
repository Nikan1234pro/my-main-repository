#include "board_view.h"
#include "ui_board_view.h"

#include "board_presenter.h"
#include "constants/common/strings.h"
#include "domain/common/single_usecase_executor.h"

#include <QMessageBox>

namespace summit {

BoardView::BoardView(ViewManager *view_manager)
    : view_manager(view_manager),
      widgets_viewer(new ScrollableWidgetsViewer(WIDGET_BOARD_HEIGHT)),
      ui(new Ui::BoardView) {

  ui->setupUi(this);
  layout()->addWidget(widgets_viewer);

  createPresenter();
}

BoardView::~BoardView() { delete ui; }

void BoardView::showError(const PresentationError &error) {
  QMessageBox::information(this, QString::fromUtf8(APPLICATION_NAME),
                           error.what(), QMessageBox::Ok);
}

void BoardView::onShowAction() {}

void BoardView::onAddAction() {}

void BoardView::showEvent(QShowEvent *) {
  std::unique_ptr<ScrollableWidgetsViewer> new_viewer(
      new ScrollableWidgetsViewer(WIDGET_BOARD_HEIGHT));

  auto current =
      layout()->replaceWidget(dynamic_cast<QWidget *>(widgets_viewer),
                              dynamic_cast<QWidget *>(new_viewer.get()));
  delete widgets_viewer;
  delete current;

  widgets_viewer = new_viewer.release();
  onShowAction();
}

void BoardView::createPresenter() {
  presenter = new BoardPresenter(this, SingleUseCaseExecutor::getInstance());
}

} // namespace summit
