#include "board_presenter.h"

namespace summit {

BoardPresenter::BoardPresenter(
    summit::BoardView *view,
    const std::shared_ptr<summit::UseCaseExecutor> &executor)
    : QObject(view), view(view), executor(executor) {}

} // namespace summit
