#ifndef TRAININGLISTVIEW_H
#define TRAININGLISTVIEW_H

#include "domain/trainings/load_training_list_usecase.h"
#include "presentation/boards/board_view.h"
#include "presentation/common/view_manager.h"
#include "training_list_widget_view_adapter.h"
#include "training_widget_view_adapter.h"

namespace summit {

template <> struct WidgetAdapterTraits<Training> {
  using type = TrainingWidgetViewAdapter;
};

template <> struct WidgetAdapterTraits<TrainingList> {
  using type = TrainingListWidgetViewAdapter;
};

class TrainingListView : public BoardView {
  Q_OBJECT

public:
  static constexpr const char *TITLE = "#Тренировки";

  explicit TrainingListView(ViewManager *view_manager);
  ~TrainingListView() = default;

private:
  static ImagePointer getBackgroundById(int id);

  void onShowAction() override;
  void onAddAction() override;

  std::shared_ptr<LoadTrainingListUseCase> load_all_trainings_use_case;

  ViewManager *view_manager;
};

} // namespace summit

#endif // TRAININGLISTVIEW_H
