#include "training_list_view.h"

#include "data/trainings/trainings_repository.h"
#include "presentation/actions/trainings/training_edit_view.h"
#include "presentation/boards/board_presenter.h"

namespace summit {

TrainingListView::TrainingListView(ViewManager *view_manager)
    : BoardView(view_manager),
      load_all_trainings_use_case(
          new LoadTrainingListUseCase(TrainingsRepository::create())),
      view_manager(view_manager) {}

ImagePointer TrainingListView::getBackgroundById(int id) {
  static const ImagePointer IMAGES[] = {
      fromFile(":/binary/resource/training_window/training_back_1.png"),
      fromFile(":/binary/resource/training_window/training_back_2.png"),
      fromFile(":/binary/resource/training_window/training_back_3.png"),
  };
  static constexpr int size = std::size(IMAGES);
  return IMAGES[id % size];
}

void TrainingListView::onShowAction() {
  getPresenter()->loadAllData<Training>(
      load_all_trainings_use_case,
      [this](const Training &training, Widget *widget) {
        widget->setImage(getBackgroundById(training.get_id()));
        connect(widget, &Widget::clicked, this, [this, training]() {
          view_manager->nextView(
              new TrainingEditView(view_manager, training, true));
        });
      });
}

void TrainingListView::onAddAction() {
  view_manager->nextView(new TrainingEditView(view_manager));
}

} // namespace summit
