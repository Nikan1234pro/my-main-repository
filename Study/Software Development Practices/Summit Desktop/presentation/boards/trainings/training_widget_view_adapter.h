#ifndef TRAINING_WIDGET_VIEW_ADAPTER_H
#define TRAINING_WIDGET_VIEW_ADAPTER_H

#include "model/trainings/training.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

class TrainingWidgetViewAdapter : public WidgetsViewerAdapter<Training> {
public:
  nonstd::optional<WidgetData>
  createWidget(const Training &object) const override;
};

} // namespace summit
#endif // TRAINING_WIDGET_VIEW_ADAPTER_H
