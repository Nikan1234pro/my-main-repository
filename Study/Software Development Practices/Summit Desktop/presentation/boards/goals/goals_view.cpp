#include "goals_view.h"
#include "data/goals/goals_repository.h"
#include "presentation/actions/goals/goals_edit_view.h"
#include "presentation/boards/board_presenter.h"

#include <QDebug>
#include <QImage>
#include <QLabel>
#include <QLayout>
#include <QPainter>

namespace summit {

GoalsView::GoalsView(ViewManager *view_manager)
    : BoardView(view_manager),
      use_case(new LoadGoalsUseCase(GoalsRepository::create())),
      view_manager(view_manager) {}

GoalsView::~GoalsView() {}

void GoalsView::onShowAction() {
  getPresenter()->loadAllData<Goal>(use_case, [this](const Goal &goal,
                                                     Widget *widget) {
    addImageSubscriber(goal.get_backgroundId(), widget);
    connect(widget, &Widget::clicked, this, [this, goal]() {
      this->view_manager->nextView(new GoalsEditView(this->view_manager, goal));
    });
  });
}

void GoalsView::onAddAction() {
  view_manager->nextView(new GoalsEditView(view_manager));
}

} // namespace summit
