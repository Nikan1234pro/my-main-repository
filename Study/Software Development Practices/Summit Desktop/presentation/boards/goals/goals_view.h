#ifndef GOALS_SCREEN_H
#define GOALS_SCREEN_H

#include "domain/goals/load_goals_usecase.h"
#include "presentation/boards/board_view.h"
#include "presentation/common/view_manager.h"

#include "goal_widget_view_adapter.h"

#include <QDialog>

namespace summit {

template <> struct WidgetAdapterTraits<Goal> {
  using type = GoalWidgetViewerAdapter;
};

class GoalsView : public BoardView {
  Q_OBJECT

public:
  static constexpr const char *TITLE = "#Цели";

  explicit GoalsView(ViewManager *view_manager);
  ~GoalsView();

private:
  void onShowAction() override;
  void onAddAction() override;

  std::shared_ptr<LoadGoalsUseCase> use_case;
  ViewManager *view_manager;
};

} // namespace summit
#endif // GOALS_SCREEN_H
