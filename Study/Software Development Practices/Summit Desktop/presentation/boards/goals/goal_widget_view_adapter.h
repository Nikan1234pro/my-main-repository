#ifndef GOALWIDGETCREATOR_H
#define GOALWIDGETCREATOR_H

#include "constants/presentation/widget_constants.h"
#include "model/goals/goal.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

QColor taskStageToColor(const QString &stage);
WidgetItem *createTaskItem(const Task &task, WidgetType widget_type = LARGE);

class GoalWidgetViewerAdapter : public WidgetsViewerAdapter<Goal> {
public:
  nonstd::optional<WidgetData> createWidget(const Goal &goal) const override;
};

} // namespace summit

#endif // GOALWIDGETCREATOR_H
