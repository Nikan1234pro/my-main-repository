#include "goal_widget_view_adapter.h"

#include "presentation/common/font/font.h"
#include "presentation/common/widget/widget.h"
#include "presentation/common/widget/widget_utilities.h"

#include <unordered_map>

namespace summit {

QColor taskStageToColor(const QString &stage) {
  static const std::unordered_map<QString, QColor> STAGE_TO_COLOR = {
      {"New", Qt::red}, {"In progress", Qt::yellow}, {"Done", Qt::green}};

  auto iter = STAGE_TO_COLOR.find(stage);
  if (iter != STAGE_TO_COLOR.end()) {
    return iter->second;
  }
  return Qt::white;
}

nonstd::optional<GoalWidgetViewerAdapter::WidgetData>
GoalWidgetViewerAdapter::createWidget(const Goal &goal) const {

  QSize widget_size;
  if (auto size_optional = widgetSizeFromString(goal.get_size())) {
    widget_size = size_optional.value();
  } else {
    qWarning() << "Wrong widget size";
    return nonstd::nullopt;
  }
  auto widget_type = computeWidgetType(widget_size);

  Widget *widget =
      new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS, goal.get_name());
  widget->setBaseSize(WIDGET_BASE_SIZE[widget_type]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);

  int item_count = 0;
  for (auto &task : goal.get_tasks()) {
    if (++item_count > WIDGET_ITEM_MAX_COUNT[widget_type]) {
      break;
    }
    widget->addItem(createTaskItem(task, widget_type));
  }
  return WidgetData{widget, widget_size};
}

WidgetItem *createTaskItem(const Task &task, WidgetType widget_type) {
  TitledWidgetItem *item =
      new TitledWidgetItem(task.get_name(), task.get_description());
  item->setTitleDefaultFont(ITEM_TITLE_FONT);
  item->setBodyDefaultFont(ITEM_BODY_FONT);
  item->setTitleTextColor(Qt::white);
  item->setBodyTextColor(Qt::white);
  item->isDisplayBody(widget_type != SMALL);
  item->setStripeColor(taskStageToColor(task.get_stage()));
  return item;
}

} // namespace summit
