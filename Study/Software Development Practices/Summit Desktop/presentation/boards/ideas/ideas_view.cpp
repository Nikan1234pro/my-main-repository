#include "ideas_view.h"

#include "data/ideas/ideas_repository.h"
#include "idea_widget_view_adapter.h"
#include "presentation/boards/board_presenter.h"

#include "presentation/actions/ideas/ideas_edit_view.h"

namespace summit {

IdeasView::IdeasView(ViewManager *view_manager)
    : BoardView(view_manager),
      use_case(new LoadIdeasUseCase(IdeasRepository::create())),
      view_manager(view_manager) {}

IdeasView::~IdeasView() {}

void IdeasView::onShowAction() {
  getPresenter()->loadAllData<Idea>(use_case, [this](const Idea &idea,
                                                     Widget *widget) {
    addImageSubscriber(idea.get_backgroundId(), widget);
    connect(widget, &Widget::clicked, this, [this, idea]() {
      this->view_manager->nextView(new IdeasEditView(this->view_manager, idea));
    });
  });
}

void IdeasView::onAddAction() {
  view_manager->nextView(new IdeasEditView(view_manager));
}

} // namespace summit
