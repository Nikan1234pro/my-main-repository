#ifndef DEFAULT_VIEW_H
#define DEFAULT_VIEW_H

#include <QDialog>

#include "constants/presentation/widget_constants.h"
#include "domain/common/usecase.h"
#include "domain/common/usecase_executor.h"
#include "domain/goals/load_goals_usecase.h"
#include "presentation/common/base_view.h"
#include "presentation/common/presentation_error.h"
#include "presentation/common/view_manager.h"
#include "presentation/common/widget/scrollable_widgets_viewer.h"
#include "presentation/common/widget/widgets_viewer.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"
#include "presentation/images/images_viewer.h"

namespace Ui {
class BoardView;
}

namespace summit {

template <class DataType> struct WidgetAdapterTraits;

class BoardPresenter;

class BoardView : public QDialog, protected ImagesView {
  Q_OBJECT

public:
  explicit BoardView(ViewManager *view_manager);
  virtual ~BoardView();

  template <class DataType, class CallbackType>
  void
  showData(const QList<DataType> &data, CallbackType callback,
           const nonstd::optional<QPoint> &widget_position = nonstd::nullopt);

  void showError(const PresentationError &error);

  BoardPresenter *getPresenter() { return presenter; }

  virtual void onShowAction();
  virtual void onAddAction();

protected:
  void createPresenter() override;
  void showEvent(QShowEvent *) override;

private:
  BoardPresenter *presenter;

  ViewManager *view_manager;
  ScrollableWidgetsViewer *widgets_viewer;
  Ui::BoardView *ui;
};

template <class DataType, class CallbackType>
void BoardView::showData(const QList<DataType> &data, CallbackType callback,
                         const nonstd::optional<QPoint> &widget_position) {
  using Adapter = typename WidgetAdapterTraits<DataType>::type;

  Adapter adapter;
  for (const auto &value : data) {
    if (widget_position) {
      adapter.addWidget(widgets_viewer, value, callback, widget_position->x(),
                        widget_position->y());
      continue;
    }
    adapter.addWidget(widgets_viewer, value, callback);
  }

  auto position = widgets_viewer->getPosition();
  loadAllImages();
}

} // namespace summit

#endif // DEFAULT_VIEW_H
