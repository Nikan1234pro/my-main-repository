#include "training_editor.h"
#include "ui_training_editor.h"

#include "constants/presentation/styles.h"
#include "presentation/common/calendar/calendar.h"
#include "presentation/common/widget/widget_item.h"

#include <QAbstractTextDocumentLayout>
#include <QCommonStyle>
#include <QDebug>
#include <QDialog>
#include <QIntValidator>
#include <QScrollBar>
#include <QStringListModel>
#include <QTextEdit>

namespace summit {

static const QString TRAINING_NAME_PLACEHOLDER =
    QString::fromUtf8("Название тренировки...");

static const QString EXERCISE_NAME_PLACEHOLDER =
    QString::fromUtf8("Название...");
static const QString EXERCISE_DESC_PLACEHOLDER =
    QString::fromUtf8("Описание...");
static const QString EXERCISE_REPETITIONS_PLACEHOLDER =
    QString::fromUtf8("Повт.");

static const QStringList TRAINING_CYCLES{
    QString::fromUtf8("легкий"), QString::fromUtf8("средний"),
    QString::fromUtf8("сложный"), QString::fromUtf8("персональный")};

static const QStringList EXERCISE_TYPES{QString::fromUtf8("Зал"),
                                        QString::fromUtf8("Улица")};

ExerciseItem::ExerciseItem(QListWidget *list_widget, const Exercise &exercise)
    : QListWidgetItem(list_widget) {

  widget = new Widget(this, exercise);
  list_widget->setItemWidget(this, widget);
}

const Exercise &ExerciseItem::getExercise() const {
  return widget->getExercise();
}

void ExerciseItem::adjustSize() {
  widget->adjustSize();
  setSizeHint(widget->sizeHint());
}

ExerciseItem::Widget::Widget(QListWidgetItem *item, const Exercise &exercise)
    : exercise(exercise), layout(new QHBoxLayout) {

  static const QString title_pattern("%1 (%2)");
  static const QString repetitions_pattern = QString::fromUtf8("%1 повторений");

  auto exercise_info = std::make_unique<WidgetItem>(
      title_pattern.arg(exercise.get_name()).arg(exercise.get_type()));
  exercise_info->setStripeColor(Qt::transparent);
  exercise_info->setBodyTextColor(Qt::white);

  auto edit = std::make_unique<QTextEdit>(exercise.get_description());
  edit->setFrameStyle(QFrame::NoFrame);
  edit->document()->setDocumentMargin(0);
  edit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
  edit->setDisabled(true);
  connect(edit->document()->documentLayout(),
          &QAbstractTextDocumentLayout::documentSizeChanged, this,
          [this, edit = edit.get(), item](const QSizeF &r) {
            edit->setFixedHeight(int((r.height())));
            item->setSizeHint(sizeHint());
          });
  this->edit = edit.get();

  exercise_info->getLayout()->addWidget(edit.release());

  auto repetitions = std::make_unique<QPushButton>(
      repetitions_pattern.arg(exercise.get_repetitions()));
  repetitions->setStyleSheet(BUTTON_STYLE);
  repetitions->setFixedWidth(BUTTON_WIDTH);

  setLayout(layout);
  layout->setSpacing(0);
  layout->addWidget(exercise_info.release(), 2);
  layout->addWidget(repetitions.release(), 1);
}

void ExerciseItem::Widget::adjustSize() {
  QSize size = edit->document()->size().toSize();
  edit->setFixedSize(size);
}

TrainingEditor::TrainingEditor(QWidget *parent)
    : QWidget(parent), ui(new Ui::TrainingEditor) {
  ui->setupUi(this);

  training_name = ui->training_name;
  training_name->setPlaceholderText(TRAINING_NAME_PLACEHOLDER);

  training_cycle = ui->trainig_cycle;
  training_cycle->addItems(TRAINING_CYCLES);

  exercises_list = ui->exercises_edit;
  exercises_list->verticalScrollBar()->setStyle(new QCommonStyle);
  exercises_list->verticalScrollBar()->setStyleSheet(VERTICAL_SCROLLBAR_STYLE);
  exercises_list->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  exercises_list->setWordWrap(true);
  exercises_list->setTextElideMode(Qt::TextElideMode::ElideLeft);

  exercise_name = ui->exercise_name;
  exercise_name->setPlaceholderText(EXERCISE_NAME_PLACEHOLDER);

  exercise_description = ui->exercise_description;
  exercise_description->setPlaceholderText(EXERCISE_DESC_PLACEHOLDER);

  exercise_repetitions = ui->exercise_repetitions;
  exercise_repetitions->setPlaceholderText(EXERCISE_REPETITIONS_PLACEHOLDER);
  exercise_repetitions->setValidator(new QIntValidator(0, 100));

  exercise_type = ui->exercise_type;
  exercise_type->addItems(EXERCISE_TYPES);

  schedule_list = ui->schedule_list;
  schedule_list->verticalScrollBar()->setStyle(new QCommonStyle);
  schedule_list->verticalScrollBar()->setStyleSheet(VERTICAL_SCROLLBAR_STYLE);
  schedule_list->setWordWrap(true);
  schedule_list->setTextElideMode(Qt::TextElideMode::ElideLeft);
}

TrainingEditor::TrainingEditor(const Training &training, bool is_editable,
                               QWidget *parent)
    : TrainingEditor(parent) {

  training_id = training.get_id();

  const auto &exercises = training.get_exercises();
  std::for_each(exercises.begin(), exercises.end(),
                [this](const Exercise &exercise) { addExercise(exercise); });

  training_name->setText(training.get_name());
  training_cycle->setCurrentText(training.get_cycle());
  training_cycle->setDisabled(!is_editable);

  ui->editor_panel->setVisible(is_editable);
  ui->schedule_panel->setVisible(is_editable &&
                                 training.has_trainingSchedules());

  if (training.has_trainingSchedules()) {
    for (auto &schedule : training.get_trainingSchedules()) {
      const auto &date = schedule.get_date();
      schedule_list->addItem(date);
      dates.push_back(QDate::fromString(date, Qt::ISODate));
    }
  }
}

TrainingEditor::~TrainingEditor() { delete ui; }

Training TrainingEditor::getTrainig() const {
  Training training;
  training.set_id(training_id);
  training.set_name(training_name->text());
  training.set_cycle(training_cycle->currentText());

  std::vector<Exercise> exercises;
  int exercises_count = exercises_list->count();
  exercises.reserve(exercises_count);
  for (int i = 0; i < exercises_count; ++i) {
    QListWidgetItem *item = exercises_list->item(i);
    exercises.push_back(convertToExercise(item));
  }
  training.set_exercises(std::move(exercises));

  int schedule_count = schedule_list->count();
  if (!schedule_count) {
    training.set_trainingSchedules(nonstd::nullopt);
    return training;
  }

  std::vector<TrainingSchedule> schedules;
  schedules.reserve(schedule_count);
  for (int i = 0; i < schedule_count; ++i) {
    QListWidgetItem *item = schedule_list->item(i);
    schedules.push_back(convertToSchedule(item));
  }
  training.set_trainingSchedules(std::move(schedules));
  return training;
}

void TrainingEditor::showEvent(QShowEvent *) {
  int count = exercises_list->count();
  for (int i = 0; i < count; ++i) {
    ExerciseItem *item = dynamic_cast<ExerciseItem *>(exercises_list->item(i));
    item->adjustSize();
  }
}

void TrainingEditor::addExercise(const Exercise &exercise) {
  new ExerciseItem(exercises_list, exercise);
}

Exercise TrainingEditor::convertToExercise(QListWidgetItem *item) const {
  if (auto exercise_widget = dynamic_cast<ExerciseItem::Widget *>(
          exercises_list->itemWidget(item))) {
    return exercise_widget->getExercise();
  }
  throw std::runtime_error("Wrong widget");
}

TrainingSchedule
TrainingEditor::convertToSchedule(QListWidgetItem *item) const {
  TrainingSchedule schedule;
  schedule.set_trainingId(training_id);
  schedule.set_date(item->text());
  return schedule;
}

void TrainingEditor::on_select_date_clicked() {
  Calendar *calendar = new Calendar(this->dates, this);
  calendar->setModal(true);
  calendar->exec();

  this->dates = calendar->getDates();

  schedule_list->clear();
  for (auto &date : calendar->getDates()) {
    schedule_list->addItem(date.toString(Qt::DateFormat::ISODate));
  }
}

void TrainingEditor::on_add_exercise_clicked() {
  QString name = exercise_name->text();
  QString desc = exercise_description->text();
  QString repetitios_string = exercise_repetitions->text();
  QString type = exercise_type->currentText();

  if (name.isEmpty() || desc.isEmpty() || repetitios_string.isEmpty()) {
    return;
  }
  Exercise exercise;
  exercise.set_name(name);
  exercise.set_description(desc);
  exercise.set_trainingId(training_id);
  exercise.set_repetitions(repetitios_string.toInt());
  exercise.set_type(type);

  addExercise(exercise);

  exercise_name->clear();
  exercise_description->clear();
  exercise_repetitions->clear();
}

void TrainingEditor::on_remove_exercise_clicked() {
  int index = exercises_list->currentRow();
  if (index < 0) {
    return;
  }

  QListWidgetItem *item = exercises_list->takeItem(index);
  delete item;
}

} // namespace summit
