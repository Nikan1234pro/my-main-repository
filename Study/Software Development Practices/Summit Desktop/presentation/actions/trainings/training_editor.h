#ifndef TRAINING_EDITOR_H
#define TRAINING_EDITOR_H

#include "model/trainings/training.h"

#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QTextEdit>
#include <QWidget>

namespace Ui {
class TrainingEditor;
}

namespace summit {

class ExerciseItem : public QListWidgetItem {
  static constexpr int BUTTON_WIDTH = 220;

public:
  class Widget : public QWidget {
    Exercise exercise;

    QTextEdit *edit;
    QLabel *repetitions;
    QBoxLayout *layout;

  public:
    explicit Widget(QListWidgetItem *item, const Exercise &exercise);
    const Exercise &getExercise() const { return exercise; }

    void adjustSize();
  };

  explicit ExerciseItem(QListWidget *list_widget, const Exercise &exercise);
  const Exercise &getExercise() const;
  void adjustSize();

private:
  Widget *widget;
};

class TrainingEditor : public QWidget {
  Q_OBJECT

public:
  explicit TrainingEditor(QWidget *parent = nullptr);
  TrainingEditor(const Training &training, bool is_editable = true,
                 QWidget *parent = nullptr);

  ~TrainingEditor();

  Training getTrainig() const;

protected:
  void showEvent(QShowEvent *event) override;

private slots:
  void on_select_date_clicked();

  void on_add_exercise_clicked();

  void on_remove_exercise_clicked();

private:
  void addExercise(const Exercise &exercise);

  Exercise convertToExercise(QListWidgetItem *item) const;
  TrainingSchedule convertToSchedule(QListWidgetItem *item) const;

  int training_id;

  QLineEdit *training_name;
  QComboBox *training_cycle;
  QListWidget *exercises_list;

  QLineEdit *exercise_name;
  QLineEdit *exercise_description;
  QLineEdit *exercise_repetitions;
  QComboBox *exercise_type;

  QListWidget *schedule_list;
  QList<QDate> dates;

  Ui::TrainingEditor *ui;
};

} // namespace summit

#endif // TRAINING_EDITOR_H
