#include "training_edit_view.h"

#include "data/trainings/trainings_repository.h"
#include "presentation/actions/action_presenter.h"

namespace summit {

TrainingEditView::TrainingEditView(ViewManager *view_manager)
    : ActionView(view_manager), is_editable(true), editor(new TrainingEditor),
      update_use_case(new UpdateTrainingUseCase(TrainingsRepository::create())),
      view_manager(view_manager) {

  init();
}

TrainingEditView::TrainingEditView(ViewManager *view_manager,
                                   const Training &trainig, bool is_editable)
    : ActionView(view_manager, is_editable), is_editable(is_editable),
      editor(new TrainingEditor(trainig, is_editable)),
      update_use_case(new UpdateTrainingUseCase(TrainingsRepository::create())),
      delete_use_case(new DeleteTrainingUseCase(TrainingsRepository::create())),
      view_manager(view_manager) {

  init();
}

void TrainingEditView::onAcceptAction() {
  if (!is_editable) {
    view_manager->prevView();
    return;
  }
  Training training = editor->getTrainig();
  getPresenter()->updateData(update_use_case, training);
}

void TrainingEditView::onRemoveAction() {
  Training training = editor->getTrainig();
  getPresenter()->updateData(delete_use_case, training.get_id());
}

TrainingEditView::~TrainingEditView() {}

void TrainingEditView::onUpdateSuccess() { view_manager->prevView(); }

void TrainingEditView::init() {
  getLayout()->addWidget(editor);
  setImageSelectorVisible(false);
  setSizeSelectorVisible(false);
}

} // namespace summit
