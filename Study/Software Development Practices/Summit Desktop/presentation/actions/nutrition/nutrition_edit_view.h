#ifndef NUTRITIONEDITVIEW_H
#define NUTRITIONEDITVIEW_H

#include <QLineEdit>

#include "domain/nutrition/delete_recipe_usecase.h"
#include "domain/nutrition/update_recipe_usecase.h"
#include "model/nutrition/recipe.h"
#include "presentation/actions/action_view.h"
#include "recipe_editor.h"

namespace summit {

class NutritionEditView : public ActionView {

public:
  explicit NutritionEditView(ViewManager *view_manager);

  NutritionEditView(ViewManager *view_manager, const Recipe &recipe);

  void onUpdateSuccess() override;

private:
  void init();

  void onAcceptAction() override;
  void onRemoveAction() override;

  std::shared_ptr<UpdateRecipeUseCase> update_use_case;
  std::shared_ptr<DeleteRecipeUseCase> delete_use_case;

  RecipeEditor *recipe_editor;
  ViewManager *view_manager;
};

} // namespace summit

#endif // NUTRITIONEDITVIEW_H
