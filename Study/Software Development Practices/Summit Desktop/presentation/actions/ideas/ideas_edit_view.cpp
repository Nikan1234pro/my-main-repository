#include "ideas_edit_view.h"

#include "constants/presentation/styles.h"
#include "data/ideas/ideas_repository.h"
#include "presentation/actions/action_presenter.h"

namespace summit {

IdeasEditView::IdeasEditView(ViewManager *view_manager)
    : ActionView(view_manager, false), idea(nonstd::nullopt),
      update_use_case(new UpdateIdeaUseCase(IdeasRepository::create())),
      view_manager(view_manager) {

  init();
}

IdeasEditView::IdeasEditView(ViewManager *view_manager, const Idea &idea)
    : ActionView(view_manager, true), idea(idea),
      update_use_case(new UpdateIdeaUseCase(IdeasRepository::create())),
      delete_use_case(new DeleteIdeaUseCase(IdeasRepository::create())),
      view_manager(view_manager) {

  init();

  idea_name->setText(idea.get_name());
  idea_content->setText(idea.get_content());
  setSize(idea.get_size());
  setBackgroundId(idea.get_backgroundId());
}

void IdeasEditView::onUpdateSuccess() {
  view_manager->prevView();
  ActionView::onUpdateSuccess();
}

void IdeasEditView::init() {
  idea_name = new QLineEdit();
  idea_name->setStyleSheet(LINE_EDIT_STYLE);
  getLayout()->addWidget(idea_name, 0, Qt::AlignTop);

  idea_content = new QTextEdit();
  idea_content->setStyleSheet(TEXT_EDIT_STYLE);
  getLayout()->addWidget(idea_content);

  idea_name->setPlaceholderText("Введите название...");
  idea_content->setPlaceholderText("Введите текст...");
}

void IdeasEditView::onAcceptAction() {
  Idea idea_value;
  if (idea) {
    idea_value = idea.value();
  }
  idea_value.set_name(idea_name->text());
  idea_value.set_content(idea_content->toPlainText());
  idea_value.set_size(getSize());
  idea_value.set_backgroundId(getBackgroundId());

  getPresenter()->updateData(update_use_case, idea_value);
}

void IdeasEditView::onRemoveAction() {
  assert(delete_use_case && idea);
  getPresenter()->updateData(delete_use_case, idea.value().get_id());
}

} // namespace summit
