#include "action_view.h"
#include "ui_action_view.h"

#include "action_presenter.h"
#include "constants/common/strings.h"
#include "constants/presentation/images.h"
#include "domain/common/single_usecase_executor.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

namespace summit {

ActionView::ActionView(ViewManager *view_manager, bool has_data)
    : QDialog(view_manager), background_id(DEFAULT_BACKGROUND_ID),
      view_manager(view_manager), ui(new Ui::ActionView) {
  ui->setupUi(this);

  createPresenter();

  ui->remove_button->setVisible(has_data);

  ui->size_box->addItem("2x2");
  ui->size_box->addItem("2x1");
  ui->size_box->addItem("1x1");

  background_placeholder = ui->image_button;

  connect(ui->accept_button, &QPushButton::clicked, this,
          [this]() { prepareOnAcceptAction(); });
  connect(ui->cancel_button, &QPushButton::clicked, this,
          [this]() { this->view_manager->prevView(); });
  connect(ui->remove_button, &QPushButton::clicked, this,
          [this]() { prepareOnRemoveAction(); });

  connect(background_placeholder, &QPushButton::clicked, this,
          [this]() { selectBackgroundImage(); });
}

ActionView::~ActionView() { delete ui; }

void summit::ActionView::onUpdateSuccess() { blockingAction(false); }

void ActionView::onUpdateFailure(const PresentationError &error) {
  QMessageBox::information(this, QString::fromUtf8(APPLICATION_NAME),
                           error.what(), QMessageBox::Ok);
  blockingAction(false);
}

void ActionView::setSizeSelectorVisible(bool value) {
  ui->size_box->setVisible(value);
}

void ActionView::setImageSelectorVisible(bool value) {
  ui->image_button->setVisible(value);
}

void ActionView::setAcceptButtonVisible(bool value) {
  ui->accept_button->setVisible(value);
}

void ActionView::setRemoveButtonVisible(bool value) {
  ui->remove_button->setVisible(value);
}

void ActionView::setCustomBackgroundPlaceholder(QPushButton *placeholder) {
  disconnect(background_placeholder, &QPushButton::clicked, this, nullptr);

  background_placeholder = placeholder;
  connect(background_placeholder, &QPushButton::clicked, this,
          [this]() { selectBackgroundImage(); });
}

void ActionView::createPresenter() {
  presenter = new ActionPresenter(this, SingleUseCaseExecutor::getInstance());
}

QVBoxLayout *ActionView::getLayout() { return ui->frame_layout; }

ActionPresenter *ActionView::getPresenter() { return presenter; }

void ActionView::setSize(const QString &size) {
  ui->size_box->setCurrentIndex(ui->size_box->findText(size));
}

QString ActionView::getSize() const { return ui->size_box->currentText(); }

void ActionView::setImage(const ImagePointer &image) {
  background_image = image;

  auto icon_size = background_placeholder->iconSize();
  background_placeholder->setIcon(
      background_image->getScaled(icon_size).fit(icon_size).getAsPixmap());
  background_placeholder->setProperty("background", image->getImageId());
  blockingAction(false);
}

void ActionView::setBackgroundId(int id) {
  background_id.store(id);

  blockingAction(true);
  addImageSubscriber(id, this);
  loadAllImages();
}

int ActionView::getBackgroundId() const { return background_id.load(); }

void ActionView::blockingAction(bool block) {
  ui->accept_button->blockSignals(block);
  ui->remove_button->blockSignals(block);
  background_placeholder->blockSignals(block);
}

void ActionView::prepareOnAcceptAction() {
  blockingAction(true);

  if (background_image &&
      background_image->getImageId() == ImageWrapper::NO_IMAGE_ID) {
    sendImage(background_image);
  } else {
    onAcceptAction();
  }
}

void ActionView::prepareOnRemoveAction() {
  blockingAction(true);
  onRemoveAction();
}

void ActionView::onImageSendSuccess(const ImagePointer &image) {
  background_id.store(image->getImageId());
  onAcceptAction();
}

void ActionView::onImageActionFailure(const PresentationError &error) {
  QMessageBox::information(
      this, QString::fromUtf8(APPLICATION_NAME),
      QString::fromUtf8("Невозможно добавить изображение:\n") + error.what(),
      QMessageBox::Ok);
}

void ActionView::selectBackgroundImage() {
  QString filename = QFileDialog::getOpenFileName(
      this, QString::fromUtf8("Выбареите изображение"), QDir::homePath(),
      tr("Images (*.png *.jpeg *.jpg)"));
  if (filename.isNull()) {
    return;
  }

  const auto loaded_image = fromFile(filename);
  if (!loaded_image) {
    QMessageBox::information(
        this, QString::fromUtf8(APPLICATION_NAME),
        QString::fromUtf8("Невозможно добавить изображение"), QMessageBox::Ok);
    return;
  }
  setImage(ImagePointer::create(loaded_image->getScaled(
      IMAGE_CROP_SIZE.value_or(loaded_image->getSize()))));
}

} // namespace summit
