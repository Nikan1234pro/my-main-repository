#include "goals_editor.h"
#include "ui_goals_editor.h"

#include "constants/presentation/styles.h"
#include "presentation/boards/goals/goal_widget_view_adapter.h"

#include <QAbstractTextDocumentLayout>
#include <QCommonStyle>
#include <QItemDelegate>
#include <QScrollBar>
#include <QTextEdit>

namespace summit {

static const QString GOAL_NAME_PLACEHOLDER =
    QString::fromUtf8("Название цели...");
static const QString GOAL_DESC_PLACEHOLDER =
    QString::fromUtf8("Описание цели...");
static const QString TASK_NAME_PLACEHOLDER =
    QString::fromUtf8("Название задачи...");
static const QString TASK_DESC_PLACEHOLDER =
    QString::fromUtf8("Описание задачи...");

TaskItem::TaskItem(QListWidget *list_widget, const Task &task)
    : QListWidgetItem(list_widget) {

  widget = new Widget(this, task);
  list_widget->setItemWidget(this, widget);
}

TaskItem::Widget::Widget(QListWidgetItem *item, const Task &task)
    : task(task), layout(new QHBoxLayout) {

  auto task_info = std::make_unique<WidgetItem>(task.get_name());
  task_info->setStripeColor(taskStageToColor(task.get_stage()));
  task_info->setBodyTextColor(Qt::white);

  auto edit = std::make_unique<QTextEdit>(task.get_description());
  edit->setFrameStyle(QFrame::NoFrame);
  edit->document()->setDocumentMargin(0);
  edit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
  edit->setDisabled(true);
  connect(edit->document()->documentLayout(),
          &QAbstractTextDocumentLayout::documentSizeChanged, this,
          [this, edit = edit.get(), item](const QSizeF &r) {
            edit->setFixedHeight(int((r.height())));
            item->setSizeHint(sizeHint());
          });
  this->edit = edit.get();
  task_info->getLayout()->addWidget(edit.release());

  auto action_button = std::make_unique<QPushButton>();
  action_button->setStyleSheet(BUTTON_STYLE);
  action_button->setFixedWidth(BUTTON_WIDTH);
  connect(action_button.get(), &QPushButton::clicked, this,
          [this, task_info = task_info.get()]() {
            handler->handle(this->task);
            task_info->setStripeColor(taskStageToColor(this->task.get_stage()));
            update();
          });

  this->action_button = action_button.get();

  setLayout(layout);

  layout->setSpacing(0);
  layout->addWidget(task_info.release(), 1);
  layout->addWidget(action_button.release(), 2);

  /* Init factory */
  factory.add<NewTask>("New");
  factory.add<InProgressTask>("In progress");
  factory.add<ResolvedTask>("Done");

  update();
}

void TaskItem::Widget::update() {
  auto current_handler = factory.create(task.get_stage());
  if (!current_handler) {
    qWarning() << "Incorrect task stage";
    return;
  }
  handler = current_handler->nextAction();
  action_button->setText(handler->getDescription());
}

void TaskItem::Widget::adjustSize() {
  QSize size = edit->document()->size().toSize();
  edit->setFixedSize(size);
}

const Task &TaskItem::getTask() const { return widget->getTask(); }

void TaskItem::adjustSize() {
  widget->adjustSize();
  setSizeHint(widget->sizeHint());
}

GoalsEditor::GoalsEditor(QWidget *parent)
    : QWidget(parent), ui(new Ui::GoalsEditor) {
  ui->setupUi(this);

  goal_name = ui->goal_name;
  goal_name->setPlaceholderText(GOAL_NAME_PLACEHOLDER);

  goal_description = ui->goal_description;
  goal_description->setPlaceholderText(GOAL_DESC_PLACEHOLDER);

  tasks_list = ui->tasks_list;
  tasks_list->verticalScrollBar()->setStyle(new QCommonStyle);
  tasks_list->verticalScrollBar()->setStyleSheet(VERTICAL_SCROLLBAR_STYLE);
  tasks_list->setWordWrap(true);
  tasks_list->setTextElideMode(Qt::TextElideMode::ElideLeft);

  task_name = ui->task_name;
  task_name->setPlaceholderText(TASK_NAME_PLACEHOLDER);

  task_decription = ui->task_description;
  task_decription->setPlaceholderText(TASK_DESC_PLACEHOLDER);
}

GoalsEditor::GoalsEditor(const Goal &goal, QWidget *parent)
    : GoalsEditor(parent) {
  this->goal = goal;

  auto &tasks = goal.get_tasks();
  std::for_each(tasks.begin(), tasks.end(),
                [this](const Task &task) { addTask(task); });
  goal_name->setText(goal.get_name());
  goal_description->setText(goal.get_description());
}

GoalsEditor::~GoalsEditor() { delete ui; }

Goal &GoalsEditor::getGoal() {
  goal.set_name(goal_name->text());
  goal.set_description(goal_description->text());

  /* Get tasks from editor */
  std::vector<Task> tasks;
  int task_count = tasks_list->count();
  tasks.reserve(task_count);
  for (int i = 0; i < task_count; ++i) {
    QListWidgetItem *item = tasks_list->item(i);
    tasks.push_back(convertToTask(item));
  }
  goal.set_tasks(std::move(tasks));

  return goal;
}

void GoalsEditor::addTask(const Task &task) { new TaskItem(tasks_list, task); }

const Task &GoalsEditor::convertToTask(QListWidgetItem *item) const {
  if (auto task_item =
          dynamic_cast<TaskItem::Widget *>(tasks_list->itemWidget(item))) {
    return task_item->getTask();
  }
  throw std::runtime_error("Wrong widget");
}

void GoalsEditor::on_create_task_clicked() {

  QString name = task_name->text();
  QString description = task_decription->text();
  if (name.isEmpty() || description.isEmpty()) {
    return;
  }
  Task task;
  task.set_name(name);
  task.set_description(description);
  task.set_stage(TASK_STAGES[New]);

  addTask(task);

  task_name->clear();
  task_decription->clear();
}

void GoalsEditor::on_remove_task_clicked() {
  int index = tasks_list->currentRow();
  if (index < 0) {
    return;
  }

  QListWidgetItem *item = tasks_list->takeItem(index);
  delete item;
}

void GoalsEditor::showEvent(QShowEvent *) {
  int count = tasks_list->count();
  for (int i = 0; i < count; ++i) {
    TaskItem *item = dynamic_cast<TaskItem *>(tasks_list->item(i));
    item->adjustSize();
  }
}

} // namespace summit
