#ifndef GOALS_EDITOR_H
#define GOALS_EDITOR_H

#include <QLineEdit>
#include <QListWidget>
#include <QMutex>
#include <QPushButton>
#include <QTextEdit>
#include <QWidget>

#include "model/goals/goal.h"
#include "presentation/common/widget/widget_item.h"
#include "task_actions.h"

namespace Ui {
class GoalsEditor;
}

namespace summit {

class TaskItem : public QListWidgetItem {
  static constexpr int BUTTON_WIDTH = 200;

public:
  class Widget : public QWidget {
    Task task;
    TaskActionFactory factory;
    std::unique_ptr<TaskStateHandler> handler;

    QTextEdit *edit;
    QBoxLayout *layout;
    QPushButton *action_button;

  public:
    explicit Widget(QListWidgetItem *item, const Task &task);
    void update();
    void adjustSize();

    const Task &getTask() const { return task; }
  };

  explicit TaskItem(QListWidget *list_widget, const Task &task);
  const Task &getTask() const;

  void adjustSize();

private:
  Widget *widget;
  void update();
};

class GoalsEditor : public QWidget {
  Q_OBJECT

public:
  explicit GoalsEditor(QWidget *parent = nullptr);
  explicit GoalsEditor(const Goal &goal, QWidget *parent = nullptr);
  ~GoalsEditor();

  Goal &getGoal();

private slots:
  void on_create_task_clicked();
  void on_remove_task_clicked();

  void showEvent(QShowEvent *event) override;

private:
  void addTask(const Task &task);
  const Task &convertToTask(QListWidgetItem *item) const;

  Goal goal;

  QLineEdit *goal_name;
  QLineEdit *goal_description;
  QLineEdit *task_name;
  QLineEdit *task_decription;

  QListWidget *tasks_list;

  Ui::GoalsEditor *ui;
};

} // namespace summit

#endif // GOALS_EDITOR_H
