#include "task_actions.h"

namespace summit {

const QString NewTask::description = QString::fromUtf8("Повторить");
const QString InProgressTask::description = QString::fromUtf8("Начать");
const QString ResolvedTask::description = QString::fromUtf8("Завершить");

NewTask::NewTask() { setDescription(description); }

std::unique_ptr<TaskStateHandler> NewTask::nextAction() const {
  return std::make_unique<InProgressTask>();
}

void NewTask::handle(Task &task) { task.set_stage(TASK_STAGES[New]); }

InProgressTask::InProgressTask() { setDescription(description); }

std::unique_ptr<TaskStateHandler> InProgressTask::nextAction() const {
  return std::make_unique<ResolvedTask>();
}

void InProgressTask::handle(Task &task) {
  task.set_stage(TASK_STAGES[InProgress]);
}

ResolvedTask::ResolvedTask() { setDescription(description); }

std::unique_ptr<TaskStateHandler> ResolvedTask::nextAction() const {
  return std::make_unique<NewTask>();
}

void ResolvedTask::handle(Task &task) { task.set_stage(TASK_STAGES[Resolved]); }

} // namespace summit
