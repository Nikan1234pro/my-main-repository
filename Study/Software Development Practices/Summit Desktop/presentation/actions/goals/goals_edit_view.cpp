#include "goals_edit_view.h"

#include "data/goals/goals_repository.h"
#include "presentation/actions/action_presenter.h"

namespace summit {

GoalsEditView::GoalsEditView(ViewManager *view_manager)
    : ActionView(view_manager, false),
      update_use_case(new UpdateGoalUseCase(GoalsRepository::create())),
      editor(new GoalsEditor()), view_manager(view_manager) {

  init();
}

GoalsEditView::GoalsEditView(ViewManager *view_manager, const Goal &goal)
    : ActionView(view_manager, true),
      update_use_case(new UpdateGoalUseCase(GoalsRepository::create())),
      delete_use_case(new DeleteGoalUseCase(GoalsRepository::create())),
      editor(new GoalsEditor(goal)), view_manager(view_manager) {

  init();
  setSize(goal.get_size());
  setBackgroundId(goal.get_backgroundId());
}

GoalsEditView::~GoalsEditView() {}

void GoalsEditView::onAcceptAction() {
  auto &&goal = editor->getGoal();
  goal.set_size(getSize());
  goal.set_backgroundId(getBackgroundId());

  getPresenter()->updateData(update_use_case, goal);
}

void GoalsEditView::onRemoveAction() {
  assert(delete_use_case);
  getPresenter()->updateData(delete_use_case, editor->getGoal().get_id());
}

void GoalsEditView::onUpdateSuccess() {
  view_manager->prevView();
  ActionView::onUpdateSuccess();
}

void GoalsEditView::init() { getLayout()->addWidget(editor); }

} // namespace summit
