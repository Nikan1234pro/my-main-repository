#ifndef GOALS_EDIT_VIEW_H
#define GOALS_EDIT_VIEW_H

#include <QWidget>

#include "domain/goals/delete_goal_usecase.h"
#include "domain/goals/update_goal_usecase.h"
#include "goals_editor.h"
#include "model/goals/goal.h"
#include "presentation/actions/action_view.h"
#include "presentation/common/view_manager.h"

namespace summit {

class GoalsEditView : public ActionView {
  Q_OBJECT

public:
  explicit GoalsEditView(ViewManager *view_manager);
  GoalsEditView(ViewManager *view_manager, const Goal &goal);

  ~GoalsEditView();

private:
  void onAcceptAction() override;
  void onRemoveAction() override;

  void onUpdateSuccess() override;

  void init();

  std::shared_ptr<UpdateGoalUseCase> update_use_case;
  std::shared_ptr<DeleteGoalUseCase> delete_use_case;

  GoalsEditor *editor;
  ViewManager *view_manager;
};

} // namespace summit

#endif // GOALS_EDIT_VIEW_H
