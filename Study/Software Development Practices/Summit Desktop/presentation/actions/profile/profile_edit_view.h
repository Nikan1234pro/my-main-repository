#ifndef PROFILE_EDIT_VIEW_H
#define PROFILE_EDIT_VIEW_H

#include "domain/profile/update_profile_usecase.h"
#include "model/profile/user_profile.h"
#include "presentation/actions/action_view.h"
#include "profile_editor.h"

namespace summit {

class ProfileEditView : public ActionView {

public:
  ProfileEditView(ViewManager *view_manager, const UserProfilePointer &profile);

private:
  void onAcceptAction() override;
  void onRemoveAction() override;

  void onUpdateSuccess() override;

  ProfileEditor *editor;

  std::shared_ptr<UpdateProfileUseCase> use_case;

  ViewManager *view_manager;
};

} // namespace summit

#endif // PROFILE_EDIT_VIEW_H
