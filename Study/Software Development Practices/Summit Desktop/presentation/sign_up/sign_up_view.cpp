#include "sign_up_view.h"
#include "ui_sign_up_view.h"

#include "constants/common/strings.h"
#include "domain/common/single_usecase_executor.h"
#include "presentation/sign_in/sign_in_view.h"
#include "sign_up_presenter.h"
#include "utilities/common/json/json_parser.h"

#include <QMessageBox>
#include <QPainter>
#include <QValidator>

namespace summit {

static const QString NAME_EDIT_PLACEHOLDER = QString::fromUtf8("Имя");
static const QString SURNAME_EDIT_PLACEHOLDER = QString::fromUtf8("Фамилия");
static const QString AGE_EDIT_PLACEHOLDER = QString::fromUtf8("Возраст");

static const QString USERNAME_EDIT_PLACEHOLDER =
    QString::fromUtf8("Имя пользователя");
static const QString PASSWORD_EDIT_PLACEHOLDER = QString::fromUtf8("Пароль");

SignUpView::SignUpView(ViewManager *view_manager)
    : QDialog(view_manager), view_manager(view_manager),
      ui(new Ui::SignUpView) {
  ui->setupUi(this);

  ui->name_line_edit->setPlaceholderText(NAME_EDIT_PLACEHOLDER);
  ui->surname_line_edit->setPlaceholderText(SURNAME_EDIT_PLACEHOLDER);
  ui->age_line_edit->setPlaceholderText(AGE_EDIT_PLACEHOLDER);
  ui->age_line_edit->setValidator(new QIntValidator(0, 100, this));

  ui->username_line_edit->setPlaceholderText(USERNAME_EDIT_PLACEHOLDER);

  ui->password_line_edit->setEchoMode(QLineEdit::EchoMode::Password);
  ui->password_line_edit->setPlaceholderText(PASSWORD_EDIT_PLACEHOLDER);

  /* Create presenter */
  createPresenter();
}

SignUpView::~SignUpView() { delete ui; }

void SignUpView::setBackgroundImage(const ImagePointer &background) {
  this->background = background;
}

void SignUpView::signUpSuccess() {
  QMessageBox::information(
      this, QString::fromUtf8(APPLICATION_NAME),
      QString::fromUtf8("Вы были успешно зарегистрированы!"), QMessageBox::Ok);

  SignInView *sign_in = new SignInView(view_manager);
  sign_in->setBackgroundImage(background);
  view_manager->nextView(sign_in);

  blockingAction(false);
}

void SignUpView::signUpFailure(const PresentationError &error) {
  QMessageBox::information(this, QString::fromUtf8(APPLICATION_NAME),
                           "Невозможно выполнить регистрацию\n" + error.what(),
                           QMessageBox::Ok);
  blockingAction(false);
}

void SignUpView::paintEvent(QPaintEvent *event) {
  QDialog::paintEvent(event);

  QPainter painter(this);
  if (background) {
    displayCentered(painter, background->getScaled(size()));
  }
}

void SignUpView::createPresenter() {
  presenter = new SignUpPresenter(this, SingleUseCaseExecutor::getInstance());
}

void SignUpView::blockingAction(bool block) {
  ui->sign_up_button->setDisabled(block);
}

void SignUpView::on_sign_up_button_clicked() {
  blockingAction(true);

  RegisterData register_data;

  register_data.set_name(ui->name_line_edit->text());
  register_data.set_surname(ui->surname_line_edit->text());
  register_data.set_age(ui->age_line_edit->text().toInt());
  register_data.set_username(ui->username_line_edit->text());
  register_data.set_password(ui->password_line_edit->text());
  register_data.set_email(QString("test@gmail.com"));

  presenter->signUp(register_data);
}

void SignUpView::on_cancel_button_clicked() { view_manager->prevView(); }

} // namespace summit
