#ifndef SIGN_UP_CONTRACT_H
#define SIGN_UP_CONTRACT_H

#include "model/auth/register_data.h"
#include "presentation/common/base_presenter.h"
#include "presentation/common/base_view.h"
#include "presentation/common/presentation_error.h"

namespace summit {

class SignUpContract {
public:
  class Presenter : public IBasePresenter {
  public:
    virtual void signUp(const RegisterData &register_data) = 0;
    virtual ~Presenter() = default;
  };

  class View : public IBaseView {
  public:
    virtual void signUpSuccess() = 0;
    virtual void signUpFailure(const PresentationError &) = 0;

    virtual ~View() = default;
  };
};

} // namespace summit

#endif // SIGN_UP_CONTRACT_H
