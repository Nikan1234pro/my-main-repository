#include "entry_presenter.h"

namespace summit {

EntryPresenter::EntryPresenter(
    EntryContract::View *view, const std::shared_ptr<UseCaseExecutor> &executor,
    const std::shared_ptr<LoadTokenUseCase> &use_case)
    : view(view), executor(executor), use_case(use_case) {

  connectView();
  connectUseCase();
}

void EntryPresenter::tryAuthorize() {

  /* Execute */
  executor->execute(use_case);
}

void EntryPresenter::connectView() {
  QObject *view_obj = dynamic_cast<QObject *>(view);
  connect(this, SIGNAL(autohorizeCompletedTrigger()), view_obj,
          SLOT(autohorizeCompleted()));
  connect(this, SIGNAL(authorizeRejectedTrigger()), view_obj,
          SLOT(authorizeRejected()));
  setParent(view_obj);
}

void EntryPresenter::connectUseCase() {
  connect(use_case.get(), &LoadTokenUseCase::finished, this,
          [this](const LoadTokenUseCase::ResponseType &token) {
            emit autohorizeCompletedTrigger();
          });

  connect(use_case.get(), &LoadTokenUseCase::failed, this,
          [this](const DomainError &) { emit authorizeRejectedTrigger(); });
}

} // namespace summit
