#ifndef MAIN_CONTRACT_H
#define MAIN_CONTRACT_H

#include "presentation/common/base_presenter.h"
#include "presentation/common/base_view.h"

#include <QObject>

namespace summit {

class EntryContract {
public:
  class Presenter : public IBasePresenter {
  public:
    virtual void tryAuthorize() = 0;
  };

  class View : public IBaseView {
    virtual void autohorizeCompleted() = 0;
    virtual void authorizeRejected() = 0;
  };
};
} // namespace summit

#endif // MAIN_CONTRACT_H
