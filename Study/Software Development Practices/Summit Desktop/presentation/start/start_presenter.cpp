#include "start_presenter.h"

#include <QDialog>
#include <QPainter>
#include <QThread>
#include <QtDebug>
#include <mutex>

#include "domain/common/single_usecase_executor.h"

#include <QFile>

inline void fillData(summit::StartPresenter &images) {
  using namespace summit;

  QFile fileIn(":/resource/start_window/sportsman_1.jpeg");
  fileIn.open(QFile::ReadOnly);
  QByteArray data = fileIn.readAll();
  images.addQuote(
      {ImagePointer(new ImageWrapper(data)),
       "Однажды Эрнест Хемингуэй поспорил, что напишет самый короткий "
       "рассказ, способный растрогать любого"});

  QFile fileIn1(":/resource/start_window/sportsman_2.jpg");
  fileIn1.open(QFile::ReadOnly);
  QByteArray data1 = fileIn1.readAll();
  images.addQuote(
      {ImagePointer(new ImageWrapper(data1)), "Ю ВОННА ПЛЕЙ? ЛЕТС ПЛЕЙ!"});
}

/*
 *
 *
 *
 *
 *
 *
 *
 *
 */

namespace summit {

StartPresenter::StartPresenter(StartContract::View *view,
                               const std::shared_ptr<UseCaseExecutor> &executor)
    : QObject(dynamic_cast<QObject *>(view)), mutex(QMutex::Recursive),
      view(view), executor(executor) {
  /* Remove later */
  fillData(*this);
  ready();
}

void StartPresenter::ready() {
  std::lock_guard<QMutex> lock(mutex);
  current_quote = quotes.start();

  /* Display first*/
  view->display(*current_quote);
}

void StartPresenter::addQuote(const Quote &quote) {
  quotes.emplace_back(QuotePointer::create(quote));
}

void StartPresenter::transitionLeft() {
  std::lock_guard<QMutex> lock(mutex);

  /* Already running */
  if (isBusy()) {
    return;
  }

  auto &&current = (*current_quote);
  auto &&previous = (*--current_quote);
  runTransition(current, previous);
}

void StartPresenter::transitionRight() {
  std::lock_guard<QMutex> lock(mutex);

  /* Already running */
  if (isBusy()) {
    return;
  }

  auto &&current = (*current_quote);
  auto &&next = (*++current_quote);
  runTransition(current, next);
}

void StartPresenter::runTransition(const QuotePointer &from,
                                   const QuotePointer &to) {
  std::lock_guard<QMutex> lock(mutex);
  use_case.reset(new QuoteTransitionUseCase(from, to));

  /* Subscribe on updates */
  connect(use_case.get(), &QuoteTransitionUseCase::requestUpdate, this,
          [this](double opacity) {
            this->view->display(use_case->getFrom(), use_case->getTo(),
                                opacity);
          });

  /* On finish cleanup */
  connect(use_case.get(), &QuoteTransitionUseCase::finished, this, [this]() {
    std::lock_guard<QMutex> lock(mutex);
    this->use_case->terminate();
    this->view->display(use_case->getTo());
  });

  /* Execute */
  executor->execute(use_case);
}

bool StartPresenter::isBusy() const {
  if (use_case && use_case->isValid()) {
    return true;
  }
  return false;
}

StartPresenter::~StartPresenter() {}
} // namespace summit
