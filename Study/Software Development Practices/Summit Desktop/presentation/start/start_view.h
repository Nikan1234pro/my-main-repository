#ifndef HELLOWINDOW_H
#define HELLOWINDOW_H

#include "presentation/common/base_view.h"
#include "presentation/common/label/transparent_label.h"
#include "presentation/common/view_manager.h"
#include "presentation/start/start_contract.h"

#include <QColorDialog>
#include <QDebug>
#include <QDialog>
#include <QLabel>
#include <QMutex>
#include <QPainter>
#include <QPixmap>
#include <QThread>

namespace Ui {
class StartView;
}

namespace summit {

class StartView : public QDialog, public StartContract::View {
  Q_OBJECT

  using QuotePointer = QSharedPointer<const Quote>;

public:
  explicit StartView(ViewManager *view_manager);
  ~StartView();

  struct QuoteData {
    QuotePointer from;
    QuotePointer to;
    double opacity{};
  };

private slots:
  void on_rotate_left_clicked();
  void on_rotate_right_clicked();

  void on_sign_up_button_clicked();
  void on_sign_in_button_clicked();

public slots:
  void display(const QuotePointer &from, const QuotePointer &to,
               double opacity) override;
  void display(const QuotePointer &quote) override;

protected:
  void resizeEvent(QResizeEvent *) override;
  void paintEvent(QPaintEvent *) override;

private:
  virtual void createPresenter() override;

  StartContract::Presenter *presenter;
  ViewManager *view_manager;

  QuoteData quote_data;
  TransparentLabel *quote_label;

  QMutex mutex;
  static constexpr int IMAGE_BUF_SIZE = 2;
  ImageWrapper image_buffer[IMAGE_BUF_SIZE];

  Ui::StartView *ui;
};

} // namespace summit

#endif // HELLOWINDOW_H
