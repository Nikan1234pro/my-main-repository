#ifndef IMAGESUBSCRIBERSHOLDER_H
#define IMAGESUBSCRIBERSHOLDER_H

#include "image_subscriber.h"
#include "model/images/image.h"

#include <unordered_map>

namespace summit {

class ImageSubscribersHolder {
  std::unordered_multimap<int, ImageSubscriber *> subscribers;

public:
  ImageSubscribersHolder();

  void addSubscriber(int key, ImageSubscriber *widget);
  void setImage(const ImagePointer &image);

  std::vector<int> getKeys() const;
};

} // namespace summit
#endif // IMAGESUBSCRIBERSHOLDER_H
