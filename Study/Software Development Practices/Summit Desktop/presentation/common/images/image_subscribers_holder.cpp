#include "image_subscribers_holder.h"

namespace summit {

ImageSubscribersHolder::ImageSubscribersHolder() {}

void ImageSubscribersHolder::addSubscriber(int key, ImageSubscriber *widget) {
  subscribers.insert({key, widget});
}

void ImageSubscribersHolder::setImage(const ImagePointer &image) {
  auto range = subscribers.equal_range(image->getImageId());
  for (auto it = range.first; it != range.second; ++it) {
    it->second->setImage(image);
  }
  /* Remove after setting image */
  subscribers.erase(range.first, range.second);
}

std::vector<int> ImageSubscribersHolder::getKeys() const {
  std::vector<int> keys;
  for (auto it = subscribers.begin(), end = subscribers.end(); it != end;
       it = subscribers.equal_range(it->first).second) {
    keys.push_back(it->first);
  }
  return keys;
}

} // namespace summit
