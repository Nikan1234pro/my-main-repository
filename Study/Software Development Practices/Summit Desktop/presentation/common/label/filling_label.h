#ifndef TEXTRESIZABLELABEL_H
#define TEXTRESIZABLELABEL_H

#include <QEvent>
#include <QLabel>
#include <QWidget>

#include "utilities/common/nonstd/optional.h"

namespace summit {

class TextFillingLabel : public QLabel {
  Q_OBJECT
  Q_PROPERTY(QString text READ text WRITE setText)

  int id;
  static int counter;

  QString content;

public:
  explicit TextFillingLabel(QWidget *parent = nullptr);
  explicit TextFillingLabel(const QString &text, QWidget *parent = nullptr);

  void setText(const QString &text) {
    this->content = text;
    update();
  }

  const QString &text() const { return content; }

protected:
  // virtual QSize sizeHint() const override;
  virtual void paintEvent(QPaintEvent *event) override;
  virtual void resizeEvent(QResizeEvent *event) override;
};

class ScaleTextlabel : public QLabel {
  Q_OBJECT
  Q_DISABLE_COPY(ScaleTextlabel)

  int id;
  static int counter;

public:
  explicit ScaleTextlabel(QWidget *parent = Q_NULLPTR,
                          Qt::WindowFlags f = Qt::WindowFlags());
  explicit ScaleTextlabel(const QString &text, QWidget *parent = Q_NULLPTR,
                          Qt::WindowFlags f = Qt::WindowFlags());

protected:
  void resizeEvent(QResizeEvent *event) override;
protected slots:
  void resizeFont();

private:
  void initTimer();

  nonstd::optional<QSize> DEFAULT_SIZE;
  nonstd::optional<double> fontSize;
  QTimer *m_resizeTimer;
};

ScaleTextlabel *create(QString text);

} // namespace summit

#endif // TEXTRESIZABLELABEL_H
