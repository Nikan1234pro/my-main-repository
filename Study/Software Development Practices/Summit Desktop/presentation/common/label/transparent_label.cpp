#include "transparent_label.h"

#include "presentation/common/font/font.h"
#include <QPainter>

namespace summit {

TransparentLabel::TransparentLabel(QWidget *parent) : QLabel(parent) {
  setWordWrap(true);
}

void TransparentLabel::setOpacity(qreal opacity) { this->opacity = opacity; }

qreal TransparentLabel::getOpacity() const { return opacity; }

void TransparentLabel::paintEvent(QPaintEvent *) {
  QPainter p(this);
  p.setOpacity(opacity);
  p.setFont(this->font());

  /* Paint text */
  if (!text().isEmpty()) {
    p.drawText(rect(), Qt::AlignCenter | Qt::TextWordWrap, text());
  }
}

} // namespace summit
