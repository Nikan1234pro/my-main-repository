#include "size_fixer.h"

namespace summit {

static constexpr double ASPECT_RATIO_DEFAULT = 1.0;

SizeFixer::SizeFixer(QWidget *parent)
    : QWidget(parent), wrapper(nullptr), aspect_ratio(ASPECT_RATIO_DEFAULT) {}

void SizeFixer::setWidget(QWidget *widget) {
  if ((this->wrapper = widget)) {
    setParent(widget->parentWidget());
    widget->setParent(this);
  }
}

QWidget *SizeFixer::getWidget() { return wrapper; }

QSize SizeFixer::sizeHint() const {
  return (wrapper ? wrapper->sizeHint() : QWidget::sizeHint());
}

void SizeFixer::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
  fixSize();
}

SizeFixer::~SizeFixer() = default;

void SizeFixer::setAspectRatio(double aspect_ratio) {
  this->aspect_ratio = aspect_ratio;
}

double SizeFixer::getAspectRatio() const { return aspect_ratio; }

WidthForHeightFixer::WidthForHeightFixer(QWidget *parent) : SizeFixer(parent) {}

void WidthForHeightFixer::fixSize() {
  auto widget = getWidget();
  if (widget) {
    QRect r(QPoint(),
            QSize(static_cast<int>(getAspectRatio() * height()), height()));
    r.moveCenter(rect().center());
    widget->setGeometry(r);
  }
}

HeightForWidthFixer::HeightForWidthFixer(QWidget *parent) : SizeFixer(parent) {}

void HeightForWidthFixer::fixSize() {
  auto widget = getWidget();
  if (widget) {
    QRect r(QPoint(),
            QSize(width(), static_cast<int>(width() * getAspectRatio())));
    r.moveCenter(rect().center());
    widget->setGeometry(r);
  }
}

QWidget *heigthForWidth(QWidget *widget, double ratio) {
  auto fixer = new HeightForWidthFixer(nullptr);
  fixer->setWidget(widget);
  fixer->setAspectRatio(ratio);
  return fixer;
}

QWidget *widthForHeight(QWidget *widget, double ratio) {
  auto fixer = new WidthForHeightFixer(nullptr);
  fixer->setWidget(widget);
  fixer->setAspectRatio(ratio);
  return fixer;
}

} // namespace summit
