#ifndef BASE_VIEW_H
#define BASE_VIEW_H

class IBaseView {
public:
  virtual ~IBaseView() = default;

protected:
  virtual void createPresenter() = 0;
};

#endif // BASE_VIEW_H
