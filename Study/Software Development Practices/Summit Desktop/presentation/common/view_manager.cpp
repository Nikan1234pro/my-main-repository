#include "view_manager.h"

#include <QHBoxLayout>

ViewManager::ViewManager(QWidget *parent)
    : QWidget(parent), layout(new QHBoxLayout),
      manager(new QStackedWidget(parent)) {
  layout->setMargin(0);
  layout->addWidget(manager);
}

void ViewManager::nextView(QWidget *scene, bool do_deallocation) {
  widgets_data.push({scene, do_deallocation});

  manager->addWidget(scene);
  manager->setCurrentIndex(static_cast<int>(widgets_data.size() - 1));
  manager->show();
}

void ViewManager::update() { manager->show(); }

void ViewManager::prevView() {
  auto widget_data = widgets_data.top();
  widgets_data.pop();

  manager->removeWidget(widget_data.widget);
  if (widget_data.do_deallocation) {
    widget_data.widget->deleteLater();
  }
}

void ViewManager::removeWidgets() {
  while (!widgets_data.empty()) {
    auto widget_data = widgets_data.top();
    widgets_data.pop();

    manager->removeWidget(widget_data.widget);
    if (widget_data.do_deallocation) {
      widget_data.widget->deleteLater();
    }
  }
}

QLayout *ViewManager::getLayout() { return layout; }
