#include "rounded_button.h"

#include <QDebug>
#include <QPainter>

namespace summit {

RoundedButton::RoundedButton(QWidget *parent) : QPushButton(parent) {
  QSizePolicy policy(QSizePolicy::Fixed, QSizePolicy::Preferred);
  setSizePolicy(policy);
}

QFont RoundedButton::getScaledFont() const {
  double ratio = double(height()) / fontMetrics().boundingRect(text()).height();
  auto font = this->font();
  font.setPointSizeF(font.pointSizeF() * ratio);
  return font;
}

void RoundedButton::resizeEvent(QResizeEvent *) {
  if (text().isEmpty()) {
    return;
  }

  int h = height();
  int r = h / 2;

  int width = this->width();
  int new_width =
      QFontMetrics(getScaledFont()).boundingRect(text()).width() + 2 * r;

  int x = this->x() - (new_width - width);
  setGeometry(x, y(), new_width, h);
}

void RoundedButton::paintEvent(QPaintEvent *) {
  QPainter painter(this);

  painter.setPen(Qt::NoPen);
  painter.setBrush(palette().base());
  painter.setRenderHint(QPainter::Antialiasing);

  int w = width();
  int h = height();
  int r = h / 2;

  painter.drawRoundedRect(0, 0, w, h, r, r);

  /* Paint text */
  if (!text().isEmpty()) {
    painter.setFont(getScaledFont());
    painter.setPen(Qt::black);
    painter.drawText(rect(), Qt::AlignCenter | Qt::TextWordWrap, text());
  }

  /* Paint icon */
  auto icon = this->icon();
  if (!icon.isNull()) {
    QRect rect;
    rect.setWidth(h - r);
    rect.setHeight(h - r);
    rect.moveCenter(this->rect().center());

    icon.paint(&painter, rect);
  }
}
} // namespace summit
