#include "font.h"

#include <QFontDatabase>

namespace summit {

void loadFonts() {
  QFontDatabase::addApplicationFont(DEFAULT_HEADER_FONT_PATH);
}

} // namespace summit
