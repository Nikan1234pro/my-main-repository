#ifndef WIDGET_H
#define WIDGET_H

#include <QLabel>
#include <QString>
#include <QVBoxLayout>

#include "constants/presentation/widget_constants.h"
#include "presentation/common/images/image_subscriber.h"
#include "utilities/common/nonstd/optional.h"
#include "widget_item.h"

namespace summit {

class Widget : public QFrame, public ImageSubscriber {
  Q_OBJECT

  WidgetType type;

  /* Widget geometry */
  QRectF widget_bounds;
  QVBoxLayout *widget_layout;
  QVBoxLayout *items_layout;

  qreal default_border_radius;
  qreal current_border_radius;

  /* Header */
  ElidedLabel *header;
  QFont header_font;
  static constexpr qreal HEADER_PERCENTS = 20.0;

  /* Background image and borders */
  QPixmap background;
  ImagePointer background_wrapper;

  static const QColor DIMMING_COLOR;
  static constexpr qreal BACKGROUND_ALPHA = 0.6;
  static constexpr qreal EXPANSION_DELTA = 2.0;

  static constexpr qreal BORDER_WIDTH = 10.0;
  static constexpr qreal BORDER_SHIFT = 2.0;
  static constexpr qreal MARGIN_RATIO = 1.5;

  /* Style */
  static const QString WIDGET_DEFAULT_STYLE;

public:
  explicit Widget(WidgetType type, const ImagePointer &background_wrapper,
                  qreal border_radius, const QString &header_text,
                  QWidget *parent = nullptr);

  void addItem(WidgetItem *item);
  void setHeaderFont(const QFont &font);

  void setImage(const ImagePointer &image) override;
  const ImagePointer &getImage() const;

  WidgetType getType() const { return type; }

protected:
  void showEvent(QShowEvent *) override;
  void resizeEvent(QResizeEvent *event) override;
  void paintEvent(QPaintEvent *event) override;

  void mousePressEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;

private:
  void expandBounds(qreal delta);
  void resizeHeader(qreal ratio);
  QPixmap updateBackgroundPixmap();

signals:
  void clicked();
  void imageChanged();
  void expandChildren(qreal ratio);
};

QWidget *squareWidget(QWidget *widget);
QWidget *horizontalWidget(QWidget *widget);

} // namespace summit

#endif // WIDGET_H
