#include "widget_utilities.h"

namespace summit {

nonstd::optional<QSize> widgetSizeFromString(const QString &string) {
  static const std::regex rx("([0-9])x([0-9])");

  static constexpr std::size_t MATCH_SIZE = 3;
  static constexpr std::size_t FIRST_NUMBER = 1;
  static constexpr std::size_t SECOND_NUMBER = 2;

  std::string input = string.toStdString();
  std::smatch match;

  if (std::regex_match(input, match, rx)) {
    if (match.size() != MATCH_SIZE) {
      return nonstd::nullopt;
    }
    return QSize{std::stoi(match[FIRST_NUMBER]),
                 std::stoi(match[SECOND_NUMBER])};
  }
  return nonstd::nullopt;
}

WidgetType computeWidgetType(const QSize &size) {
  static constexpr int SMALL = 1;
  static constexpr int LARGE = 2;

  if (size.width() == SMALL && size.height() == SMALL) {
    return WidgetType::SMALL;
  } else if (size.width() == LARGE && size.height() == LARGE) {
    return WidgetType::LARGE;
  } else if (size.width() == LARGE && size.height() == SMALL) {
    return WidgetType::HORIZONTAL;
  }
  /* If we have other size - return LARGE */
  return WidgetType::LARGE;
}

QWidget *wrapWidget(WidgetType type, Widget *widget) {
  if (type == WidgetType::HORIZONTAL) {
    return horizontalWidget(widget);
  }
  return squareWidget(widget);
}

} // namespace summit
