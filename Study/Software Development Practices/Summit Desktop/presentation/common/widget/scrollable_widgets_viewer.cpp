#include "scrollable_widgets_viewer.h"
#include "constants/presentation/styles.h"

#include <QDebug>
#include <QResizeEvent>
#include <QScrollBar>
#include <QSpacerItem>
#include <QTimer>
#include <QWheelEvent>

#include <iostream>

namespace summit {

namespace {

struct WidgetPosition {
  int row;
  int column;

  int rowspan;
  int colspan;
};

WidgetPosition gridPosition(QWidget *widget) {
  if (!widget->parentWidget())
    return {};
  auto layout = qobject_cast<QGridLayout *>(widget->parentWidget()->layout());
  if (!layout)
    return {};
  int index = layout->indexOf(widget);
  Q_ASSERT(index >= 0);

  WidgetPosition pos;
  layout->getItemPosition(index, &pos.row, &pos.column, &pos.rowspan,
                          &pos.colspan);
  return pos;
}

QPoint findPosition(QGridLayout *layout, int height, int width) {
  static constexpr int MAX_ROWS = 2;

  assert(MAX_ROWS >= layout->rowCount());

  std::vector<std::vector<char>> occupied(MAX_ROWS);

  for (std::size_t i = 0; i < occupied.size(); ++i) {
    occupied[i].resize(layout->columnCount(), 0);
  }

  /* Iterate over widgets */
  for (int i = 0; i < layout->count(); ++i) {
    auto *w = layout->itemAt(i)->widget();
    if (w) {
      auto result = gridPosition(w);
      for (int x = 0; x < result.rowspan; ++x) {
        for (int y = 0; y < result.colspan; ++y) {
          occupied.at(result.row + x).at(result.column + y) = 1;
        }
      }
    }
  }

  auto NX = layout->rowCount();
  auto NY = layout->columnCount();

  for (int x = 0; x < NX; ++x) {
    for (int y = 0; y < NY; ++y) {
      /* Check i, j */
      bool ok = true;

      for (int u = 0; u < height; ++u) {
        if (x + u >= NX) {
          ok = false;
          break;
        }
        for (int v = 0; v < width; ++v) {
          if (y + v < NY) {
            ok &= !occupied[x + u][y + v];
          }
        }
      }
      /* Do action */
      if (ok) {
        return {x, y};
      }
    }
  }
  if (layout->count() == 0) {
    return {0, 0};
  }
  return {0, NY};
}

} // namespace

#define STRETCH 1

ScrollableWidgetsViewer::ScrollableWidgetsViewer(int row_count, QWidget *parent)
    : QScrollArea(parent), frame(new QFrame), layout(new QGridLayout) {
  frame->setStyleSheet(FRAME_STYLE);
  frame->setLayout(layout);
  setWidget(frame);

  setStyleSheet(SCROLL_AREA_STYLE);
  horizontalScrollBar()->setStyleSheet(INVISIBLE_SCROLLBAR_STYLE);

  /* Add invisible widget to forse row count */
  auto dummy = new QWidget;
  dummy->setFixedWidth(0);
  addWidget(dummy, 0, 0, row_count, 1);
}

void ScrollableWidgetsViewer::addWidgetAuto(QWidget *widget,
                                            Qt::Alignment alignment) {
  QPoint p = findPosition(layout, 1, 1);
  int row = p.x();
  int column = p.y();
  addWidget(widget, row, column, alignment);
}

void ScrollableWidgetsViewer::addWidgetAuto(QWidget *widget, int rowSpan,
                                            int columnSpan,
                                            Qt::Alignment alignment) {
  QPoint p = findPosition(layout, rowSpan, columnSpan);
  int row = p.x();
  int column = p.y();
  addWidget(widget, row, column, rowSpan, columnSpan, alignment);
}

void ScrollableWidgetsViewer::addWidget(QWidget *widget, int row, int column,
                                        Qt::Alignment alignment) {

  updateStretch(row, row + 1, column, column + 1);
  layout->addWidget(widget, row, column, alignment);
  updateSize();
}

void ScrollableWidgetsViewer::addWidget(QWidget *widget, int row, int column,
                                        int rowSpan, int columnSpan,
                                        Qt::Alignment alignment) {
  updateStretch(row, row + rowSpan, column, column + columnSpan);
  layout->addWidget(widget, row, column, rowSpan, columnSpan, alignment);
  updateSize();
}

void ScrollableWidgetsViewer::setPosition(int position) {
  horizontalScrollBar()->setValue(position);
}

int ScrollableWidgetsViewer::getPosition() const {
  return horizontalScrollBar()->value();
}

void ScrollableWidgetsViewer::clear() {
  QLayoutItem *child;
  while ((child = layout->takeAt(0)) != 0) {
    if (auto widget = child->widget()) {
      widget->deleteLater();
    }
    delete child;
  }
}

void ScrollableWidgetsViewer::resizeEvent(QResizeEvent *event) {
  QScrollArea::resizeEvent(event);
  updateSize();
}

void ScrollableWidgetsViewer::wheelEvent(QWheelEvent *event) {
  horizontalScrollBar()->event(event);
}

void ScrollableWidgetsViewer::updateStretch(int row_begin, int row_end,
                                            int column_begin, int column_end) {
  for (int row = row_begin; row < row_end; ++row) {
    layout->setRowStretch(row, STRETCH);
  }
  for (int column = column_begin; column < column_end; ++column) {
    layout->setColumnStretch(column, STRETCH);
  }
}

void ScrollableWidgetsViewer::updateSize() {
  qreal width_ratio = 0.0;
  qreal frame_height = height() - HEIGHT_MARGIN;

  int row_count = layout->rowCount();
  if (row_count > 0) {
    width_ratio = frame_height / row_count;
  }
  QSize size{static_cast<int>((layout->columnCount() - 1) * width_ratio),
             static_cast<int>(frame_height)};

  /* Do alignment */
  int avaliable_space = width() - size.width();
  int frame_margin = 0;
  if (avaliable_space > 0) {
    frame_margin = avaliable_space / 2;
  }
  frame->setContentsMargins(frame_margin, 0, frame_margin, 0);
  frame->resize(size.width() + 2 * frame_margin, size.height());

  /* TODO: FIX MARGIN */
#pragma message("DO MARGIN")
  return;
  /* Do widget margin */
  auto rect = std::min(width(), height());
  for (int i = 0; i < layout->count(); ++i) {
    auto *widget = layout->itemAt(i)->widget();
    if (widget) {
      auto widget_layout = widget->layout();
      if (widget_layout) {
        widget_layout->setMargin(static_cast<int>(rect / WIDGET_MARGIN_RATIO));
      }
    }
  }
}

} // namespace summit
