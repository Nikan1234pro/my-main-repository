#ifndef WIDGETITEM_H
#define WIDGETITEM_H

#include <QHBoxLayout>
#include <QLabel>
#include <cmath>
#include <mutex>

#include "presentation/common/label/elided_label.h"

#define INVISIBILITY_INTERVAL 300

namespace summit {

class WidgetItem : public QFrame {
  Q_OBJECT

  /* Stripe */
  QLabel *stripe;
  QColor stripe_color = Qt::transparent;
  static const QString STRIPE_STYLE;
  static constexpr qreal STRIPE_WIDTH = 5;

  /* Body */
  ElidedLabel *body;
  QFont body_default_font;

  /* Layout */
  QFrame *content_frame;
  QVBoxLayout *content_layout;
  static constexpr int CONTENT_SPACING = 10;

public:
  explicit WidgetItem(const QString &body_text, QWidget *parent = nullptr);
  virtual ~WidgetItem();

  void setBodyDefaultFont(const QFont &font);
  const QFont &getBodyFont() const;

  void setBodyText(const QString &text);
  QString getBodyText() const;

  void setBodyTextColor(const QColor &color);
  const QColor &getBodyTextColor() const;

  void setStripeColor(const QColor &color);
  const QColor &getStripeColor();

  QBoxLayout *getLayout() const;

public slots:
  virtual void onExpansion(qreal ratio);

protected:
  QLabel *getBody() const;

  void paintEvent(QPaintEvent *) override;
  void showEvent(QShowEvent *) override;
};

class TitledWidgetItem : public WidgetItem {
  Q_OBJECT

  ElidedLabel *title;
  QFont title_default_font;

public:
  TitledWidgetItem(const QString &title_text, const QString &body_text,
                   QWidget *parent = nullptr);

  void isDisplayBody(bool value);
  bool isDisplayBody() const;

  void setTitleDefaultFont(const QFont &font);
  const QFont &getTitleFont() const;

  void setTitleText(const QString &text);
  QString getTitleText() const;

  void setTitleTextColor(const QColor &color);

public slots:
  virtual void onExpansion(qreal ratio) override;
};

inline QFont expandFont(QFont font, qreal ratio) {
  static constexpr qreal exp = 10;

  ratio = std::trunc(ratio * exp) / exp;
  font.setPointSizeF(font.pointSizeF() * ratio);
  return font;
}

} // namespace summit

#endif // WIDGETITEM_H
