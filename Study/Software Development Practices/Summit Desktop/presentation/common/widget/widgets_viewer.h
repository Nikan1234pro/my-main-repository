#ifndef WIDGETS_VIEWER_H
#define WIDGETS_VIEWER_H

#include <QWidget>

namespace summit {

class WidgetsViewer {
public:
  virtual void addWidgetAuto(QWidget *widget,
                             Qt::Alignment alignment = Qt::Alignment()) = 0;

  virtual void addWidgetAuto(QWidget *widget, int rowSpan, int columnSpan,
                             Qt::Alignment alignment = Qt::Alignment()) = 0;

  virtual void addWidget(QWidget *widget, int row, int column,
                         Qt::Alignment alignment = Qt::Alignment()) = 0;
  virtual void addWidget(QWidget *widget, int row, int column, int rowSpan,
                         int columnSpan,
                         Qt::Alignment alignment = Qt::Alignment()) = 0;
  virtual void setPosition(int position) = 0;
  virtual int getPosition() const = 0;
  virtual void clear() = 0;

  virtual ~WidgetsViewer() = default;
};

} // namespace summit

#endif // WIDGETS_VIEWER_H
