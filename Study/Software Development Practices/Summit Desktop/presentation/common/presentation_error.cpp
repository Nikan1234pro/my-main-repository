#include "presentation_error.h"

PresentationError::PresentationError() {}

PresentationError::PresentationError(const QString &message)
    : message(message) {}

const QString &PresentationError::what() const { return message; }
