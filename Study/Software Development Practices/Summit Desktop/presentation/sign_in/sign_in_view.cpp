#include "sign_in_view.h"
#include "ui_sign_in_view.h"

#include "constants/common/strings.h"
#include "domain/common/single_usecase_executor.h"
#include "presentation/menu/menu_view.h"
#include "sign_in_presenter.h"

#include <QDebug>
#include <QMessageBox>
#include <QPainter>

namespace summit {

SignInView::SignInView(ViewManager *view_manager)
    : QDialog(view_manager), view_manager(view_manager),
      ui(new Ui::SignInView) {
  ui->setupUi(this);

  ui->username_line_edit->setPlaceholderText(QString::fromUtf8("Логин..."));

  ui->password_line_edit->setEchoMode(QLineEdit::EchoMode::Password);
  ui->password_line_edit->setPlaceholderText(QString::fromUtf8("Пароль..."));

  /* Create presenter */
  createPresenter();
}

void SignInView::createPresenter() {
  presenter = new SignInPresenter(this, SingleUseCaseExecutor::getInstance());
}

void SignInView::blockingAction(bool block) {
  ui->sign_in_button->setDisabled(block);
}

void SignInView::on_sign_in_button_clicked() {
  blockingAction(true);

  QString username = ui->username_line_edit->text();
  QString password = ui->password_line_edit->text();

  AuthenticationData auth_data;
  auth_data.set_username(username);
  auth_data.set_password(password);

  presenter->signIn(auth_data);
}

void SignInView::signInSuccess() {
  view_manager->nextView(new MenuView(view_manager));
  blockingAction(false);
}

void SignInView::signInFailure(const PresentationError &error) {
  QMessageBox::information(this, QString::fromUtf8(APPLICATION_NAME),
                           "Невозможно выполнить авторизацию\n" + error.what(),
                           QMessageBox::Ok);
  blockingAction(false);
}

void SignInView::paintEvent(QPaintEvent *event) {
  QDialog::paintEvent(event);

  QPainter painter(this);
  if (background) {
    displayCentered(painter, background->getScaled(size()));
  }
}

SignInView::~SignInView() { delete ui; }

void SignInView::setBackgroundImage(const ImagePointer &background) {
  this->background = background;
}

} // namespace summit

void summit::SignInView::on_pushButton_clicked() { view_manager->prevView(); }
