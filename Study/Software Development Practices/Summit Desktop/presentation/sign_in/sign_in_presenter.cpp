#include "sign_in_presenter.h"

#include "data/auth/remote_auth_service.h"

namespace summit {

SignInPresenter::SignInPresenter(
    SignInContract::View *view,
    const std::shared_ptr<UseCaseExecutor> &executor)
    : view(view), executor(executor),
      use_case(new SignInUseCase(RemoteAuthService::getInstance())) {

  connectView();
  connectUseCase();
}

void SignInPresenter::connectView() {
  /* Connect view */
  QObject *view_obj = dynamic_cast<QObject *>(view);
  connect(this, SIGNAL(signInSuccessTrigger()), view_obj,
          SLOT(signInSuccess()));
  connect(this, SIGNAL(signInFailureTrigger(const PresentationError &)),
          view_obj, SLOT(signInFailure(const PresentationError &)));
  setParent(view_obj);
}

void SignInPresenter::connectUseCase() {
  /* Connect use case */
  connect(use_case.get(), &SignInUseCase::finished, this,
          [this](const SecurityManager::AuthorizationToken &token) {
            /* Save token */
            SecurityManager::getInstance().setToken(token);

            emit signInSuccessTrigger();
          });

  connect(use_case.get(), &SignInUseCase::failed, this,
          [this](const DomainError &error) {
            emit signInFailureTrigger(PresentationError{error.getMessage()});
          });
}

void SignInPresenter::signIn(const AuthenticationData &auth_data) {
  executor->execute(use_case, auth_data);
}

} // namespace summit
