#ifndef SIGN_IN_CONTRACT_H
#define SIGN_IN_CONTRACT_H

#include "model/auth/authentication_data.h"
#include "presentation/common/base_presenter.h"
#include "presentation/common/base_view.h"
#include "presentation/common/presentation_error.h"

namespace summit {

class SignInContract {
public:
  class Presenter : public IBasePresenter {
  public:
    virtual void signIn(const AuthenticationData &auth_data) = 0;
    virtual ~Presenter() = default;
  };

  class View : public IBaseView {
  public:
    virtual void signInSuccess() = 0;
    virtual void signInFailure(const PresentationError &error) = 0;

    virtual ~View() = default;
  };
};

} // namespace summit

#endif // SIGN_IN_CONTRACT_H
