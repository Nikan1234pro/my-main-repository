#ifndef SIGNINPRESENTER_H
#define SIGNINPRESENTER_H

#include "domain/auth/sign_in_usecase.h"
#include "domain/common/usecase_executor.h"
#include "sign_in_contract.h"

namespace summit {

class SignInPresenter : public QObject, public SignInContract::Presenter {
  Q_OBJECT
public:
  SignInPresenter(SignInContract::View *view,
                  const std::shared_ptr<UseCaseExecutor> &executor);

  void signIn(const AuthenticationData &auth_data) override;

signals:
  void signInSuccessTrigger();
  void signInFailureTrigger(const PresentationError &error);

private:
  void connectView();
  void connectUseCase();

  SignInContract::View *view;
  std::shared_ptr<UseCaseExecutor> executor;
  std::shared_ptr<SignInUseCase> use_case;
};

} // namespace summit

#endif // SIGNINPRESENTER_H
