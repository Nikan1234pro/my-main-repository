#include "menu_view.h"

#include "presentation/boards/goals/goals_view.h"
#include "presentation/boards/ideas/ideas_view.h"
#include "presentation/boards/nutrition/nutrition_view.h"
#include "presentation/boards/profile/profile_view.h"
#include "presentation/boards/trainings/trainings_view.h"

#include <QPainter>

namespace summit {

MenuView::MenuView(ViewManager *view_manager)
    : QDialog(view_manager), view_manager(view_manager),
      menu_manager(new ViewManager(this)), menu_bar(new MenuBar),
      ui(new Ui::MenuView) {
  ui->setupUi(this);

  PresentationSettings::getInstance().setListener(this);

  auto ICONS = initIcons();
  auto on_start = addMenuItem<ProfileView>(ProfileView::TITLE, ICONS[Profile]);
  addMenuItem<NutritionView>(NutritionView::TITLE, ICONS[Nutrition]);
  addMenuItem<GoalsView>(GoalsView::TITLE, ICONS[Goals]);
  addMenuItem<IdeasView>(IdeasView::TITLE, ICONS[Ideas]);
  addMenuItem<TrainingsView>(TrainingsView::TITLE, ICONS[Trainings]);

  ui->menu_bar_placeholder->addWidget(
      widthForHeight(menu_bar, MENU_BAR_ASPECT_RATIO));
  ui->central_widget->setLayout(menu_manager->getLayout());
  on_start();
}

MenuView::~MenuView() { delete ui; }

void MenuView::onSettingsChanged() {
  auto &settings = PresentationSettings::getInstance();
  items_buffer[Profile].second = settings.getImage<ProfileView>();
  items_buffer[Nutrition].second = settings.getImage<NutritionView>();
  items_buffer[Goals].second = settings.getImage<GoalsView>();
  items_buffer[Ideas].second = settings.getImage<IdeasView>();
  items_buffer[Trainings].second = settings.getImage<IdeasView>();

  repaint();
}

void MenuView::paintEvent(QPaintEvent *event) {
  QPainter painter(this);

  if (isValidIndex(current_item)) {
    auto image_ptr = items_buffer[current_item].second;
    if (image_ptr) {
      displayCentered(painter, size(), *image_ptr);
    }
  }

  QDialog::paintEvent(event);
}

bool MenuView::isValidIndex(std::size_t index) {
  return (index < items_buffer.size());
}

void MenuView::on_action_button_clicked() {
  if (isValidIndex(current_item)) {
    items_buffer[current_item].first->onAddAction();
  }
}

} // namespace summit
