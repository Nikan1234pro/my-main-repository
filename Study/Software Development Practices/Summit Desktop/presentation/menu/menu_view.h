#ifndef MENU_SCREEN_H
#define MENU_SCREEN_H

#include "constants/presentation/styles.h"
#include "presentation/boards/board_view.h"
#include "presentation/common/base_view.h"
#include "presentation/common/button/rounded_button.h"
#include "presentation/common/utils/size_fixer.h"
#include "presentation/common/view_manager.h"
#include "presentation/settings/presentation_settings.h"

#include "ui_menu_view.h"

#include <QDialog>
#include <QPushButton>

namespace Ui {
class MenuView;
}

namespace summit {

enum MenuItems {
  Profile = 0,
  Nutrition = 1,
  Goals = 2,
  Ideas = 3,
  Trainings = 4
};

static decltype(auto) initIcons() {
  static std::array<QPixmap, 5> icons = {
      QPixmap(":/binary/resource/menu_window/profile.png"),
      QPixmap(":/binary/resource/menu_window/nutrition.png"),
      QPixmap(":/binary/resource/menu_window/goals.png"),
      QPixmap(":/binary/resource/menu_window/ideas.png"),
      QPixmap(":/binary/resource/menu_window/training.png")};
  return icons;
}

static constexpr double MENU_BAR_ASPECT_RATIO = 8.0;

class MenuBar : public QWidget {
  QHBoxLayout *layout;

public:
  explicit MenuBar(QWidget *parent = nullptr)
      : QWidget(parent), layout(new QHBoxLayout) {
    layout->setMargin(0);
    setLayout(layout);
  }

  void addMenuItem(QWidget *item) { layout->addWidget(item); }
};

class MenuView : public QDialog, public IBaseView, public SettingsListener {
  Q_OBJECT

  std::size_t current_item = INT_MAX;

public:
  explicit MenuView(ViewManager *view_manager);
  ~MenuView();

  /* No presenter */
  void createPresenter() override {}

  void onSettingsChanged() override;

  template <class ItemType>
  auto addMenuItem(const char *title, const QPixmap &icon);

protected:
  void paintEvent(QPaintEvent *event) override;

private slots:
  void on_action_button_clicked();

private:
  bool isValidIndex(std::size_t index);

  std::vector<std::pair<BoardView *, ImagePointer>> items_buffer;

  ViewManager *view_manager;
  ViewManager *menu_manager;

  MenuBar *menu_bar;
  Ui::MenuView *ui;
};

template <class ItemType>
auto MenuView::addMenuItem(const char *title, const QPixmap &icon) {
  static constexpr double ASPECT_RATIO = 1.2;

  /* Add item button */
  QPushButton *button = new RoundedButton;
  button->setIcon(QIcon(icon));
  button->setStyleSheet(BUTTON_STYLE);
  menu_bar->addMenuItem(widthForHeight(button, ASPECT_RATIO));

  /* Lazy form loading */
  items_buffer.emplace_back(nullptr, nullptr);

  std::size_t item_index = items_buffer.size() - 1;

  auto botton_action = [this, item_index, title] {
    auto &item_ptr = items_buffer[item_index];
    if (!item_ptr.first) {
      item_ptr.first = new ItemType(menu_manager);
    }
    menu_manager->removeWidgets();
    menu_manager->nextView(item_ptr.first, false);
    current_item = item_index;

    ui->title->setText(title);
    repaint();
  };

  connect(button, &QPushButton::clicked, this, botton_action);
  return botton_action;
}

} // namespace summit
#endif // MENU_SCREEN_H
