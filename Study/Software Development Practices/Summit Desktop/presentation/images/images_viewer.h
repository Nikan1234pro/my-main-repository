#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include "images_contract.h"
#include "presentation/common/images/image_subscribers_holder.h"

namespace summit {

class ImagesView : public ImagesContract::View {
public:
  ImagesView();

  void addImageSubscriber(int id, ImageSubscriber *subscriber);

  void loadImage(int id);
  void loadAllImages();

  void sendImage(const ImagePointer &image);

  void onImageLoadSuccess(const ImagePointer &image) override;
  void onImageSendSuccess(const ImagePointer &image) override;
  void onImageActionFailure(const PresentationError &error) override;

private:
  void createPresenter() override;

  std::shared_ptr<ImagesContract::Presenter> presenter;
  ImageSubscribersHolder holder;
};

} // namespace summit

#endif // IMAGEVIEWER_H
