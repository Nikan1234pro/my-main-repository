#include "request.h"

#include <QBuffer>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

#include <chrono>
#include <thread>

namespace summit {
namespace network {

namespace {
class ReplyRAII {
  QNetworkReply *reply;

public:
  explicit ReplyRAII(QNetworkReply *reply) : reply(reply) {}

  ~ReplyRAII() {
    reply->close();
    reply->deleteLater();
  }
};
} // namespace

const QString Requester::http_template = "http://%1:%2/%3";
const QString Requester::https_template = "https://%1:%2/%3";
const QString Requester::KEY_QNETWORK_REPLY_ERROR = "QNetworkReplyError";
const QString Requester::KEY_CONTENT_NOT_FOUND = "ContentNotFoundError";

Requester::Requester(QObject *parent) : QObject(parent) {
  manager = new QNetworkAccessManager(this);
}

void Requester::initialize(const QString &host, int port,
                           QSslConfiguration *ssl) {
  this->host = host;
  this->port = port;
  ssl_config = ssl;
  path_template = (ssl_config) ? https_template : http_template;
}

QNetworkRequest Requester::createRequest(const QString &apiStr) {
  QNetworkRequest request;
  QString url = path_template.arg(host).arg(port).arg(apiStr);
  request.setUrl(QUrl(url));
  request.setRawHeader("Content-Type", "application/json");

  if (!token.isEmpty())
    request.setRawHeader("Authorization", token.toUtf8());
  if (ssl_config != nullptr)
    request.setSslConfiguration(*ssl_config);

  return request;
}

QNetworkReply *Requester::sendCustomRequest(QNetworkAccessManager *manager,
                                            QNetworkRequest &request,
                                            const QString &type,
                                            const QJsonDocument &data) {
  request.setRawHeader("HTTP", type.toUtf8());
  QByteArray postDataByteArray = data.toJson();
  QBuffer *buff = new QBuffer;
  buff->setData(postDataByteArray);
  buff->open(QIODevice::ReadOnly);
  QNetworkReply *reply =
      manager->sendCustomRequest(request, type.toUtf8(), buff);
  buff->setParent(reply);
  return reply;
}

QNetworkReply *Requester::GET(QNetworkRequest &request,
                              const QJsonDocument &data) {
  if (!data.isNull()) {
    QUrl url = request.url();
    url.setQuery(jsonToUrlQuery(data));
    request.setUrl(url);
  }
  return manager->get(request);
}

QNetworkReply *Requester::POST(QNetworkRequest &request,
                               const QJsonDocument &data) {
  return manager->post(request, data.toJson());
}

QNetworkReply *Requester::PUT(QNetworkRequest &request,
                              const QJsonDocument &data) {
  return sendCustomRequest(manager, request, "PUT", data);
}

QNetworkReply *Requester::DELETE(QNetworkRequest &request,
                                 const QJsonDocument &data) {
  if (data.isEmpty()) {
    return manager->deleteResource(request);
  }
  return sendCustomRequest(manager, request, "DELETE", data);
}

void Requester::sendRequest(const QString &api, RequestType type,
                            const QVariantMap &data) {
  sendRequest(api, type, QJsonDocument::fromVariant(data));
}

void Requester::sendRequest(const QString &api, RequestType type,
                            const QJsonDocument &data) {
  QNetworkRequest request = createRequest(api);
  QNetworkReply *reply = nullptr;

  switch (type) {
  case RequestType::GET: {
    reply = GET(request, data);
    break;
  }
  case RequestType::POST: {
    reply = POST(request, data);
    break;
  }
  case RequestType::PUT: {
    reply = PUT(request, data);
    break;
  }
  case RequestType::DELETE: {
    reply = DELETE(request, data);
    break;
  }
  default:
    qWarning() << "Unsupported request type. Ignore...";
    return;
  }
  connect(reply, &QNetworkReply::finished, this, [this, reply]() {
    ReplyRAII r(reply);

    try {
      QJsonDocument json = parseReply(reply);
      if (onFinishRequest(reply)) {
        emit requestSuccess(json);
      } else {
        QString error = handleNetworkErrors(reply);
        emit requestFailure(json, error);
      }
    } catch (...) {
      qWarning() << "Unexpected error occurred...";
    }
    emit finished();
  });
}

const AuthorizationToken &Requester::getToken() const { return token; }

void Requester::setToken(const AuthorizationToken &token) {
  this->token = token;
}

QUrlQuery Requester::jsonToUrlQuery(const QJsonDocument &data) {
  static const auto convert_to_str =
      [](const QJsonValue &value) noexcept -> QString {
    if (value.isBool()) {
      bool b = value.toBool();
      return (b) ? "true" : "false";
    } else if (value.isDouble()) {
      return QString::number(value.toDouble());
    } else if (value.isString()) {
      return value.toString();
    }
    qWarning() << "Couldn't parse" << value;
    return QString{};
  };

  QUrlQuery query;
  if (!data.isObject()) {
    qWarning() << "GET-method requires json object";
    return query;
  }
  const QJsonObject object = data.object();
  for (auto &key : object.keys()) {
    const QJsonValue value = object.value(key);

    /* If value is list */
    if (value.isArray()) {
      for (const auto list_value : value.toArray()) {
        query.addQueryItem(key, convert_to_str(list_value));
      }
    }
    /* else convertible to string value */
    else {
      query.addQueryItem(key, convert_to_str(value));
    }
  }
  return query;
}

QJsonDocument Requester::parseReply(QNetworkReply *reply) const {
  /* Try get auth token */
  QByteArray auth_header = reply->rawHeader("Authorization");
  if (!auth_header.isNull()) {
    emit tokenAccepted(QString::fromUtf8(auth_header));
  }

  /* Get reply body */
  auto replyText = reply->readAll();
  if (replyText.isEmpty()) {
    return {};
  }

  QJsonParseError parse_error;
  QJsonDocument json = QJsonDocument::fromJson(replyText, &parse_error);

  if (parse_error.error != QJsonParseError::NoError) {
    qWarning() << "Json parse error: " << parse_error.errorString();
    qWarning() << QString::fromUtf8(replyText);

    QJsonObject o;
    o["code"] =
        reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    o["message"] = QString::fromUtf8(replyText);
    return QJsonDocument(o);
  }
  return json;
}

bool Requester::onFinishRequest(QNetworkReply *reply) const {
  auto replyError = reply->error();

  int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
  if (replyError == QNetworkReply::NoError) {
    if ((code >= 200) && (code < 300)) {
      return true;
    }
  }
  return false;
}

QString Requester::handleNetworkErrors(QNetworkReply *reply) const {
  auto replyError = reply->error();
  if (!(replyError == QNetworkReply::NoError ||
        replyError == QNetworkReply::ContentNotFoundError ||
        replyError == QNetworkReply::ContentAccessDenied ||
        replyError == QNetworkReply::ProtocolInvalidOperationError)) {
    qWarning() << reply->error();
    return "QNetwork reply error";
  } else if (replyError == QNetworkReply::ContentNotFoundError) {
    return "Content not found";
  }
  return reply->errorString();
}

} // namespace network
} // namespace summit
