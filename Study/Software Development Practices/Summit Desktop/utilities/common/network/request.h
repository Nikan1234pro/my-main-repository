#ifndef NETWORK_REQUEST_H
#define NETWORK_REQUEST_H

#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QObject>
#include <QSslConfiguration>
#include <QString>
#include <QUrlQuery>

#include "security/security_manager.h"

namespace summit {

namespace network {
using AuthorizationToken = QString;

enum class RequestType { POST, GET, PUT, DELETE };

class Requester : public QObject {
  Q_OBJECT

  static const QString http_template;
  static const QString https_template;

  static const QString KEY_QNETWORK_REPLY_ERROR;
  static const QString KEY_CONTENT_NOT_FOUND;

  QString host;
  int port;

  QSslConfiguration *ssl_config;
  QString path_template;
  QString token;

  QNetworkAccessManager *manager;

  /* Request common */
  QNetworkRequest createRequest(const QString &apiStr);

  QNetworkReply *sendCustomRequest(QNetworkAccessManager *manager,
                                   QNetworkRequest &request,
                                   const QString &type,
                                   const QJsonDocument &data);
  bool onFinishRequest(QNetworkReply *reply) const;

  /* Request methods */
  QNetworkReply *GET(QNetworkRequest &request, const QJsonDocument &data);
  QNetworkReply *POST(QNetworkRequest &request, const QJsonDocument &data);
  QNetworkReply *PUT(QNetworkRequest &request, const QJsonDocument &data);
  QNetworkReply *DELETE(QNetworkRequest &request, const QJsonDocument &data);

  /* Reply */
  QJsonDocument parseReply(QNetworkReply *reply) const;
  QString handleNetworkErrors(QNetworkReply *reply) const;

  /* Stuff */
  static QUrlQuery jsonToUrlQuery(const QJsonDocument &data);

public:
  explicit Requester(QObject *parent = nullptr);

  void initialize(const QString &host, int port, QSslConfiguration *ssl);

  void sendRequest(const QString &api, RequestType type,
                   const QVariantMap &data);
  void sendRequest(const QString &api, RequestType type,
                   const QJsonDocument &data = QJsonDocument{});

  const AuthorizationToken &getToken() const;
  void setToken(const AuthorizationToken &token);

signals:
  void tokenAccepted(const AuthorizationToken &token) const;
  void requestSuccess(const QJsonDocument &);
  void requestFailure(const QJsonDocument &, const QString &error);
  void finished();
};
} // namespace network
} // namespace summit

#endif
