#ifndef JSON_COMMON_H
#define JSON_COMMON_H

#include <array>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>

#pragma warning(disable : 4100)

namespace json {
namespace types {
using Integer = std::int32_t;
using Double = double;
using String = QString;
template <class T> using Pointer = std::unique_ptr<T>;
template <class T> using Vector = std::vector<T>;

template <class T, class... Args>
_NODISCARD auto make_pointer(Args &&...args) -> Pointer<T> {
  return Pointer<T>(new T(std::forward<Args>(args)...));
}
} // namespace types

class JsonParseException : public std::exception {
  std::string cause;

public:
  explicit JsonParseException(const std::string &cause) : cause(cause) {}

  const char *what() const override { return cause.c_str(); }
};

using JsonObject = QJsonObject;
using JsonValue = QJsonValue;
} // namespace json

#endif // !JSON_COMMON_H
