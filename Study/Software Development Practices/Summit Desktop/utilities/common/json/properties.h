#ifndef JSON_PROPERTIES_H
#define JSON_PROPERTIES_H

#include "common.h"
#include "utilities/common/nonstd/optional.h"

#include <tuple>

namespace json {

template <typename Class, typename T> struct PropertyImpl {
  constexpr PropertyImpl(nonstd::optional<T> Class::*member, const char *name)
      : member(member), name(name) {}

  using value_type = T;

  nonstd::optional<T> Class::*member;
  const char *name;
};

template <class T> struct JsonValueUnpacker;
template <class T> struct JsonValuePacker;

template <typename Class, typename T>
constexpr PropertyImpl<Class, T> property(nonstd::optional<T> Class::*member,
                                          const char *name) {
  return PropertyImpl<Class, T>(member, name);
}

template <size_t Begin, size_t End, class T> struct PropertyUnpackIterator {
  template <class Properties>
  static void action(const Properties &properties, T &object,
                     const JsonObject &json) {
    auto property = std::get<Begin>(properties);

    /* Use JsonUnpacker to assign field */
    auto iter = json.find(property.name);
    if (iter == json.end()) {
      throw JsonParseException("Key not found");
    }
    JsonValue value = *iter;
    if (!value.isNull()) {
      object.*(property.member) =
          JsonValueUnpacker<typename decltype(property)::value_type>::unpack(
              *iter);
    }
    PropertyUnpackIterator<Begin + 1, End, T>::action(properties, object, json);
  }
};

template <size_t End, class T> struct PropertyUnpackIterator<End, End, T> {
  template <class Properties>
  static void action(const Properties &, T &, const JsonObject &) {}
};

template <size_t Begin, size_t End, class T> struct PropertyPackIterator {
  template <class Properties>
  static void action(const Properties &properties, const T &object,
                     JsonObject &json) {
    auto property = std::get<Begin>(properties);

    auto &&field = object.*(property.member);
    if (field) {
      /* Set value */
      json.insert(
          property.name,
          JsonValuePacker<typename decltype(property)::value_type>::pack(
              *field));
    } else {
      /* Set null */
      json.insert(property.name, QJsonValue{});
    }

    PropertyPackIterator<Begin + 1, End, T>::action(properties, object, json);
  }
};

template <size_t End, class T> struct PropertyPackIterator<End, End, T> {
  template <class Properties>
  static void action(const Properties &, const T &, JsonObject &) {}
};

template <size_t N, class T> struct PropertyCycle {
  template <class Properties>
  static void run(const Properties &properties, T &object,
                  const JsonObject &json) {
    PropertyUnpackIterator<0U, N, T>::action(properties, object, json);
  }

  template <class Properties>
  static void run(const Properties &properties, const T &object,
                  JsonObject &json) {
    PropertyPackIterator<0U, N, T>::action(properties, object, json);
  }
};

template <class Serializable> struct Properties {
  constexpr static std::tuple<> value{};
};

} // namespace json

#endif // !JSON_PROPERTIES_H
