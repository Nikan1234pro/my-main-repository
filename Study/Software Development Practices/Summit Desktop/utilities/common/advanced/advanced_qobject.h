#ifndef ADVANCEDQOBJECT_H
#define ADVANCEDQOBJECT_H

#include <QDebug>
#include <QMetaMethod>
#include <QMetaObject>
#include <QObject>

namespace summit {

class QObjectAdvanced : public QObject {
  Q_OBJECT
public:
  QObjectAdvanced(QObject *parent = nullptr);

  template <typename DataType, typename Func1, typename Func2>
  inline static void
  singleShot(const typename QtPrivate::FunctionPointer<Func1>::Object *sender,
             Func1 signal, const QObject *context, Func2 slot,
             Qt::ConnectionType type = Qt::AutoConnection) {
    QMetaObject::Connection *const connection = new QMetaObject::Connection;
    *connection = connect(
        sender, signal, context,
        [connection, slot](const DataType &data) {
          slot(data);

          QObject::disconnect(*connection);
          delete connection;
        },
        type);
  }
};

} // namespace summit

#endif // ADVANCEDQOBJECT_H
