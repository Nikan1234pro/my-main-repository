#ifndef NUTRITION_DATA_SOURCE_H
#define NUTRITION_DATA_SOURCE_H

#include "data/common/data_error.h"
#include "data/common/source/remote/default_data_loader.h"
#include "model/nutrition/recipe.h"

#include <QObject>

namespace summit {

class NutritionDataSource : public QObject {
  Q_OBJECT

public:
  virtual ~NutritionDataSource() = default;

  virtual void loadRecipes() = 0;
  virtual void updateRecipe(const Recipe &recipe) = 0;
  virtual void deleteRecipe(int id) = 0;

signals:
  void loadSuccess(const RecipeList &goals);
  void loadFailure(const DataError &error);

  void updateSuccess();
  void updateFailure(const DataError &error);

  void deleteSuccess();
  void deleteFailure(const DataError &error);
};

} // namespace summit

#endif // NUTRITION_DATA_SOURCE_H
