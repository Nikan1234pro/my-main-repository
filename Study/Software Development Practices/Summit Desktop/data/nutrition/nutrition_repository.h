#ifndef NUTRITIONREPOSITORY_H
#define NUTRITIONREPOSITORY_H

#include "data/common/source/remote/default_data_deleter.h"
#include "data/common/source/remote/default_data_loader.h"
#include "data/common/source/remote/default_data_updater.h"
#include "nutrition_data_source.h"

namespace summit {

class NutritionRepository : public NutritionDataSource {

  DefaultDataLoader loader;
  DefaultDataUpdater updater;
  DefaultDataDeleter deleter;

  static const QString LOAD_RECIPES_API;
  static const QString UPDATE_RECIPE_API;
  static const QString DELETE_RECIPE_API;

public:
  static std::unique_ptr<NutritionRepository> create();

  void loadRecipes() override;
  void updateRecipe(const Recipe &recipe) override;
  void deleteRecipe(int id) override;

private:
  NutritionRepository();
};

} // namespace summit

#endif // NUTRITIONREPOSITORY_H
