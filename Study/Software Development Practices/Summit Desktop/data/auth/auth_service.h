#ifndef SIGNINDATASOURCE_H
#define SIGNINDATASOURCE_H

#include <QObject>

#include "data/common/data_error.h"
#include "model/auth/authentication_data.h"
#include "model/auth/register_data.h"
#include "security/security_manager.h"

namespace summit {

class AuthService : public QObject {
  Q_OBJECT
public:
  virtual void signIn(const AuthenticationData &auth_data) = 0;
  virtual void signUp(const RegisterData &register_data) = 0;

  virtual ~AuthService() = default;

signals:
  void signInSuccess(const SecurityManager::AuthorizationToken &);
  void signInFailure(const DataError &);

  void signUpSuccess();
  void signUpFailure(const DataError &);
};

} // namespace summit

#endif // SIGNINDATASOURCE_H
