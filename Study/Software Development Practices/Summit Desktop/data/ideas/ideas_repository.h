#ifndef IDEASREPOSITORY_H
#define IDEASREPOSITORY_H

#include "data/common/source/remote/default_data_deleter.h"
#include "data/common/source/remote/default_data_loader.h"
#include "data/common/source/remote/default_data_updater.h"
#include "ideas_data_source.h"

namespace summit {

class IdeasRepository : public IdeasDataSource {

  static const QString LOAD_IDEAS_API;
  static const QString UPDATE_IDEA_API;
  static const QString DELETE_IDEA_API;

  DefaultDataLoader loader;
  DefaultDataUpdater updater;
  DefaultDataDeleter deleter;

public:
  static std::unique_ptr<IdeasRepository> create();

  void loadIdeas() override;
  void updateIdea(const Idea &idea) override;
  void deleteIdea(int id) override;

private:
  IdeasRepository();
};

} // namespace summit

#endif // IDEASREPOSITORY_H
