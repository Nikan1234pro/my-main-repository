#ifndef IDEAS_DATA_SOURCE_H
#define IDEAS_DATA_SOURCE_H

#include "data/common/data_error.h"
#include "data/common/source/remote/default_data_loader.h"
#include "model/ideas/idea.h"
#include <QObject>

namespace summit {

class IdeasDataSource : public QObject {
  Q_OBJECT

public:
  virtual ~IdeasDataSource() = default;

  virtual void loadIdeas() = 0;
  virtual void updateIdea(const Idea &idea) = 0;
  virtual void deleteIdea(int id) = 0;

signals:
  void loadSuccess(const IdeasList &ideas);
  void loadFailure(const DataError &error);

  void updateSuccess();
  void updateFailure(const DataError &error);

  void deleteSuccess();
  void deleteFailure(const DataError &error);
};

} // namespace summit

#endif // IDEAS_DATA_SOURCE_H
