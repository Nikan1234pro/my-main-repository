#ifndef IMAGES_DATA_SOURCE_H
#define IMAGES_DATA_SOURCE_H

#include <QObject>

#include "data/common/data_error.h"
#include "model/images/image.h"

namespace summit {

class ImagesDataSource : public QObject {
  Q_OBJECT

public:
  virtual ~ImagesDataSource() = default;

  virtual void sendImage(const ImagePointer &image) = 0;
  virtual void loadImage(int id) = 0;

signals:
  void loadSuccess(const ImagePointer &image);
  void loadFailure(const DataError &error);

  void sendSuccess(const ImagePointer &image);
  void sendFailure(const DataError &error);
};

} // namespace summit
#endif // IMAGES_DATA_SOURCE_H
