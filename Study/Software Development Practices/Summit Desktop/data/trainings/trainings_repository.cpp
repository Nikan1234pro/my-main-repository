#include "trainings_repository.h"

#include <iostream>

namespace summit {

const QString TrainingsRepository::DEFAULT_TRAINING_API =
    "api/training/default";
const QString TrainingsRepository::LOAD_ALL_TRAININGS_API = "api/trainings";
const QString TrainingsRepository::UPDATE_TRAININGS_API = "api/training";
const QString TrainingsRepository::DELETE_TRAININGS_API = "api/training/delete";

std::unique_ptr<TrainingsRepository> TrainingsRepository::create() {
  return std::unique_ptr<TrainingsRepository>(new TrainingsRepository);
}

void TrainingsRepository::loadDefaultTraining(const TrainingType &type) {
  const QString api = DEFAULT_TRAINING_API + '/' + type.toString();
  loader.loadSingleObject<Training>(
      api,
      [this](const Training &training) {
        emit onDefaultTrainingLoadSuccess(training);
      },
      [this](const DataError &error) { emit onFailure(error); });
}

void TrainingsRepository::loadAllTrainings() {
  loader.loadAllObjects<Training>(
      LOAD_ALL_TRAININGS_API,
      [this](const TrainingList &trainings) {
        emit onTrainingListLoadSuccess(trainings);
      },
      [this](const DataError &error) { emit onFailure(error); });
}

void TrainingsRepository::updateTraining(const Training &training) {
  updater.updateObject<Training, void>(
      UPDATE_TRAININGS_API, [this]() { emit onTrainingUpdateSuccess(); },
      [this](const DataError &error) { emit onFailure(error); }, training);
}

void TrainingsRepository::deleteTraining(int id) {
  deleter.deteteObject(
      DELETE_TRAININGS_API, [this]() { emit onTrainingDeleteSuccess(); },
      [this](const DataError &error) { emit onFailure(error); }, id);
};

TrainingsRepository::TrainingsRepository() {}

} // namespace summit
