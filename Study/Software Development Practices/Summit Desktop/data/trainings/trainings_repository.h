#ifndef TRAININGSREPOSITORY_H
#define TRAININGSREPOSITORY_H

#include "data/common/source/remote/default_data_deleter.h"
#include "data/common/source/remote/default_data_loader.h"
#include "data/common/source/remote/default_data_updater.h"
#include "data/trainings/trainings_data_source.h"

namespace summit {

class TrainingsRepository : public TrainingsDataSource {

  static const QString DEFAULT_TRAINING_API;
  static const QString LOAD_ALL_TRAININGS_API;
  static const QString UPDATE_TRAININGS_API;
  static const QString DELETE_TRAININGS_API;

  DefaultDataLoader loader;
  DefaultDataUpdater updater;
  DefaultDataDeleter deleter;

public:
  static std::unique_ptr<TrainingsRepository> create();

  void loadDefaultTraining(const TrainingType &type) override;
  void loadAllTrainings() override;
  void updateTraining(const Training &training) override;
  void deleteTraining(int id) override;

private:
  TrainingsRepository();
};

} // namespace summit

#endif // TRAININGSREPOSITORY_H
