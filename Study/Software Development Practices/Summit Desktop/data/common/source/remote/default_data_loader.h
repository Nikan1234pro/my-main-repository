#ifndef DEFAULTDATALOADER_H
#define DEFAULTDATALOADER_H

#include <QJsonArray>
#include <QObject>
#include <QString>

#include "constants/network/network_constants.h"
#include "data/common/data_error.h"
#include "security/security_manager.h"
#include "setup.h"

#include "utilities/common/json/json_parser.h"
#include "utilities/common/network/request.h"
#include "utilities/common/nonstd/error.h"

namespace summit {

static const QString LOADER_DATA_ERROR_MESSAGE =
    QString::fromUtf8("Сервер отправил недействительные данные");

class DefaultDataLoader : public QObject {
public:
  DefaultDataLoader() = default;

  template <class UserType, class SuccessFuction, class FailureFunction>
  void loadAllObjects(const QString &api, SuccessFuction on_success,
                      FailureFunction on_failure, const QVariantMap &map = {});

  template <class UserType, class SuccessFuction, class FailureFunction>
  void loadSingleObject(const QString &api, SuccessFuction on_success,
                        FailureFunction on_failure,
                        const QVariantMap &map = {});
};

template <class UserType, class SuccessFuction, class FailureFunction>
void DefaultDataLoader::loadAllObjects(const QString &api,
                                       SuccessFuction on_success,
                                       FailureFunction on_failure,
                                       const QVariantMap &map) {
  using namespace network;

  Requester *requester = new Requester(this);
  auto result = setupRequester(requester);
  if (result != SETUP_SUCCESS) {
    emit on_failure(DataError{});
    return;
  }

  /* Connect */
  connect(requester, &Requester::requestSuccess, this,
          [this, on_success, on_failure](const QJsonDocument &response) {
            if (!response.isArray()) {
              on_failure(DataError{-1, LOADER_DATA_ERROR_MESSAGE});
              return;
            }
            const QJsonArray array = response.array();
            try {
              QList<UserType> objects;
              std::transform(array.begin(), array.end(),
                             std::back_inserter(objects),
                             [](const QJsonValue &json_value) {
                               return json::JsonParser<UserType>::fromJson(
                                   json_value.toObject());
                             });
              on_success(objects);
            } catch (const json::JsonParseException &) {
              on_failure(DataError{-1, LOADER_DATA_ERROR_MESSAGE});
            }
          });

  connect(
      requester, &Requester::requestFailure, this,
      [this, on_failure](const QJsonDocument &response, const QString &error) {
        QJsonObject json = response.object();
        on_failure(DataError{json["code"].toInt(), json["message"].toString()});
      });

  /* On finish cleanup */
  connect(requester, &Requester::finished, requester, &Requester::deleteLater);
  requester->sendRequest(api, RequestType::GET, map);
}

template <class UserType, class SuccessFuction, class FailureFunction>
void DefaultDataLoader::loadSingleObject(const QString &api,
                                         SuccessFuction on_success,
                                         FailureFunction on_failure,
                                         const QVariantMap &map) {
  using namespace network;

  Requester *requester = new Requester(this);
  auto result = setupRequester(requester);
  if (result != SETUP_SUCCESS) {
    emit on_failure(DataError{});
    return;
  }

  /* Connect */
  connect(requester, &Requester::requestSuccess, this,
          [this, on_success, on_failure](const QJsonDocument &response) {
            if (!response.isObject()) {
              on_failure(DataError{});
            }
            try {
              on_success(
                  json::JsonParser<UserType>::fromJson(response.object()));
            } catch (const json::JsonParseException &) {
              on_failure(DataError{-1, LOADER_DATA_ERROR_MESSAGE});
            }
          });

  connect(
      requester, &Requester::requestFailure, this,
      [this, on_failure](const QJsonDocument &response, const QString &error) {
        QJsonObject json = response.object();
        on_failure(DataError{json["code"].toInt(), json["message"].toString()});
      });

  /* On finish cleanup */
  connect(requester, &Requester::finished, requester, &Requester::deleteLater);
  requester->sendRequest(api, RequestType::GET, map);
}

} // namespace summit

#endif // DEFAULTDATALOADER_H
