#include "profile_repository.h"

namespace summit {

const QString ProfileRepository::PROFILE_API = "api/profile";

std::unique_ptr<ProfileRepository> ProfileRepository::create() {
  std::unique_ptr<ProfileRepository> INSTANCE(new ProfileRepository);
  return INSTANCE;
}

void ProfileRepository::loadProfile() {
  loader.loadSingleObject<UserProfile>(
      PROFILE_API,
      [this](const UserProfile &profile) {
        auto profile_pointer = UserProfilePointer::create(profile);
        emit loadSuccess(profile_pointer);
      },
      [this](const DataError &error) { emit loadFailure(error); });
}

void ProfileRepository::updateProfile(const UserProfilePointer &profile) {
  updater.updateObject<UserProfile, void>(
      PROFILE_API, [this]() { emit updateSuccess(); },
      [this](const DataError &error) { emit updateFailure(error); }, *profile);
}

ProfileRepository::ProfileRepository() {}

} // namespace summit
