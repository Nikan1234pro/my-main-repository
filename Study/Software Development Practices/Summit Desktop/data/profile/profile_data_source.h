#ifndef PROFILE_DATA_SOURCE_H
#define PROFILE_DATA_SOURCE_H

#include <QObject>

#include "data/common/data_error.h"
#include "model/profile/user_profile.h"

namespace summit {

class ProfileDataSource : public QObject {
  Q_OBJECT

public:
  virtual ~ProfileDataSource() = default;

  virtual void loadProfile() = 0;
  virtual void updateProfile(const UserProfilePointer &profile) = 0;

signals:
  void loadSuccess(const UserProfilePointer &profile);
  void loadFailure(const DataError &error);

  void updateSuccess();
  void updateFailure(const DataError &error);
};

} // namespace summit

#endif // PROFILE_DATA_SOURCE_H
