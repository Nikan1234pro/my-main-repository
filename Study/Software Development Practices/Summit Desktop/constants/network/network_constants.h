#ifndef NETWORK_CONSTANTS_H
#define NETWORK_CONSTANTS_H

#include <QString>

#pragma warning(disable : 4100)

extern const QString REMOTE_HOSTNAME;
static constexpr int REMOTE_PORT = 8080;

#endif // NETWORK_CONSTANTS_H
