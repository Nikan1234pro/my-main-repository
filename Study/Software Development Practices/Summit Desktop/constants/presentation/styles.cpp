#include "styles.h"

namespace summit {

const QString SCROLL_AREA_STYLE = "QScrollArea { "
                                  "background-color:transparent;"
                                  "border: none; }";

const QString VERTICAL_SCROLLBAR_STYLE =
    QString::fromUtf8("QScrollBar:vertical {"
                      "    border: 0px transparent;"
                      "    background:transparent;"
                      "    width: 4px;    "
                      "    margin: 0px 0px 0px 0px;"
                      "}"
                      "QScrollBar::handle:vertical {"
                      "    background: white;"
                      "    min-height: 0px;"
                      "}"
                      "QScrollBar::add-line:vertical {"
                      "    background: white;"
                      "    height: 0px;"
                      "    subcontrol-position: bottom;"
                      "    subcontrol-origin: margin;"
                      "}"
                      "QScrollBar::sub-line:vertical {"
                      "    background: white;"
                      "    height: 0 px;"
                      "    subcontrol-position: top;"
                      "    subcontrol-origin: margin;"
                      "}");

const QString HORIZONTAL_SCROLLBAR_STYLE =
    QString::fromUtf8("QScrollBar:horizontal {"
                      "    background: white;"
                      "    border: 0px transparent;"
                      "    width: 4px;    "
                      "    margin: 0px 0px 0px 0px;"
                      "}");

const QString INVISIBLE_SCROLLBAR_STYLE = "QScrollBar:horizontal {"
                                          "    border: 0px solid #999999;"
                                          "    background:transparent;"
                                          "    width:0px;    "
                                          "    margin: 0px 0px 0px 0px;"
                                          "}";

const QString BUTTON_STYLE = QString::fromUtf8(
    "QPushButton { border-radius:15px; background-color: white; color : black; "
    "padding-left: 25px; padding-right: 25px; font: 18pt mr_SilverAgeQueensG}");

const QString LINE_EDIT_STYLE =
    "QLineEdit {background:transparent; border:none; color: "
    "white; border-bottom:1px solid #717072; font: 18pt mr_SilverAgeQueensG;}";

const QString TEXT_EDIT_STYLE =
    "QTextEdit {background:rgba(0, 0, 0, 0); color: white; border:none; font: "
    "16pt Verdana;}";

const QString FRAME_STYLE = "QFrame { background:transparent; }";

} // namespace summit
