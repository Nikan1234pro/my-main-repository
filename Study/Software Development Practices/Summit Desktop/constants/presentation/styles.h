#ifndef STYLES_H
#define STYLES_H

#include <QString>

namespace summit {

extern const QString SCROLL_AREA_STYLE;
extern const QString VERTICAL_SCROLLBAR_STYLE;
extern const QString HORIZONTAL_SCROLLBAR_STYLE;
extern const QString INVISIBLE_SCROLLBAR_STYLE;

extern const QString BUTTON_STYLE;

extern const QString LINE_EDIT_STYLE;
extern const QString TEXT_EDIT_STYLE;

extern const QString FRAME_STYLE;

} // namespace summit

#endif // STYLES_H
