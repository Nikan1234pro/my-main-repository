#include "widget_constants.h"

#include "presentation/common/font/font.h"

namespace summit {

const QFont WIDGET_HEADER_FONT(DEFAULT_TEXT_FONT_NAME, 14, QFont::Bold);
const QFont ITEM_TITLE_FONT(DEFAULT_TEXT_FONT_NAME, 10, QFont::Bold);
const QFont ITEM_BODY_FONT(DEFAULT_TEXT_FONT_NAME, 10);

} // namespace summit
