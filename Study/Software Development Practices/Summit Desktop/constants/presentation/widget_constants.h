#ifndef SIZE_CONSTANTS_H
#define SIZE_CONSTANTS_H

#include <QFont>
#include <QSize>

#include "presentation/common/font/font.h"

namespace summit {

enum WidgetType { SMALL = 0, HORIZONTAL = 1, LARGE = 2 };

constexpr static QSize WIDGET_BASE_SIZE[3] = {QSize{180, 180}, QSize{360, 180},
                                              QSize{360, 360}};

constexpr static int WIDGET_ITEM_MAX_COUNT[3] = {2, 2, 4};

static constexpr int WIDGET_BORDER_RADIUS = 50;
static constexpr int WIDGET_BOARD_HEIGHT = 2;

extern const QFont WIDGET_HEADER_FONT;
extern const QFont ITEM_TITLE_FONT;
extern const QFont ITEM_BODY_FONT;

} // namespace summit
#endif // SIZE_CONSTANTS_H
