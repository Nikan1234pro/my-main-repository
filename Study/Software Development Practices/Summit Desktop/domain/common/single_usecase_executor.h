#ifndef SINGLEUSECASEEXECUTOR_H
#define SINGLEUSECASEEXECUTOR_H

#include "usecase_executor.h"

#include <memory>

namespace summit {

class SingleUseCaseExecutor : public UseCaseExecutor {
public:
  static std::shared_ptr<SingleUseCaseExecutor> &getInstance();

private:
  SingleUseCaseExecutor();
};

} // namespace summit
#endif // SINGLEUSECASEEXECUTOR_H
