#ifndef USECASE_H
#define USECASE_H

#include "domain_error.h"
#include "utilities/common/nonstd/optional.h"
#include <memory>

#include <QSharedPointer>

namespace summit {

template <class RequestParametersType, class ResponseType> class UseCase {
public:
  virtual void finished(const ResponseType &) = 0;
  virtual void failed(const DomainError &) = 0;

  UseCase() = default;
  virtual ~UseCase() = default;

  void run() { execute(request_parameters.value()); }

  void setRequestParameters(const RequestParametersType &request_parameters) {
    this->request_parameters = request_parameters;
  }

  void setRequestParameters(RequestParametersType &&request_parameters) {
    this->request_parameters = std::move(request_parameters);
  }

  const RequestParametersType &getRequestParameters() const {
    return *request_parameters;
  }

private:
  nonstd::optional<RequestParametersType> request_parameters;

  virtual void execute(const RequestParametersType &request_parameters) = 0;
};

template <class RequestParametersType>
class UseCase<RequestParametersType, void> {
public:
  virtual void finished() = 0;
  virtual void failed(const DomainError &) = 0;

  UseCase() = default;
  virtual ~UseCase() = default;

  void run() { execute(request_parameters); }

  void setRequestParameters(const RequestParametersType &request_parameters) {
    this->request_parameters = request_parameters;
  }

  void setRequestParameters(RequestParametersType &&request_parameters) {
    this->request_parameters = std::move(request_parameters);
  }

  const RequestParametersType &getRequestParameters() const {
    return request_parameters;
  }

private:
  RequestParametersType request_parameters;

  virtual void execute(const RequestParametersType &request_parameters) = 0;
};

template <class ResponseType> class UseCase<void, ResponseType> {
public:
  virtual void finished(const ResponseType &) = 0;
  virtual void failed(const DomainError &) = 0;

  UseCase() = default;
  virtual ~UseCase() = default;

  void run() { execute(); }

private:
  virtual void execute() = 0;
};

/* For daemon usecase */
template <> class UseCase<void, void> {
public:
  virtual void finished() = 0;
  virtual void failed(DomainError) = 0;

  UseCase() = default;
  virtual ~UseCase() = default;

  void run() { execute(); }

private:
  virtual void execute() = 0;
};

} // namespace summit

#endif // USECASE_H
