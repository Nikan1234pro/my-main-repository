#include "single_usecase_executor.h"

namespace summit {

SingleUseCaseExecutor::SingleUseCaseExecutor() = default;

std::shared_ptr<SingleUseCaseExecutor> &SingleUseCaseExecutor::getInstance() {
  static std::shared_ptr<SingleUseCaseExecutor> INSTANCE(
      new SingleUseCaseExecutor);
  return INSTANCE;
}

} // namespace summit
