#ifndef LOADTOKENUSECASE_H
#define LOADTOKENUSECASE_H

#include "domain/common/usecase.h"
#include "security/security_manager.h"

#include <QSharedPointer>

namespace summit {

class LoadTokenUseCase
    : public QObject,
      public UseCase<void,
                     QSharedPointer<SecurityManager::AuthorizationToken>> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = QSharedPointer<SecurityManager::AuthorizationToken>;

  LoadTokenUseCase();

signals:
  void finished(const ResponseType &responce) override;
  void failed(const DomainError &error) override;

private:
  void execute() override;
};

} // namespace summit
#endif // LOADTOKENUSECASE_H
