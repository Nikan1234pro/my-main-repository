#include "delete_goal_usecase.h"

namespace summit {

DeleteGoalUseCase::DeleteGoalUseCase(
    const std::shared_ptr<GoalsDataSource> &goals_repository)
    : goals_repository(goals_repository) {

  connect(goals_repository.get(), &GoalsDataSource::deleteSuccess, this,
          [this]() { emit finished(); });
  connect(goals_repository.get(), &GoalsDataSource::deleteFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void summit::DeleteGoalUseCase::execute(const int &id) {
  goals_repository->deleteGoal(id);
}

} // namespace summit
