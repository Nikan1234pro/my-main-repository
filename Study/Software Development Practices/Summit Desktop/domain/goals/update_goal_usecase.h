#ifndef CREATEGOALUSECASE_H
#define CREATEGOALUSECASE_H

#include "data/goals/goals_data_source.h"
#include "domain/common/usecase.h"
#include "model/goals/goal.h"

namespace summit {

class UpdateGoalUseCase : public QObject, public UseCase<Goal, void> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = GoalsList;

  explicit UpdateGoalUseCase(
      const std::shared_ptr<GoalsDataSource> &goals_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const Goal &goal) override;

  std::shared_ptr<GoalsDataSource> goals_repository;
};

} // namespace summit

#endif // CREATEGOALUSECASE_H
