#ifndef DELETEIDEAUSECASE_H
#define DELETEIDEAUSECASE_H

#include "data/ideas/ideas_data_source.h"
#include "domain/common/usecase.h"
#include "model/ideas/idea.h"

namespace summit {

class DeleteIdeaUseCase : public QObject, public UseCase<int, void> {
  Q_OBJECT

public:
  using RequestType = int;
  using ResponseType = void;

  explicit DeleteIdeaUseCase(
      const std::shared_ptr<IdeasDataSource> &ideas_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const int &id) override;

  std::shared_ptr<IdeasDataSource> ideas_repository;
};

} // namespace summit

#endif // DELETEIDEAUSECASE_H
