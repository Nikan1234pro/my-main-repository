#include "load_ideas_usecase.h"

namespace summit {

LoadIdeasUseCase::LoadIdeasUseCase(
    const std::shared_ptr<IdeasDataSource> &ideas_repository)
    : ideas_repository(std::move(ideas_repository)) {

  connect(ideas_repository.get(), &IdeasDataSource::loadSuccess, this,
          [this](const IdeasList &goals) { emit finished(goals); });
  connect(ideas_repository.get(), &IdeasDataSource::loadFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadIdeasUseCase::execute() { ideas_repository->loadIdeas(); }

} // namespace summit
