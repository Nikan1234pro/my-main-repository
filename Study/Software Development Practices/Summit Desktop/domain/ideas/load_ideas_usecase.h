#ifndef LOADIDEASUSECASE_H
#define LOADIDEASUSECASE_H

#include "data/ideas/ideas_data_source.h"
#include "domain/common/usecase.h"
#include "model/ideas/idea.h"

#include <list>

namespace summit {

class LoadIdeasUseCase : public QObject, public UseCase<void, IdeasList> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = IdeasList;

  explicit LoadIdeasUseCase(
      const std::shared_ptr<IdeasDataSource> &ideas_repository);

signals:
  void finished(const IdeasList &responce) override;
  void failed(const DomainError &error) override;

private:
  void execute() override;

  std::shared_ptr<IdeasDataSource> ideas_repository;
};

} // namespace summit

#endif // LOADIDEASUSECASE_H
