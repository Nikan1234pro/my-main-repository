#ifndef DELETE_TRAINING_USE_CASE_H
#define DELETE_TRAINING_USE_CASE_H

#include "data/trainings/trainings_data_source.h"
#include "domain/common/usecase.h"
#include "model/trainings/training_type.h"

namespace summit {

class DeleteTrainingUseCase : public QObject, public UseCase<int, void> {
  Q_OBJECT
public:
  using RequestType = int;
  using ResponceType = void;

  explicit DeleteTrainingUseCase(
      std::unique_ptr<TrainingsDataSource> training_repository);

signals:
  void finished() override;
  void failed(const DomainError &) override;

private:
  void execute(const int &id) override;

  std::unique_ptr<TrainingsDataSource> training_repository;
};

} // namespace summit

#endif // DELETE_TRAINING_USE_CASE_H
