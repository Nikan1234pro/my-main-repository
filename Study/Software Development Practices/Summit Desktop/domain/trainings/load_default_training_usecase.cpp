#include "load_default_training_usecase.h"

namespace summit {

LoadDefaultTrainingUseCase::LoadDefaultTrainingUseCase(
    std::unique_ptr<TrainingsDataSource> training_repository)
    : training_repository(std::move(training_repository)) {

  connect(this->training_repository.get(),
          &TrainingsDataSource::onDefaultTrainingLoadSuccess, this,
          [this](const Training &training) { emit finished(training); });
  connect(this->training_repository.get(), &TrainingsDataSource::onFailure,
          this, [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadDefaultTrainingUseCase::execute(const TrainingType &type) {
  training_repository->loadDefaultTraining(type);
}

} // namespace summit
