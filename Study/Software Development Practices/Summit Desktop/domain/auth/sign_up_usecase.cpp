#include "sign_up_usecase.h"

namespace summit {

SignUpUseCase::SignUpUseCase(const std::shared_ptr<AuthService> &service)
    : service(service) {

  connect(service.get(), &AuthService::signUpSuccess, this,
          [this]() { emit finished(); });
  connect(service.get(), &AuthService::signUpFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void SignUpUseCase::execute(const RegisterData &request_parameters) {
  service->signUp(request_parameters);
}

} // namespace summit
