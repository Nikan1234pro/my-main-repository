#ifndef SIGN_UP_USECASE_H
#define SIGN_UP_USECASE_H

#include "data/auth/auth_service.h"
#include "domain/common/usecase.h"

namespace summit {

class SignUpUseCase : public QObject, public UseCase<RegisterData, void> {

  Q_OBJECT
public:
  using RequestType = RegisterData;
  using ResponseType = SecurityManager::AuthorizationToken;

  SignUpUseCase(const std::shared_ptr<AuthService> &service);

private:
  std::shared_ptr<AuthService> service;

  void execute(const RegisterData &request_parameters) override;

signals:
  void finished() override;
  void failed(const DomainError &) override;
};

} // namespace summit

#endif // SIGN_UP_USECASE_H
