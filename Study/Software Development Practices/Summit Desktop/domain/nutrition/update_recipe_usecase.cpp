#include "update_recipe_usecase.h"

namespace summit {
UpdateRecipeUseCase::UpdateRecipeUseCase(
    const std::shared_ptr<NutritionDataSource> &recipes_repository)
    : recipes_repository(recipes_repository) {

  connect(recipes_repository.get(), &NutritionDataSource::updateSuccess, this,
          [this]() { emit finished(); });
  connect(recipes_repository.get(), &NutritionDataSource::updateFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void UpdateRecipeUseCase::execute(const Recipe &recipe) {
  recipes_repository->updateRecipe(recipe);
}
} // namespace summit
