#ifndef UPDATE_RECIPE_USECASE_H
#define UPDATE_RECIPE_USECASE_H

#include "data/nutrition/nutrition_data_source.h"
#include "domain/common/usecase.h"
#include "model/nutrition/recipe.h"

namespace summit {

class UpdateRecipeUseCase : public QObject, public UseCase<Recipe, void> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = RecipeList;

  explicit UpdateRecipeUseCase(
      const std::shared_ptr<NutritionDataSource> &recipes_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const Recipe &recipe) override;

  std::shared_ptr<NutritionDataSource> recipes_repository;
};

} // namespace summit

#endif // UPDATE_RECIPE_USECASE_H
