#ifndef LOADRECIPESUSECASE_H
#define LOADRECIPESUSECASE_H

#include "data/nutrition/nutrition_data_source.h"
#include "domain/common/usecase.h"
#include "model/nutrition/recipe.h"

namespace summit {

class LoadRecipesUseCase : public QObject, public UseCase<void, RecipeList> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = RecipeList;

  explicit LoadRecipesUseCase(
      const std::shared_ptr<NutritionDataSource> &recipes_repository);

signals:
  void finished(const RecipeList &recipes) override;
  void failed(const DomainError &error) override;

private:
  void execute() override;

  std::shared_ptr<NutritionDataSource> recipes_repository;
};

} // namespace summit

#endif // LOADRECIPESUSECASE_H
