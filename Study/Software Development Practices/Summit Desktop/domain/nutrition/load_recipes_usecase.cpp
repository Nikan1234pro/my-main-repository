#include "load_recipes_usecase.h"

namespace summit {

LoadRecipesUseCase::LoadRecipesUseCase(
    const std::shared_ptr<NutritionDataSource> &recipes_repository)
    : recipes_repository(recipes_repository) {

  connect(recipes_repository.get(), &NutritionDataSource::loadSuccess, this,
          [this](const RecipeList &recipes) { emit finished(recipes); });
  connect(recipes_repository.get(), &NutritionDataSource::loadFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadRecipesUseCase::execute() { recipes_repository->loadRecipes(); }

} // namespace summit
