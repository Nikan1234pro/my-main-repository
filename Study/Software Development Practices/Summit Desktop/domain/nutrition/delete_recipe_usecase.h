#ifndef DELETE_RECIPE_USECASE_H
#define DELETE_RECIPE_USECASE_H

#include "data/nutrition/nutrition_data_source.h"
#include "domain/common/usecase.h"
#include "model/nutrition/recipe.h"

namespace summit {

class DeleteRecipeUseCase : public QObject, public UseCase<int, void> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = RecipeList;

  explicit DeleteRecipeUseCase(
      const std::shared_ptr<NutritionDataSource> &recipes_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const int &id) override;

  std::shared_ptr<NutritionDataSource> recipes_repository;
};

} // namespace summit

#endif // DELETE_RECIPE_USECASE_H
