#include "load_images_usecase.h"

namespace summit {

LoadImagesUseCase::LoadImagesUseCase(
    const std::shared_ptr<ImagesDataSource> &images_repository)
    : images_repository(images_repository) {

  connect(images_repository.get(), &ImagesDataSource::loadSuccess, this,
          [this](const ImagePointer &image) { emit finished(image); });
  connect(images_repository.get(), &ImagesDataSource::loadFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadImagesUseCase::execute(const int &id) {
  images_repository->loadImage(id);
}

} // namespace summit
