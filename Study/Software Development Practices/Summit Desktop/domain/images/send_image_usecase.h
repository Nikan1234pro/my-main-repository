#ifndef SEND_IMAGE_USECASE_H
#define SEND_IMAGE_USECASE_H

#include "data/images/images_data_source.h"
#include "domain/common/usecase.h"
#include "model/images/image.h"

namespace summit {

class SendImageUseCase : public QObject,
                         public UseCase<ImagePointer, ImagePointer> {
  Q_OBJECT
public:
  explicit SendImageUseCase(
      const std::shared_ptr<ImagesDataSource> &images_repository);

signals:
  void finished(const ImagePointer &responce) override;
  void failed(const DomainError &error) override;

private:
  void execute(const ImagePointer &image) override;

  std::shared_ptr<ImagesDataSource> images_repository;
};

} // namespace summit

#endif // SEND_IMAGE_USECASE_H
