#include "update_profile_usecase.h"

namespace summit {

UpdateProfileUseCase::UpdateProfileUseCase(
    const std::shared_ptr<ProfileDataSource> &profile_repository)
    : profile_repository(profile_repository) {

  connect(profile_repository.get(), &ProfileDataSource::updateSuccess, this,
          [this]() { emit finished(); });
  connect(profile_repository.get(), &ProfileDataSource::updateFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void UpdateProfileUseCase::execute(const UserProfilePointer &profile) {
  profile_repository->updateProfile(profile);
}

} // namespace summit
