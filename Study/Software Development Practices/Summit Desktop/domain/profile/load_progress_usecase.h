#ifndef LOADPROGRESSUSECASE_H
#define LOADPROGRESSUSECASE_H

#include "data/profile/progress_data_source.h"
#include "domain/common/usecase.h"

namespace summit {

class LoadProgressUseCase : public QObject,
                            public UseCase<QDate, UserProgressList> {
  Q_OBJECT
public:
  using RequestType = QDate;
  using ResponseType = UserProgressList;

  explicit LoadProgressUseCase(
      const std::shared_ptr<ProgressDataSource> &progress_repository);

signals:
  void finished(const UserProgressList &progress) override;
  void failed(const DomainError &error) override;

private:
  void execute(const QDate &date) override;

  std::shared_ptr<ProgressDataSource> progress_repository;
};

} // namespace summit

#endif // LOADPROGRESSUSECASE_H
