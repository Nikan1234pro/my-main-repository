#ifndef AUTHENTICATIONDATA_H
#define AUTHENTICATIONDATA_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class AuthenticationData {
  JSON_META_ACCESS(AuthenticationData)

  JSON_ATTRIBUTE(json::types::String, username);
  JSON_ATTRIBUTE(json::types::String, password);

public:
  AuthenticationData();
};

} // namespace summit

ADD_JSON_PROPERTIES(summit::AuthenticationData,
                    /* Properties */
                    JSON_PROPERTY(summit::AuthenticationData, username),
                    JSON_PROPERTY(summit::AuthenticationData, password))

#endif // AUTHENTICATIONDATA_H
