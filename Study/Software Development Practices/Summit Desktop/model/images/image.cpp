#include "image.h"

#include <QBuffer>
#include <QImageReader>
#include <QPainter>

namespace summit {

ImageWrapper::ImageWrapper(const ImageJsonAdapter &adapter) {
  pixmap.loadFromData(QByteArray::fromBase64(adapter.get_content().toUtf8()));
  if (pixmap.isNull()) {
    qDebug() << "Failed to load image";
  }
  id = adapter.get_id();
}

ImageWrapper::ImageWrapper(const QPixmap &image) : pixmap(image) {}

ImageWrapper::ImageWrapper(QPixmap &&image) : pixmap(std::move(image)) {}

ImageWrapper::ImageWrapper(QByteArray binary) {
  QBuffer buffer(&binary);
  QImageReader reader(&buffer);

  auto temp = reader.read();
  if (temp.isNull()) {
    qDebug() << "Failed to load image";
  }
  pixmap = QPixmap::fromImage(temp);
}

ImageWrapper ImageWrapper::getScaled(const QSize &size,
                                     Qt::AspectRatioMode aspect_mode,
                                     Qt::TransformationMode mode) const {
  return ImageWrapper(pixmap.scaled(size, aspect_mode, mode));
}

ImageWrapper ImageWrapper::fit(const QSize &size) const {
  QRect rect;
  rect.setWidth(size.width());
  rect.setHeight(size.height());
  rect.moveCenter(pixmap.rect().center());

  return ImageWrapper(pixmap.copy(rect));
}

int ImageWrapper::getImageId() const { return id; }

void ImageWrapper::setImageId(int id) { this->id = id; }

ImageJsonAdapter ImageWrapper::makeAdapter() const {
  QImage image = pixmap.toImage();
  QByteArray byteArray;

  QBuffer buffer(&byteArray);
  image.save(&buffer, "PNG");

  ImageJsonAdapter adapter;
  adapter.set_id(id);
  adapter.set_content(QString::fromLatin1(byteArray.toBase64().data()));
  return adapter;
}

void displayCentered(QPainter &painter, const ImageWrapper &image_wrapper) {

  auto &image = image_wrapper.getAsPixmap();

  /* Move window */
  QRect visible_area = painter.window();
  visible_area.moveCenter(image.rect().center());
  painter.setWindow(visible_area);
  painter.drawPixmap(0, 0, image);
}

void displayCentered(QPainter &painter, const QSize &size,
                     const ImageWrapper &image_wrapper) {
  auto image = image_wrapper.getAsPixmap().scaled(
      size, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

  /* Move window */
  QRect visible_area = painter.window();
  visible_area.moveCenter(image.rect().center());
  painter.setWindow(visible_area);
  painter.drawPixmap(0, 0, image);
}

ImagePointer fromFile(const QString &filename) {
  QPixmap pixmap(filename);
  if (pixmap.isNull()) {
    return nullptr;
  }
  return ImagePointer::create(std::move(pixmap));
}

} // namespace summit
