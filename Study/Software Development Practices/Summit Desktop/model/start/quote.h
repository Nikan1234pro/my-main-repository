#ifndef QUOTE_H
#define QUOTE_H

#include "model/images/image.h"

namespace summit {

class Quote {
  ImagePointer image;
  QString text;

public:
  Quote() = default;

  Quote(Quote &&other) = default;
  Quote(const Quote &other) = default;

  Quote &operator=(const Quote &other) = default;
  Quote &operator=(Quote &&other) = default;

  Quote(const ImagePointer &image, const QString &text);

  void setImage(const ImagePointer &image);
  const ImagePointer &getImage() const;

  void setText(const QString &text);
  void setText(QString &&text);
  const QString &getText() const;
};

} // namespace summit
#endif // QUOTE_H
