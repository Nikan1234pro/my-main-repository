#ifndef USERPROFILE_H
#define USERPROFILE_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class UserSettings {
  JSON_META_ACCESS(UserSettings)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::Integer, profileId)
  JSON_ATTRIBUTE(json::types::Integer, goalsScreenBackgroundId)
  JSON_ATTRIBUTE(json::types::Integer, ideasScreenBackgroundId)
  JSON_ATTRIBUTE(json::types::Integer, profileScreenBackgroundId)
  JSON_ATTRIBUTE(json::types::Integer, nutritionScreenBackgroundId)
  JSON_ATTRIBUTE(json::types::Integer, trainingScreenBackgroundId)
  JSON_ATTRIBUTE(json::types::Integer, welcomeScreenBackgroundId)

public:
  UserSettings();
};

class UserProfile {
  JSON_META_ACCESS(UserProfile)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, email)
  JSON_ATTRIBUTE(json::types::Integer, age)
  JSON_ATTRIBUTE(json::types::Integer, imageId)
  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, surname)
  JSON_ATTRIBUTE(json::types::Integer, userId)
  JSON_ATTRIBUTE(UserSettings, settingsDto)

public:
  UserProfile();
};

using UserProfilePointer = QSharedPointer<UserProfile>;

} // namespace summit

ADD_JSON_PROPERTIES(
    summit::UserSettings,
    /* Properties */
    JSON_PROPERTY(summit::UserSettings, id),
    JSON_PROPERTY(summit::UserSettings, profileId),
    JSON_PROPERTY(summit::UserSettings, goalsScreenBackgroundId),
    JSON_PROPERTY(summit::UserSettings, ideasScreenBackgroundId),
    JSON_PROPERTY(summit::UserSettings, profileScreenBackgroundId),
    JSON_PROPERTY(summit::UserSettings, nutritionScreenBackgroundId),
    JSON_PROPERTY(summit::UserSettings, trainingScreenBackgroundId),
    JSON_PROPERTY(summit::UserSettings, welcomeScreenBackgroundId))

ADD_JSON_PROPERTIES(summit::UserProfile,
                    /* Properties */
                    JSON_PROPERTY(summit::UserProfile, id),
                    JSON_PROPERTY(summit::UserProfile, email),
                    JSON_PROPERTY(summit::UserProfile, age),
                    JSON_PROPERTY(summit::UserProfile, imageId),
                    JSON_PROPERTY(summit::UserProfile, name),
                    JSON_PROPERTY(summit::UserProfile, surname),
                    JSON_PROPERTY(summit::UserProfile, userId),
                    JSON_PROPERTY(summit::UserProfile, settingsDto))

#endif // USERPROFILE_H
