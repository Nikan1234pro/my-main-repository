#ifndef USER_PROGRESS_H
#define USER_PROGRESS_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class UserProgress {
  JSON_META_ACCESS(UserProgress)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::Integer, profileId)
  JSON_ATTRIBUTE(json::types::Integer, userId)
  JSON_ATTRIBUTE(json::types::String, date)
  JSON_ATTRIBUTE(json::types::Integer, imageId)

public:
  UserProgress();
};

using UserProgressPointer = QSharedPointer<UserProgress>;
using UserProgressList = QList<UserProgress>;

} // namespace summit

ADD_JSON_PROPERTIES(summit::UserProgress,
                    /* Properties */
                    JSON_PROPERTY(summit::UserProgress, id),
                    JSON_PROPERTY(summit::UserProgress, profileId),
                    JSON_PROPERTY(summit::UserProgress, userId),
                    JSON_PROPERTY(summit::UserProgress, date),
                    JSON_PROPERTY(summit::UserProgress, imageId))

#endif // USER_PROGRESS_H
