#ifndef TRAINING_H
#define TRAINING_H

#include "model/exercises/exercise.h"

namespace summit {

class TrainingSchedule {
  JSON_META_ACCESS(TrainingSchedule)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, date)
  JSON_ATTRIBUTE(json::types::Integer, trainingId)

public:
  TrainingSchedule();
};

class Training {
  JSON_META_ACCESS(Training)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, cycle)
  JSON_ATTRIBUTE(json::types::Vector<Exercise>, exercises)
  JSON_ATTRIBUTE(json::types::Vector<TrainingSchedule>, trainingSchedules)

public:
  Training();
};

using TrainingList = QList<Training>;

} // namespace summit

ADD_JSON_PROPERTIES(summit::TrainingSchedule,
                    /* Properties */
                    JSON_PROPERTY(summit::TrainingSchedule, id),
                    JSON_PROPERTY(summit::TrainingSchedule, date),
                    JSON_PROPERTY(summit::TrainingSchedule, trainingId))

ADD_JSON_PROPERTIES(summit::Training,
                    /* Properties */
                    JSON_PROPERTY(summit::Training, id),
                    JSON_PROPERTY(summit::Training, name),
                    JSON_PROPERTY(summit::Training, cycle),
                    JSON_PROPERTY(summit::Training, exercises),
                    JSON_PROPERTY(summit::Training, trainingSchedules))

#endif // TRAINING_H
