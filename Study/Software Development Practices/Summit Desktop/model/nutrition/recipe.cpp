#include "recipe.h"

namespace summit {

Ingredient::Ingredient() = default;

Recipe::Recipe() = default;

} // namespace summit
