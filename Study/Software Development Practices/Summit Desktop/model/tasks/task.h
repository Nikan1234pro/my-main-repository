#ifndef TASK_H
#define TASK_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class Task {
  JSON_META_ACCESS(Task)

  JSON_ATTRIBUTE(json::types::Integer, id);
  JSON_ATTRIBUTE(json::types::String, name);
  JSON_ATTRIBUTE(json::types::String, description);
  JSON_ATTRIBUTE(json::types::String, stage);

public:
  Task();
};

} // namespace summit

ADD_JSON_PROPERTIES(summit::Task,
                    /* Prperties */
                    JSON_PROPERTY(summit::Task, id),
                    JSON_PROPERTY(summit::Task, name),
                    JSON_PROPERTY(summit::Task, description),
                    JSON_PROPERTY(summit::Task, stage))

#endif // TASK_H
