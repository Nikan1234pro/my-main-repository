#include <QApplication>
#include <QDebug>

#include "presentation/common/font/font.h"
#include "presentation/entry/entry_view.h"

using namespace summit;

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  a.setFont({DEFAULT_HEADER_FONT_NAME, 20});

  EntryView w;
  w.setMinimumSize({800, 600});
  w.show();

  return a.exec();
}
