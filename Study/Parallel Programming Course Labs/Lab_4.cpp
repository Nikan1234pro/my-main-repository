#include <iostream>
#include <cstdlib>
#include <mpi.h>
#include <cmath>
#define in 320
#define jn 320
#define kn 320
#define a 100000
#define EPS 10e-8
double Dx = 2.0;
double Dy = 2.0;
double Dz = 2.0;

int L0 = 1;
int L1 = 0;

double hx = Dx / in;
double hy = Dy / jn;
double hz = Dz / kn;
double owy = hy * hy;
double owz = hz * hz;
double owx = hx * hx;

double c = 2 / owx + 2 / owy + 2 / owz + a;
double* F[2];
double* buffer[2];
double Fi, Fj, Fk, F1;
int I, J, K;
int tmpf, flag;

double Fresh(double x, double y, double z) {
	double res;
	res = x * x + y * y + z * z;
	return res;
}

double Ro(double x, double y, double z) {
	double d;
	d = 6.0 - a * (x * x + y * y + z * z);
	return d;
}

void Inic(int* lines, int* offset, int rank) {
	for (int i = 0, start = offset[rank]; i < I; ++i, ++start)
		for (int j = 0; j < J; ++j)
			for (int k = 0; k < K; k++)
			{
				//F[L0][i][j][k] equivalent to F[L0][i * J * K + j * K + k]
				if ((start != 0) && (j != 0) && (k != 0) && (start != in) && (j != jn) && (k != kn)) {
					F[0][i * J * K + j * K + k] = 0.0;
					F[1][i * J * K + j * K + k] = 0.0;
				}
				else {
					F[0][i * J * K + j * K + k] = Fresh(start * hx, j * hy, k * hz);
					F[1][i * J * K + j * K + k] = Fresh(start * hx, j * hy, k * hz);
				}
			}
}

void process_center(int* lines, int* offsets, int rank) {
	for (int i = 1; i < I - 1; ++i)
		for (int j = 1; j < J - 1; j++)
			for (int k = 1; k < K - 1; ++k) {
				Fi = (F[L0][(i + 1) * J * K + j * K + k] + F[L0][(i - 1) * J * K + j * K + k]) / owx;
				Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
				Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
				F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offsets[rank]) * hx, j * hy, k * hz)) / c;
				if (fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[rank]) * hx, j * hy, k * hz)) > EPS)
					flag = 0;
			}
}

void process_bound(int* lines, int* offsets, int rank, int size) {
	for (int j = 1; j < J - 1; ++j)
		for (int k = 1; k < K - 1; ++k) {
			if (rank != 0) { //i = 0
				Fi = (F[L0][J * K + j * K + k] + buffer[0][j * K + k]) / owx;
				Fj = (F[L0][(j + 1) * K + k] + F[L0][(j - 1) * K + k]) / owy;
				Fk = (F[L0][j * K + (k + 1)] + F[L0][j * K + (k - 1)]) / owz;
				F[L1][j * K + k] = (Fi + Fj + Fk - Ro(offsets[rank] * hx, j * hy, k * hz)) / c;
				if (fabs(F[L1][j * K + k] - Fresh(offsets[rank] * hx, j * hy, k * hz)) > EPS)
					flag = 0;
			}
			if (rank != size - 1) {
				int i = lines[rank] - 1;
				Fi = (buffer[1][j * K + k] + F[L0][(i - 1) * J * K + j * K + k]) / owx;
				Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
				Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
				F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offsets[rank]) * hx, j * hy, k * hz)) / c;
				if (fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[rank]) * hx, j * hy, k * hz)) > EPS)
					flag = 0;
			}
		}
}

void max_difference(int* lines, int* offsets, int rank) {
	double max = 0.0;
	double tmpM = 0.0;
	for (int i = 1; i < I - 1; ++i) {
		for (int j = 1; j < J - 1; ++j) {
			for (int k = 1; k < K - 1; ++k)
				if ((F1 = fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[rank]) * hx, j * hy, k * hz))) > max)
					max = F1;
		}
	}
	MPI_Allreduce(&max, &tmpM, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if (rank == 0)
		std::cout << "Max differ = " << tmpM << std::endl;
}

int main(int argc, char** argv) {
	int size, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int* lines = new int[size]();
	int* offsets = new int[size]();
	int height = in + 1;
	int tmp = size - (height % size);
	for (int i = 0, cur_line = 0; i < size; ++i) {
		lines[i] = (i < tmp) ? (height / size) : (height / size + 1);
		offsets[i] = cur_line;
		cur_line += lines[i];
	}
	I = lines[rank];
	J = jn + 1;
	K = kn + 1;
	buffer[0] = new double[K * J](); //buffer[0] - down bound of block in prev process
	buffer[1] = new double[K * J](); //buffer[1] - up bound of block in next process
	F[0] = new double[I * J * K];
	F[1] = new double[I * J * K];

	Inic(lines, offsets, rank);
	MPI_Request send_req[2];
	MPI_Request recv_req[2];
	double start = MPI_Wtime();
	int it = 0;

	do {
		it++;
		flag = 1;
		L0 = 1 - L0;
		L1 = 1 - L1;
		if (rank != 0) {
			//down bound
			MPI_Isend(&(F[L0][0]), K * J, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &send_req[0]);
			MPI_Irecv(buffer[0], K * J, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, &recv_req[1]);
		}
		if (rank != size - 1) {
			//up bound
			MPI_Isend(&(F[L0][(lines[rank] - 1) * J * K]), K * J, MPI_DOUBLE, rank + 1, 1, MPI_COMM_WORLD, &send_req[1]);
			MPI_Irecv(buffer[1], K * J, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &recv_req[0]);
		}
		//calculate central part of blocks
		process_center(lines, offsets, rank);
		//Halt!
		if (rank != 0) {
			MPI_Wait(&recv_req[1], MPI_STATUS_IGNORE);
			MPI_Wait(&send_req[0], MPI_STATUS_IGNORE);
		}
		if (rank != size - 1) {
			MPI_Wait(&recv_req[0], MPI_STATUS_IGNORE);
			MPI_Wait(&send_req[1], MPI_STATUS_IGNORE);
		}
		//calculate bounds of blocks
		process_bound(lines, offsets, rank, size);
		//if flag = 0 in one of process
		MPI_Allreduce(&flag, &tmpf, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
		flag = tmpf;
	} while (flag == 0);
	
	max_difference(lines, offsets, rank);
	if (!rank)
		std::cout << MPI_Wtime() - start << "\nIterations = " << it << std::endl;

	delete[] F[0];
	delete[] F[1];
	delete[] buffer[0];
	delete[] buffer[1];
	delete[] lines;
	delete[] offsets;
	MPI_Finalize();
	return EXIT_SUCCESS;
}