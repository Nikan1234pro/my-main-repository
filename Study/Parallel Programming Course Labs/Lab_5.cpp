#include <iostream>
#include <mpi.h>
#include <pthread.h>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <climits>
#include <cstdarg>
#include <unistd.h>
#include <stddef.h>
#define DEBUG 1
#define ITER_COUNT 50
#define TASK_COUNT 60
#define ANSWER_TAG 0
#define REQUEST_TAG 1
#define TASK_TAG 2

MPI_Datatype Task_Info_Type;
pthread_mutex_t task_mutex;
pthread_t control_thread;

int rank;
int size;
int cur_iteration = 0;
int cur_task = 0;
int delegations = 0;

struct Task {
	int owner;
	int count;
	double step;
	Task(int o, int c, int s) : owner(o), count(c), step(s) {
	}
	Task() : owner(0), count(0), step(0.0) {
	}
	double do_task() {
		if (DEBUG)
			std::cerr << "Process " << rank << ": Do task. Owner: " << owner << std::endl;
		double sum = 0;
		double tmp = 12;
		for (int i = 0; i < count; ++i) {
			tmp = std::log10(tmp);
			tmp = std::pow(10.0, tmp);
			sum += tmp;
			tmp *= step;
		}
		return sum;
	}
};

std::vector<Task> tasks;


int get_tasks(int other_rank) {
	if (rank == other_rank)
		return EXIT_FAILURE;
	if (DEBUG)
		std::cerr << "Process " << rank << ": Try to get task from process " << other_rank << std::endl;
	int request = 1;
	int answer;
	Task task;
	MPI_Send(&request, 1, MPI_INT, other_rank, REQUEST_TAG, MPI_COMM_WORLD);
	MPI_Recv(&answer, 1, MPI_INT, other_rank, ANSWER_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	if (answer == 0)
		return EXIT_FAILURE;
	MPI_Recv(&task, 1, Task_Info_Type, other_rank, TASK_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	pthread_mutex_lock(&task_mutex);
	tasks.push_back(task);
	pthread_mutex_unlock(&task_mutex);
	if (DEBUG)
		std::cerr << "Process " << rank << ": got task from process " << other_rank << std::endl;
	return EXIT_SUCCESS;
}


void generate_tasks(int task_count) {
	if (DEBUG)
		std::cerr << "Process " << rank << ": Generating " << task_count << " tasks!" << std::endl;
	tasks.clear();
	for (int i = 0; i < task_count; i++) {
		if (rank % 3 == 0) {
			Task task(rank, 1000000, 0.3);
			tasks.push_back(task);
		}
		else if (rank % 3 == 1) {
			Task task(rank, 500, 0.3);
			tasks.push_back(task);
		}
		else {
			Task task(rank, 12000, 0.3);
			tasks.push_back(task);
		}
	}
}


void create_task_info_type() {
	int blocklen[] = { 1, 1, 1 };
	MPI_Datatype types[] = { MPI_INT, MPI_INT, MPI_DOUBLE };
	MPI_Aint offsets[3];
	offsets[0] = offsetof(Task, owner);
	offsets[1] = offsetof(Task, count);
	offsets[2] = offsetof(Task, step);
	MPI_Type_create_struct(3, blocklen, offsets, types, &Task_Info_Type);
	MPI_Type_commit(&Task_Info_Type);
}


void* control_thread_function(void* p) {
	while (cur_iteration < ITER_COUNT) {
		MPI_Status st;
		int res;
		MPI_Recv(&res, 1, MPI_INT, MPI_ANY_SOURCE, REQUEST_TAG, MPI_COMM_WORLD, &st);
		if (res == 0)
			break;
		pthread_mutex_lock(&task_mutex);
		if (cur_task < tasks.size()) {
			int answer = 1;
			MPI_Send(&answer, 1, MPI_INT, st.MPI_SOURCE, ANSWER_TAG, MPI_COMM_WORLD);
			MPI_Send(&tasks.back(), 1, Task_Info_Type, st.MPI_SOURCE, TASK_TAG, MPI_COMM_WORLD);
			delegations++;
			tasks.pop_back();
		}
		else {
			int answer = 0;
			MPI_Send(&answer, 1, MPI_INT, st.MPI_SOURCE, ANSWER_TAG, MPI_COMM_WORLD);
		}
		pthread_mutex_unlock(&task_mutex);
	}
	return NULL;
}


void task_resolver() {
	int count = TASK_COUNT / size;
	if (rank >= size - (TASK_COUNT % size))
		count++;
	for (cur_iteration = 0; cur_iteration < ITER_COUNT; cur_iteration++) {
		pthread_mutex_lock(&task_mutex);
		generate_tasks(count);
		cur_task = 0;
		int i = 0;
		while (i < size) {
			while (cur_task < tasks.size()) {
				cur_task++;
				pthread_mutex_unlock(&task_mutex);
				double result = tasks[cur_task - 1].do_task();
				if (DEBUG)
					std::cerr << "Process " << rank << ": Result = " << result << std::endl;
				pthread_mutex_lock(&task_mutex);
			}
			pthread_mutex_unlock(&task_mutex);
			int other_rank = (rank + i) % size;
			if (get_tasks(other_rank) != EXIT_SUCCESS) {
				i++;
				continue;
			}
			pthread_mutex_lock(&task_mutex);
		}
		pthread_mutex_unlock(&task_mutex);
		MPI_Barrier(MPI_COMM_WORLD);
	}
	if (DEBUG)
		std::cerr << "Process " << rank << ": Resolver Thread was closed!" << std::endl;
	int request = 0;
	MPI_Send(&request, 1, MPI_INT, rank, REQUEST_TAG, MPI_COMM_WORLD);
}


int make_thread() {
	pthread_attr_t attribute;
	if (pthread_attr_init(&attribute) != EXIT_SUCCESS) {
		std::cerr << "Error in initialization attribute!" << std::endl;
		return EXIT_FAILURE;
	}
	if (pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_JOINABLE) != EXIT_SUCCESS) {
		std::cerr << "Error in setting attribute!" << std::endl;
		return EXIT_FAILURE;
	}
	if (pthread_create(&control_thread, &attribute, &control_thread_function, NULL) != EXIT_SUCCESS) {
		std::cerr << "Error in thread creating" << std::endl;
		return EXIT_FAILURE;
	}
	pthread_attr_destroy(&attribute);
	task_resolver();
	if (pthread_join(control_thread, NULL) != EXIT_SUCCESS) {
		std::cerr << "Couldn't join thread!" << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


int main(int argc, char** argv) {
	int provided;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	std::cout << "Count of processes: " << size << std::endl;

	if (provided != MPI_THREAD_MULTIPLE) {
		std::cerr << "Error in MPI_Init" << std::endl;
		return EXIT_FAILURE;
	}

	std::srand(std::time(NULL));
	pthread_mutex_init(&task_mutex, NULL);
	create_task_info_type();
	double begin = MPI_Wtime();
	if (make_thread() != EXIT_SUCCESS) {
		pthread_mutex_destroy(&task_mutex);
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	double end = MPI_Wtime();
	if (rank == 0)
		std::cout << "Time = " << end - begin << std::endl;

	pthread_mutex_destroy(&task_mutex);
	std::cout << "Process " << rank << " : Count of delegations:" << delegations << std::endl;
	MPI_Finalize();
	return EXIT_SUCCESS;
}