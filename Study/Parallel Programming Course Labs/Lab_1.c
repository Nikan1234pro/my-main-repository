#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

const double EPS = 10e-9;
const int n = 12000;

double Length(double* a, int n)
{
	double tmp = 0.0;
	for (int i = 0; i < n; i++)
		tmp += a[i] * a[i];
	return sqrt(tmp);
}

void Mul(double* A, double* x, double* res, int lines, int n)
{
	for (int i = 0; i < lines; i++)
	{
		double sum = 0;
		for (int j = 0; j < n; j++)
			sum += A[i * n + j] * x[j];
		res[i] = sum;
	}
}

void Sub(double* a, double* b, double* res, int n, int start)
{
	for (int i = 0; i < n; i++)
		res[i] = a[i] - b[start + i];
}

int initialize(double** A, double** x, double** b, int** lines, int** displs, int threads_size, int thread_rank, int n)
{
	int start = 0;
	int dist = 0;
	int tmp = threads_size - (n % threads_size);
	*lines = (int*)malloc(sizeof(int) * threads_size);
	*displs = (int*)malloc(sizeof(int) * threads_size);
	for (int i = 0; i < threads_size; ++i)
	{
		(*lines)[i] = (i < tmp) ? (n / threads_size) : (n / threads_size + 1);
		(*displs)[i] = dist;
		if (i < thread_rank)
			start = dist;
		dist += (*lines)[i];
	}
	*A = (double*)malloc(sizeof(double) * (*lines)[thread_rank] * n);
	for (int i = 0; i < (*lines)[thread_rank]; i++)
		for (int j = 0; j < n; j++)
			(*A)[i * n + j] = (i + start == j) ? 2.0 : 1.0;
	*x = (double*)calloc(sizeof(double), n);
	*b = (double*)malloc(sizeof(double) * n);
	for (int i = 0; i < n; i++)
		(*b)[i] = n + 1;
	return start;
}

int main(int argc, char** argv)
{
	double begin = 0, end = 0;
	double sum = 0.0;
	double* A = NULL;
	double* x = NULL;
	double* b = NULL;
	int* lines = NULL;
	int* displs = NULL;
	int size;
	int rank;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int lines_per_thread = n / size;
	int start = initialize(&A, &x, &b, &lines, &displs, size, rank, n);
	double* tmp = (double*)malloc(sizeof(double) * lines[rank]);
	begin = MPI_Wtime();
	double Len_b = Length(b, n);
	
	int flag = 1;
	int counter = 0;
	
	while (flag)
	{
		counter++;
		Mul(A, x, tmp, lines[rank], n);
		Sub(tmp, b, tmp, lines[rank], start);
		double Ans = 0.0;
		for (int i = 0; i < lines[rank]; i++)
		{
			Ans += tmp[i] * tmp[i];
			tmp[i] = x[start + i] - (10e-6) * tmp[i];
		}
		MPI_Allgatherv(tmp, lines[rank], MPI_DOUBLE, x, lines, displs, MPI_DOUBLE, MPI_COMM_WORLD);
		MPI_Allreduce(&Ans, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		if (sqrt(sum) / Len_b < EPS)
			flag = 0;
	}
	end = MPI_Wtime();
	if (!rank) {
		printf("Time = %.8f sec.\n", end - begin);
		printf("Count = %d\n", counter);
	}

	free(A);
	free(x);
	free(b);
	free(tmp);
	free(lines);
	free(displs);
	MPI_Finalize();
	return 0;
}