#include <iostream>
#include <mpi.h>

double* mul(double* partA, double* partB, int M, int partA_size, int partB_size) {
	double* tmp = new double[partA_size * partB_size]();
	for (int i = 0; i < partA_size; i++)
		for (int j = 0; j < partB_size; j++)
			for (int k = 0; k < M; k++)
				tmp[i * partB_size + j] += partA[i * M + k] * partB[k * partB_size + j];
	return tmp;
}

int main(int argc, char** argv) {
	const int N = 5000;
	const int M = 5000;
	const int K = 5000;

	int world_rank;
	int world_size;
	int rows_size;
	int cols_size;
	double* Answer;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	if (world_size != 16 && world_size != 4 && world_size != 1) {
		if (world_rank == 0)
			std::cerr << "Invalid size! Must be 4 or 16" << std::endl;
		return 1;
	}

	int dims[] = { 0, 0 };

	MPI_Dims_create(world_size, 2, dims);
	rows_size = dims[0];
	cols_size = dims[1];

	if (world_rank == 0)
		std::cout << "p1 = " << rows_size << " p2 = " << cols_size << std::endl;

	MPI_Comm row_comm;
	MPI_Comm col_comm;

	int row_key = world_rank / rows_size;
	int col_key = world_rank % cols_size;

	MPI_Comm_split(MPI_COMM_WORLD, row_key, world_rank, &row_comm);
	MPI_Comm_split(MPI_COMM_WORLD, col_key, world_rank, &col_comm);

	int partA_size = N / rows_size;
	int partB_size = K / cols_size;
	double* partA = new double[partA_size * M];
	double* partB = new double[partB_size * M];
	double* A, * B;

	int lb = 0;
	int extent = partB_size * sizeof(double);
	
	MPI_Datatype vector;
	MPI_Datatype sized_vector;
	MPI_Type_vector(M, partB_size, K, MPI_DOUBLE, &vector);
	MPI_Type_create_resized(vector, lb, extent, &sized_vector);
	MPI_Type_commit(&sized_vector);

	if (world_rank == 0) {
		Answer = new double[N * K]();
		A = new double[N * M];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				A[i * M + j] = (i == j) ? 12.0 : 0;
		B = new double[M * K];
		for (int i = 0; i < M; i++)
			for (int j = 0; j < K; j++)
				B[i * K + j] = (i == j) ? i + j : 0;
	}

	double begin = MPI_Wtime();
	if (col_key == 0) {
		MPI_Scatter(A, partA_size * M, MPI_DOUBLE, partA, partA_size * M, MPI_DOUBLE, 0, col_comm);
	}
	if (row_key == 0) {
		MPI_Scatter(B, 1, sized_vector, partB, partB_size * M, MPI_DOUBLE, 0, row_comm);
	}

	MPI_Bcast(partA, partA_size * M, MPI_DOUBLE, 0, row_comm);
	MPI_Bcast(partB, partB_size * M, MPI_DOUBLE, 0, col_comm);
	
	double* tmp = mul(partA, partB, M, partA_size, partB_size);
	if (world_rank != 0) {
		for (int i = 0; i < partA_size; i++)
			MPI_Send(tmp + i * partB_size, partB_size, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD);
	}
	else {
		for (int line = 0; line < N; line++) {
			int start = (line / partA_size) * cols_size;
			for (int rank = start; rank < start + cols_size; rank++) {
				if (rank != 0)
					MPI_Recv(Answer + line * K + (rank % cols_size) * partB_size, partB_size,
						MPI_DOUBLE, rank, 123, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
		}
		for (int i = 0; i < partA_size; i++)
			for (int j = 0; j < partB_size; j++)
				Answer[i * K + j] = tmp[i * partB_size + j];
		double end = MPI_Wtime();
		std::cout << "Time = " << end - begin << std::endl;
		delete[] A;
		delete[] B;
		delete[] Answer;
	}
	
	MPI_Comm_free(&row_comm);
	MPI_Comm_free(&col_comm);
	MPI_Finalize();
	return 0;
}