#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <omp.h>
#include <stdio.h>

const int N = 12000;
const double eps = 10e-9;

int main(int argc, char** argv)
{
	double* A = new double[N * N];
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			A[i * N + j] = (i == j) ? 2.0 : 1.0;

	double* b = new double[N];
	for (int i = 0; i < N; i++)
		b[i] = N + 1;

	double* x = new double[N]();
	double* tmp = new double[N]();

	double Ans = 0.0;
	double Len_b = 0;
	int flag = 1;
	double start = omp_get_wtime();
	
	#pragma omp parallel
	{
		#pragma omp for reduction(+:Len_b)
		for (int i = 0; i < N; ++i)
			Len_b += b[i] * b[i];

		#pragma omp single
			Len_b = sqrt(Len_b);
			
		while (flag)
		{
			#pragma omp for reduction(+:Ans)
			for (int i = 0; i < N; i++)
			{
				double sum = 0.0;
				for (int j = 0; j < N; j++)
					sum += A[i * N + j] * x[j];
				sum -= b[i];
				Ans += sum * sum;
				tmp[i] = x[i] - (10e-6) * sum;
			}
			#pragma parallel for
			for (int i = 0; i < N; i++)
				x[i] = tmp[i];

			#pragma omp single
			{
				if (sqrt(Ans) / Len_b < eps)
					flag = 0;
				Ans = 0.0;
			}
		}

		double finish = omp_get_wtime();
		if (omp_get_thread_num() == 0)
			std::cout << "Time: " << (finish - start) << std::endl;
	}

	delete[] tmp;
	delete[] x;
	delete[] b;
	delete[] A;
	return 0;
}