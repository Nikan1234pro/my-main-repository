package server;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Arrays;

public class Connection extends Thread {
    private Socket client;
    private Server server;
    private File directory;

    private final long UPDATE_INTERVAL = 3000;
    private final int  BUF_SIZE = Protocol.MAX_NAME_LENGTH;

    public Connection(Socket cli, Server srv, File dir) {
        client = cli;
        server = srv;
        directory = dir;
        start();
    }

    @Override
    public void run() {
        File output_file = null;
        try (DataInputStream recv_from = new DataInputStream(client.getInputStream());
             DataOutputStream send_to = new DataOutputStream(client.getOutputStream())) {

            /* Create file with data from client */
            output_file = createFile(recv_from);
            if (output_file == null) {
                return;
            }

            /* Get file */
            if (fillFile(output_file, recv_from)) {
                send_to.write(Protocol.ACK);
                System.out.println(Colors.ANSI_CYAN +
                        output_file.getName() + "\t" +
                        "OK" + Colors.ANSI_RESET);
            }
            else {
                send_to.write(Protocol.NEG);
                System.out.println(Colors.ANSI_RED +
                        output_file.getName() + "\t" +
                        "FAIL" + Colors.ANSI_RESET);
            }
        }
        catch (final IOException e) {
            System.err.println("Error: " + e.getMessage());
            if (output_file != null) {
                output_file.delete();
            }
        }
        finally {
            try {
                if (!client.isClosed())
                    client.close();
                server.destroy(client);
            } catch (IOException e) {
                /* Ignore */
            }
        }
    }

    private File createFile(DataInputStream stream) throws IOException {
        byte[] buf = new byte[Protocol.MAX_NAME_LENGTH];

        /* Get filename size */
        int name_length = stream.readInt();
        if (name_length > Protocol.MAX_NAME_LENGTH)
            return null;

        /* Get filename */
        stream.readFully(buf, 0, name_length);

        /* Create file */
        File tmp = new File(new String(
                Arrays.copyOf(buf, name_length),
                StandardCharsets.UTF_8));
        return new File(directory, tmp.getName());
    }

    private boolean fillFile(File file, DataInputStream stream) throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        DecimalFormat df = new DecimalFormat("0.00");

        /* Get expect size of file */
        long expect_file_size = stream.readLong();
        long real_file_size = 0L;
        long byte_count = 0L;

        /* Writer must closing regardless of whether an exception is thrown */
        try (FileOutputStream output = new FileOutputStream(file)) {
            long start_time = System.currentTimeMillis();
            long ticks = System.currentTimeMillis();
            while (real_file_size < expect_file_size) {
                /* Count loading speed */
                if (System.currentTimeMillis() - ticks > UPDATE_INTERVAL) {
                    double dt = (System.currentTimeMillis() - ticks) / 1000.0;
                    double all_time = (System.currentTimeMillis() - start_time) / 1000.0;

                    /* Print speed to screen */
                    System.out.println(file.getName() + "\t" +
                            df.format((double)real_file_size / expect_file_size * 100) + "%\t" +
                            "Current speed: " + df.format(byte_count / 1024.0f / dt) + " KByte/s" + "\t" +
                            "Average speed: " + df.format(real_file_size / 1024.0f / all_time) + " KByte/s");
                    ticks = System.currentTimeMillis();
                    byte_count = 0;
                }

                /* Set timeout to receive  */
                long remain = UPDATE_INTERVAL - (System.currentTimeMillis() - ticks);
                if (remain > 0)
                    client.setSoTimeout((int)remain);
                try {
                    /* Get payload */
                    int payload_size = stream.read(buf, 0, buf.length);
                    real_file_size += payload_size;
                    byte_count += payload_size;

                    /* Write to file */
                    output.write(buf, 0, payload_size);
                }
                /* Check only timeout event */
                catch (final SocketTimeoutException e) {
                    /* Ignore */
                }
            }
            /* Print speed to screen */
            double dt = (System.currentTimeMillis() - start_time) / 1000.0f;
            double speed = real_file_size / 1024.0f / dt;
            System.out.println(file.getName() + "\t" +
                    df.format(100.00) + "%\t" +
                    "Current speed: " + df.format(speed) + " KByte/s" + "\t" +
                    "Average speed: " + df.format(speed) + " KByte/s");
        }
        return (real_file_size == expect_file_size);
    }
}
