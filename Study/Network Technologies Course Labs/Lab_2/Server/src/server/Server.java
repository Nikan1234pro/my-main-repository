package server;

import java.io.*;
import java.net.*;
import java.util.HashSet;

public class Server extends Thread {
    private ServerSocket server;

    private File directory;
    private static final String folder_name = "upload";

    private final HashSet<Socket> connections = new HashSet<>();


    public Server(String ip, String port) throws IOException {
        directory = new File(folder_name);
        if (!directory.exists()) {
            if (!directory.mkdirs())
                throw new IOException("Couldn't create directory");
        }
        server = new ServerSocket(Integer.parseInt(port), 0, InetAddress.getByName(ip));
        start();
    }

    @Override
    public void interrupt() {
        synchronized (connections) {
            for (Socket s : connections) {
                try {
                    s.close();
                }
                catch (final IOException e) {
                    /* Ignore */
                }
            }
        }
    }

    void destroy(Socket socket) {
        synchronized (connections) {
            connections.remove(socket);
        }
    }

    @Override
    public void run() {
        while (!server.isClosed()) {
            try {
                Socket client = server.accept();
                new Connection(client, this, directory);

                synchronized (connections) {
                    connections.add(client);
                }
            }
            catch (final IOException e) {
                System.err.println("Couldn't accept client");
            }
        }
    }
}
