import server.Server;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        try {
            Server server = new Server(args[0], args[1]);
            Runtime.getRuntime().addShutdownHook(new Thread(server::interrupt));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
