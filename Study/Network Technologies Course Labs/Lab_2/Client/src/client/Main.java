package client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Main {
    public static final int BUF_SIZE = Protocol.MAX_NAME_LENGTH;

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Wrong args count!");
            return;
        }

        /* Open socket and connect to server */
        try (Socket socket = new Socket(
                InetAddress.getByName(args[0]), Integer.parseInt(args[1]))) {

            /* Open file */
            File file = new File(args[2]);
            FileInputStream reader = new FileInputStream(file);
            byte[] filename = file.getName().getBytes(StandardCharsets.UTF_8);

            try (DataInputStream recv_from = new DataInputStream(socket.getInputStream());
                 DataOutputStream send_to = new DataOutputStream(socket.getOutputStream())) {

                /* Send info about file */
                send_to.writeInt(filename.length);
                send_to.write(filename);
                send_to.writeLong(file.length());

                byte[] buf = new byte[BUF_SIZE];
                int length;

                /* Send file */
                while ((length = reader.read(buf)) > 0) {
                    send_to.write(buf, 0, length);
                }

                /* Get answer from server */
                byte[] result = new byte[1];
                recv_from.read(result, 0, 1);

                if (Arrays.compare(result, Protocol.ACK) == 0) {
                    System.out.println("Transaction status: OK");
                }
                else {
                    System.err.println("Transaction status: FAIL");
                }
            }
        }
        catch (final FileNotFoundException e) {
            System.err.println("File not found");
            return;
        }
        catch (final IOException e) {
            System.err.println("Transaction terminated: " + e.getMessage());
        }
    }
}
