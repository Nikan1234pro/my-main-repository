package client;

public class Protocol {
    public static final int MAX_NAME_LENGTH = 4096;

    public static final byte[] ACK = { 0x00 };
    public static final byte[] NEG = { 0x01 };
}
