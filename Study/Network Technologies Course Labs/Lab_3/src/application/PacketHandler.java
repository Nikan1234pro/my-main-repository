package application;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

class PacketHandler {
    /* DATA packets - with UUID */
    static DatagramPacket generatePacket(byte type,
                                         UUID uuid,
                                         byte[] data,
                                         InetSocketAddress socketAddress) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(
                Byte.BYTES + UuidUtils.BYTES + data.length);
        byteBuffer.put(type);
        byteBuffer.put(UuidUtils.asBytes(uuid));
        byteBuffer.put(data);

        byte[] array = byteBuffer.array();
        return new DatagramPacket(array,
                array.length,
                socketAddress.getAddress(),
                socketAddress.getPort());
    }

    /* System packets - without UUID */
    static DatagramPacket generatePacket(byte type,
                                         byte[] data,
                                         InetSocketAddress socketAddress) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(
                Byte.BYTES + UuidUtils.BYTES + data.length);
        byteBuffer.put(type);
        byteBuffer.put(data);

        byte[] array = byteBuffer.array();
        return new DatagramPacket(array,
                array.length,
                socketAddress.getAddress(),
                socketAddress.getPort());
    }

    static byte[] concat(final byte[] ...arrays ) {
        int size = 0;
        for (byte[] a : arrays )
            size += a.length;

        byte[] result = new byte[size];

        int destPos = 0;
        for ( int i = 0; i < arrays.length; i++ ) {
            if ( i > 0 ) destPos += arrays[i-1].length;
            int length = arrays[i].length;
            System.arraycopy(arrays[i], 0, result, destPos, length);
        }
        return result;
    }

    static byte[]intToBytes(int value) {
        return  ByteBuffer.allocate(Integer.BYTES).putInt(value).array();
    }


    static UUID getUUID(DatagramPacket packet) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(packet.getData());
        /* Skip flag */
        byte type = byteBuffer.get();
        if (type != PacketType.DATA && type != PacketType.DATA_ACK)
            return null;
        byte[] uuidBytes = new byte[UuidUtils.BYTES];
        byteBuffer.get(uuidBytes, 0, uuidBytes.length);
        return UuidUtils.asUuid(uuidBytes);
    }

    static InetSocketAddress getSocketAddress(DatagramPacket packet)
            throws UnknownHostException{
        ByteBuffer byteBuffer = ByteBuffer.wrap(packet.getData());
        /* Skip flag */
        byte type = byteBuffer.get();
        if (type != PacketType.SYS_ANSWER)
            return null;

        byte[] addr = new byte[byteBuffer.get()];
        byteBuffer.get(addr, 0, addr.length);
        int port = byteBuffer.getInt();
        return new InetSocketAddress(InetAddress.getByAddress(addr), port);
    }


    static byte[] getMessageBytes(DatagramPacket packet) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(packet.getData());
        if (byteBuffer.get() != PacketType.DATA)
            return null;
        byteBuffer.position(Byte.BYTES + UuidUtils.BYTES);
        int length = byteBuffer.getInt();
        byte[] messageBytes = new byte[length];
        byteBuffer.get(messageBytes, 0, length);
        return messageBytes;
    }
}

class PacketType {
    /* System request packets starts with SYS_REQUEST byte*/
    static final byte SYS_REQUEST = 0x00;

    /* System request packets starts with SYS_ANSWER byte*/
    static final byte SYS_ANSWER = 0x01;

    /* Data-packets starts with DATA byte */
    static final byte DATA = 0x02;

    /* ACK-packets starts with DATA_ACK byte */
    static final byte DATA_ACK = 0x03;
}
