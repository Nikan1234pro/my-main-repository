package application;
import java.net.DatagramPacket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


class NodeStatus {
    private long lastCheckTime;

    ConcurrentHashMap<UUID, PacketData> dataPackets;
    LinkedList<DatagramPacket> systemPackets;
    UUIDStorage receivedPackets;

    public NodeStatus() {
        lastCheckTime = System.currentTimeMillis();

        dataPackets = new ConcurrentHashMap<>();
        systemPackets = new LinkedList<>();
        receivedPackets = new UUIDStorage();

    }

    public void update() {
        lastCheckTime = System.currentTimeMillis();
    }

    public long check() {
        return lastCheckTime;
    }
}

class UUIDStorage {
    private HashSet<UUID> receivedPackets;
    private  LinkedList<UUID> queue;
    private final int MAX_QUEUE_SIZE = 256;

    public UUIDStorage() {
        receivedPackets = new HashSet<>();
        queue = new LinkedList<>();
    }

    public void cleanup() {
        while (queue.size() > MAX_QUEUE_SIZE) {
            UUID uuid = queue.removeFirst();
            if (uuid != null) {
                receivedPackets.remove(uuid);
            }
        }
    }

    public boolean add(UUID uuid) {
        queue.addLast(uuid);
        return receivedPackets.add(uuid);
    }
}

class PacketData {
    DatagramPacket packet;
    private Timer timer;

    PacketData(DatagramPacket packet, long timeout) {
        this.packet = packet;
        this.timer = new Timer(timeout);
    }

    boolean readyToSend() {
        return timer.accept();
    }
}

