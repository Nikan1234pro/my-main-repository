package application;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/** Rules
 * 1. Children nodes send his parent request packet
 * This packet includes only SYS_REQUEST byte
 *
 * 2. Parent get request packet and send answer packet.
 * It includes one byte for SYS_ANSWER flag
 * If it contains 8 more bytes, then it is a reserve ip and port.
 *
 * 3. Data packet includes DATA flag, 4 bytes for UUID and data.
 *
 * 4. ACK packet includes ACK_DATA flag and 4 bytes - UUID from got packet
 */

public class Application extends Thread {
    private final String nodeName;

    private final int losses_percent;

    private final DatagramSocket socket;

    private InetSocketAddress parent = null;
    private InetSocketAddress reserve = null;
    private InetSocketAddress reserve_to_send = null;

    private final int BUF_SIZE = 4096;

    /* If node didn't answer for 8 sec, we suggest that it dead */
    private final int CHECK_TIMEOUT = 5000;

    /* Timeout to resend datagram */
    private final int SEND_TIMEOUT = 500;

    /* Timeout to send request */
    private final int REQUEST_TIMEOUT = 100;

    Timer timer;
    Random random;

    /* Contains all neighbourhood */
    private final ConcurrentHashMap<InetSocketAddress, NodeStatus> nodes;

    public Application(String name, int port, int losses_per,
                            InetAddress dest_ip, int dest_port)
            throws SocketException {
        this(name, port, losses_per);

        /* Identify parent and add it to send list */
        parent = new InetSocketAddress(dest_ip, dest_port);
        nodes.put(parent, new NodeStatus());
    }

    public Application(String name, int port, int losses_per)
            throws SocketException {
        nodeName = name;
        losses_percent = losses_per;

        socket = new DatagramSocket(port);
        nodes = new ConcurrentHashMap<>();

        timer = new Timer(REQUEST_TIMEOUT);
        random = new Random();
    }

    @Override
    public void run() {
        try {
            while (!socket.isClosed()) {
                receive();
                sendAll();
            }
        }
        catch (final SocketException e) {
            /* Ignore */
        }
        catch (final IOException e) {
            e.printStackTrace();
        }
        finally {
            socket.close();
        }
    }

    @Override
    public void interrupt() {
        socket.close();
    }

    /* Check console and put messages to send queue */
    public void listen() {
        Scanner scanner = new Scanner(System.in, "Cp866");
        try {
            while (!socket.isClosed()) {
                String to_send = nodeName + ": " + scanner.nextLine();
                sendMessage(to_send);
            }
        }
        catch (final NoSuchElementException e){
            /* Ignore */
        }
    }

    /* This function receives packets and handles it */
    private void receive() throws IOException {
        try {
            socket.setSoTimeout((int)timer.remainedToAccept() + 1);

            /* Read datagram packet */
            byte[] buffer = new byte[BUF_SIZE];
            DatagramPacket recv_packet = new DatagramPacket(
                    buffer, buffer.length);

            socket.receive(recv_packet);

            /* Artificial drop */
            if (random.nextInt(100) < losses_percent) {
                return;
            }

            /* Update node status */
            updateNodeStatus(getSocketAddress(recv_packet));

            /* Parse data from packet */
            byte[] data = recv_packet.getData();
            switch (data[0]) {
                case PacketType.SYS_REQUEST:
                    sendSystemAnswer(recv_packet);
                    break;

                case PacketType.SYS_ANSWER:
                    handleSystemAnswer(recv_packet);
                    break;

                case PacketType.DATA:
                    if (sendDataAck(recv_packet)) {
                        /* If we haven't received  it yet - print */
                        byte[] message = PacketHandler.getMessageBytes(recv_packet);
                        if (message != null)
                            System.out.println(
                                    new String(message, StandardCharsets.UTF_8));
                        resendDataPacket(recv_packet);
                    }
                    break;

                case PacketType.DATA_ACK:
                    handleDataAck(recv_packet);
                    break;
            }
        }
        catch (final SocketTimeoutException e) {
            /* Ignore it and exit */
        }
    }


    /* This function sends messages and manages nodes status */
    private void sendAll() throws IOException {
        var iterator = nodes.entrySet().iterator();

        long check_time = System.currentTimeMillis();
        /* Send request to parent every REQUEST_TIMEOUT milliseconds */
        if (timer.accept()) {
            sendRequest();
        }

        while (iterator.hasNext()) {
            var entry = iterator.next();
            NodeStatus nodeStatus = entry.getValue();
            InetSocketAddress nodeAddress = entry.getKey();

            /* First of all, check last time, when it send request */
            if (check_time - nodeStatus.check() > CHECK_TIMEOUT) {
                /* We suggest, that it dead =( */
                System.out.println("Node " + nodeAddress.getAddress()
                        + " " + nodeAddress.getPort() + " is dead");

                if (nodeAddress.equals(parent)) {
                    parent = reserve;
                    reserve = null;
                }
                if (nodeAddress.equals(reserve_to_send)) {
                    reserve_to_send = null;
                }
                iterator.remove();
                continue;
            }
            /* Send system packets */
            for (DatagramPacket packet : nodeStatus.systemPackets) {
                socket.send(packet);
            }
            /* We mustn't resend system packets */
            nodeStatus.systemPackets.clear();

            /* Send data packets*/
            for (var pair : nodeStatus.dataPackets.entrySet()) {
                if (pair.getValue().readyToSend()) {
                    socket.send(pair.getValue().packet);
                }
            }
            /* Do cleanup */
            nodeStatus.receivedPackets.cleanup();
        }
    }


    /******************** Application helpers ********************/

    private static InetSocketAddress getSocketAddress(DatagramPacket packet) {
        return  new InetSocketAddress(
                packet.getAddress(), packet.getPort());
    }

    /* We update node status only if got from it SYS_REQUEST or SYS_ANSWER */
    private void updateNodeStatus(InetSocketAddress address) {
        if (nodes.containsKey(address)) {
                nodes.get(address).update();
                return;
        }
        nodes.put(address, new NodeStatus());
    }

    private void sendMessage(String message) {
        for (var pair : nodes.entrySet()) {
            UUID uuid = UUID.randomUUID();

            byte[] messageBytes = message.getBytes(StandardCharsets.UTF_8);

            int messageLength = messageBytes.length;
            byte[] payload = PacketHandler.concat(
                    PacketHandler.intToBytes(messageLength), messageBytes);

            DatagramPacket packet = PacketHandler.generatePacket(
                    PacketType.DATA, uuid, payload, pair.getKey());

            pair.getValue().dataPackets.put(
                    uuid, new PacketData(packet, SEND_TIMEOUT));
        }
    }

    private void sendRequest() {
        if (parent == null)
            return;

        DatagramPacket request = PacketHandler.generatePacket(
                PacketType.SYS_REQUEST, new byte[0], parent);

        if (!nodes.containsKey(parent)) {
            updateNodeStatus(parent);
        }
        nodes.get(parent).systemPackets.add(request);
    }

    private void sendSystemAnswer(DatagramPacket packet) {
        InetSocketAddress address = getSocketAddress(packet);

        /* If we have parent - it will be reserve*/
        if (parent != null) {
            reserve_to_send = parent;
        }
        DatagramPacket answer;
        /* If we have reserve address - send it... */
        if (reserve_to_send != null && !reserve_to_send.equals(address)) {
            byte[] ip = reserve_to_send.getAddress().getAddress();
            byte[] port = PacketHandler.intToBytes(reserve_to_send.getPort());

            answer = PacketHandler.generatePacket(PacketType.SYS_ANSWER,
                    PacketHandler.concat(new byte[]{(byte)ip.length}, ip, port),
                    address);
        }
        /* ...else set reserve address equal to this node */
        else {
            reserve_to_send = address;
            answer = PacketHandler.generatePacket(
                    PacketType.SYS_ANSWER, new byte[]{0x00}, address);
        }
        /* Add packet */
        nodes.get(address).systemPackets.add(answer);
    }

    private void handleSystemAnswer(DatagramPacket packet)
            throws UnknownHostException {
        if (packet.getData()[1] == 0x00) {
            reserve = null;
            return;
        }
        reserve = PacketHandler.getSocketAddress(packet);
    }

    private boolean sendDataAck(DatagramPacket packet) {
        UUID uuid = PacketHandler.getUUID(packet);
        InetSocketAddress address = getSocketAddress(packet);

        var node = nodes.get(address);
        if (node == null)
            return false;

        DatagramPacket answer = PacketHandler.generatePacket(
                PacketType.DATA_ACK, uuid, new byte[0], address);
        node.systemPackets.add(answer);
        return node.receivedPackets.add(uuid);
    }

    private void handleDataAck(DatagramPacket packet) {
        InetSocketAddress address = getSocketAddress(packet);
        UUID uuid = PacketHandler.getUUID(packet);

        var node = nodes.get(address);
        if (node == null)
            return;
        /* Clear it */
        node.dataPackets.remove(uuid);
    }

    private void resendDataPacket(DatagramPacket packet) {
        InetSocketAddress address = getSocketAddress(packet);

        for (var entry : nodes.entrySet()) {
            InetSocketAddress to = entry.getKey();
            if (!to.equals(address)) {
                /* Resend packets and don't change data. Only UUID */
                UUID uuid = UUID.randomUUID();

                byte[] messageBytes = PacketHandler.getMessageBytes(packet);
                if (messageBytes == null)
                    return;

                byte[] payload = PacketHandler.concat(
                        PacketHandler.intToBytes(messageBytes.length), messageBytes);

                DatagramPacket reply = PacketHandler.generatePacket(
                        PacketType.DATA, uuid, payload, to);

                /* Add to data packets queue for all neighbourhood*/
                entry.getValue().dataPackets.put(
                        uuid, new PacketData(reply, SEND_TIMEOUT));
            }
        }
    }
}
