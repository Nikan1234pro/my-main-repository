import application.*;

import java.net.InetAddress;

public class Main {

    public static void main(String[] args) {
        int argc = args.length;
        if (argc != 3 && argc != 5) {
            System.err.println("Wrong args count!");
            return;
        }
        try {
            Application app;
            if (argc == 3) {
                app = new Application(
                        args[0],
                        Integer.parseInt(args[1]),
                        Integer.parseInt(args[2]));
            }
            else {
                app = new Application(
                        args[0],
                        Integer.parseInt(args[1]),
                        Integer.parseInt(args[2]),
                        InetAddress.getByName(args[3]),
                        Integer.parseInt(args[4]));
            }
            Runtime.getRuntime().addShutdownHook(
                    new Thread(app::interrupt));
            app.start();
            app.listen();

        }
        catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
