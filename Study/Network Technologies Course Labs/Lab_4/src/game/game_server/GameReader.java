package game.game_server;

import game.GameController;
import game.GameModel;
import me.ippolitov.fit.snakes.SnakesProto;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class GameReader implements Runnable {
    GameModel model;
    boolean isRunning = true;

    public GameReader(GameModel model) {
        this.model = model;
    }

    @Override
    public void run() {
        //try {
            //GameSocket.dataSocket.setSoTimeout(10);
            while (isRunning) {
            }
        //}
        //catch (IOException e) {
        //    e.printStackTrace();
        //}
    }

    private void handleJoin(InetAddress ip, int port) throws IOException {
        SnakesProto.GameMessage message;
        if (GameController.MAX_PLAYERS <  model.getSnakes().size()) {
            SnakesProto.GameMessage.ErrorMsg err = SnakesProto.GameMessage.ErrorMsg.newBuilder()
                    .setErrorMessage("Server full")
                    .build();

            message = SnakesProto.GameMessage.newBuilder()
                    .setError(err)
                    .build();
            byte[] bytes = message.toByteArray();
            //GameSocket.dataSocket.send(new DatagramPacket(bytes, bytes.length, ip, port));
        }
        else {
            model.addPlayer(SnakesProto.GamePlayer.newBuilder()
                    .setId(GameModel.generateId())
                    .setName("TEST")
                    .setIpAddress(ip.toString())
                    .setPort(port)
                    .setRole(SnakesProto.NodeRole.NORMAL)
                    .build());
        }
    }


    public void quit() {
        isRunning = false;
    }

}
