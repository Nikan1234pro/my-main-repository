package game.game_server;

import game.BaseModelManager;
import game.GameController;
import game.GameModel;
import game.network.DeliverySystem;
import javafx.scene.input.KeyEvent;
import me.ippolitov.fit.snakes.SnakesProto;


public class ServerModelManager extends BaseModelManager {
    private GameLoop loop;
    private GameWriter writer;
    private GameReader reader;
    GameModel model;

    public ServerModelManager(GameModel model) {
        loop = new GameLoop(model);
        writer = new GameWriter(model);
        reader = new GameReader(model);

        model.addPlayer(SnakesProto.GamePlayer.newBuilder()
                    .setId(GameModel.generateId())
                    .setName(GameController.name)
                    .setIpAddress("")
                    .setPort(DeliverySystem.getInstance().myPort)
                    .setRole(SnakesProto.NodeRole.MASTER)
                    .build());

        new Thread(loop).start();
        new Thread(writer).start();
        //new Thread(reader).start();
    }


    public void handle(KeyEvent event) {

    }

    public void quit() {
        System.out.println("Model quit");
        loop.quit();
        writer.quit();
        reader.quit();
    }
}
