package game.game_server;

import game.GameModel;
import game.objects.*;
import javafx.scene.input.KeyEvent;
import system.Timer;
import java.util.ArrayList;

public class GameLoop implements Runnable {
    GameModel model;
    Timer timer;

    boolean running = true;

    public GameLoop(GameModel model) {
        this.model = model;
        this.timer = new Timer(30);
    }

    public void handle(KeyEvent event) {
        for (Snake s : model.getSnakes())
            s.handle(event);
    }


    public void update() {
        cleanUp();

        ArrayList<Snake> snakes = model.getSnakes();
        ArrayList<Apple> apples = model.getObjects();

        for (Snake snake : snakes)
            snake.update();

        for (Snake snake : snakes) {
            for (Apple apple : apples)
                snake.checkCollide(apple);
        }

        for (int i = 0; i < snakes.size() - 1; ++i) {
            for (int j = i + 1; j < snakes.size(); ++j) {
                snakes.get(i).checkCollide(snakes.get(j));
            }
        }
    }

    public void render() {
        model.render();
    }

    @Override
    public void run() {
        while (running) {
            if (timer.accept()) {
                update();
                render();
            }
        }
    }

    public void quit() {
        running = false;
    }

    private void cleanUp() {
        ArrayList<Apple> objects = model.getObjects();
        objects.removeIf(object -> !object.isAlive());

        ArrayList<Snake> snakes = model.getSnakes();
        snakes.removeIf(object -> !object.isAlive());
    }
}
