package game.game_server;

import game.GameModel;
import game.network.DeliverySystem;
import game.objects.Snake;
import me.ippolitov.fit.snakes.SnakesProto;
import system.Timer;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;

public class GameWriter implements Runnable {
    private Timer multicastTimer;
    private GameModel game;

    boolean isRunning = true;

    GameWriter(GameModel game) {
        final int timeout = 1000;
        multicastTimer = new Timer(timeout);

        this.game = game;
    }

    @Override
    public void run() {
        try {
            while (isRunning) {
                if (multicastTimer.accept()) {
                    sendAnnouncement();
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void sendAnnouncement() throws IOException {
        ArrayList<SnakesProto.GamePlayer> playersList = new ArrayList<>();

        for (Snake snake : game.getSnakes()) {
            playersList.add(snake.getPlayer().toBuilder().build());
        }

        SnakesProto.GamePlayers players = SnakesProto.GamePlayers
                .newBuilder()
                .addAllPlayers(playersList)
                .build();

        SnakesProto.GameMessage.AnnouncementMsg announcementMsg
                = SnakesProto.GameMessage.AnnouncementMsg
                .newBuilder()
                .setPlayers(players)
                .setConfig(game.getConfig())
                .build();

        SnakesProto.GameMessage message = SnakesProto.GameMessage.newBuilder()
                .setAnnouncement(announcementMsg)
                .setMsgSeq(-1)    //don't confirm multicast
                .build();

        DeliverySystem.getInstance().sendWithoutСonfirm(message,
                new InetSocketAddress(InetAddress.getByName(
                        DeliverySystem.multicast_ip), DeliverySystem.multicast_port));
    }


    void quit() {
        isRunning = false;
    }
}
