package game.objects;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;

import java.util.LinkedList;


public interface GameObject {
    void draw(GraphicsContext graphicsContext);

    LinkedList<Point> getCoords();

    void destroy();
    boolean isAlive();
}
