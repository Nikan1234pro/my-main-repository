package game.objects;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class Grid {
    final int COLS_COUNT;
    final int ROWS_COUNT;

    private float stepX;
    private float stepY;

    private Point left_upper;
    private Point right_lower;

    public Grid(Point left_upper, Point right_lower, int cols, int rows) {
        COLS_COUNT = cols;
        ROWS_COUNT = rows;

        this.left_upper = left_upper;
        this.right_lower = right_lower;

        stepX = ((float)(right_lower.x - left_upper.x)) / COLS_COUNT;
        stepY = ((float)(right_lower.y - left_upper.y)) / ROWS_COUNT;
    }

    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setFill(Color.GREY);
        graphicsContext.fillRect(left_upper.x, left_upper.y,
                stepX * COLS_COUNT, stepY * ROWS_COUNT);


        float x = left_upper.x;
        for (int i = 0; i <= COLS_COUNT; ++i) {
            graphicsContext.strokeLine(x, left_upper.y, x, right_lower.y);
            x += stepX;
        }

        float y = left_upper.y;
        for (int i = 0; i <= ROWS_COUNT; ++i) {
            graphicsContext.strokeLine(left_upper.x, y, right_lower.x, y);
            y += stepY;
        }
    }

    public void drawCell(GraphicsContext graphicsContext, int i, int j) {
        float x = stepX * i + left_upper.x;
        float y = stepY * j + left_upper.y;
        graphicsContext.fillRect(x, y, stepX, stepY);
    }

    public void drawCell(GraphicsContext graphicsContext, int i, int j, Image image) {
        float x = stepX * i + left_upper.x;
        float y = stepY * j + left_upper.y;
        graphicsContext.drawImage(image, x, y, stepX, stepY);
    }
}

