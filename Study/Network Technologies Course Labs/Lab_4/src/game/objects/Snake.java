package game.objects;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import me.ippolitov.fit.snakes.SnakesProto;

import java.util.HashSet;
import java.util.LinkedList;


public class Snake implements GameObject {
    private SnakesProto.GamePlayer player;
    private Grid grid;
    private LinkedList<Point> body;
    private HashSet<Point> container;

    private Point reserve;
    private Direction current_direction;
    private boolean growUp = false;

    private boolean isAlive = true;

    private boolean isUpdated = true;

    public Snake(SnakesProto.GamePlayer player, Grid grid, int i, int j) {
        this.grid = grid;
        this.player = player;

        body = new LinkedList<>();
        container = new HashSet<>();

        body.add(new Point(i, j));
        container.add(new Point(i, j));

        current_direction = Direction.RIGHT;
    }

    public int getScore() {
        return body.size();
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setFill(Color.WHEAT);
        for (Point p : body) {
            grid.drawCell(graphicsContext, p.x, p.y);
        }
    }

    public void update() {
        isUpdated = true;
        if (growUp) {
            growUp = false;

            body.add(new Point(reserve));
            container.add(new Point(reserve));
        }
        container.remove(body.getLast());

        reserve = new Point(body.getLast());

        Point head = new Point(body.getFirst());
        switch (current_direction) {
            case LEFT:  head.x = (grid.COLS_COUNT + head.x - 1) % grid.COLS_COUNT;
                break;
            case RIGHT: head.x = (grid.COLS_COUNT + head.x + 1) % grid.COLS_COUNT;
                break;
            case UP:    head.y = (grid.ROWS_COUNT + head.y - 1) % grid.ROWS_COUNT;
                break;
            case DOWN:  head.y = (grid.ROWS_COUNT + head.y + 1) % grid.ROWS_COUNT;
                break;
        }
        body.addFirst(head);
        body.removeLast();
        //Check self-collision
        if (!container.add(new Point(head)))
            destroy();
    }

    public void handle(KeyEvent event) {
        if (!isUpdated)
            return;
        switch (event.getCode()) {
            case W:
                moveUp();
                break;
            case S :
                moveDown();
                break;
            case A :
                moveLeft();
                break;
            case D:
                moveRight();
                break;
        }
        isUpdated = false;
    }

    public void growUp() {
        growUp = true;
    }


    private void moveDown() {
        if (current_direction != Direction.UP)
            current_direction = Direction.DOWN;
    }

    private void moveUp() {
        if (current_direction != Direction.DOWN)
            current_direction = Direction.UP;
    }

    private void moveLeft() {
        if (current_direction != Direction.RIGHT)
            current_direction = Direction.LEFT;
    }

    private void moveRight() {
        if (current_direction != Direction.LEFT)
            current_direction = Direction.RIGHT;
    }


    public void checkCollide(Apple a) {
        if (body.getFirst().equals(a.getPosition())) {
            growUp();
            a.destroy();
        }
    }

    public SnakesProto.GamePlayer getPlayer() {
        return player;
    }

    public void checkCollide(Snake s) {
        if (s != this) {

        }
    }

    @Override
    public void destroy() {
        isAlive = false;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public LinkedList<Point> getCoords() {
        return body;
    }
}
