package game.objects;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

import java.util.LinkedList;

public class Apple implements GameObject {
    private Grid grid;
    private final Point point;
    private boolean isAlive = true;
    private final Image image;

    public Apple(Grid grid, int i, int j) {
        image = new Image("textures/apple.png");
        this.grid = grid;

        point = new Point(i, j);
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setFill(Color.RED);
        grid.drawCell(graphicsContext, point.x, point.y, image);
    }


    @Override
    public void destroy() {
        isAlive = false;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public LinkedList<Point> getCoords() {
        LinkedList<Point> list = new LinkedList<>();
        list.add(point);
        return list;
    }

    public Point getPosition() {
        return point;
    }
}
