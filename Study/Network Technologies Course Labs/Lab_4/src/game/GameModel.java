package game;

import game.objects.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.util.Pair;
import me.ippolitov.fit.snakes.SnakesProto;
import java.util.ArrayList;

public class GameModel  {
    private Grid grid;
    private ArrayList<Snake> players;
    private ArrayList<Apple> foods;
    private GraphicsContext graphicsContext;

    SnakesProto.GameConfig config;

    private final int ROWS_COUNT;
    private final int COLS_COUNT;

    public GameModel(SnakesProto.GameConfig config) {
        this.config = config;

        this.COLS_COUNT = config.getWidth();
        this.ROWS_COUNT = config.getHeight();

        this.players = new ArrayList<>();
        this.foods = new ArrayList<>();
    }

    synchronized public void addPlayer(SnakesProto.GamePlayer player) {
        Pair<Integer, Integer> p = computeCoords();
        players.add(new Snake(player, grid, p.getKey(), p.getValue()));
    }

    synchronized public ArrayList<Snake> getSnakes() {
        return players;
    }

    synchronized public ArrayList<Apple> getObjects() {
        return foods;
    }

    synchronized public SnakesProto.GameConfig getConfig() {
        return config;
    }

    public void initGraphics(GraphicsContext graphicsContext, int WIDTH, int HEIGHT) {
        this.graphicsContext = graphicsContext;

        grid = new Grid(
                new Point(0, 0),
                new Point(WIDTH, HEIGHT),
                COLS_COUNT, ROWS_COUNT);
    }

    synchronized public void render() {
        if (grid == null)
            return;
        grid.draw(graphicsContext);

        for (Snake s : players)
            s.draw(graphicsContext);

        for (Apple a : foods)
            a.draw(graphicsContext);
    }

    public static int generateId() {
        return (int)(Math.random() * 10000);
    }

    private Pair<Integer, Integer> computeCoords() {
        byte[][] matrix = new byte[COLS_COUNT][ROWS_COUNT];
        for (Snake o : players) {
            for (Point p : o.getCoords()) {
                matrix[p.x][p.y] = 0x01;
            }
        }

        int i = 0;
        int j = 0;
        //while () {
            int x = (int)(Math.random()) * COLS_COUNT;
            int y = (int)(Math.random()) * ROWS_COUNT;

        //}
        return new Pair<>(x, y);
    }
}
