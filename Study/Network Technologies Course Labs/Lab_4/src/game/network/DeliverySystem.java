package game.network;

import me.ippolitov.fit.snakes.SnakesProto;
import system.Timer;


import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class DeliverySystem implements Runnable{
    private HashMap<InetSocketAddress, LinkedList<TimedMessage>> toSend;
    private HashMap<InetSocketAddress, LinkedList<SnakesProto.GameMessage>> received;

    private HashMap<InetSocketAddress, Seq> lastSendSeq;
    private HashMap<InetSocketAddress, Seq> lastReceivedSeq;

    private LinkedList<DatagramPacket> toSendWithoutConfirm;

    private boolean isRunning = true;

    private static volatile DeliverySystem instance;
    private DatagramSocket socket = null;
    public int myPort;

    public static final String multicast_ip = "239.192.0.4";
    public static final int multicast_port = 9192;

    public MulticastSocket multicast;

    private DeliverySystem() {
        toSend = new HashMap<>();
        received = new HashMap<>();

        lastSendSeq = new HashMap<>();
        lastReceivedSeq = new HashMap<>();

        toSendWithoutConfirm = new LinkedList<>();
    }

    public static DeliverySystem getInstance() {
        DeliverySystem localInstance = instance;
        if (localInstance == null) {
            synchronized (DeliverySystem.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DeliverySystem();
                }
            }
        }
        return localInstance;
    }

    synchronized public void bind(int port) throws IOException{
        socket = new DatagramSocket(port);
        myPort = port;
    }

    synchronized public void sendWithoutСonfirm(SnakesProto.GameMessage message, InetSocketAddress to) {
        byte[] bytes = message.toByteArray();
        toSendWithoutConfirm.addLast(
                new DatagramPacket(bytes, bytes.length, to.getAddress(), to.getPort()));
    }

    synchronized public void sendTo(SnakesProto.GameMessage packet, InetSocketAddress to) {
        if (!toSend.containsKey(to)) {
            toSend.put(to, new LinkedList<>());
            lastSendSeq.put(to, new Seq());
        }
        SnakesProto.GameMessage message = packet.toBuilder()
                .setMsgSeq(lastSendSeq.get(to).getSeq())
                .build();

        toSend.get(to).addLast(new TimedMessage(message));
        lastSendSeq.get(to).update();
    }

    @Override
    public void run() {
        try {
            while (isRunning) {
                sendMessages();
                receiveMessages();
            }
        }
        catch (IOException e) {
            /* Ignore */
        }
    }

    synchronized private void sendMessages() throws IOException {
        for (Map.Entry<InetSocketAddress, LinkedList<TimedMessage>> e : toSend.entrySet()) {
            LinkedList<TimedMessage> messageList = e.getValue();
            InetSocketAddress address = e.getKey();
            for (TimedMessage timedMessage : messageList) {
                if (timedMessage.timer.accept()) {
                    byte[] bytes = timedMessage.message.toByteArray();
                    socket.send(new DatagramPacket(
                            bytes, bytes.length,
                            address.getAddress(), address.getPort()));
                }
            }
        }

        for (DatagramPacket p : toSendWithoutConfirm) {
            socket.send(p);
        }
        toSendWithoutConfirm.clear();
    }

    synchronized private void receiveMessages() throws IOException {
        final int RECV_TIMEOUT = 5;

        byte[] data = new byte[2048];
        DatagramPacket recv = new DatagramPacket(data, data.length);

        try {
            socket.setSoTimeout(RECV_TIMEOUT);
            socket.receive(recv);
        }
        catch (SocketTimeoutException e) {
            return;
        }

        byte[] bytes = new byte[recv.getLength()];
        System.arraycopy(recv.getData(), recv.getOffset(), bytes, 0, recv.getLength());
        SnakesProto.GameMessage message = SnakesProto.GameMessage.parseFrom(bytes);

        InetSocketAddress address = new InetSocketAddress(recv.getAddress(), recv.getPort());
        long seq = message.getMsgSeq();

        /* If it was ACK */
        if (message.hasAck()) {
            System.out.println("ACK: " + seq);
            toSend.get(address).removeIf(timedMessage -> timedMessage.message.getMsgSeq() == seq);
            return;
        }

        /* Message from unknown Node */
        if (!received.containsKey(address)) {
            received.put(address, new LinkedList<>());
            received.get(address).addLast(message);
            lastReceivedSeq.put(address, new Seq(seq));
            System.out.println("Send ACK");
        }
        else {
            /* If it required message */
            if (lastReceivedSeq.get(address).getSeq() + 1 == seq) {
                received.get(address).addLast(message);
            }
            /* It repeated wrong message */
            else if (lastReceivedSeq.get(address).getSeq() + 1 < seq)
                return;
        }

        /* Send ack */
        SnakesProto.GameMessage.AckMsg ack = SnakesProto.GameMessage.AckMsg.newBuilder().build();
        sendWithoutСonfirm(SnakesProto.GameMessage.newBuilder()
                .setAck(ack)
                .setMsgSeq(seq)
                .build(), address);
    }

    public void quit() {
        System.out.println("Delivery EXITED");
        isRunning = false;
        if (multicast != null)
            multicast.close();
        socket.close();
    }

    public boolean isRunning() {
        return isRunning;
    }
}


class TimedMessage {
    final long ACK_TIMEOUT = 1000;

    SnakesProto.GameMessage message;
    Timer timer;

    TimedMessage(SnakesProto.GameMessage message) {
        this.timer = new Timer(ACK_TIMEOUT);
        this.message = message;
    }
}


class Seq {
    private long s = 0L;

    Seq() {}

    Seq(long val) {
        s = val;
    }

    void update() {
        ++s;
    }

    long getSeq() {
        return s;
    }
}
