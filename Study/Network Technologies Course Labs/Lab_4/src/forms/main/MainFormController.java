package forms.main;

import forms.FormLoader;
import game.GameController;
import game.network.DeliverySystem;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MainFormController extends Application {
    public static Stage stage;

    public static final int WIDTH = 700;
    public static final int HEIGHT = 450;

    @FXML
    private Button createButton;

    @FXML
    private Button joinButton;

    @FXML
    private TextField nickname;

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setOnCloseRequest(e -> DeliverySystem.getInstance().quit());

        FormLoader.getInstance().setStage(primaryStage);

        FormLoader.getInstance().addScene("main/mainform.fxml",     "mainform",   WIDTH, HEIGHT);
        FormLoader.getInstance().addScene("create/createform.fxml", "createform", WIDTH, HEIGHT);
        FormLoader.getInstance().addScene("join/joinform.fxml",     "joinform",   WIDTH, HEIGHT);

        FormLoader.getInstance().showScene("mainform");
    }


    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Wrong args count");
            return;
        }

        int port = Integer.parseInt(args[0]);

        try {
            DeliverySystem.getInstance().bind(port);
            new Thread(DeliverySystem.getInstance()).start();
        }
        catch (final IOException e) {
            System.err.println(e.getLocalizedMessage());
            return;
        }

        launch(args);
    }

    @FXML
    void initialize() {
        createButton.setOnAction(event -> {
            if (DeliverySystem.getInstance().multicast != null)
                DeliverySystem.getInstance().multicast.close();

            GameController.name = nickname.getText();
            FormLoader.getInstance().showScene("createform");
        });

        joinButton.setOnAction(event -> {
            try {
                if (DeliverySystem.getInstance().multicast != null)
                    DeliverySystem.getInstance().multicast.close();

                GameController.name = nickname.getText();

                DeliverySystem.getInstance().multicast = new MulticastSocket(
                        DeliverySystem.multicast_port);

                DeliverySystem.getInstance().multicast.joinGroup(
                        InetAddress.getByName(DeliverySystem.multicast_ip));
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            FormLoader.getInstance().showScene("joinform");
        });
    }
}
