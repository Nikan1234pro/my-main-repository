package forms.create;

import forms.FormLoader;
import game.GameController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import javafx.fxml.FXML;
import javafx.util.Pair;
import me.ippolitov.fit.snakes.SnakesProto;

import java.util.HashMap;


public class CreateFormController {

    HashMap<String, Pair<Integer, Integer>> sizes;

    @FXML
    private ComboBox<String> fieldSize;

    @FXML
    private TextField foodCount;

    @FXML
    private TextField gameSpeed;

    @FXML
    private TextField playersCount;

    @FXML
    private Button nextButton;

    @FXML
    private Button prevButton;

    @FXML
    void initialize() {
        sizes = new HashMap<>();
        sizes.put("40x30", new Pair<>(40, 30));
        sizes.put("100x80", new Pair<>(100, 80));
        sizes.put("50x50", new Pair<>(50, 50));

        ObservableList<String> options =
                FXCollections.observableArrayList();
        for (HashMap.Entry entry : sizes.entrySet())
            options.add((String)entry.getKey());

        fieldSize.setItems(options);


        prevButton.setOnAction(event -> {
            FormLoader.getInstance().prevScene();
        });

        nextButton.setOnAction(event -> {
            Pair<Integer, Integer> size = sizes.get(fieldSize.getValue());
            int ispeed = 0;
            int ifoodCount = 0;
            try {
                ispeed = Integer.parseInt(gameSpeed.getText());
                ifoodCount = Integer.parseInt(foodCount.getText());
                GameController.MAX_PLAYERS = Integer.parseInt(playersCount.getText());
            }
            catch (Exception e) {
                return;
            }
            SnakesProto.GameConfig config =
                    SnakesProto.GameConfig.newBuilder()
                            .setWidth(size.getKey())
                            .setHeight(size.getValue())
                            .setDelayMs(ispeed)
                            .setDeadFoodProb(0.2f)
                            .setFoodPerPlayer(5.0f)
                            .setFoodStatic(ifoodCount)
                            .build();

            new GameController(config, SnakesProto.NodeRole.MASTER);
        });


    }
}
