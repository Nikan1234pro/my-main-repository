package forms.join;

import game.network.DeliverySystem;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import me.ippolitov.fit.snakes.SnakesProto;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.HashMap;

public class ConnectionsListener implements Runnable {
    boolean isRunning = true;
    //ArrayList<String> options;
    private final ObservableList<String> options;

    private HashMap<InetSocketAddress,
            SnakesProto.GameMessage.AnnouncementMsg> availableServers;

    private HashMap<String, InetSocketAddress> optionToAddress;

    ConnectionsListener(ObservableList<String> options,
                        HashMap<String, InetSocketAddress> optionToAddress) {
        this.options = options;
        this.availableServers = new HashMap<>();
        this.optionToAddress = optionToAddress;
    }


    @Override
    public void run() {
        try {
            while (isRunning) {
                byte[] data = new byte[2048];
                DatagramPacket recv = new DatagramPacket(data, data.length);
                DeliverySystem.getInstance().multicast.receive(recv);

                byte[] bytes = new byte[recv.getLength()];

                System.arraycopy(recv.getData(), recv.getOffset(), bytes, 0, recv.getLength());
                SnakesProto.GameMessage message = SnakesProto.GameMessage.parseFrom(bytes);

                if (!message.hasAnnouncement())
                    return;

                availableServers.put(
                        new InetSocketAddress(recv.getAddress(), recv.getPort()),
                        message.getAnnouncement());

                synchronized (optionToAddress) {
                    Platform.runLater(() -> {
                        for (HashMap.Entry e : availableServers.entrySet()) {
                            InetSocketAddress address = (InetSocketAddress) e.getKey();
                            SnakesProto.GameMessage.AnnouncementMsg announcementMsg =
                                    (SnakesProto.GameMessage.AnnouncementMsg) e.getValue();

                            String opt = address.getAddress().toString() + " " + address.getPort() +
                                    " players: " + announcementMsg.getPlayers().getPlayersList().size();

                            if (!options.contains(opt))
                                options.add(opt);
                            optionToAddress.put(opt, address);
                        }
                    });
                }
            }
        }
        catch (IOException e) {
            System.out.println("Connection Listener EXITED");
        }
    }
}
