package forms.join;

import forms.FormLoader;
import game.network.DeliverySystem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import me.ippolitov.fit.snakes.SnakesProto;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

public class JoinFormController {
    ConnectionsListener listener;

    @FXML
    private ListView<String> chooseGame;


    @FXML
    private Button prevButton;

    @FXML
    private Button startButton;


    @FXML
    void initialize() {
        ObservableList<String> options =
                FXCollections.observableArrayList();

        HashMap<String, InetSocketAddress> optionsToAddress = new HashMap<>();


        chooseGame.setItems(options);

        startButton.setOnMouseClicked(event -> {
            if (listener == null) {
                listener = new ConnectionsListener(options, optionsToAddress);
                new Thread(listener).start();
            }
        });

        chooseGame.setOnMouseClicked(click -> {
            if (click.getClickCount() == 2) {
                //Use ListView's getSelected Item
                synchronized (optionsToAddress) {
                    String elem = chooseGame.getSelectionModel()
                            .getSelectedItem();

                    tryJoin(optionsToAddress.get(elem));
                }

                DeliverySystem.getInstance().multicast.close();
                listener = null;
            }
        });

        prevButton.setOnAction(event -> {
            DeliverySystem.getInstance().multicast.close();
            listener = null;

            FormLoader.getInstance().prevScene();
        });
    }

    private boolean tryJoin(InetSocketAddress address) {
        SnakesProto.GameMessage.JoinMsg join = SnakesProto.GameMessage.JoinMsg.newBuilder()
                .setOnlyView(false)
                .build();

        SnakesProto.GameMessage joinRequest = SnakesProto.GameMessage.newBuilder()
                .setJoin(join)
                .setMsgSeq(228)
                .build();

        DeliverySystem.getInstance().sendTo(joinRequest, address);

        return true;
    }
}
