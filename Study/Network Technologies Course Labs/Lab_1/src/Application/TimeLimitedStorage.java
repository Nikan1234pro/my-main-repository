package Application;

import java.util.HashMap;
import java.util.Iterator;

public class TimeLimitedStorage<T> {
    private HashMap<T, Long> table;
    private long time_limit;
    private boolean upd_flag = false;

    public TimeLimitedStorage(long time_limit) {
        this.time_limit = time_limit;
        table = new HashMap<>();
    }

    public void update(T key) {
        upd_flag = false;

        if (!table.containsKey(key))
            upd_flag = true;

        table.put(key, System.currentTimeMillis());

        Iterator<HashMap.Entry<T, Long>> it = table.entrySet().iterator();
        long check_time = System.currentTimeMillis();
        while (it.hasNext()) {
            HashMap.Entry<T, Long> pair = it.next();
            if (check_time - pair.getValue() > time_limit) {
                it.remove();
                upd_flag = true;
            }
        }
    }

    public void print() {
        if (!upd_flag)
            return;
        System.out.println("---------------------------");
        System.out.println("Ip List:");
        for (HashMap.Entry<T, Long> elem : table.entrySet()) {
                System.out.println(elem.getKey());
        }
        System.out.println("---------------------------");
        System.out.flush();
    }
}
