package Application;

import java.io.IOException;
import java.net.*;


public class Application {
    private MulticastSocket socket;

    private static final int send_timeout = 5000;
    private static final int max_timeout = send_timeout * 3;

    private static final int port = 5555;
    private  InetAddress group;

    public Application(String groupIp)
            throws  IOException {
        group = InetAddress.getByName(groupIp);

        socket = new MulticastSocket(port);
        socket.joinGroup(group);
    }

    public void run() {
        byte[] sendBuffer = {};
        DatagramPacket sendPacket = new DatagramPacket(
                sendBuffer, sendBuffer.length, group, port
        );

        TimeLimitedStorage<InetAddress> storage =
                new TimeLimitedStorage<InetAddress>(max_timeout);
        Timer timer = new Timer(send_timeout);
        try {
            while(!socket.isClosed()) {
                if (timer.accept())
                    socket.send(sendPacket);

                byte[] receiveBuffer = {};
                DatagramPacket receivePacket = new DatagramPacket(
                        receiveBuffer, receiveBuffer.length
                );

                int remain = (int)timer.remainedToAccept();
                if (remain > 0) {
                    socket.setSoTimeout(remain);
                    try {
                        socket.receive(receivePacket);
                        storage.update(receivePacket.getAddress());
                        storage.print();
                    }
                    catch (SocketTimeoutException e) {
                        /* Ignore */
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
