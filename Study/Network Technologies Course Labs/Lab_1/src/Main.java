import Application.Application;

public class Main {
    public static void main(String[] args) {
        try {
            Application app = new Application(args[0]);
            app.run();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
