import ds/tree;
import runtime;
import lingo/pegcode/driver;

export {

// Var declaration
Type ::= Int, Array;
Int();
Array(type : Type);


VarDecl(var : string, type : Type);

// Operators
Expr ::= BinaryOp, Variable, Constant, App, Upd;

BinaryOp(op : string, l : Expr, h : Expr);
Variable(var : string);

Constant(c : int);
Val ::= IntVal, ArrVal;
IntVal(i :int);
ArrVal(a : Tree<int, Val>);

App(arr : Expr, index : Expr);
Upd(arr : Expr, value : Expr, index : Expr);

Label(l : int);

//1.
Assign(var : string, e : Expr, goto : [Label]);

//2.
Condition(body : CompareOp, yes : [Label], no : [Label]);
CompareOp(op : string, lhs : Expr, rhs : Expr);

//3.
Print(e : Expr, goto : [Label]);

VCommand ::= Assign, Condition, Print;
VOperator(label : Label, command : VCommand);
VProgram(decls : [VarDecl], operators : [VOperator]);


Config(label : Label, state : Tree<string, Val>);    


/* Nemo data structures */
NemoOperation ::= NemoAssign, NemoTest, NemoSequence, NemoParallel, NemoIteration, NemoPrint; 
NemoAssign(var : string, e : Expr);
NemoTest(op : string, lhs : Expr, rhs : Expr);
NemoSequence(first : NemoOperation, second : NemoOperation);
NemoParallel(first : NemoOperation, second : NemoOperation);
NemoIteration(first : NemoOperation);
NemoPrint(e : Expr);

NemoProgram(decls : [VarDecl], operation : NemoOperation);


parseNemo(file : string, parserPath : string) -> NemoProgram  {
    gram = "#include sandbox/VNeMo/nemo.gram"; 
    specialPegActions = {
	    t = setTree(setTree(defaultPegActions.t, "parseIfElse", parseIfElse), "parseWhileDo", parseWhileDo);
	    SemanticActions(t);
    }
    parsic(compilePegGrammar(gram), file, specialPegActions);
}

translate(nemo : NemoProgram) -> VProgram {
    VProgram(nemo.decls, translateOperation(nemo.operation, 0));
}


getOpposite(op : string, lhs : Expr, rhs : Expr) -> NemoTest {
    if (op == "==") {
        NemoTest("!=", lhs, rhs);
    }
    else if (op == "!=") {
        NemoTest("==", lhs, rhs);
    }
    else if (op == ">=") {
        NemoTest("<", lhs, rhs);
    }
    else if (op == "<=") {
        NemoTest(">", lhs, rhs);
    }
    else if (op == ">") {
        NemoTest("<=", lhs, rhs);
    }
    else if (op == "<") {
        NemoTest(">=", lhs, rhs);
    }
    else 
        NemoTest("==", Constant(0), Constant(0));
}
 
parseIfElse(args : [flow]) -> flow {
    NemoParallel(
        NemoSequence(NemoTest(args[0], args[1], args[2]), args[3]), 
        NemoSequence(getOpposite(args[0], args[1], args[2]), args[4]));
}


parseWhileDo(args : [flow]) -> flow {
    NemoSequence(
        NemoIteration(NemoSequence(NemoTest(args[0], args[1], args[2]), args[3])),
        getOpposite(args[0], args[1], args[2])
    );
}

translateOperation(operation : NemoOperation, start : int) -> [VOperator] {
    switch (operation) {
        NemoAssign(var, expr) : {
            [VOperator(Label(start), Assign(var, expr, [Label(start + 1)]))];
        }
        NemoPrint(e) : {
            [VOperator(Label(start), Print(e, [Label(start + 1)]))];
        }
        NemoTest(op, lhs, rhs) : {
            [VOperator(
                Label(start), 
                Condition(
                    CompareOp(op, lhs, rhs), 
                    [Label(start + 1)], [Label(-1)])
            )];
        }
        NemoSequence(first, second) : {
            op1 = translateOperation(first, start);
            op2 = translateOperation(second, maxLabel(op1));
            concat(op1, op2);
        }
        NemoParallel(first, second) : {
            op2 = translateOperation(first, start + 1);
            end2 = maxLabel(op2);

            op3 = translateOperation(second, end2);
            end3 = maxLabel(op3);

            op1 = [VOperator(
                Label(start), 
                Condition(
                    CompareOp("==", Constant(0), Constant(0)), 
                    [Label(start + 1), Label(end2)], 
                    [Label(-1)])
            )];
            concat3(op1, replaceLabel(op2, Label(end2), Label(end3)), op3);
        }
        NemoIteration(first) : {
            op2 = translateOperation(first, start + 1);
            end2 = maxLabel(op2);

            op1 = [VOperator(
                Label(start), 
                Condition(
                    CompareOp("==", Constant(0), Constant(0)), 
                    [Label(start + 1), Label(end2)], 
                    [Label(-1)])
            )];
            concat(op1, replaceLabel(op2, Label(end2), Label(start)));
        }
    }
}

maxLabel(operators : [VOperator]) -> int {
		index = findiExtreme(operators, labelComparator);
		operators[index].label.l + 1;
}

labelComparator(x : VOperator, y : VOperator) -> double {
		cast(x.label.l - y.label.l : int -> double);
}

replaceLabel(operators : [VOperator], to_replace : Label, new_label : Label) -> [VOperator] {
    map(operators, \v -> {

        VOperator(switch(v.label) {
            Label(l) : {
                if (l == to_replace.l)
                    new_label
                else
                    Label(l)
            }
        },  switch(v.command) {
                Assign(var, e, goto) : {
					Assign(var, e, map(goto, \j -> {
						if(j.l == to_replace.l) 
							new_label
                        else 
                            j;
					}));
				}
				Condition(comp, yes, no) : {
					Condition(comp, map(yes, \j -> {
						if(j.l == to_replace.l) 
							new_label
						else  
                            j;
					}), no);	
				}
				Print(e, goto) : {
					Print(e, map(goto, \j -> {
						if(j.l == to_replace.l) 
							new_label
						else 
                            j; 
					}));
				}  
        })   
    });
}
 
}

