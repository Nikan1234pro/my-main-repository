/////////////////////////////////////
//     ���������� ����������       //
//        � ����������             //
/////////////////////////////////////
#include <vector>
#include <iostream>
template <typename T>
class My_Array;

template <typename T>
class My_Iterator : public std::iterator<std::input_iterator_tag, T>
{
	friend class My_Array<T>;
private:
	T *ptr;
	My_Iterator(T *p) : ptr(p)
	{}
public:
	My_Iterator(const My_Iterator &it) : ptr(it.ptr)
	{}

	bool operator == (const My_Iterator &other) const
	{
		return ptr == other.ptr;
	}

	bool operator != (const My_Iterator &other) const
	{
		return ptr != other.ptr;
	}

	typename My_Iterator::reference operator * () const
	{
		return *ptr;
	}

	My_Iterator& operator++ (int)
	{
		My_Iterator tmp(ptr);
		ptr++;
		return tmp;
	}

	My_Iterator& operator-- (int)
	{
		My_Iterator tmp(ptr);
		ptr--;
		return ptr;
	}

	My_Iterator operator++ ()
	{
		ptr++;
		return *this;
	}

	My_Iterator operator-- ()
	{
		ptr--;
		return *this;
	}

	bool operator < (const My_Iterator &other)
	{
		return ptr < other.ptr;
	}

	My_Iterator operator+ (int i)
	{
		return My_Iterator(ptr + i);
	}

	My_Iterator operator- (int i)
	{
		return My_Iterator(ptr - i);
	}

	ptrdiff_t operator- (const My_Iterator &other) const
	{
		return ptr - other.ptr;
	}
};

template <typename T>
class My_Array
{
private:
	T *arr;
	int size;
public:
	typedef My_Iterator<T> iterator;
	typedef const My_Iterator<T> const_iterator;

	My_Array() : arr(NULL), size(0)
	{}

	explicit My_Array(int s) : size(s)
	{
		arr = new T[s];
	}

	void set_size(int s)
	{
		if (arr)
			delete arr;
		arr = new T[s];
		size = s;
	}

	T& operator[](int i)
	{
		if (i < 0 || i >= size)
			throw std::out_of_range("Out of Range");
		return arr[i];
	}

	iterator begin()
	{
		return iterator(arr);
	}

	iterator end()
	{
		return (arr + size);
	}

	~My_Array()
	{
		delete[] arr;
	}
};

