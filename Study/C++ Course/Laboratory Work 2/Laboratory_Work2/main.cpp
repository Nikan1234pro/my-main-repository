#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <vector>
#include "operators.h"
#include "my_array.h"
#include "element.h"
#define SIZE 10

void bubble_sort(My_Array<Elem>::iterator begin, My_Array<Elem>::iterator end)
{
	int i = 0;
	for (auto iter1 = begin; iter1 != end - 1; iter1++)
		for (auto iter2 = iter1 + 1; iter2 != end; iter2++)
		{
			if (*iter1 < *iter2)
			{
				Elem tmp = *iter1;
				*iter1 = *iter2;
				*iter2 = tmp;
			}
		}
}

int main()
{
	setlocale(LC_ALL, "RUS");
	std::srand(static_cast<unsigned int>(std::time(NULL)));
	
	My_Array<Elem> arr(SIZE);
	for (int i = 0; i < SIZE; i++)
		arr[i].push_value(std::rand() % 1000, i);	// create array

	My_Array<Elem>::iterator it = arr.begin();
	std::sort(arr.begin(), arr.end());
										// print debug information
	Elem::print();
	std::cout << "Result:" << std::endl;
	for (; it != arr.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
	
	system("pause");
	return 0;
}