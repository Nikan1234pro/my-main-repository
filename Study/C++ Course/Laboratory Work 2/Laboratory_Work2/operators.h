/////////////////////////////////
//	   ������ ����������       //
/////////////////////////////////
#pragma once
#include <iostream>
class Operator
{
public:
	virtual void print() const = 0;
	~Operator()
	{}
};


class Creater : public Operator
{
private:
	int data1;
	int index1;
public:
	Creater(int d, int i) : data1(d), index1(i)
	{}
	void print() const
	{
		std::cout << "Created array[" << index1 << "] = " << data1 << std::endl;
	}
};

class Comparison_less : public Operator
{
private:
	int data1;
	int index1;
	int data2;
	int index2;
public:
	Comparison_less(int d1, int i1, int d2, int i2) : data1(d1), index1(i1), data2(d2), index2(i2)
	{}
	void print() const
	{
		std::cout << "Check array[" << index1 << "] = " << data1
			<< " < array[" << index2 << "] = " << data2 << "	";
		if (data1 < data2)
			std::cout << "|TRUE |" << std::endl;
		else
			std::cout << "|FALSE|" << std::endl;
	}

};

class �omparison_greater : public Operator
{
private:
	int data1;
	int index1;
	int data2;
	int index2;
public:
	�omparison_greater(int d1, int i1, int d2, int i2) : data1(d1), index1(i1), data2(d2), index2(i2)
	{}
	void print() const
	{
		std::cout << "Check array[" << index1 << "] = " << data1
			<< " > array[" << index2 << "] = " << data2 << "	";
		if (data1 > data2)
			std::cout << "|TRUE |" << std::endl;
		else
			std::cout << "|FALSE|" << std::endl;
	}
	
};

class Copy : public Operator
{
private:
	int data1;
	int index1;
public:
	Copy(int d, int i) : data1(i), index1(i)
	{}
	void print() const
	{
		std::cout << "Copy " << "arr[" << index1 << "] = " << data1 << std::endl;
	}
};

class Assignment : public Operator
{
private:
	int data1;
	int index1;
	int data2;
	int index2;
public:
	Assignment(int d1, int i1, int d2, int i2) : data1(d1), index1(i1), data2(d2), index2(i2)
	{}
	void print() const
	{
		std::cout << "arr[" << index1 << "] = arr[" << index2 << "]" << std::endl; 
	}
};

