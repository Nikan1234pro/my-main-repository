/////////////////////////////
//    ������� ����������   //
/////////////////////////////
#pragma once
#include <iostream>
#include "operators.h"
#include <vector>

class Elem
{
private:
	int data;
	int index;
	static std::vector<Operator*> _operators;
public:

	Elem() : data(0), index(-1)
	{}

	Elem(const Elem &other) : data(other.data), index(other.index)
	{
		Operator *ptr = new Copy(other.data, other.index);			//create debug object
		_operators.push_back(ptr);
	}

	void push_value(int d, int i)
	{
		data = d;
		index = i;
		Operator *ptr = new Creater(data, index);
		_operators.push_back(ptr);
	}

	friend std::ostream& operator<<(std::ostream &s, const Elem &e)
	{
		s << e.data;
		return s;
	}

	bool operator >(const Elem &other) const
	{
		Operator *ptr = new �omparison_greater(data, index, other.data, other.index);	//create debug object
		_operators.push_back(ptr);
		return data > other.data;
	}

	bool operator <(const Elem &other) const
	{
		Operator *ptr = new Comparison_less(data, index, other.data, other.index);	//create debug object
		_operators.push_back(ptr);
		return data < other.data;
	}

	void operator=(const Elem &other)
	{
		Operator *ptr = new Assignment(data, index, other.data, other.index);		//create debug object	
		_operators.push_back(ptr);
		data = other.data;
	}

	static void print()
	{
		std::cout << "--------------DEBUG----INFORMATION--------------" << std::endl;
		for (int i = 0; i < _operators.size(); i++)
			_operators[i]->print();
		std::cout << "------------------------------------------------" << std::endl;
	}

	~Elem()
	{}
};

std::vector<Operator*> Elem::_operators;