#pragma once
#include <typeinfo>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <algorithm>
#include <memory.h>

template <class T>
class CircularBuffer
{
private:
	T *buffer;
	int b_capacity;
	int b_size;
	int start;
	int make_index(int i) const;
public:
	CircularBuffer();
	CircularBuffer(const CircularBuffer &cb);
	CircularBuffer(int capacity, const T &elem);
	explicit CircularBuffer(int capacity);
	CircularBuffer& operator=(const CircularBuffer & cb);
	T& operator[](int i);
	const T& operator[](int i) const;
	T& at(int i);
	const T& at(int i) const;
	T& front();
	T& back();
	const T& front() const;
	const T& back() const;
	int size() const;
	int capacity() const;
	int reserve() const;
	bool full() const;
	bool empty() const;
	void push_back(const T& item = T());
	void push_front(const T& item = T());
	void pop_back();
	void pop_front();
	void rotate(int new_start);
	bool is_linearized() const;
	T* linearize();
	void set_capacity(int new_capacity);
	void resize(int new_size, const T& item = T());
	void swap(CircularBuffer &cb);
	void insert(int pos, const T& item = T());
	void erase(int first, int last);
	void clear();
	~CircularBuffer();
};

template <class T>
int CircularBuffer<T>::make_index(int i) const
{
	return (b_capacity + start + i) % b_capacity;
}

template <class T>
bool operator==(const CircularBuffer<T> &a, const CircularBuffer<T> &b)
{
	if (a.size() != b.size())
		return false;

	for (int i = 0; i < a.size(); i++)
		if (a[i] != b[i])
			return false;
	return true;
}

template <class T>
bool operator!=(const CircularBuffer<T> &a, const CircularBuffer<T> &b)
{
	return !(a == b);
}


template <class T>
CircularBuffer<T>::CircularBuffer() : buffer(NULL), b_capacity(0), b_size(0), start(0)
{}

template <class T>
CircularBuffer<T>::CircularBuffer(const CircularBuffer &cb) : b_capacity(cb.b_capacity), b_size(cb.b_size), start(cb.start)
{
	buffer = NULL;
	if (b_capacity)
	{
		buffer = new T[b_capacity];
		for (int i = 0; i < b_size; i++)
			at(i) = cb.at(i);
	}
}

template <class T>
CircularBuffer<T>::CircularBuffer(int capacity, const T &elem) : b_capacity(capacity), b_size(capacity), start(0)
{
	if (capacity <= 0)
		throw std::bad_alloc();

	buffer = new T[b_capacity];
	for (int i = 0; i < b_capacity; i++)
		at(i) = elem;
}

template <class T>
CircularBuffer<T>::CircularBuffer(int capacity) : b_capacity(capacity), b_size(0), start(0)
{
	if (b_capacity <= 0)
		throw std::bad_alloc();
	buffer = new T[b_capacity];
}

template <class T>
CircularBuffer<T>& CircularBuffer<T>::operator=(const CircularBuffer & cb)
{
	delete[] buffer;
	b_capacity = cb.b_capacity;
	b_size = cb.b_size;
	start = cb.start;
	buffer = NULL;
	if (!b_capacity)
		return *this;

	buffer = new T[b_capacity];
	for (int i = 0; i < b_size; i++)
		at(i) = cb.at(i);
	return *this;
}

template <class T>
T& CircularBuffer<T>::operator[](int i)
{
	return buffer[make_index(i)];
}

template <class T>
const T& CircularBuffer<T>::operator[](int i) const
{
	return buffer[make_index(i)];
}

template <class T>
T& CircularBuffer<T>::at(int i)
{
	if (i < 0 || i >= b_size)
		throw std::out_of_range("Out of range");
	else
		return buffer[make_index(i)];
}

template <class T>
const T& CircularBuffer<T>::at(int i) const
{
	if (i < 0 || i >= b_size)
		throw std::out_of_range("Out of range");
	else
		return buffer[make_index(i)];
}

template <class T>
T& CircularBuffer<T>::front()
{
	return at(0);
}

template <class T>
T& CircularBuffer<T>::back()
{
	return at(b_size - 1);
}

template <class T>
const T& CircularBuffer<T>::front() const
{
	return at(0);
}

template <class T>
const T& CircularBuffer<T>::back() const
{
	return at(b_size);
}

template <class T>
int CircularBuffer<T>::size() const
{
	return b_size;
}

template <class T>
int CircularBuffer<T>::capacity() const
{
	return b_capacity;
}

template <class T>
int CircularBuffer<T>::reserve() const
{
	return b_capacity - b_size;
}

template <class T>
bool CircularBuffer<T>::full() const
{
	return b_size == b_capacity;
}

template <class T>
bool CircularBuffer<T>::empty() const
{
	if (!b_capacity)
		return false;
	return !b_size;
}

template <class T>
void CircularBuffer<T>::push_back(const T& item)
{
	if (!b_capacity)
		throw std::out_of_range("Out of range");
	if (full())
	{
		at(0) = item;
		start = make_index(1);
	}
	else
	{
		b_size++;
		at(b_size - 1) = item;
	}
}

template <class T>
void CircularBuffer<T>::push_front(const T& item)
{
	if (!b_capacity)
		throw std::out_of_range("Out of range");
	if (!full())
		b_size++;
	start = make_index(-1);
	at(0) = item;
}

template <class T>
void CircularBuffer<T>::pop_back()
{
	if (!b_size)
		throw std::out_of_range("Out of range");
	at(b_size - 1).~T();
	b_size--;

}

template <class T>
void CircularBuffer<T>::pop_front()
{
	if (!b_size)
		throw std::out_of_range("Out of range");
	at(0).~T();
	start = make_index(1);
	b_size--;
}

template <class T>
void CircularBuffer<T>::rotate(int new_start)
{
	if (new_start < 0 || new_start >= b_size)
		throw std::out_of_range("Out of range");
	if (!full())
	{
		for (int i = 0; i < new_start; i++)
		{
			buffer[make_index(b_size)] = buffer[start];
			start = make_index(1);
		}
	}
	else
		start = make_index(new_start);

}

template <class T>
bool CircularBuffer<T>::is_linearized() const
{
	return !start;
}

template <class T>
T* CircularBuffer<T>::linearize()
{
	if (!buffer)
		throw std::out_of_range("Out of range");
	std::rotate(buffer, buffer + start, buffer + b_capacity);
	start = 0;
	return buffer;
}

template <class T>
void CircularBuffer<T>::set_capacity(int new_capacity)
{
	if (new_capacity <= 0)
		throw std::bad_alloc();
	T *tmp = new T[new_capacity];

	b_size = (b_size < new_capacity) ? b_size : new_capacity;
	for (int i = 0; i < b_size; i++)
		tmp[(start + i) % new_capacity] = at(i);

	start %= new_capacity;
	b_capacity = new_capacity;
	delete[] buffer;
	buffer = tmp;
}

template <class T>
void CircularBuffer<T>::resize(int new_size, const T& item)
{
	set_capacity(new_size);
	if (b_size < new_size)
		for (int i = b_size; i < b_capacity; i++)
			push_back(item);
	b_size = b_capacity;
}

template <class T>
void CircularBuffer<T>::swap(CircularBuffer &cb)
{
	std::swap(buffer, cb.buffer);
	std::swap(b_capacity, cb.b_capacity);
	std::swap(b_size, cb.b_size);
	std::swap(start, cb.start);
}

template <class T>
void CircularBuffer<T>::insert(int pos, const T& item)
{
	if (pos > b_size || pos >= b_capacity)
		throw std::out_of_range("Out of range");
	if (!full())
		b_size++;
	for (int i = b_size - 1; i > pos; i--)
		at(i) = at(i - 1);
	at(pos) = item;
}

template <class T>
void CircularBuffer<T>::erase(int first, int last)
{
	int len = last - first + 1;
	if (first < 0 || first >= b_size || last < 0 || last >= b_size || first > last)
		throw std::out_of_range("Out of range");


	for (int i = 0; i < b_size - first - len; i++)
	{
		at((first + i) % b_capacity) = at((first + i + len) % b_capacity);
		at((first + i + len) % b_capacity).~T();
	}
	b_size -= len;
}

template <class T>
void CircularBuffer<T>::clear()
{
	for (int i = 0; i < b_size; i++)
		at(i).~T();
	b_size = 0;
	start = 0;
}

template <class T>
CircularBuffer<T>::~CircularBuffer()
{
	delete[] buffer;
}


