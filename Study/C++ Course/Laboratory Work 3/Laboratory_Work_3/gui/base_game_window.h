#pragma once

#include "base_window.h"

namespace gui {

class BaseGameWindow {
public:
  BaseGameWindow();
  virtual ~BaseGameWindow();

  int GetWidth() const {
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    return w;
  }

  int GetHeight() const {
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    return h;
  }

  SDL_Renderer* GetRenderer() {
    return renderer;
  }

  virtual void Render() = 0;

private:
  SDL_Window *window = nullptr;
  SDL_Renderer *renderer = nullptr;
};

} // namespace gui
