#include "base_game_window.h"
#include <iostream>

namespace gui {

BaseGameWindow::BaseGameWindow() {
  window = SDL_CreateWindow("Game", 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SDL_WINDOW_FULLSCREEN);
  renderer = SDL_CreateRenderer(
      window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

BaseGameWindow::~BaseGameWindow() {
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
}

} // namespace gui
