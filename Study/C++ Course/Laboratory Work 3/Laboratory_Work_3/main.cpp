#include <Windows.h>

#include "gui/SDL2/include/SDL.h"
#include "gui/SDL2/include/SDL_opengl.h"
#include "gui/SDL2/include/SDL_ttf.h"
#include "gui/SDL2/include/SDL_mixer.h"

#include "game.h"
#include <ctime>
#include <iostream>
#include <stdio.h>
#include "game/Textures.h"
#include "game/Animations.h"
#include "Intelligence.h"


#pragma comment(lib, "gdiplus")
#pragma comment(lib, "winmm")
#pragma comment(lib, "gui/SDL2/lib/x64/SDL2.lib")
#pragma comment(lib, "gui/SDL2/lib/x64/SDL2_image.lib")
#pragma comment(lib, "gui/SDL2/lib/x64/SDL2_ttf.lib")
#pragma comment(lib, "gui/SDL2/lib/x64/SDL2_mixer.lib")

struct DebugConsole {
  DebugConsole() {
    AllocConsole();
    freopen_s(&old_, "CONOUT$", "wt", stdout);
  }
  ~DebugConsole() {
    fflush(stdout);

    FreeConsole();
    fclose(old_);
  }

private:
  FILE *old_ = nullptr;
};

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine, int nCmdShow) {
	std::srand(std::time(NULL));
	//DebugConsole debug_console_holder; //(отключение консоли)
	GdiPlus graphics;
	gui::GUIInitializer gui(hInstance);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO) != 0)
	{
		SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
		return 1;
	}
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		SDL_Log("Failed to initialize SDL: %s", Mix_GetError());
		return 1;
	}
	TTF_Init();
	Mix_Music *music = Mix_LoadMUS("game/music/soundByMaxGoldman.mp3");
	Animation_Manager *animations = Animation_Manager::GetInstance();
	Textures *textures = Textures::GetInstance();
	Intelligence *intelligence = Intelligence::GetInstance();
	{ 
		Game game;
		textures->Preload(game.GetRenderer());
		timeBeginPeriod(1);
		Mix_PlayMusic(music, -1);
		DWORD ticks = timeGetTime();
		bool quit = false;
		while (!quit) 
		{
			float dt = (timeGetTime() - ticks) / 1000.0f;
			ticks = timeGetTime();
			SDL_Event event;
			while(SDL_PollEvent(&event)) 
			{
				if (event.type == SDL_QUIT)
					quit = true;
				else if (event.type == SDL_KEYDOWN)
				{
					if (event.key.keysym.sym == SDLK_p)
					{
						if (Mix_PausedMusic())
							Mix_ResumeMusic();
						else
							Mix_PauseMusic();
					}
					if (event.key.keysym.sym == SDLK_ESCAPE)
					{
						quit = true;
						Mix_HaltMusic();
					}
				}
			}
	
			game.Update(dt);

			BYTE sys_keys[256];
			bool keys[256];
			GetKeyboardState(sys_keys);
			for (int i = 0; i < sizeof(sys_keys); ++i)
				keys[i] = (sys_keys[i] & 0x80) == 0x80;
			game.ProcessInput(keys);

			game.Render();
		}
    timeEndPeriod(1);
	}
	Mix_FreeMusic(music);
	delete textures; 
	delete animations;
	delete intelligence;
	SDL_Quit();
	TTF_Quit();
	Mix_Quit();
	return 0;
}
