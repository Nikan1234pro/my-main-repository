#include "game.h"
#include "game/Textures.h"
#include <iostream>
#include "parameters.h"

const int FieldWidth = 96;
const int FieldHeight = FieldWidth * (float)GetSystemMetrics(SM_CYSCREEN) / GetSystemMetrics(SM_CXSCREEN);
float size= 20.0f;
float head = 2.0f * size;
float texture_size_x = (float)GetSystemMetrics(SM_CXSCREEN) / FieldWidth;
float texture_size_y = (float)GetSystemMetrics(SM_CYSCREEN) / FieldHeight;
float eps_x = (size > texture_size_x) ? (size - texture_size_x) / size * 1.1f : 0.0f;
float eps_y = (size > texture_size_y) ? (size - texture_size_y) / size * 1.1f : 0.0f;

Game::Game() : gui::BaseGameWindow(),
arkanoid(FieldWidth * size, FieldHeight * size)
{}

void Game::Render() {
  SDL_Renderer* canvas = GetRenderer();
  auto &image = Textures::GetInstance()->GetTexture("background")[0];
  SDL_RenderCopy(canvas, image.get(), nullptr, nullptr);
  arkanoid.Draw(canvas);
  SDL_RenderPresent(canvas);
}

void Game::ProcessInput(const bool keys[256]) 
{
	 arkanoid.ProcessInput(keys);
}

void Game::Update(float dt) 
{
	arkanoid.UpdateWorld(dt); 
}

Game::~Game()
{}
