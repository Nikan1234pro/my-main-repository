#include "Vector_2D.h"

Vector_2D::Vector_2D() : x(0.0f), y(0.0f)
{}

Vector_2D::Vector_2D(float x_, float y_) : x(x_), y(y_)
{}

Vector_2D::operator bool()
{
	if (x || y)
		return true;
	return false;
}

//���������, ��� ����� ����� � ��������������, �������� ������� ������� � � �
bool Vector_2D::IsInsideRect(const Vector_2D &A, const Vector_2D &B) const
{
	if (min(A.x, B.x) <= x && x <= min(A.x, B.x) &&
		min(A.y, B.y) <= y && y <= max(A.y, B.y))
		return true;
	return false;
}

//�������� ���� ��������
Vector_2D Vector_2D::operator+(const Vector_2D &A) const
{
	return Vector_2D(x + A.x, y + A.y);
}

//��������� ���� ��������
Vector_2D Vector_2D::operator-(const Vector_2D &A) const
{
	return Vector_2D(x - A.x, y - A.y);
}

float Vector_2D::operator* (const Vector_2D &A) const
{
	return (x * A.x + y * A.y);
}

Vector_2D Vector_2D::operator* (const float &n) const
{
	return Vector_2D(x * n, y * n);
}

Vector_2D Vector_2D::round() const
{
	return Vector_2D(std::roundf(x), std::roundf(y));
}

//���������� ����� �� ���� �������� �������, ����� ������� �� ������� ��������
Vector_2D Vector_2D::MoveToArc(Vector_2D &speed, const Vector_2D &A, const Vector_2D &B, const Vector_2D &Center)
{
	//a*dt^2 + 2*b *dt + c = 0  - solution
	if (*this == A || *this == B)
		return *this;

	float r = std::abs(A.x - B.x);
	float a = speed * speed;
	float b = (*this - Center) * speed;
	float c = (*this - Center) * (*this - Center) - r * r;
	if (b * b - a * c < 0)
		return *this;

	float dt1 = (-b - std::sqrt(b * b - a * c)) / a;
	float dt2 = (-b + std::sqrt(b * b - a * c)) / a;
	Vector_2D tmp1 = *this + (speed * dt1);
	Vector_2D tmp2 = *this + (speed * dt2);
	if (dt1 < dt2)
		return tmp1.round();
	else
		return tmp2.round();
}


Vector_2D Vector_2D::Move(Vector_2D &speed, Vector_2D &center, float Shift) const
{
	if (!speed)
	{
		return center;
	}
		
	float dt = Shift / std::sqrtf(speed * speed);
	return (center + speed * dt);
}

//�������� ������ �� ������ (������� ������ ����� �������)
Vector_2D Vector_2D::Reflection(const Vector_2D &A, const Vector_2D &B) const
{
	Vector_2D n = A - B;
	Vector_2D new_speed = *this - (n * 2.0f) * (((*this) * n) / (n * n));
	return new_speed;
}

//�������� ������ �� ������ (������� ������ ��������)
Vector_2D Vector_2D::Reflection(const Vector_2D &normal) const
{
	Vector_2D new_speed = *this - (normal * 2.0f) * (((*this) * normal) / (normal * normal));
	return new_speed;
}

//���������� ����� ����������� ������� � ������� AB
Vector_2D Vector_2D::FindIntersection(const Vector_2D &speed, const Vector_2D &A, const Vector_2D &B) const
{
	if (A.x < 0 || A.y < 0 || B.x < 0 || B.y < 0)
		return Vector_2D{};

	if (A.x == B.x)
	{
		float dt = (x - A.x) / speed.x;
		if (dt < 0)	//dt �� ����� ���� �������������!
			return Vector_2D{};

		float y_new = y - speed.y * dt;
		if (min(A.y, B.y) <= y_new && y_new <= max(A.y, B.y))
			return Vector_2D(std::roundf(A.x), std::roundf(y_new));
		else
			return Vector_2D{};
	}
	else if (A.y == B.y)
	{
		float dt = (y - A.y) / speed.y;
		if (dt < 0)	//dt �� ����� ���� �������������!
			return Vector_2D{};

		float x_new = x - speed.x * dt;
		if (min(A.x, B.x) <= x_new && x_new <= max(A.x, B.x))
			return Vector_2D(std::roundf(x_new), std::roundf(A.y));
		else
			return Vector_2D{};
	}
	else
		return Vector_2D{};
}


//���������� ����� ����������� ���� �����, �������� ������ ������ � �������� �����������
Vector_2D FindIntersection(const Vector_2D &A, const Vector_2D &speedA, const Vector_2D &B, const Vector_2D &speedB)
{
	float dtB = ((B.x - A.x) * speedA.y - (B.y - A.y) * speedA.x) /
		(speedA.x * speedB.y - speedA.y * speedB.x);
	float dtA = (B.x - A.x + speedB.x * dtB) / speedA.x;
	return (A + speedA * dtA);
}

float Vector_2D::GetDistance(const Vector_2D &A) const
{
	return std::sqrtf((x - A.x) * (x - A.x) + (y - A.y) * (y - A.y));
}

float Vector_2D::GetDistance(const float x1, const float y1) const
{
	return std::sqrtf((x - x1) * (x - x1) + (y - y1) * (y - y1));
}

bool Vector_2D::operator==(const Vector_2D &other) const
{
	return (x == other.x && y == other.y);
}

void Vector_2D::operator=(const Vector_2D &other)
{
	x = other.x;
	y = other.y;
}

float Vector_2D::Get_x() const
{
	return x;
}

float Vector_2D::Get_y() const
{
	return y;
}

float& Vector_2D::Get_x()
{
	return x;
}

float& Vector_2D::Get_y()
{
	return y;
}

Vector_2D::~Vector_2D()
{}


float Cos(float x0, float y0, float x1, float y1)
{
	Vector_2D A(x0, y0);
	float dist = A.GetDistance(x1, y1);
	return (x1 - x0) / dist;
}

float Cos(const Vector_2D &A, const Vector_2D &B)
{
	float dist = A.GetDistance(B);
	return (B.Get_x() - A.Get_x()) / dist;
}

float Sin(float x0, float y0, float x1, float y1)
{
	Vector_2D A(x0, y0);
	float dist = A.GetDistance(x1, y1);
	return (y1 - y0) / dist;
}

float Sin(const Vector_2D &A, const Vector_2D &B)
{
	float dist = A.GetDistance(B);
	return (B.Get_y() - A.Get_y()) / dist;
}