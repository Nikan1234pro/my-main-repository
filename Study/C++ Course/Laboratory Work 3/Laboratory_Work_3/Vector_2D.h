#pragma once
#include <cmath>
#include <algorithm>
#include <iostream>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))


class Vector_2D
{
private:
	float x;
	float y;
public:
	Vector_2D();
	Vector_2D(float x_, float y_);
	operator bool();
	bool IsInsideRect(const Vector_2D &A, const Vector_2D &B)const;
	Vector_2D operator+(const Vector_2D &A) const;
	Vector_2D operator-(const Vector_2D &A) const;
	float operator* (const Vector_2D &A) const;
	Vector_2D operator* (const float &n) const;
	Vector_2D round() const;
	Vector_2D MoveToArc(Vector_2D &speed, const Vector_2D &A, const Vector_2D &B, const Vector_2D &Center);
	Vector_2D Move(Vector_2D &speed, Vector_2D &center, float Shift) const;
	Vector_2D Reflection(const Vector_2D &A, const Vector_2D &B) const;
	Vector_2D Reflection(const Vector_2D &normal) const;
	Vector_2D FindIntersection(const Vector_2D &speed, const Vector_2D &A, const Vector_2D &B) const;
	friend Vector_2D FindIntersection(const Vector_2D &A, const Vector_2D &speedA, const Vector_2D &B, const Vector_2D &speedB);
	float GetDistance(const Vector_2D &A) const;
	float GetDistance(const float x1, const float y1) const;
	bool operator==(const Vector_2D &other) const;
	void operator=(const Vector_2D &other);
	float Get_x() const;
	float Get_y() const;
	float& Get_x();
	float& Get_y();
	~Vector_2D();
};

float Cos(float x0, float y0, float x1, float y1);
float Cos(const Vector_2D &A, const Vector_2D &B);
float Sin(float x0, float y0, float x1, float y1);
float Sin(const Vector_2D &A, const Vector_2D &B);
