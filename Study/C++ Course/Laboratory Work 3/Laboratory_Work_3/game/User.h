#pragma once
#include <string>
#include "Field.h"
#include "Shapes.h"
#include "../Vector_2D.h"
#include "../gui/base_game_window.h"

class User
{
protected:
	Ball ball;
	int score;
public:
	User(float x, float y, int color);
	virtual void Update(float dt, Field &field, int screen_w, int screen_h) = 0;
	virtual void Draw(SDL_Renderer* canvas, int screen_w, int screen_h, Field &field) const = 0;
	virtual void ProcessInput(const bool keys[256]) = 0;
	int GetScore() const;
	friend void Update(User *A, User *B);
	~User();
};

class Player : public User
{
public:
	Player(float x, float y, int color);
	void Update(float dt, Field &field, int screen_w, int screen_h);
	void Draw(SDL_Renderer* canvas, int screen_w, int screen_h, Field &field) const;
	void ProcessInput(const bool keys[256]);
};

class Bot : public User
{
private:
	float min_dt;
public:
	Bot(float x, float y, int color);
	void Update(float dt, Field &field, int screen_w, int screen_h);
	void Draw(SDL_Renderer* canvas, int screen_w, int screen_h, Field &field) const;
	void ProcessInput(const bool keys[256]);
};

void DrawScore(SDL_Renderer* canvas, int screen_w, int screen_h, const User *A, const User *B);


