#include "../Vector_2D.h"
#include "../gui/base_game_window.h"
#include "../parameters.h"
#include <typeinfo>
#include <vector>
class Field;

class Shapes
{
protected:
	static int count;
public:
	virtual void Draw(SDL_Renderer* canvas, int i, int j) const = 0;
	static int GetCount();
	virtual ~Shapes()
	{}
};

class Rect : public Shapes
{
private:
	float a;
	int color;
public:
	Rect();
	void Draw(SDL_Renderer* canvas, int i, int j) const final;
	~Rect();
};

class SuperRect : public Shapes
{
private:
	float a;
public:
	SuperRect();
	void Draw(SDL_Renderer* canvas, int i, int j) const final;
	~SuperRect();
};


class Ball
{
private:
	Vector_2D center;
	Vector_2D speed;
	Vector_2D base;
	int color;
	float r;
	const float v = 200.0f;// * min(texture_size_x / size, texture_size_y / size);
public:
	Ball(float x, float y, int _color);
	void SetSpeed(float x, float y);
	void SetSpeed(Vector_2D &Cursor);
	void SetCenter();
	void Update(float dt, int screen_w, int screen_h);
	void Draw(SDL_Renderer* canvas) const;
	bool IsInsideRect(const Vector_2D &point, const Vector_2D &Rect) const;
	bool OnBase() const;
	int GetPhysics(Field &field);
	const float& Get_x() const;
	const float& Get_y() const;
	float& Get_x();
	float& Get_y();
	std::pair<Vector_2D, Vector_2D> GetCollisionRect(std::vector<Vector_2D> &vect);
	friend void CollisionBall(Ball &ball_1, Ball &ball_2);
};


class Line
{
private:
	Vector_2D start;
	gdi::Pen pen;
public:
	Line(float x, float y);
	void Draw(SDL_Renderer *canvas, Field &field) const;
};

bool isSuperRect(Shapes *ptr);