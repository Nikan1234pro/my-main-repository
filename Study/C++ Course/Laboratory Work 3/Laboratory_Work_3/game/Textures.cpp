#include "Textures.h"
#include <iostream>
#include <sstream>

Textures::Textures()
{}

Textures* Textures::GetInstance()
{
	static Textures *instance = new Textures;
	return instance;
}

void Textures::Preload(SDL_Renderer* canvas) {
	for (int i = 0; i < 20; i++)
	{
		std::stringstream name;
		name << "game/textures/wall" << i << ".png";
		textures["walls"].push_back(TexturePtr(IMG_LoadTexture(canvas, name.str().c_str()), TextureDeleter{}));
	}

	for (int i = 0; i < 8; i++)
	{	
		std::stringstream name;
		name << "game/textures/destruction" << i << ".png";
		textures["destructions"].push_back(TexturePtr(IMG_LoadTexture(canvas, name.str().c_str()), TextureDeleter{}));
	}

	textures["background"].push_back(TexturePtr(IMG_LoadTexture(canvas, "game/textures/background0.png"), TextureDeleter{}));
	textures["TNT"].push_back(TexturePtr(IMG_LoadTexture(canvas, "game/textures/TNT.png"), TextureDeleter{}));
	textures["ball"].push_back(TexturePtr(IMG_LoadTexture(canvas, "game/textures/player.png"), TextureDeleter{}));
	textures["ball"].push_back(TexturePtr(IMG_LoadTexture(canvas, "game/textures/bot.png"), TextureDeleter{}));
	textures["empty"].push_back(TexturePtr(IMG_LoadTexture(canvas, "game/textures/empty.png"), TextureDeleter{}));
	
}

const Textures::TextureStorage& Textures::GetTexture(const std::string& name) const
{
	auto it = textures.find(name);
	if (it == textures.end())
		throw std::out_of_range("No Textures");
	return it->second;
}

Textures::~Textures()
{}
