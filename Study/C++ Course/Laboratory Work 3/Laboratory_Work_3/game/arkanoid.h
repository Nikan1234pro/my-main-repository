#pragma once
#include "../gui/base_game_window.h"
#include "User.h"

class Arkanoid 
{
private:
	int screen_w = 0;
	int screen_h = 0;
	Field field;
	User *user1;
	User *user2;
public:
	Arkanoid(int _screen_w, int _screen_h);
	void Draw(SDL_Renderer* canvas);
	void UpdateWorld(float dt);
	void ProcessInput(const bool keys[256]);
};


