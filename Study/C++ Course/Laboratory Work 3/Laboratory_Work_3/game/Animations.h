#include "../Vector_2D.h"
#include "../gui/base_game_window.h"
#include <list>

class Animation
{
protected:
	float timeline;
	float time;
	int count_of_frames;
public:
	Animation(float _timeline, float _count);
	bool Update(float dt);
	virtual void Draw(SDL_Renderer* canvas) const = 0;
	virtual ~Animation()
	{}
};

class Destruction : public Animation
{
private:
	float x;
	float y;
public:
	Destruction(float _x, float _y);
	void Draw(SDL_Renderer* canvas) const override;
	~Destruction()
	{}
};

class Animation_Manager
{
private:
	std::list<Animation*> animations;
	Animation_Manager();
public:
	static Animation_Manager* GetInstance();
	template <typename T>
	void Add(float x, float y);
	void Update(float dt);
	void Draw(SDL_Renderer* canvas) const;
	~Animation_Manager();
};

template <typename T>
void Animation_Manager::Add(float x, float y)
{
	Animation *ptr = new T(x, y);
	animations.push_back(ptr);
}