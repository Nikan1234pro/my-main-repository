#include "User.h"
#include "../Intelligence.h"
#include "../gui/SDL2/include/SDL.h"
#include "../gui/SDL2/include/SDL_ttf.h"

User::User(float x, float y, int color) : ball(x, y, color), score(0)
{}

void User::Update(float dt, Field &field, int screen_w, int screen_h)
{
	ball.Update(dt, screen_w, screen_h);
	score += ball.GetPhysics(field);
}

int User::GetScore() const
{
	return score;
}

void Update(User *A, User *B)
{
	CollisionBall(A->ball, B->ball);
}

User::~User()
{}


Player::Player(float x, float y, int color) : User(x, y, color)
{}

void Player::Draw(SDL_Renderer* canvas, int screen_w, int screen_h, Field &field) const
{
	if (ball.OnBase())
	{
		Line line(ball.Get_x(), ball.Get_y());
		line.Draw(canvas, field);
	}
	ball.Draw(canvas);
}

void Player::Update(float dt, Field &field, int screen_w, int screen_h)
{
	User::Update(dt, field, screen_w, screen_h);
}

void Player::ProcessInput(const bool keys[256])
{
	if (keys[32]) //SPACE
	{
		POINT p;
		if (GetCursorPos(&p))
			ball.SetSpeed(p.x * size / texture_size_x, p.y * size / texture_size_y);
	}
	if (keys[8])
		ball.SetCenter();
}


Bot::Bot(float x, float y, int color) : User(x, y, color), min_dt(0)
{}

void Bot::Draw(SDL_Renderer* canvas, int screen_w, int screen_h, Field &field) const
{
	ball.Draw(canvas);
}

void Bot::ProcessInput(const bool keys[256])
{}

void Bot::Update(float dt, Field &field, int screen_w, int screen_h)
{
	User::Update(dt, field, screen_w, screen_h);
	if (ball.OnBase())
	{
		min_dt += dt;
		if (min_dt > 1.0f)
		{
			float x = std::rand() % 1900;
			float y = std::rand() % 1000;
			Vector_2D Position = Intelligence::GetInstance()->GetPoint2(ball.Get_x(), ball.Get_y(), field, screen_w, screen_h);
			ball.SetSpeed(Position);
			min_dt = 0;
		}
	}
}

void DrawScore(SDL_Renderer* canvas, int screen_w, int screen_h, const User *A, const User *B)
{
	SDL_SetRenderDrawColor(canvas, 40, 100, 190, 140);
	SDL_SetRenderDrawBlendMode(canvas, SDL_BLENDMODE_BLEND);
	SDL_Rect head_rect = {0, 0, GetSystemMetrics(SM_CXSCREEN), texture_size_y * 2.0f};
	SDL_RenderFillRect(canvas, &head_rect);
	SDL_SetRenderDrawColor(canvas, 40, 100, 138, 255);
	SDL_RenderDrawRect(canvas, &head_rect);
	SDL_SetRenderDrawBlendMode(canvas, SDL_BLENDMODE_NONE);

	TTF_Font *Font0 = TTF_OpenFont("game/fonts/FELIXTI.TTF", 15); 
	TTF_Font *Font1 = TTF_OpenFont("game/fonts/vgafixr.fon", 250);

	SDL_Color colorBlack = { 0, 0, 0 };
	SDL_Surface *surfaceMessage1 = TTF_RenderText_Solid(Font0, " : ", colorBlack); 
	SDL_Rect rect1{ GetSystemMetrics(SM_CXSCREEN) / 2.0f - 0.5f * texture_size_x, 0.0f, texture_size_x * 1.0f, texture_size_y * 1.8f };
	SDL_Texture *Message1 = SDL_CreateTextureFromSurface(canvas, surfaceMessage1); 
	SDL_RenderCopy(canvas, Message1, NULL, &rect1);
	
	SDL_Color colorRed = { 255, 0, 0 };
	std::string scoreA = std::to_string(A->GetScore());
	SDL_Surface *surfaceMessage2 = TTF_RenderText_Solid(Font1, scoreA.c_str(), colorRed);
	SDL_Rect rect2;
	rect2.x = GetSystemMetrics(SM_CXSCREEN) / 2.0f - (scoreA.size() + 1) * texture_size_x;
	rect2.y = texture_size_y / 4.0f;
	rect2.w = texture_size_x * scoreA.size();
	rect2.h = texture_size_y * 1.5f;
	SDL_Texture *Message2 = SDL_CreateTextureFromSurface(canvas, surfaceMessage2);
	SDL_RenderCopy(canvas, Message2, NULL, &rect2);
	
	SDL_Color colorBlue = { 0, 0, 255 };
	std::string scoreB = std::to_string(B->GetScore());
	SDL_Surface *surfaceMessage3 = TTF_RenderText_Solid(Font1, scoreB.c_str(), colorBlue);
	SDL_Rect rect3;
	rect3.x = GetSystemMetrics(SM_CXSCREEN) / 2.0f + texture_size_x;
	rect3.y = texture_size_y / 4.0f;
	rect3.w = texture_size_x * scoreB.size();
	rect3.h = texture_size_y * 1.5f;
	SDL_Texture *Message3 = SDL_CreateTextureFromSurface(canvas, surfaceMessage3);
	SDL_RenderCopy(canvas, Message3, NULL, &rect3);
	
	TTF_CloseFont(Font0);
	TTF_CloseFont(Font1);
	SDL_DestroyTexture(Message1);
	SDL_DestroyTexture(Message2);
	SDL_DestroyTexture(Message3);
	SDL_FreeSurface(surfaceMessage1);
	SDL_FreeSurface(surfaceMessage2);
	SDL_FreeSurface(surfaceMessage3);
}

