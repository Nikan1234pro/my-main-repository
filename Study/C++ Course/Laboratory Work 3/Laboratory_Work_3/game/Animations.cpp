#include "Animations.h"
#include "Textures.h"

Animation::Animation(float _timeline, float _count) :
	timeline(_timeline), time(0), count_of_frames(_count)
{}

bool Animation::Update(float dt)
{
	time += dt;
	if (time > timeline)
		return true;
	return false;
}


Destruction::Destruction(float _x, float _y) : x(_x), y(_y), Animation(2.5f, 8)
{}


void Destruction::Draw(SDL_Renderer* canvas) const
{
	int cur_frame = (time / timeline) * count_of_frames;
	auto& image = Textures::GetInstance()->GetTexture("destructions")[cur_frame];
	float a = texture_size_x * 2;
	float b = texture_size_y * 2;
	SDL_Rect r{x - texture_size_x * 0.5f, y - texture_size_y * 0.5f, a, b};
	SDL_SetRenderDrawBlendMode(canvas, SDL_BLENDMODE_BLEND);
	SDL_RenderCopy(canvas, image.get(), NULL, &r);
	SDL_SetRenderDrawBlendMode(canvas, SDL_BLENDMODE_NONE);
}


Animation_Manager::Animation_Manager()
{
	animations.clear();
}

Animation_Manager* Animation_Manager::GetInstance()
{
	static Animation_Manager *instance = new Animation_Manager;
	return instance;
}

void Animation_Manager::Update(float dt)
{
	for (auto it = animations.begin(); it != animations.end();	)
	{
		bool is_completed = (*it)->Update(dt);
		if (is_completed)
		{
			delete *it;
			it = animations.erase(it);
		}
		else
			it++;
	}
}

void Animation_Manager::Draw(SDL_Renderer* canvas) const
{
	for (auto *anim : animations)
		anim->Draw(canvas);
}

Animation_Manager::~Animation_Manager()
{}