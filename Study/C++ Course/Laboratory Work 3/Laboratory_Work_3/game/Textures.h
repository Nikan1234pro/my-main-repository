#pragma once
#include "../gui/base_game_window.h"
#include "../parameters.h"
#include <map>
#include <string>
#include <vector>
#include <iostream>
class TextureDeleter
{
public:
	TextureDeleter()
	{}

	void operator()(SDL_Texture *ptr) const
	{
		SDL_DestroyTexture(ptr);
	}

	~TextureDeleter()
	{}
};

class Textures
{
private:
	using TexturePtr = std::unique_ptr<SDL_Texture, TextureDeleter>;
	using TextureStorage = std::vector<TexturePtr>;

	std::map< std::string, TextureStorage > textures;
	Textures();
public:
	static Textures* GetInstance();
	void Preload(SDL_Renderer* canvas);
	const TextureStorage& GetTexture(const std::string& name) const;
	~Textures();
};


