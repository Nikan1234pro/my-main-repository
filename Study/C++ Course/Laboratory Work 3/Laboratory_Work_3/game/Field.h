#include <vector>
#include <iostream>
#include "../gui/base_game_window.h"
#include "../parameters.h"
#include "../Vector_2D.h"
#pragma once
class Shapes;

class Field
{
private:
	std::vector< std::vector<Shapes*> > Matrix;
	int Width;
	int Height;
	int Space;
	int CountOfWalls;
public:
	Field();
	std::vector <Shapes*> &operator[](int i);
	const std::vector <Shapes*> &operator[](int i) const;
	void Draw(SDL_Renderer* canvas) const;
	int Destroy(int i, int j);
	int GetWidth() const;
	int GetHeight() const;
	int GetCount() const;
	~Field();
};

