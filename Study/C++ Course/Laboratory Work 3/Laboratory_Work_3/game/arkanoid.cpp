#include "arkanoid.h"
#include "Animations.h"
#include "../parameters.h"

Arkanoid::Arkanoid(int _screen_w, int _screen_h) : screen_w(_screen_w), screen_h(_screen_h), field()
{
	user1 = new Bot(size / 2, (_screen_h + head) / 2, 0);
	user2 = new Bot(_screen_w - size / 2, (_screen_h + head) / 2, 1);
}

void Arkanoid::Draw(SDL_Renderer* canvas) 
{
	field.Draw(canvas);
	user1->Draw(canvas, screen_w, screen_h, field);
	user2->Draw(canvas, screen_w, screen_h, field);
	DrawScore(canvas, screen_w, screen_h, user1, user2);
	Animation_Manager::GetInstance()->Draw(canvas);
}

void Arkanoid::UpdateWorld(float dt)
{
	float dt2 = 0.0f;
	for (; dt2 < dt; dt2 += 0.05f)
	{
		Animation_Manager::GetInstance()->Update(0.05f);
		user1->Update(0.05f, field, screen_w, screen_h);
		user2->Update(0.05f, field, screen_w, screen_h);
		Update(user1, user2);
	}
	Animation_Manager::GetInstance()->Update(dt2 - dt);
	user1->Update(dt2 - dt, field, screen_w, screen_h);
	user2->Update(dt2 - dt, field, screen_w, screen_h);
	Update(user1, user2);
}

void Arkanoid::ProcessInput(const bool keys[256])
{
	user1->ProcessInput(keys);
	user2->ProcessInput(keys);
}