#pragma once
#include "Vector_2D.h"
#include "game/Field.h"
//Update coming soon  :-P
class Intelligence
{
public:
	Intelligence();
	static Intelligence* GetInstance();
	Vector_2D GetPoint(float x, float y, Field &field, int screen_w, int screen_h) const;
	Vector_2D GetPoint2(float x, float y, Field &field, int screen_w, int screen_h) const;
	Vector_2D GetPoint3(float x, float y, Field &field, int screen_w, int screen_h) const;
	~Intelligence();
};

