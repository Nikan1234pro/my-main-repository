#include "Intelligence.h"
#include "game/Shapes.h"


Intelligence::Intelligence()
{}

Intelligence* Intelligence::GetInstance()
{
	Intelligence *instance = new Intelligence;
	return instance;
}

Vector_2D Intelligence::GetPoint(float x, float y, Field &field, int screen_w, int screen_h) const
{
	float max_dist_x = 0.0f;
	Vector_2D Point;
	int count = field.GetCount();
	if (count < 200)
	{
		for (int i = 0; i < field.GetWidth(); i++)
			for (int j = 2; j < field.GetHeight(); j++)
				if (field[i][j])
					return Vector_2D(i * size, j * size);
	}
	for (int k = 0; k < 10; k++)
	{
		float _x = x;
		float _y = y;
		float px = rand() % static_cast<int>(screen_w);
		float py = rand() % static_cast<int>((screen_h - head)) + head;
		while (true)
		{
			_x += 2.0f * Cos(x, y, px, py);
			_y += 2.0f * Sin(x, y, px, py);
			Vector_2D tmp = Vector_2D(_x, _y);
			if (_x <= 0 || _x >= screen_w || _y <= 0 || _y >= screen_h)
			{

				float dist = std::fabs(_x - x);
				if (dist > max_dist_x)
				{
					max_dist_x = dist;
					Point = tmp;
				}
				break;
			}
			int i = static_cast<int>(_x / size);
			int j = static_cast<int>(_y / size);
			if (0 <= i && i < field.GetWidth() && 0 <= j && j < field.GetHeight())
			{
				float dist = std::fabs(_x - x);
				if (field[i][j] || j < 2)
				{
					if (isSuperRect(field[i][j]))
						return Vector_2D(i * size, j * size);
					if (dist > max_dist_x)
					{
						max_dist_x = dist;
						Point = tmp;
					}
					break;
				}
			}
		}

	}
	return Point;
}

Vector_2D Intelligence::GetPoint2(float x, float y, Field &field, int screen_w, int screen_h) const
{
	Vector_2D Point;
	float max_dist = 0.0f;
	int count = field.GetCount();
	if (count < 200)
	{
		for (int i = 0; i < field.GetWidth(); i++)
			for (int j = 2; j < field.GetHeight(); j++)
				if (field[i][j])
					return Vector_2D(i * size, j * size);
	}
	for (float alpha = 0.0f; alpha < 360.0f; alpha++)
	{
		float px1 = x + size * std::cos((alpha + 90.0f) * PI / 180.0f);
		float py1 = y + size * std::sin((alpha + 90.0f) * PI / 180.0f);
		float px2 = x + size * std::cos((alpha - 90.0f) * PI / 180.0f);
		float py2 = y + size * std::sin((alpha - 90.0f) * PI / 180.0f);
		while (true)
		{
			px1 += 1.0f * std::cos(alpha * PI / 180);
			py1 += 1.0f * std::sin(alpha * PI / 180);
			px2 += 1.0f * std::cos(alpha * PI / 180);
			py2 += 1.0f * std::sin(alpha * PI / 180);
			Vector_2D tmp1(px1, py1);
			Vector_2D tmp2(px2, py2);
			if (px1 <= 0 || px1 >= screen_w || py1 <= head || py1 >= screen_h ||
				px2 <= 0 || px2 >= screen_w || py2 <= head || py2 >= screen_h)
			{
				float dist = std::abs(max(px1, px2) - x);
				if (dist >= max_dist)
				{
					max_dist = dist;
					Point = tmp1;
				}
				break;
			}
			int i = static_cast<int>(px1 / size);
			int j = static_cast<int>(py1 / size);
			if (0 <= i && i < field.GetWidth() && 0 <= j && j < field.GetHeight())
			{
				if (field[i][j])
				{
					if (isSuperRect(field[i][j]))
						return Vector_2D(i * size, j * size);
					float dist = std::abs(px1 - x);
					if (dist >= max_dist)
					{
						Point = tmp1;
						max_dist = dist;
					}
					break;
				}
			}
			i = static_cast<int>(px2 / size);
			j = static_cast<int>(py2 / size);
			if (0 <= i && i < field.GetWidth() && 0 <= j && j < field.GetHeight())
			{
				if (field[i][j])
				{
					if (isSuperRect(field[i][j]))
						return Vector_2D(i * size, j * size);
					float dist = std::abs(px2 - x);
					if (dist >= max_dist)
					{
						Point = tmp2;
						max_dist = dist;
					}
					break;
				}
			}
		}
	}
	return Point;
}

Vector_2D Intelligence::GetPoint3(float x, float y, Field &field, int screen_w, int screen_h) const
{
	return Vector_2D(x - 10, y);
	Vector_2D Point;
	Vector_2D p1(x, y);
	Vector_2D p2(x, y);
	for (float alpha = 0.0f; alpha < 360.0f; alpha++)
	{
		Ball testBall(x, y, 0);
	}
	return Point;
}

Intelligence::~Intelligence()
{}
