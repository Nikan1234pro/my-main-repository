#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <iterator>

#include "tuple_handler.h"

template <class... Args>
class CSVParser {
    std::istream& file_;           //reference to a file
    std::ios::pos_type position_;   //beginning of data
    char row_delim_;                //row delimiter
    char col_delim_ ;               //collumn delimiter
    char escape_;                   //shield 
    char current_line_;

    class Iterator : public std::iterator<
                        std::input_iterator_tag,
                        std::tuple<Args...>,
                        std::ptrdiff_t,
                        nullptr_t,
                        const std::tuple<Args...>&
                                            >{
        CSVParser<Args...> *parser_;
        std::tuple<Args...> current_;
    public:
        using reference = typename Iterator::reference;
        Iterator(CSVParser<Args...> *parser);
        Iterator(const Iterator &other);
        
        reference operator*() const;

        Iterator& operator++();
        Iterator operator++(int);
        bool operator==(const Iterator &other) const;
        bool operator!=(const Iterator &other) const;
    };

    std::string getline();
    
    std::vector<std::string> parse_line(std::string &line);

    void fill_tuple(const std::vector<std::string> parsed_line, 
        std::tuple<Args...> &tuple);
public:
    using iterator = Iterator;

    CSVParser(std::istream &file, int skip, char row_delim = '\n', 
        char col_delim = ',', char escape = '\"');
    bool parse(std::tuple<Args...> &tuple);
    iterator begin();
    iterator end();
};


template <class... Args>
CSVParser<Args...>::CSVParser(std::istream &file, int skip, char row_delim, 
char col_delim, char escape)
: file_(file), row_delim_(row_delim), col_delim_(col_delim), escape_(escape) {
    file_.seekg(std::ios::beg);

    for (int skipln = 0; skipln < skip; ++skipln) {
        std::string dummy;
        std::getline(file_, dummy, row_delim_);
        if (!file_) {
            throw std::range_error(
                std::string("Couldn't skip line #") + 
                std::to_string(skipln + 1)
            );
        }
    }
    current_line_ = skip;
    position_ = file_.tellg();
}

template <class... Args>
typename CSVParser<Args...>::iterator CSVParser<Args...>::begin() {
    file_.seekg(position_, std::ios::beg);
    return iterator(this);
}

template <class... Args>
typename CSVParser<Args...>::iterator CSVParser<Args...>::end() {
    return iterator(nullptr);
}

template <class... Args>
bool CSVParser<Args...>::parse(std::tuple<Args...> &tuple) {
    std::string line;
    if (!std::getline(file_, line, row_delim_))
        return false;
    
    try {
        auto parsed_line = parse_line(line);
        fill_tuple(parsed_line, tuple);
    }
    catch(std::exception &ex) {
        std::cerr << ex.what() << std::endl;
        throw std::runtime_error(
            std::string("Couldn't parse line #") +
            std::to_string(current_line_ + 1)
        );
    }
    ++current_line_;
    return true;
}

template <class... Args>
std::vector<std::string> CSVParser<Args...>::parse_line(std::string &line) {
    std::vector<std::string> result;
    std::vector<std::pair<size_t, size_t>> delimiters;
    size_t begin = 0;
    bool escape_flag = false;
    
    for (size_t i = 0; i < line.length(); ++i) {
        if (line.at(i) == escape_) 
            escape_flag = !escape_flag;
        if (line.at(i) == col_delim_) {
            if (!escape_flag) {
                delimiters.emplace_back(begin, i);
                begin = i + 1;
            }
        }
    }
    delimiters.emplace_back(begin, line.length()); 
    
    for (size_t i = 0; i < delimiters.size(); ++i) {   
        //parse one world from [first, last) range
        auto[first, last] = delimiters.at(i);
        if (first > last) 
            throw std::out_of_range("Out of range");
        if (line.at(first) == escape_ &&
            line.at(last - 1) == escape_) {
                //ignore " at the beginning and at the end 
                ++first;
                --last;
        }
        std::string data = line.substr(first, last - first);
        if (data.length() > 0) {
            for (int i = 0; i < data.length() - 1; ) {
                if (data.at(i) == escape_ && data.at(i + 1) == escape_) {
                    data.erase(data.begin() + i);
                    continue;
                }
                ++i;
            }
        }
        result.push_back(data);  
    }
    return result;
}

template <class... Args>
void CSVParser<Args...>::fill_tuple(const std::vector<std::string> parsed_line, 
std::tuple<Args...> &tuple) {
    if (sizeof...(Args) != parsed_line.size()) 
        throw std::logic_error("Wrong count of values");
    TupleFiller<sizeof...(Args) - 1, Args...>::fill(parsed_line, tuple);
}



template <class... Args> 
CSVParser<Args...>::Iterator::Iterator(CSVParser<Args...> *parser) 
: parser_(parser) {
    if (nullptr == parser_) 
        return;
    if (!parser_->parse(current_))
        parser_ = nullptr;
}

template <class... Args> 
typename CSVParser<Args...>::Iterator::reference  CSVParser<Args...>::Iterator::operator*() const {
    if (nullptr == parser_) 
        throw std::out_of_range("Out of range!");
    return current_;    
}

template <class... Args> 
CSVParser<Args...>::Iterator::Iterator(const Iterator &other)
: parser_(other.parser_), current_(other.current_) {}

template <class... Args>
typename CSVParser<Args...>::Iterator& CSVParser<Args...>::Iterator::operator++() {
    if (nullptr == parser_) 
        throw std::out_of_range("Out of range!");
            
    if (!parser_->parse(current_))
        parser_ = nullptr;
    return *this;
}

template <class... Args>
typename CSVParser<Args...>::Iterator CSVParser<Args...>::Iterator::operator++(int) {
    Iterator temp{*this};
    ++(*this);
    return temp;
}

template <class... Args>
bool CSVParser<Args...>::Iterator::operator==(const Iterator &other) const {
    if (!parser_ && !other.parser_) 
        return true;

    if (!parser_ || !other.parser_) 
        return false;
    
    return (&parser_->file_ == &other.parser_->file_);
}

template <class... Args>
bool CSVParser<Args...>::Iterator::operator!=(const Iterator &other) const {
    return !(*this == other);
}