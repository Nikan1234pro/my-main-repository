#pragma once 
#include <iostream>
#include <tuple>
#include <sstream>

//Handler to print tuple//

template <class StreamType, class TupleType, size_t Cur>
struct TuplePrinter {
    static void print(StreamType &os, const TupleType &t, char delim) {
        TuplePrinter<StreamType, TupleType, Cur - 1>::print(os, t, delim);
        os << delim << std::get<Cur>(t);
    } 
};

template <class StreamType, class TupleType>
struct TuplePrinter<StreamType, TupleType, 0> {
    static void print(StreamType &os, const TupleType &t, char delim) {
        os << std::get<0>(t);
    } 
};

template <class Ch, class Tr, class... Args>
auto& operator<<(std::basic_ostream<Ch, Tr> &os, const std::tuple<Args...> &t) {
    TuplePrinter<std::basic_ostream<Ch, Tr>, std::tuple<Args...>, sizeof...(Args) - 1>::print(os, t, '\t');
    return os;
}

//Handlers to fill item with a value//
template <class T>
void write_tuple(const std::string &str, T t) {
    std::stringstream ss{str};
    ss >> t;
} 

template <>
void write_tuple(const std::string &str, std::string& t) {
    t = str;
}

template <>
void write_tuple(const std::string &str, float& t) {
    t = std::stof(str);
}

template <>
void write_tuple(const std::string &str, double& t) {
    t = std::stod(str);
}

template <>
void write_tuple(const std::string &str, int& t) {
    t = std::stoi(str);
}

template <>
void write_tuple(const std::string &str, bool& t) {
    if (str == "true" || str == "true " || str.at(0) == '1')
		t = true;
	else if (str == "false" || str == "false " || str.at(0) == '0')
		t = false;
	else throw std::invalid_argument("Unknown type");
}

template <>
void write_tuple(const std::string &str, short int &t) {
    int tmp = std::stoi(str);
	if (tmp > std::numeric_limits<short>::max() || 
        tmp < std::numeric_limits<short>::min())
		throw std::out_of_range("Short out of range");
	t = tmp;
}

template <>
void write_tuple(const std::string &str, unsigned short int& t) {
	unsigned int tmp = std::stoi(str);
	if (tmp > std::numeric_limits<unsigned short>::max())
		throw std::out_of_range("Unsigned short out of range");
	t = tmp;
}

template <>
void write_tuple(const std::string &str, unsigned int& t) {
    t = std::stoul(str);
}



//Handlers to fill tuple with a values//
template<size_t Cur, class... Args> 
struct TupleFiller {
    static void fill(const std::vector<std::string> &line, std::tuple<Args...> &tuple) {
        TupleFiller<Cur - 1, Args...>::fill(line, tuple);
        write_tuple<decltype(std::get<Cur>(tuple))>(line[Cur], std::get<Cur>(tuple));
    }
};

template<class... Args> 
struct TupleFiller<0, Args...> {
    static void fill(const std::vector<std::string> &line, std::tuple<Args...> &tuple) {
        write_tuple<decltype(std::get<0>(tuple))>(line[0], std::get<0>(tuple));
    }
};
