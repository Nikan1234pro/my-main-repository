#include "gtest/gtest.h"
#include "../CSV_Parser/csv_parser.h"
#include <string>
#include <fstream>
#include <sstream>
#include <tuple>


//uses this delimiters:
constexpr char row_delim = '\n';    
constexpr char col_delim = ',';


TEST(Test_1, Constructor) {
    constexpr int lines_count = 3;

    std::string csv_table{ 
        std::string("1234,234.56")  + row_delim +
        std::string("2345,66.5432") + row_delim +
        std::string("9876,0.0012")  + row_delim 
    };

    std::vector<std::tuple<int, double>> lines {
        std::tuple<int, double>(1234, 234.56),
        std::tuple<int, double>(2345, 66.5432),
        std::tuple<int, double>(9876, 0.0012)
    };
 
    //skip i lines and check all readed
    for (int skip = 0; skip < lines_count; ++skip) {
        std::stringstream input;
        input << csv_table;
        CSVParser<int, double> parser(input, skip);
        int index = 0;
        for (auto t : parser) {
            EXPECT_EQ(t, lines.at(index + skip));
            index++;
        }
    };

    //try to skip more lines than file have 
    try {
        std::stringstream input;
        input << csv_table;
        //must throw exception
        CSVParser<int, double> parser(input, lines_count + 1);
        for (auto t : parser) {
            /* Nothing */
        }
        EXPECT_TRUE(false);
    }
    catch(...) {
        EXPECT_TRUE(true);
    }
}

TEST(Test_2, Parsing) {
    std::stringstream input;
    input << "1997,Ford,E350,\"ac, abs, moon\",3000.99" << row_delim << 
        "1999,Chevy,\"Venture \"\"Extended Edition\"\"\",\"\",4900.49" << row_delim << 
        "1996,Jeep,Grand Cherokee,\"MUST SELL! air, moon roof, loaded\",4799.00" << row_delim;

    std::vector<std::tuple<int, std::string, std::string, std::string, double>> lines {
        std::make_tuple(1997, "Ford", "E350", "ac, abs, moon", 3000.99),
        std::make_tuple(1999, "Chevy", "Venture \"Extended Edition\"", "", 4900.49),
        std::make_tuple(1996, "Jeep", "Grand Cherokee", "MUST SELL! air, moon roof, loaded", 4799.00)
    };

    int index = 0;
    CSVParser<int, std::string, std::string, std::string, double> parser(input, 0);
    for (auto t : parser) 
        EXPECT_EQ(t, lines.at(index++));
}

TEST(Test_3, Parsing_Errors_1) {
    //too many fields in line
    std::stringstream input;
    input << "0.001,\"test\",1234" << row_delim <<
        "1.234,hello,4321,\"Error_line\"" << row_delim; 

    try {
        CSVParser<float, std::string, int> parser(input, 0);
        for (auto t : parser) {}
        EXPECT_TRUE(false);
    } 
    catch(...) {
        EXPECT_TRUE(true);
    }
}

TEST(Test_4, Parsing_Errors_2) {
    //too few fields in line
    std::stringstream input;
    input << "0.001,\"test\",1234" << row_delim <<
        "1.234,hello" << row_delim; 

    try {
        CSVParser<float, std::string, int> parser(input, 0);
        for (auto t : parser) {}
        EXPECT_TRUE(false);
    } 
    catch(...) {
        EXPECT_TRUE(true);
    }
}

TEST(Test_5, Iterators) {
    std::string csv_table{ 
        std::string("hello,234.56") + row_delim +
        std::string("test,66.5432") + row_delim +
        std::string("9876,0.0012")  + row_delim 
    };

    std::vector<std::tuple<std::string, double>> lines {
        std::tuple<std::string, double>("hello", 234.56),
        std::tuple<std::string, double>("test", 66.5432),
        std::tuple<std::string, double>("9876", 0.0012)
    };

    //operator* for end-stream iterator
    {
        std::stringstream input{csv_table};
        CSVParser<std::string, double> parser(input, 0);
        auto end = parser.end();
        ASSERT_ANY_THROW(*end);
    }

    //postfix operator++ 
    {
        std::stringstream input{csv_table};
        CSVParser<std::string, double> parser(input, 0);
        auto first = parser.begin();
        auto second = first++;
        ASSERT_EQ(*first, lines[1]);
        ASSERT_EQ(*second, lines[0]);

        //end-stream iterator
        auto end = parser.end();
        ASSERT_ANY_THROW(end++);
    }

    //prefix operator++
    {
        std::stringstream input{csv_table};
        CSVParser<std::string, double> parser(input, 0);
        auto first = parser.begin();
        auto second = ++first;
        ASSERT_EQ(*first, lines[1]);
        ASSERT_EQ(*second, lines[1]);

        //end-stream iterator
        auto end = parser.end();
        ASSERT_ANY_THROW(++end);
    }

    //two stream iterators are equal if both of them are end-of-stream iterators 
    //or both of them refer to the same stream inside Parser.
    {
        //one reference
        std::stringstream input{csv_table};
        CSVParser<std::string, double> parser(input, 0);
        auto first = parser.begin();
        auto second = parser.begin();
        ASSERT_TRUE(first == second);

        while (first == second) 
            ++second;
        
        //if they not equal - it end-stream iterator
        ASSERT_EQ(second, parser.end());
    }

    {
        //two differernt parsers - one stream
        std::stringstream input{csv_table};
        CSVParser<std::string, double> parser1(input, 0);
        CSVParser<std::string, double> parser2(input, 0);
        ASSERT_EQ(parser1.begin(), parser2.begin());
        ASSERT_EQ(parser1.end(), parser2.end());
    }

    {
        //two different streams
        std::stringstream input_1{csv_table};
        std::stringstream input_2{csv_table};
        CSVParser<std::string, double> parser1(input_1, 0);
        CSVParser<std::string, double> parser2(input_2, 0);
        ASSERT_NE(parser1.begin(), parser2.begin());
        //end-stream iterators allways equal:
        ASSERT_EQ(parser1.end(), parser2.end());
    }
}
