#pragma once
#include <iostream>
#include "../graphics/visualizer.h"
#include <vector>

class Elem
{
private:
	int data;
	int index;
public:

	Elem() : data(0), index(-1)
	{}

	Elem(const Elem &other) : data(other.data), index(other.index)
	{
		std::cout << "Copy " << "arr[" << index << "] = " << data << std::endl;
	}

    Elem(int d, int i) : data(d), index(i) {}
 
    operator int() const {
        return data;
    }

	bool operator >(const Elem &other) const
	{
		Visualizer::getInstance().pushCommand(new Comparison_Greater(index, data, other.index, other.data));
		
		std::cout << "Check array[" << index << "] = " << data
			<< " > array[" << other.index << "] = " << other.data << "\n";
		return data > other.data;
	}

	bool operator <(const Elem &other) const
	{
		Visualizer::getInstance().pushCommand(new Comparison_Less(index, data, other.index, other.data));

		std::cout << "Check array[" << index << "] = " << data
			<< " < array[" << other.index << "] = " << other.data << "\n";
		return data < other.data;
	}

	void operator=(const Elem &other)
	{
		Visualizer::getInstance().pushCommand(new Assignment(index, other.data));
		if (other.index == -1) 
			std::cout << "arr[" << index << "] = arr[" << other.index << "] - temp" << std::endl; 
		data = other.data;
	}

	~Elem()
	{}
};
