#include <vector> 
#include "window.h"

class Command {
protected:
public:
    virtual void execute(std::vector<int> &container, std::vector<Color> &colors) const = 0;
};

class Comparison_Greater : public Command {
    int index1, index2;
    int value1, value2;
public:
    Comparison_Greater(int i, int xi, int j, int xj) : index1(i), value1(xi), index2(j), value2(xj) {}
    virtual void execute(std::vector<int> &container, std::vector<Color> &colors) const {
        colors[index1].set(0, 0, 255, 1);
        colors[index2].set(0, 0, 255, 1);
    }
};

class Comparison_Less : public Command {
    int index1, index2;
    int value1, value2;
public:
    Comparison_Less(int i, int xi, int j, int xj) : index1(i), value1(xi), index2(j), value2(xj) {}
    virtual void execute(std::vector<int> &container, std::vector<Color> &colors) const {
        colors[index1].set(0, 0, 255, 1);
        colors[index2].set(0, 0, 255, 1);
    }
};

class Assignment : public Command {
    int index;
    int value;
public:
    Assignment(int i, int xi) : index(i), value(i) {}
    virtual void execute(std::vector<int> &container, std::vector<Color> &colors) const {
        container[index] = value;
    }
};