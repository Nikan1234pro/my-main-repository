#pragma once
#include <typeinfo>
#include <cstdlib>
#include <algorithm>
#include <memory.h>
#include <stdexcept>

template <class T>
class deleter {
	int& count_ref;
	int start;
	int capacity;

public:
	deleter(int start_, int capacity_, int& count_ref_) : 
	start(start_), capacity(capacity_), count_ref(count_ref_) {
	}

	void operator()(T *ptr) {
		for (int i = 0; i < count_ref; i++)
			ptr[(start + i) % capacity].~T();
		delete[] reinterpret_cast<char*>(ptr);
	}
};


template <class T>
class CircularBuffer {
private:
	T* buffer;
	int capacity_;
	int size_;
	int start;
	void check_is_valid(int index) const;
	int make_index(int i) const;
	void replace_at(int i, const T& elem);
	void place_at(int i, const T& elem);
	T* get_pointer(int i) const;
	T* allocate(size_t size);
	void destruct_elem(int index);
	void destruct_range(int first, int last);
	void destruct_all();
	void move_range(int from_begin, int from_end, int to);
public:
	using value_type = T;

	CircularBuffer();
	CircularBuffer(const CircularBuffer& cb);
	CircularBuffer(int capacity, const T& elem);
	explicit CircularBuffer(int capacity);
	CircularBuffer& operator=(const CircularBuffer& cb);
	T& operator[](int i);
	const T& operator[](int i) const;
	T& at(int i);
	const T& at(int i) const;
	T& front();
	T& back();
	const T& front() const;
	const T& back() const;
	int size() const;
	int capacity() const;
	int reserve() const;
	bool full() const;
	bool empty() const;
	void push_back(const T& item = T());
	void push_front(const T& item = T());
	void pop_back();
	void pop_front();
	void rotate(int new_start);
	bool is_linearized() const;
	T* linearize();
	void set_capacity(int new_capacity_);
	void resize(int new_size, const T& item = T());
	void swap(CircularBuffer& cb);
	void insert(int pos, const T& item = T());
	void erase(int first, int last);
	void clear();															
	~CircularBuffer();										
};

template <class T>
void CircularBuffer<T>::check_is_valid(int index) const {
	if (index < 0 || index >= size_)
		throw std::out_of_range("Out of range");
}

template <class T>
T* CircularBuffer<T>::allocate(size_t size) {
	if (size == 0)
		return nullptr;
	return reinterpret_cast<T*>(new char[size * sizeof(T)]);
}

template <class T>
int CircularBuffer<T>::make_index(int i) const {
	if (capacity_ == 0)
		throw std::out_of_range("Out of range");
	return (capacity_ + start + i) % capacity_;
}

template <class T>
T* CircularBuffer<T>::get_pointer(int i) const {
	return buffer + make_index(i);
}

template <class T>
void CircularBuffer<T>::place_at(int i, const T & elem) {
	new (get_pointer(i)) T(elem);
}

template <class T>
void CircularBuffer<T>::replace_at(int i, const T & elem) {
	destruct_elem(i);
	place_at(i, elem);
}

template <class T>
void CircularBuffer<T>::destruct_elem(int index) {
	destruct_range(index, index + 1);
}

template <class T>
void CircularBuffer<T>::destruct_range(int first, int last) {				
	if (first > last)
		throw std::out_of_range("Wrong erase range");
	while (first != last)
		at(first++).~T();
}

template <class T>
void CircularBuffer<T>::destruct_all() {
	clear();
	delete[] reinterpret_cast<char*>(buffer);
}

template <class T>
void CircularBuffer<T>::move_range(int from_begin, int from_end, int to) {
	while (from_begin != from_end) {
		new (get_pointer(to)) T(std::move(at(from_begin)));
		destruct_elem(from_begin);
		from_begin++;
		to++;
	}
}

template <class T>
bool operator==(const CircularBuffer<T> & a, const CircularBuffer<T> & b) {
	if (&a == &b)
		return true;
	if (a.size() != b.size())
		return false;
	for (int i = 0; i < a.size(); i++)
		if (a[i] != b[i])
			return false;
	return true;
}

template <class T>
bool operator!=(const CircularBuffer<T> & a, const CircularBuffer<T> & b) {
	return !(a == b);
}

template <class T>
CircularBuffer<T>::CircularBuffer()
	: buffer(nullptr), capacity_(0), size_(0), start(0) {
}

template <class T>
CircularBuffer<T>::CircularBuffer(const CircularBuffer & cb)
	: capacity_(cb.capacity_), size_(cb.size_), start(cb.start) {
	buffer = allocate(capacity_);
	for (int i = 0; i < size_; i++)
		place_at(i, cb.at(i));
}

template <class T>
CircularBuffer<T>::CircularBuffer(int capacity, const T & elem)
	: capacity_(capacity), size_(capacity), start(0) {
	buffer = allocate(capacity_);
	for (int i = 0; i < capacity_; i++)
		place_at(i, elem);
}

template <class T>
CircularBuffer<T>::CircularBuffer(int capacity)
	: capacity_(capacity), size_(0), start(0) {
	buffer = allocate(capacity_);
}

template <class T>
CircularBuffer<T>& CircularBuffer<T>::operator=(const CircularBuffer & cb) {
	if (this == &cb)
		return *this;
	destruct_all();
	capacity_ = cb.capacity_;
	size_ = cb.size_;
	start = cb.start;
	buffer = allocate(capacity_);
	for (int i = 0; i < size_; i++)
		place_at(i, cb.at(i));
	return *this;
}

template <class T>
T & CircularBuffer<T>::operator[](int i) {
	return buffer[make_index(i)];
}

template <class T>
const T& CircularBuffer<T>::operator[](int i) const {
	return buffer[make_index(i)];
}

template <class T>
T& CircularBuffer<T>::at(int i) {
	check_is_valid(i);
	return (*this)[i];
}

template <class T>
const T& CircularBuffer<T>::at(int i) const {
	check_is_valid(i);
	return (*this)[i];
}

template <class T>
T& CircularBuffer<T>::front() {
	return at(0);
}

template <class T>
T& CircularBuffer<T>::back() {
	return at(size_ - 1);
}

template <class T>
const T& CircularBuffer<T>::front() const {
	return at(0);
}

template <class T>
const T& CircularBuffer<T>::back() const {
	return at(size_ - 1);
}

template <class T>
int CircularBuffer<T>::size() const {
	return size_;
}

template <class T>
int CircularBuffer<T>::capacity() const {
	return capacity_;
}

template <class T>
int CircularBuffer<T>::reserve() const {
	return capacity_ - size_;
}

template <class T>
bool CircularBuffer<T>::full() const {
	return size_ == capacity_;
}

template <class T>
bool CircularBuffer<T>::empty() const {
	if (!capacity_)
		return false;
	return !size_;
}

template <class T>
void CircularBuffer<T>::push_back(const T &item) {
	if (full()) {
		replace_at(0, item);
		start = make_index(1);
	}
	else
		place_at(size_++, item);

}

template <class T>
void CircularBuffer<T>::push_front(const T &item) {
	start = make_index(-1);
	if (full())
		replace_at(0, item);
	else {
		place_at(0, item);
		++size_;
	}
}

template <class T>
void CircularBuffer<T>::pop_back() {
	destruct_elem(size_ - 1);
	--size_;
}

template <class T>
void CircularBuffer<T>::pop_front() {
	destruct_elem(0);
	start = make_index(1);
	--size_;
}

template <class T>	
void CircularBuffer<T>::rotate(int new_start) {
	check_is_valid(new_start);
	if (full()) {
		start = make_index(new_start);
		return;
	}
	for (int i = 0; i < new_start; i++) {
		move_range(0, 1, size_);
		start = make_index(1);
	}
}

template <class T>
bool CircularBuffer<T>::is_linearized() const {
	return !start;
}

template <class T>
T* CircularBuffer<T>::linearize() {
	if (!size_)
		return nullptr;
	int index = 0;
	int save = size_;
	T *ptr = allocate(capacity_);
	std::unique_ptr<T, deleter<T>> tmp(ptr, deleter<T>(0, capacity_, index));
	for (; index < size_; index++) 
		new (tmp.get() + index) T(std::move(at(index)));
	destruct_all();
	buffer = tmp.release();
	start = 0;
	size_ = save;
	return buffer;
}

template <class T>
void CircularBuffer<T>::set_capacity(int new_capacity_) {
	int index = 0;
	int new_size_ = (size_ < new_capacity_) ? size_ : new_capacity_;
	T *ptr = allocate(new_capacity_);
	std::unique_ptr<T, deleter<T>> tmp(ptr, deleter<T>(start, new_capacity_, index));
	for (; index < new_size_; index++) 
		new(tmp.get() + (start + index) % new_capacity_) T(std::move(at(index)));
	destruct_all();
	start = (new_capacity_ > 0) ? start % new_capacity_ : 0;
	capacity_ = new_capacity_;
	size_ = new_size_;
	buffer = tmp.release();
}

template <class T>
void CircularBuffer<T>::resize(int new_size, const T & item) {
	set_capacity(new_size);
	while (size_ < capacity_)
		push_back(item);
}

template <class T>
void CircularBuffer<T>::swap(CircularBuffer & cb) {
	std::swap(buffer, cb.buffer);
	std::swap(capacity_, cb.capacity_);
	std::swap(size_, cb.size_);
	std::swap(start, cb.start);
}

template <class T>
void CircularBuffer<T>::insert(int pos, const T & item) {
	if (full())
		pop_back();
	if (pos == 0) {
		start = make_index(-1);
		++size_;
		place_at(0, item);
		return;
	}
	int move_src = pos;
	int move_src_end = size_;
	++size_;
	for (; move_src_end != move_src; move_src_end--)
		move_range(move_src_end - 1, move_src_end, move_src_end);
	place_at(pos, item);
}

template <class T>
void CircularBuffer<T>::erase(int first, int last) {
	int length = last - first;
	if (length == 0) {
		if (first > size_)
			throw std::out_of_range("Wrong erase range!");
		return;
	}
	if (length < 0)
		throw std::out_of_range("Wrong erase range!");
	destruct_range(first, last);
	if (first == 0) {
		start = make_index(last);
		size_ -= length;
		return;
	}
	int move_src = last;
	int move_src_end = size_;
	int move_dest = first;
	for (; move_src < move_src_end; move_src++, move_dest++)
		move_range(move_src, move_src + 1, move_dest);
	size_ -= length;
}

template <class T>
void CircularBuffer<T>::clear() {
	erase(0, size_);
}

template <class T>
CircularBuffer<T>::~CircularBuffer() {
	destruct_all();
}


