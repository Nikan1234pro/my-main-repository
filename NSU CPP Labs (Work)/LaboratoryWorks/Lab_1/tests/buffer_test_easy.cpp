#include "gtest/gtest.h"
#include "../main/circular_buffer.h"

using Container_Type = CircularBuffer<char>;

CircularBuffer<char> makeSequence(std::size_t count) {
	CircularBuffer<char> buf(count);
	char ch = 0;
	for (int i = 0; i < count; i++)
		buf.push_back(ch++);
	return buf;
}

template <class ElemT, class Test> 
void CycleTest(CircularBuffer<ElemT> &buf, Test &test) {
	size_t steps = buf.size() * 2;
	for (int i = 0; i < steps; i++) {
		test(buf, i);
		buf.pop_back();
		buf.push_front(ElemT{});
	}
} 

TEST(Test_1, DefaultConstructor) {
	EXPECT_NO_THROW(Container_Type{});
}

TEST(Test_2, FillConstructors) {
	constexpr std::size_t normal_elem_count(10);
	{
		Container_Type buffer(normal_elem_count);
		EXPECT_EQ(normal_elem_count, buffer.capacity());
		EXPECT_EQ(0, buffer.size());
	}

	{
		constexpr char value = 0x0F;
		Container_Type buffer(normal_elem_count, value);
		EXPECT_EQ(normal_elem_count, buffer.capacity());
		EXPECT_EQ(normal_elem_count, buffer.size());
		for (int i = 0; i < buffer.size(); i++)
			EXPECT_EQ(value, buffer[i]);
	}
	EXPECT_ANY_THROW(Container_Type(-1));
	EXPECT_ANY_THROW(Container_Type(-1, 0x00));
}

TEST(Test_3, CopyConstructor) {
	constexpr std::size_t elem_count = 10;
	Container_Type first(makeSequence(elem_count));
	char ch = 0;
	
	Container_Type second(first);
	EXPECT_EQ(first.size(), second.size());
	EXPECT_EQ(first.capacity(), second.capacity());

	ch = 0;
	for (int i = 0; i < elem_count; i++)
		EXPECT_EQ(ch++, second[i]);

	Container_Type empty;
	Container_Type copy(empty);
	EXPECT_EQ(0, copy.size());
	EXPECT_EQ(0, copy.capacity());
}

TEST(Test_4, EqualsCopy) {
	constexpr std::size_t elem_count{10};
	Container_Type first(makeSequence(elem_count));
	Container_Type copy;

	copy = first;
	EXPECT_EQ(elem_count, copy.size());
	EXPECT_EQ(elem_count, copy.capacity());
	char ch = 0;
	for (int i = 0; i < elem_count; i++) {
		EXPECT_EQ(ch, first[i]);
		EXPECT_EQ(ch, copy[i]);
		EXPECT_NE(&first[i], &copy[i]);
		++ch;
	}

	Container_Type empty;
	Container_Type copy2(empty);
	EXPECT_EQ(0, copy2.size());
	EXPECT_EQ(0, copy2.capacity());
}

TEST(Test_5, EqualsSelf) {
	constexpr std::size_t elem_count{10};
	Container_Type buf(makeSequence(elem_count));
	buf = *&buf;
	char ch = 0;
	for (int i = 0; i < elem_count; i++) 
		EXPECT_EQ(ch++, buf[i]);
}

TEST(Test_6, Push_and_pop) {
	constexpr std::size_t elem_count{10};
	char expected_front{0x00};
	char expected_back{0x09};
	Container_Type buf(makeSequence(elem_count));

	for (int i = 0; i < 100; i++) {
		const Container_Type const_buf(buf);

		EXPECT_EQ(expected_front, buf.front());
		EXPECT_EQ(expected_front, const_buf.front());
		EXPECT_EQ(expected_back, buf.back());
		EXPECT_EQ(expected_back, const_buf.back());

		expected_front++;
		expected_back++;
		buf.pop_front();
		buf.push_back(expected_back);
	}

	for (int i = 0; i < 100; i++) {
		const Container_Type const_buf(buf);
		EXPECT_EQ(expected_front, buf.front());
		EXPECT_EQ(expected_front, const_buf.front());
		EXPECT_EQ(expected_back, buf.back());
		EXPECT_EQ(expected_back, const_buf.back());

		expected_front--;
		expected_back--;
		buf.pop_back();
		buf.push_front(expected_front);
	}
	//empty buffer
	EXPECT_ANY_THROW(Container_Type().pop_back());
	EXPECT_ANY_THROW(Container_Type().pop_front());
	EXPECT_ANY_THROW(Container_Type().push_back(0x01));
	EXPECT_ANY_THROW(Container_Type().push_back(0x01));
	EXPECT_ANY_THROW(Container_Type().back());
	EXPECT_ANY_THROW(Container_Type().front());
}

TEST(Test_7, At) {
	constexpr std::size_t elem_count{10};
	Container_Type buf(makeSequence(elem_count));

	CycleTest(buf, [](Container_Type buf, int cycle) {
		int expected_size{10};
		EXPECT_EQ(expected_size, buf.size());

		int zero_count = std::min(cycle, expected_size);
		for (int i = 0; i < zero_count; i++) {
			EXPECT_EQ(0x00, buf[i]);
			EXPECT_EQ(0x00, buf.at(i));

			const Container_Type const_buf(buf);
			EXPECT_EQ(0x00, const_buf[i]);
			EXPECT_EQ(0x00, const_buf.at(i));
		}
	});
	Container_Type copy1(buf);
	Container_Type copy2(buf);
	EXPECT_ANY_THROW(copy1.at(-1));
	EXPECT_ANY_THROW(copy2.at(elem_count));
}

TEST(Test_8, Resize) {
	constexpr char elem{0x0F};
	//Increase
	{
		constexpr std::size_t old_size{5};	
		constexpr std::size_t new_size{10};
		Container_Type buf(makeSequence(old_size));
		buf.resize(new_size, elem);
		EXPECT_EQ(new_size, buf.capacity());
		EXPECT_EQ(new_size, buf.size());
		char ch = 0;
		for (int i = 0; i < old_size; i++)
			EXPECT_EQ(ch++, buf.at(i));
		for (int i = old_size; i < new_size; i++) 
			EXPECT_EQ(elem, buf.at(i));
	}
	//Decrease
	{
		constexpr std::size_t old_size{10};
		constexpr std::size_t new_size{5};
		Container_Type buf(makeSequence(old_size));
		buf.resize(new_size, elem);
		EXPECT_EQ(new_size, buf.capacity());
		EXPECT_EQ(new_size, buf.size());
		char ch = 0;
		for (int i = 0; i < new_size; i++)
			EXPECT_EQ(ch++, buf.at(i));
	}
	//Empty
	{
		constexpr std::size_t new_size{10};
		Container_Type empty;
		Container_Type copy(empty);
		copy.resize(new_size, elem);
		EXPECT_EQ(new_size, copy.size());
		for (int i = 0; i < copy.size(); i++)
			EXPECT_EQ(elem, copy.at(i));
		copy.resize(0);
		EXPECT_EQ(0, copy.capacity());
		EXPECT_EQ(0, copy.size());
	}
	//Wrong parameter
	{	
		Container_Type c(makeSequence(10));
		EXPECT_ANY_THROW(c.resize(-1));
	}
}

TEST(Test_9, Set_capacity) {
	//Increase
	{
		constexpr std::size_t old_size{5};
		constexpr std::size_t new_size{10};
		Container_Type buf(makeSequence(old_size));
		buf.set_capacity(new_size);
		EXPECT_EQ(new_size, buf.capacity());
		EXPECT_EQ(old_size, buf.size());
		char ch = 0;
		for (int i = 0; i < old_size; i++)
			EXPECT_EQ(ch++, buf.at(i));
		
		//set_capacity() leaves tail empty 
		EXPECT_ANY_THROW(buf.at(old_size));
	}
	//Decrease
	{
		constexpr std::size_t old_size{10};
		constexpr std::size_t new_size{5};
		Container_Type buf(makeSequence(old_size));
		buf.set_capacity(new_size);
		EXPECT_EQ(new_size, buf.capacity());
		EXPECT_EQ(new_size, buf.size());
		char ch = 0;
		for (int i = 0; i < new_size; i++)
			EXPECT_EQ(ch++, buf.at(i));
	}
	//Empty
	{
		constexpr std::size_t new_size{10};
		Container_Type empty;
		Container_Type copy(empty);
		copy.set_capacity(new_size);
		EXPECT_EQ(new_size, copy.capacity());
		EXPECT_EQ(0, copy.size());
		copy.set_capacity(0);
		EXPECT_EQ(0, copy.capacity());
		EXPECT_EQ(0, copy.size());
	}
	//Wrong parameter
	{
		Container_Type c(makeSequence(10));
		EXPECT_ANY_THROW(c.resize(-1));
	}
}

TEST(Test_10, Swap) {
	constexpr std::size_t size_c1{10};
	constexpr std::size_t size_c2{15};
	char ch{0};
	Container_Type c1(makeSequence(size_c1));
	Container_Type c2(makeSequence(size_c2));

	c1.swap(c2);
	EXPECT_EQ(size_c2, c1.size());
	for (int i = 0; i < c1.size(); i++)
		EXPECT_EQ(ch++, c1[i]);
	ch = 0;
	EXPECT_EQ(size_c1, c2.size());
	for (int i = 0; i < c2.size(); i++)
		EXPECT_EQ(ch++, c2[i]);
}

TEST(Test_11, Clear) {
	constexpr std::size_t size{10};
	Container_Type buf(makeSequence(size));

	//clear() destroys elements, but capacity remains the same
	buf.clear();
	EXPECT_EQ(size, buf.capacity());
	EXPECT_EQ(0, buf.size());
 	EXPECT_EQ(size, buf.reserve());
	EXPECT_TRUE(buf.empty());
 	EXPECT_FALSE(buf.full());

	Container_Type empty;
	empty.clear();		//why not?
}

TEST(Test_12, Insert_1) {
	constexpr int capacity{4};
	Container_Type buf(makeSequence(capacity));
	//	0)	0	1	2	3
	//	1)	a	0	1	2
	//	2)  a	b	0	1
	//	3)	a	b	0	c
	char a = 'a';
	char b = 'b';
	char c = 'c';

	//Insert at the beginning
	buf.insert(0, a);
	EXPECT_EQ(capacity, buf.size());
	EXPECT_EQ(capacity, buf.capacity());
	EXPECT_EQ(a, buf[0]);
	EXPECT_EQ(0x00, buf[1]);
	EXPECT_EQ(0x01, buf[2]);
	EXPECT_EQ(0x02, buf[3]);

	//Insert in the middle
	buf.insert(1, b);
	EXPECT_EQ(capacity, buf.size());
	EXPECT_EQ(capacity, buf.capacity());
	EXPECT_EQ(a, buf[0]);
	EXPECT_EQ(b, buf[1]);
	EXPECT_EQ(0x00, buf[2]);
	EXPECT_EQ(0x01, buf[3]);

	//Insert in the end
	buf.insert(3, c);
	EXPECT_EQ(capacity, buf.size());
	EXPECT_EQ(capacity, buf.capacity());
	EXPECT_EQ(a, buf[0]);
	EXPECT_EQ(b, buf[1]);
	EXPECT_EQ(0x00, buf[2]);
	EXPECT_EQ(c, buf[3]);
}

TEST(Test_13, Insert_2) {	
	constexpr int capacity{6};
	Container_Type buf(makeSequence(capacity));
	buf.pop_back();
	buf.pop_back();
	//	0)	0	1	2	3
	//	1)	a	0	1	2	3
	//	2)  a	b	0	1	2	3
	//	3)	a	b	0	c	1	2
	char a = 'a';
	char b = 'b';
	char c = 'c';

	//Insert at the beginning
	buf.insert(0, a);
	EXPECT_EQ(capacity - 1, buf.size());
	EXPECT_EQ(capacity, buf.capacity());
	EXPECT_EQ(a, buf[0]);
	EXPECT_EQ(0x00, buf[1]);
	EXPECT_EQ(0x01, buf[2]);
	EXPECT_EQ(0x02, buf[3]);
	EXPECT_EQ(0x03, buf[4]);

	//Insert in the middle
	buf.insert(1, b);
	EXPECT_EQ(capacity, buf.size());
	EXPECT_EQ(capacity, buf.capacity());
	EXPECT_EQ(a, buf[0]);
	EXPECT_EQ(b, buf[1]);
	EXPECT_EQ(0x00, buf[2]);
	EXPECT_EQ(0x01, buf[3]);
	EXPECT_EQ(0x02, buf[4]);
	EXPECT_EQ(0x03, buf[5]);

	//Insert in the end
	buf.insert(3, c);
	EXPECT_EQ(capacity, buf.size());
	EXPECT_EQ(capacity, buf.capacity());
	EXPECT_EQ(a, buf[0]);
	EXPECT_EQ(b, buf[1]);
	EXPECT_EQ(0x00, buf[2]);
	EXPECT_EQ(c, buf[3]);
	EXPECT_EQ(0x01, buf[4]);
	EXPECT_EQ(0x02, buf[5]);
}

TEST(Test_14, Insert_3) {
	constexpr int max_capacity = 10;
	constexpr char default_value = 0x00;
	constexpr char adding_value = 0x0F;

	//placing elem after last buffer element
	for (int capacity = 0; capacity < max_capacity; capacity++) {
		Container_Type buf(capacity, default_value);
		//add place for new elem
		buf.set_capacity(capacity + 1);
		buf.insert(capacity, adding_value);
		for (int i = 0; i < buf.size() - 1; i++)
			EXPECT_EQ(default_value, buf.at(i));
		EXPECT_EQ(adding_value, buf.back());
	}

	//wrong insertion
	{
		//don't have enough space
		Container_Type buf(max_capacity, default_value);
		EXPECT_ANY_THROW(buf.insert(max_capacity, adding_value));

		Container_Type empty;
		EXPECT_ANY_THROW(empty.insert(0, adding_value));
	}

	{	
		Container_Type buf(max_capacity);
		EXPECT_ANY_THROW(buf.insert(-1, adding_value));
	}
}

TEST(Test_15, EraseRange_1) {
	Container_Type buf(makeSequence(12));		//start = 0
    buf.erase(0, 3);
	//  0	1	2	3	4	5	6	7	8	9	10	11	
	//	x	x	x											
	buf.erase(6, 9);
	//  3	4	5	6	7	8	9	10	11	
	//							x	x	x	
	buf.erase(2, 3);
	//  3	4	5	6	7	8	
	//			x				
	buf.erase(1, 3);
	//	3	4	6	7	8	
	//		x	x
	EXPECT_EQ(3, buf.at(0));
	EXPECT_EQ(7, buf.at(1));
	EXPECT_EQ(8, buf.at(2));

	EXPECT_EQ(12, buf.capacity());
	EXPECT_EQ(3, buf.size());
}

TEST(Test_16, EraseRange_2) {
	Container_Type buf(makeSequence(12));
	buf.push_back(12);
	buf.push_back(13);			//start = 2
    buf.erase(0, 3);
	//        start 
	//  12	13	2	3	4	5	6	7	8	9	10	11	
	//			x	x	x									
	buf.erase(6, 9);
	//		  start
	//  12	13	5	6	7	8	9	10	11	
	//	x	x							x	
	buf.erase(2, 3);
	//start
	//  5	6	7	8	9	10
	//			x				
	buf.erase(1, 3);
	//start
	//	5	6	8	9	10
	//		x	x
	EXPECT_EQ(5, buf[0]);
	EXPECT_EQ(9, buf[1]);
	EXPECT_EQ(10, buf[2]);

	Container_Type buf_2(10, 0x0A);
	buf_2.erase(0, 10);
	EXPECT_TRUE(buf_2.empty());
	EXPECT_EQ(10, buf_2.capacity());
}

TEST(Test_17, Rotate) {
	constexpr std::size_t elem_count{8};
	constexpr std::size_t capacity{20};

	Container_Type buf(capacity);
	for (int elems = 0; elems < capacity; elems++) {
		char ch = 0;
		for (int i = 0; i < elems; i++)
			buf.push_back(ch++);
		
		for (int i = 0; i < buf.size(); i++) {
			buf.rotate(i);
			ch = (ch + i) % buf.size();
			char tmp = ch;
			for (int j = 0; j < buf.size(); j++) 
				ASSERT_EQ((tmp + j) % buf.size(), buf[j]);
		}
		buf.clear();
	}
}

TEST(Test_18, Is_Linearized) {
	constexpr std::size_t elem_count = 10; 
	//Full
	{
		Container_Type buf(makeSequence(elem_count));
		EXPECT_TRUE(buf.is_linearized());
		buf.push_back(0x0A);			//  A	B	2	3	4	5	6	7	8	9
		buf.push_back(0x0B);			//		  start
		EXPECT_FALSE(buf.is_linearized());		
	}	
	//Not full
	{
		Container_Type buf(makeSequence(elem_count));
		buf.pop_back();					//	x	1	2	3	4	5	6	7	8	x
		buf.pop_front();				//	  start
		EXPECT_FALSE(buf.is_linearized());
	}
}

TEST(Test_19, Linearization) {
	constexpr std::size_t capacity{10};
	Container_Type buf(capacity);
	for (int iter = 0; iter < 100; iter++) {
		char ch = 0;
		for (int elem_count = 0; elem_count < iter; elem_count++) {
			buf.push_back(ch++);
		}
		for (int j = 0; j < buf.size(); j++) {
			buf.pop_front();

			char *ptr = buf.linearize();
			if (!buf.empty())
				EXPECT_NE(nullptr, ptr);
			for (int i = 0; i < buf.size(); i++)
				EXPECT_EQ(&buf.at(i), &ptr[i]);
		}
		buf.clear();
	}
}

TEST(Test_20, EqualityOperator) {
	constexpr std::size_t capacity{10};
	//Empty
	{
		Container_Type empty_1;
		Container_Type empty_2;
		EXPECT_TRUE(empty_1 == empty_2);
		EXPECT_FALSE(empty_1 != empty_2);

		empty_1.resize(capacity);
		EXPECT_FALSE(empty_1 == empty_2);
		EXPECT_TRUE(empty_1 != empty_2);
		empty_1.set_capacity(0);

		EXPECT_TRUE(empty_1 == empty_2);
		EXPECT_FALSE(empty_1 != empty_2);
	}
	
	{
		Container_Type first(makeSequence(capacity));
		Container_Type second(makeSequence(2 * capacity));
		EXPECT_TRUE(first != second);
		EXPECT_FALSE(first == second);
		for (int i = 0; i < capacity; i++)
			second.pop_back();
		EXPECT_TRUE(first == second);
		EXPECT_FALSE(first != second);

		first.rotate(1);
		EXPECT_TRUE(first != second);
		EXPECT_FALSE(first == second);

		first.pop_back();
		second.pop_front();
		EXPECT_TRUE(first == second);
		EXPECT_FALSE(first != second);

		first.clear();
		second.push_back(0xFF);
		second.erase(0, capacity);
		EXPECT_TRUE(first == second);
		EXPECT_FALSE(first != second);
	}
}












