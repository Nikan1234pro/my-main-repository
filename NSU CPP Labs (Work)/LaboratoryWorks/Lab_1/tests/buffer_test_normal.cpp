#include "gtest/gtest.h"
#include "../main/circular_buffer.h"
#include <iostream>
#include <string>

struct Constructors {
	int default_ = 0;
	int copy_ = 0;

	Constructors() 
	: default_(0), 
	  copy_(0) {} 
	Constructors(const Constructors &other) 
	: default_(other.default_), 
	  copy_(other.copy_) {}

	void reset() {
		default_ = 0;
		copy_ = 0;
	}
}; 

class ConstructorTest {
	static Constructors counter;
public:
	ConstructorTest() {
		++counter.default_;
	}
	ConstructorTest(const ConstructorTest &other) {
		++counter.copy_;
	}
	static auto check_and_reset() -> Constructors {
		auto tmp{counter};
		counter.reset();
		return tmp;
	}
	static void reset() {
		counter.reset();
	}
	static auto check() -> Constructors {
		return counter;
	}
};

class DestructorTest {
	static int destructions_count;
public:
	DestructorTest() {
	}
	static int check_and_reset() {
		int tmp = destructions_count;
		destructions_count = 0;
		return tmp;
	}
	static void reset() {
		destructions_count = 0;
	}
	static int check() {
		return destructions_count;
	}
	~DestructorTest() {
		++destructions_count;
	}
};

class RemovableElem {
	int *ptr;
public:
	RemovableElem() : ptr(nullptr) {
	}
	RemovableElem(int *p) : ptr(p) {
	}
	RemovableElem(const RemovableElem &other) : ptr(other.ptr) {
	}
	bool is_removable() const {
		return ptr != nullptr;
	}
	~RemovableElem() {
		if (nullptr != ptr)
			(*ptr)++;
	}
};

Constructors ConstructorTest::counter;
int DestructorTest::destructions_count = 0;

TEST(Test_1, DefaultConstructor) {
	CircularBuffer<ConstructorTest> buf{};
	EXPECT_EQ(0, ConstructorTest::check_and_reset().default_);
}

TEST(Test_2, FillConstructors_CopyConstructor) {
	constexpr int capacity = 10;
	{
		//Fill #1
		CircularBuffer<ConstructorTest> buf(capacity);	//we only allocate memory, not construct objects!
		EXPECT_EQ(0, ConstructorTest::check_and_reset().default_);
	}

	{
		//Fill #2
		ConstructorTest item{};
		ConstructorTest::reset();						//because default__constructor_count = 1 now
		CircularBuffer<ConstructorTest> buf(capacity, item);
		auto result = ConstructorTest::check_and_reset();
		EXPECT_EQ(0, result.default_);
		EXPECT_EQ(capacity, result.copy_);

		//Copy
		CircularBuffer<ConstructorTest> copy_(buf);
		result = ConstructorTest::check_and_reset();
		EXPECT_EQ(0, result.default_);
		EXPECT_EQ(capacity, result.copy_);
	}
}

TEST(Test_3, Equals) {
	{
		constexpr int capacity = 10;
		CircularBuffer<ConstructorTest> first(capacity, ConstructorTest{});
		CircularBuffer<ConstructorTest> second{};
		ConstructorTest::reset();
		second = first;
		auto result = ConstructorTest::check_and_reset();
		EXPECT_EQ(0, result.default_);
		EXPECT_EQ(capacity, result.copy_);
	}
	
	{
		DestructorTest::reset();
		constexpr int capacity_first = 10;
		constexpr int capacity_second = 5;
		CircularBuffer<DestructorTest> first(capacity_first, DestructorTest{});
		CircularBuffer<DestructorTest> second(capacity_second, DestructorTest{});
		DestructorTest::reset();
		second = first;
		EXPECT_EQ(capacity_second, DestructorTest::check_and_reset());
	}
}

TEST(Test_4, EqualsSelf) {	//in this case do nothing - is better
	constexpr int capacity{10};
	CircularBuffer<ConstructorTest> p(capacity, ConstructorTest{});
	CircularBuffer<DestructorTest> q(capacity, DestructorTest{});
	ConstructorTest::reset();
	DestructorTest::reset();
	p = *&p;
	q = *&q;
	EXPECT_EQ(0, ConstructorTest::check().default_);
	EXPECT_EQ(0, ConstructorTest::check().copy_);
	EXPECT_EQ(0, DestructorTest::check());
	ConstructorTest::reset();
	DestructorTest::reset();
}

TEST(Test_5, Push_1) {
	constexpr int capacity = 10;
	ConstructorTest elem;
	ConstructorTest::reset();
	//back
	{
		CircularBuffer<ConstructorTest> buf(capacity);
		for (int i = 0; i < capacity; i++) {
			buf.push_back(elem);
			EXPECT_EQ(i + 1, ConstructorTest::check().copy_);		
		}
	}
	ConstructorTest::reset();
	//front
	{
		CircularBuffer<ConstructorTest> buf(capacity);
		for (int i = 0; i < capacity; i++) {
			buf.push_front(elem);
			EXPECT_EQ(i + 1, ConstructorTest::check().copy_);		
		}
	}
	ConstructorTest::reset();
}

TEST(Test_6, Push_2) {
	//Check, what pushing in full buffer destroys elements at the beginning and at the end
	int count = 0;
	constexpr int capacity = 10;
	RemovableElem mark{&count};
	{
		CircularBuffer<RemovableElem> buf{capacity};
		buf.push_back(mark);
		for (int i = 1; i < capacity; i++)
			buf.push_back(RemovableElem{nullptr});

		buf.push_back(RemovableElem{nullptr});	//first element must be deleted
		EXPECT_EQ(1, count);
	}
	count = 0;
	{
		CircularBuffer<RemovableElem> buf{capacity};
		for (int i = 0; i < capacity - 1; i++)
			buf.push_back(RemovableElem{nullptr});
		buf.push_back(mark);

		buf.push_front(RemovableElem{nullptr});	//last element must be deleted
		EXPECT_EQ(1, count);
	}
}

TEST(Test_7, Pop) {
	constexpr int capacity = 10;
	{
		CircularBuffer<DestructorTest> buf(capacity);
		for (int i = 0; i < capacity; i++)
			buf.push_back(DestructorTest{});
		DestructorTest::reset();
		for (int i = 0; i < buf.capacity(); i++) {
			buf.pop_back();
			EXPECT_EQ(i + 1, DestructorTest::check());
		}
	}
	DestructorTest::reset();
	{
		CircularBuffer<DestructorTest> buf(capacity);
		for (int i = 0; i < capacity; i++)
			buf.push_back(DestructorTest{});
		DestructorTest::reset();
		for (int i = 0; i < buf.capacity(); i++) {
			buf.pop_front();
			EXPECT_EQ(i + 1, DestructorTest::check());
		}
	}
	DestructorTest::reset();
}

TEST(Test_8, Clear) {
	//Clear empty buffer
	DestructorTest::reset();
	CircularBuffer<DestructorTest> empty;
	empty.clear();
	EXPECT_EQ(0, DestructorTest::check_and_reset());

	//Clear normal buffer
	constexpr int capacity = 10;
	CircularBuffer<DestructorTest> buf(capacity);
	for (int elem_count = 0; elem_count < capacity; elem_count++) {
		for (int i = 0; i < elem_count; i++)
			buf.push_back(DestructorTest{});
		DestructorTest::reset();
		buf.clear();
		EXPECT_EQ(elem_count, DestructorTest::check_and_reset());
	}
}

TEST(Test_9, Rotate) {	//Test that count fo copy_ constructor call = count of destructor call
	constexpr int capacity{15};
	
	CircularBuffer<ConstructorTest> first{capacity};
	CircularBuffer<DestructorTest> second{capacity};

	for (int elem_count = 0; elem_count < capacity; elem_count++) {
		for (int i = 0; i < elem_count; i++) {
			first.push_back(ConstructorTest{});
			second.push_back(DestructorTest{});
		}
		ConstructorTest::reset();
		DestructorTest::reset();
		EXPECT_EQ(first.size(), second.size());
		for (int i = 0; i < first.size(); i++) {
			first.rotate(i);
			second.rotate(i);
		}
		EXPECT_EQ(0, ConstructorTest::check().default_);
		EXPECT_EQ(ConstructorTest::check().copy_, DestructorTest::check());
		ConstructorTest::reset();
		DestructorTest::reset();
	}
}

TEST(Test_10, Linearize) {
	constexpr int capacity{10};
	CircularBuffer<ConstructorTest> first{capacity};
	CircularBuffer<DestructorTest> second{capacity};
	for (int iter = 0; iter < 100; iter++) {
		for (int elem_count = 0; elem_count < iter; elem_count++) {
			first.push_back(ConstructorTest{});
			second.push_back(DestructorTest{});
		}
		ConstructorTest::reset();
		DestructorTest::reset();
		EXPECT_EQ(first.size(), second.size());
		for (int j = 0; j < first.size(); j++) {
			first.pop_front();
			second.pop_front();
			ConstructorTest::reset();
			DestructorTest::reset();
			first.linearize();
			second.linearize();
			EXPECT_EQ(0, ConstructorTest::check().default_);
			EXPECT_EQ(ConstructorTest::check().copy_, DestructorTest::check());
		}
		first.clear();
		second.clear();
	}
	ConstructorTest::reset();
	DestructorTest::reset();
}

TEST(Test_11, Erase) {
	int count = 0;
	constexpr int capacity{10};
	RemovableElem removable{&count};
	//Erase
	CircularBuffer<RemovableElem> buf{capacity};
	for (int last = 0; last <= buf.capacity(); last++) {
		for (int first = 0; first < last; first++) {
			for (int i = 0; i < first; i++)
				buf.push_back(RemovableElem{});
			for (int i = first; i < last; i++)
				buf.push_back(removable);
			while (!buf.full()) 
				buf.push_back(RemovableElem{});
			buf.erase(first, last);

			EXPECT_EQ(count, last - first);
			count = 0;
			buf.clear();
		}
	}
}

TEST(Test_12, Insert) {
	//Insertation in full buffer destroys last element
	int count = 0;
	constexpr int capacity{10};
	RemovableElem removable{&count};
	CircularBuffer<RemovableElem> buf{capacity};
	for (int i = 0; i < buf.capacity() - 1; i++)
		buf.push_back(RemovableElem{});	//last
	buf.push_back(removable);
	buf.insert(0, RemovableElem{});
	EXPECT_EQ(1, count);
}

TEST(Test_13, Resize) {
	constexpr int capacity{10};
	//Check constructors
	{
		CircularBuffer<ConstructorTest> q;
		q.resize(capacity, ConstructorTest{});

		EXPECT_EQ(1, ConstructorTest::check().default_); //only in call to resize
		EXPECT_EQ(10, ConstructorTest::check().copy_);
	}
	//Check destructors
	{
		CircularBuffer<DestructorTest> q;
		q.resize(capacity, DestructorTest{});
		EXPECT_EQ(1, DestructorTest::check_and_reset()); 			//only in call to resize
	}
	//Destruct tail
	{
		int count = 0;
		RemovableElem removable{&count};
		for (int new_capacity = 0; new_capacity <= capacity; new_capacity++) {
			CircularBuffer<RemovableElem> q{capacity};
			for (int i = 0; i < new_capacity; i++)
				q.push_back(RemovableElem{});
			for (int i = new_capacity; i < capacity; i++)
				q.push_back(removable);
			q.resize(new_capacity, RemovableElem{});
			for (int i = 0; i < q.size(); i++)
				EXPECT_FALSE(q.back().is_removable());	//we remove all removable elems at the tail
			EXPECT_EQ(capacity - new_capacity, count);
			count = 0;
		}
	}
	ConstructorTest::reset();
	DestructorTest::reset();
}

TEST(Test_14, Set_capacity) {
	constexpr int capacity{10};
	//Not construct objects during the increase
	{
		CircularBuffer<ConstructorTest> empty;
		empty.set_capacity(capacity);
		EXPECT_EQ(0, ConstructorTest::check().default_);
		EXPECT_EQ(0, ConstructorTest::check().copy_);
	}
	//Destructor
	{
		CircularBuffer<DestructorTest> q;
		q.set_capacity(capacity);
		EXPECT_EQ(0, DestructorTest::check_and_reset());
	}
	//Destruct tail
	{
		int count = 0;
		RemovableElem removable{&count};
		for (int new_capacity = 0; new_capacity <= capacity; new_capacity++) {
			CircularBuffer<RemovableElem> q{capacity};
			for (int i = 0; i < new_capacity; i++)
				q.push_back(RemovableElem{});
			for (int i = new_capacity; i < capacity; i++)
				q.push_back(removable);
			q.set_capacity(new_capacity);
			for (int i = 0; i < q.size(); i++)
				EXPECT_FALSE(q.back().is_removable());	//we remove all removable elems at the tail
			EXPECT_EQ(capacity - new_capacity, count);
			count = 0;
		}
	}
	ConstructorTest::reset();
	DestructorTest::reset();
}

TEST(Test_15, Destructor) {
	constexpr int capacity = 10;
	constexpr int random_number = 5;
	
	{
		CircularBuffer<DestructorTest> empty;
	}
	EXPECT_EQ(0, DestructorTest::check_and_reset());

	{
		CircularBuffer<DestructorTest> buf{capacity};
	}
	EXPECT_EQ(0, DestructorTest::check_and_reset());

	{
		CircularBuffer<DestructorTest> buf(capacity, DestructorTest{});
		DestructorTest::reset();
	}
	EXPECT_EQ(capacity, DestructorTest::check_and_reset());

	{
		CircularBuffer<DestructorTest> buf{capacity};
		for (int i = 0; i < random_number; i++)
			buf.push_back(DestructorTest{});
		DestructorTest::reset();
	}
	EXPECT_EQ(random_number, DestructorTest::check_and_reset());
}
