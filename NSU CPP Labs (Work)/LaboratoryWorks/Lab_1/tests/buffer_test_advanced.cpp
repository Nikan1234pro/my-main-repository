#include "gtest/gtest.h"
#include "../main/circular_buffer.h"
#include <string>

//------------------Typed tests------------------

template <class T>
class CircularBufferTest : public ::testing::Test {
public:
    using Type = T;
};

using TypesList = ::testing::Types<int, char, double, std::string>;
TYPED_TEST_CASE(CircularBufferTest, TypesList);

TYPED_TEST(CircularBufferTest, Construstors) {
    Type elem;
    CircularBuffer<Type> buf;
}


//------------------Move semantics------------------

struct Moveable {
    static int move_count; 
    static int constructor_calls;
    bool created_by_move_ = false;

    Moveable() {
        ++constructor_calls;
    }
    
    Moveable(const Moveable &other) {
        ++constructor_calls;
    }

    Moveable(Moveable &&other) noexcept {
        ++move_count;
        ++constructor_calls;
        created_by_move_ = true;
    }

    ~Moveable() {}
};

int Moveable::move_count = 0;
int Moveable::constructor_calls = 0;


TEST(Test_1, ResizeMove) {
    //increase
    {
        Moveable::move_count = 0;
        constexpr int old_capacity = 5;
        constexpr int new_capacity = 10;
        CircularBuffer<Moveable> buf(old_capacity, Moveable{});
        //move all old elements 
        buf.resize(new_capacity);
        EXPECT_EQ(old_capacity, Moveable::move_count);

        for (int i = 0; i < old_capacity; ++i) 
            EXPECT_TRUE(buf.at(i).created_by_move_);

        for (int i = old_capacity; i < new_capacity; ++i) 
            EXPECT_FALSE(buf.at(i).created_by_move_);
    }
    //decrease 
    {
        Moveable::move_count = 0;
        constexpr int old_capacity = 10;
        constexpr int new_capacity = 5;
        CircularBuffer<Moveable> buf(old_capacity, Moveable{});
        //move first 5 old elements - other will be destroyed 
        buf.resize(new_capacity);
        EXPECT_EQ(new_capacity, Moveable::move_count);
        for (int i = 0; i < new_capacity; ++i) 
            EXPECT_TRUE(buf.at(i).created_by_move_);
    }
}

TEST(Test_2, SetCapacityMove) {
    //increase
    {
        Moveable::move_count = 0;
        constexpr int old_capacity = 5;
        constexpr int new_capacity = 10;
        CircularBuffer<Moveable> buf(old_capacity, Moveable{});
        //move all old elements 
        buf.set_capacity(new_capacity);
        EXPECT_EQ(old_capacity, Moveable::move_count);

        for (int i = 0; i < old_capacity; ++i) 
            EXPECT_TRUE(buf.at(i).created_by_move_);
    }
    //decrease 
    {
        Moveable::move_count = 0;
        constexpr int old_capacity = 10;
        constexpr int new_capacity = 5;
        CircularBuffer<Moveable> buf(old_capacity, Moveable{});
        //move first 5 old elements - other will be destroyed 
        buf.set_capacity(new_capacity);
        EXPECT_EQ(new_capacity, Moveable::move_count);
        for (int i = 0; i < new_capacity; ++i) 
            EXPECT_TRUE(buf.at(i).created_by_move_);
    }
}

TEST(Test_3, EraseMove) {
    //this test allows optimization - elements couldn't recreate
    //if count of constructors calls = 0 - don't check move-semantics
    constexpr int capacity = 10;
    for (int last = 0; last <= capacity; last++) {
        for (int first = 0; first < last; first++) {
            CircularBuffer<Moveable> buf(capacity, Moveable{});
            Moveable::move_count = 0;
            Moveable::constructor_calls = 0;
            buf.erase(first, last);
            if (Moveable::constructor_calls > 0) {
                EXPECT_EQ(capacity - last, Moveable::move_count);
                for (int i = 0; i < first - 1; i++)
                    EXPECT_FALSE(buf.at(i).created_by_move_);
                
                for (int i = first; i < buf.size(); i++)
                    EXPECT_TRUE(buf.at(i).created_by_move_);
            }
        }
    }
}

TEST(Test_4, InsertMove) {
    //this test allows optimization - elements couldn't recreate
    //if count of constructors calls = 0 - don't check move-semantics
    constexpr int capacity = 10;
    
    for (int elems = 0; elems <= capacity; elems++) {
        CircularBuffer<Moveable> buf(capacity, Moveable{});            
        for (int i = 0; i < elems; i++) 
            buf.pop_back();
        
        Moveable::move_count = 0;
        Moveable::constructor_calls = 0;

        //if we have enough space - can insert after last elem
        int max_insertion_pos = (buf.size() < buf.capacity()) ? buf.size() : buf.size() - 1;
        for (int pos = 0; pos <= max_insertion_pos; pos++) {
            //last elem could be deleted after insertation or moved - check it
            int tail_moves = (buf.size() < buf.capacity()) ? buf.size() - pos : buf.size() - pos - 1;
            buf.insert(pos, Moveable{});

            //default constructor call for arg
            //and copy constructor call for insertation
            //if we have count > 2 => it was moves of tail in buffer
            if (Moveable::constructor_calls > 2) { 
                EXPECT_EQ(Moveable::move_count, tail_moves);
            }
            Moveable::move_count = 0;
            Moveable::constructor_calls = 0;
        }
    }
}


