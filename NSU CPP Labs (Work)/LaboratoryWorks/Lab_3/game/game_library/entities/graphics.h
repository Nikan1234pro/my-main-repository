#pragma once
#include "../basics/basic_graphics.h"
#include "physics.h"

class TankGraphics : public AnimatedBasicGraphics {
    const TankPhysics *physics;
    int static_frames;
    int move_frames;
public:
    TankGraphics(const std::string &id_, int frames_count_, 
        double framerate_, const TankPhysics *physics_);
    void draw(SDL_Renderer *renderer) override;
    void update(float dt) override;

    void shoot() const;
};



class BulletGraphics : public BasicGraphics {
public:
    BulletGraphics(const std::string &id_, const BulletPhysics *physics_);
};



class BrickGraphics : public AnimatedBasicGraphics {
public: 
    BrickGraphics(const std::string &id_, int frames_count_, const BrickPhysics *physics_);
    void onHpChanged(int hp);
};