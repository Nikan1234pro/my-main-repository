#pragma once
#include "../../framework/framework.h"
#include "../music_player/music_player.h"
#include "physics.h"

class TankSounds : public SoundObject {
    const TankPhysics *physics;
    MusicPlayer music_player;
    int engine_channel = 0;
    int shoot_channel = 1;

    int engine_volume = MIX_MAX_VOLUME / 4;
    int shoot_volume = MIX_MAX_VOLUME / 2;
public:
    TankSounds(const TankPhysics *physics_)
    : physics(physics_), music_player() {
        MusicPlayer::allocateChannels(16);
    }

    void update(float dt) override {
        if (physics->getMove() != MOVE_NONE ||
            physics->getRotation() != ROTATE_NONE) {
            if (!Mix_Playing(engine_channel)) {
                music_player.setVolume(engine_channel, engine_volume);
                music_player.performEffect("engine", engine_channel);
            }
                
        }
        else
            music_player.haltChannel(engine_channel);
    }

    void shoot() {
        music_player.setVolume(shoot_channel, shoot_volume);
        music_player.performEffect("shoot", shoot_channel);
    }
};



class BrickSounds : public SoundObject {
    const BrickPhysics *physics;
    MusicPlayer music_player;

    int explosion_channel = 2;
    int explosion_volume = MIX_MAX_VOLUME;
public:
    BrickSounds(const BrickPhysics *physics_)
    : physics(physics_), music_player() {
        MusicPlayer::allocateChannels(16);
    }

    void update(float dt) override {}

    void ohHpChanged(int hp) {
        if (hp <= 0) {
            music_player.setVolume(explosion_volume, explosion_channel);
            music_player.performEffect("explosion", explosion_channel);
        }
    }
};