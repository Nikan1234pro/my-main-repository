#include "graphics.h"
#include "../animations/animations.h"
#include <cmath>

TankGraphics::TankGraphics(const std::string &id_, int frames_count_, double framerate_, const TankPhysics *physics_)
 : AnimatedBasicGraphics(id_, physics_, framerate_, static_frames, true), physics(physics_), static_frames(1), move_frames(frames_count_) { 
}

void TankGraphics::draw(SDL_Renderer *renderer) {
    AnimatedBasicGraphics::draw(renderer);
}

void TankGraphics::update(float dt) {
    AnimatedBasicGraphics::update(dt);
    if (physics->getMove() == MOVE_NONE && physics->getRotation() == ROTATE_NONE) 
        setFramesCount(static_frames);
    else
        setFramesCount(move_frames);
}

void TankGraphics::shoot() const {
    auto length = physics->getLength() / 1.5;
    auto width = physics->getWidth() / 1.5;
    auto ratio = 0.8;

    double angle = physics->getBody()->GetAngle();
    b2Vec2 tmp = physics->getBody()->GetPosition();
    double2 center = {tmp.x, tmp.y};
    double2 normal = {1.0, 0.0};
    normal = rot(angle, normal);

    SpriteManager::getInstance()->addSprite<Shoot>(
        center + normal * physics->getLength() * ratio, 
        angle, length, width    
    );
}



BulletGraphics::BulletGraphics(const std::string &id_, const BulletPhysics *physics_) 
: BasicGraphics(id_, physics_) {}



BrickGraphics::BrickGraphics(const std::string &id_, int frames_count_, const BrickPhysics *physics_) 
    : AnimatedBasicGraphics(id_, physics_, 0.0, frames_count_, false) {
}

void BrickGraphics::onHpChanged(int hp) {
    const BasicPhysics *physics = getPhysics();
    if (hp == 0) {
        b2Vec2 tmp = physics->getBody()->GetPosition();
        SpriteManager::getInstance()->addSprite<Explosion>(
            double2{tmp.x, tmp.y},
            physics->getBody()->GetAngle(),
            physics->getLength() * 2.0,
            physics->getWidth() * 2.0
        );
    }
    nextFrame();
}
