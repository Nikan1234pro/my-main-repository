#pragma once
#include "../../framework/framework.h"
#include "../basics/basic_physics.h"

class Observer {
public:
    virtual void onHit() = 0;
};

enum Move {
    MOVE_NONE, MOVE_FORWARD, MOVE_BACK
};

enum Rotate {
    ROTATE_NONE, ROTATE_LEFT, ROTATE_RIGHT
};

class TankPhysics : public BasicPhysics {
    Observer *observer_;

    Move move = MOVE_NONE;
    Rotate rotate = ROTATE_NONE;

    double2 speed;
    double V = 70.0;   //Move speed
    double W = 1.0;    //Rotate speed  

    double repair_time = 5.0;
    double time = 0.0;   


public:
    TankPhysics(Observer *observer, const double2 &center, double angle, 
        double length, double width);
    
    void update (float dt) override;

    const Move& getMove() const;
    const Rotate& getRotation() const; 
    Observer *getObserver() const;

    //rotation
    void rotateLeft();
    void rotateRight();
    void stopRotate();

    //movement
    void moveForward();
    void moveBack();
    void stopMove();

    void onHpChanged(int hp);

    TYPE getType() const override;
};



class BulletPhysics : public BasicPhysics {
    Observer *observer_;
    double V = 5000.0;
public:
    BulletPhysics(Observer *observer, const double2 &center, double angle, double length, double width);
    void update(float dt) override;

    Observer *getObserver() const;

    void onHpChanged(int hp);

    TYPE getType() const override;
};



class BrickPhysics : public BasicPhysics {
    Observer *observer_;
    b2Body *body;
public:
    BrickPhysics(Observer *observer, const double2 &center, double size);
    void update(float dt) override;

    Observer *getObserver() const;

    void onHpChanged(int hp);

    TYPE getType() const override;
};


void tankBrick(TankPhysics *tank, BrickPhysics *brick);
void tankTank(TankPhysics *tank_1, TankPhysics *tank_2);
void tankBullet(TankPhysics *tank, BulletPhysics *bullet);

void bulletBrick(BulletPhysics *bullet, BrickPhysics *brick);
void bulletTank(BulletPhysics *bullet, TankPhysics *tank);
void bulletBullet(BulletPhysics *bullet_1, BulletPhysics *bullet_2);

void brickTank(BrickPhysics *brick, TankPhysics *tank);
void brickBullet(BrickPhysics *brick, BulletPhysics *bullet);