#include "entities.h"
#include <iostream>

Tank::Tank(World *world, const double2 &center, double angle, double length, double width) 
: Entity(world) {
    physics = new TankPhysics(this, center, angle, length, width);
}
    
void Tank::initGraphics(const std::string &id, int frames_count, double framerate) {
    graphics = new TankGraphics(id, frames_count, framerate, physics);
}

void Tank::initSounds() {
    sound = new TankSounds(physics);
}
    
void Tank::update(float dt) {
    reload_timer += dt;

    physics->update(dt);
    if (graphics)
        graphics->update(dt);
    if (sound)
        sound->update(dt);
}
    
void Tank::draw(SDL_Renderer *renderer) {
    if (graphics)
        graphics->draw(renderer);
}

TankPhysics *Tank::getPhysics() const {
    return physics;
}
    
TankGraphics *Tank::getGraphics() const {
    return graphics;
}

TankSounds *Tank::getSounds() const {
    return sound;
}

void Tank::onHit() {
    --hp;
    physics->onHpChanged(hp);
}

void Tank::shoot() {
    if (reload_timer < reload) 
        return;
    double angle = physics->getBody()->GetAngle();
    b2Vec2 tmp = physics->getBody()->GetPosition();
    double2 center = {tmp.x, tmp.y};
    double2 normal = {1.0, 0.0};
    normal = rot(angle, normal);

    center += normal * physics->getLength() / 2.0;
    auto bullet = createEntity<Bullet>(getWorld(), center, angle, 14, 8);
    bullet->initGraphics("bullet");
    getWorld()->addEntity(std::move(bullet));
    reload_timer = 0.0;
    if (graphics)
        graphics->shoot();
    if (sound) 
        sound->shoot();
}

bool Tank::isAlive() const {
    return hp > 0;
}

void Tank::rotateLeft() {
    physics->rotateLeft();
}

void Tank::rotateRight() {
    physics->rotateRight();
}

void Tank::stopRotate() {
    physics->stopRotate();
}

void Tank::moveForward() {
    physics->moveForward();
}

void Tank::moveBack() {
    physics->moveBack();
}

void Tank::stopMove() {
    physics->stopMove();
}

Tank::~Tank() {
    delete physics;
    delete graphics;
    delete sound;
}



Bullet::Bullet(World *world, const double2 &center, double angle, double length, double width) 
: Entity(world) {
    physics = new BulletPhysics(this, center, angle, length, width);
}

void Bullet::initGraphics(const std::string &id) {
    graphics = new BulletGraphics(id, physics);
}

void Bullet::update(float dt) {
    physics->update(dt);
}

void Bullet::draw(SDL_Renderer *renderer) {
    if (graphics)
        graphics->draw(renderer);
}

BulletPhysics *Bullet::getPhysics() const {
    return physics;
}

BulletGraphics *Bullet::getGraphics() const {
    return graphics;
}

void Bullet::onHit() {
    hp = 0;
    physics->onHpChanged(hp);
}

bool Bullet::isAlive() const {
    return hp > 0;
}   

Bullet::~Bullet() {
    delete physics;
    delete graphics;
}



Brick::Brick(World *world, const double2 &center, double size) 
: Entity(world) {
    physics = new BrickPhysics(this, center, size);
}

void Brick::initGraphics(const std::string &id, int frames_count) {
    graphics = new BrickGraphics(id, frames_count, physics);
}

void Brick::initSounds() {
    sound = new BrickSounds(physics);
}

void Brick::update(float dt) {
    /* Nothing */
}

void Brick::draw(SDL_Renderer *renderer) {
    if (graphics)
        graphics->draw(renderer);
}

BrickPhysics *Brick::getPhysics() const {
    return physics;
}

BrickGraphics *Brick::getGraphics() const {
    return graphics;
}

BrickSounds *Brick::getSounds() const {
    return sound;
}

void Brick::onHit() {
    --hp;
    if (graphics)
        graphics->onHpChanged(hp);
    if (sound) 
        sound->ohHpChanged(hp);

    //at the end because it destroys body, but body needed in graphics
    physics->onHpChanged(hp);
}

bool Brick::isAlive() const {
    return hp > 0;
}

Brick::~Brick() {
    delete physics;
    delete graphics;
}