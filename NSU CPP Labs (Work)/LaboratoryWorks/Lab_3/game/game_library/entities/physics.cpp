#include "physics.h"
#include <algorithm>
#include <iostream>
#include <vector>
#include <typeinfo.h>

TankPhysics::TankPhysics(Observer *observer, const double2 &center, double angle, 
double length, double width)
 : BasicPhysics(center, angle, length, width, b2_dynamicBody), observer_(observer) {
}

void TankPhysics::update(float dt) {
    float2 speed = float2(V, 0.0);
    float angle = getBody()->GetAngle();

    switch (move)
    {
    case MOVE_NONE:
        getBody()->SetLinearVelocity({0.0, 0.0});
        break;
    case MOVE_FORWARD:
        speed = rot(angle, speed);
        getBody()->SetLinearVelocity({speed.x, speed.y});
        break;
    case MOVE_BACK:
        speed = rot(angle, speed) * -1;
        getBody()->SetLinearVelocity({speed.x, speed.y});
        break;
    }

    switch (rotate)
    {
    case ROTATE_NONE:
        getBody()->SetAngularVelocity(0.0f);
        break;
    case ROTATE_LEFT:
        getBody()->SetAngularVelocity(-W);
        break;
    case ROTATE_RIGHT:
        getBody()->SetAngularVelocity(W);
        break;
    }
}

const Move& TankPhysics::getMove() const {
    return move;
}
    
const Rotate& TankPhysics::getRotation() const {
    return rotate;
}  

Observer *TankPhysics::getObserver() const {
    return observer_;
}

void TankPhysics::onHpChanged(int hp) {
    if (hp <= 0) {
        destroyBody();
    }
}

void TankPhysics::rotateLeft() {
    rotate = ROTATE_LEFT;
}

void TankPhysics::rotateRight() {
    rotate = ROTATE_RIGHT;
}

void TankPhysics::stopRotate() {
    rotate = ROTATE_NONE;
}

void TankPhysics::moveForward() {
    move = MOVE_FORWARD;
}

void TankPhysics::moveBack() {
    move = MOVE_BACK;
}

void TankPhysics::stopMove() {
    move = MOVE_NONE;
}

TYPE TankPhysics::getType() const {
    return TYPE::DYNAMIC;
}



BulletPhysics::BulletPhysics(Observer *observer, const double2 &center, double angle, double length, double width) 
: BasicPhysics(center, angle, length, width, b2_dynamicBody), observer_(observer) {
    float2 speed = float2(V, 0.0);
    speed = rot(static_cast<float>(angle), speed);    
    getBody()->SetLinearVelocity({speed.x, speed.y});
}

void BulletPhysics::update(float dt) {
    for (b2ContactEdge* edge = getBody()->GetContactList(); edge; edge = edge->next) {
        if (edge->contact->IsTouching()) {
            b2Fixture* fixtureA = edge->contact->GetFixtureA();
            b2Fixture* fixtureB = edge->contact->GetFixtureB();    
            b2Body* bodyA = fixtureA->GetBody();
            b2Body* bodyB = fixtureB->GetBody();
            if (bodyA->GetUserData() == nullptr || 
                bodyB->GetUserData() == nullptr)
                this->getObserver()->onHit();
        }
    }
}

Observer *BulletPhysics::getObserver() const {
    return observer_;
}

void BulletPhysics::onHpChanged(int hp) {
    if (hp <= 0) {
        destroyBody();
    }
}



TYPE BulletPhysics::getType() const {
    return TYPE::DYNAMIC;
}


BrickPhysics::BrickPhysics(Observer *observer, const double2 &center, double size) 
: observer_(observer), BasicPhysics(center, 0.0, size, size, b2_staticBody) {
}   

void BrickPhysics::update(float dt) {}

Observer *BrickPhysics::getObserver() const {
    return observer_;
}

void BrickPhysics::onHpChanged(int hp) {
    if (hp <= 0) {
        destroyBody();
    }
}

TYPE BrickPhysics::getType() const {
    return TYPE::STATIC;
}



void tankBrick(TankPhysics *tank, BrickPhysics *brick) {
}

void tankTank(TankPhysics *tank_1, TankPhysics *tank_2) {
    /* Nothing */
}

void tankBullet(TankPhysics *tank, BulletPhysics *bullet) {
    // add sth..
}

void bulletBrick(BulletPhysics *bullet, BrickPhysics *brick) {
    bullet->getObserver()->onHit();
    brick->getObserver()->onHit();
}



void bulletTank(BulletPhysics *bullet, TankPhysics *tank) {
    tankBullet(tank, bullet);
}

void bulletBullet(BulletPhysics *bullet_1, BulletPhysics *bullet_2) {
    //add sth...
}


void brickTank(BrickPhysics *brick, TankPhysics *tank) {
    tankBrick(tank ,brick);
}


void brickBullet(BrickPhysics *brick, BulletPhysics *bullet) {
    bulletBrick(bullet, brick);
}