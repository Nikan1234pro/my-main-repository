#pragma once
#include "sounds.h"
#include "graphics.h"

using namespace linalg::aliases;

class Tank : public Entity, public Observer {
    TankPhysics *physics = nullptr;
    TankGraphics *graphics = nullptr;
    TankSounds *sound = nullptr;
    
    int hp = 20;

    float reload = 0.5;    //seconds
    float reload_timer = reload;
public:
    Tank(World *world, const double2 &center, double angle, double length, double width);
    void initGraphics(const std::string &id, int frames_count, double framerate);
    void initSounds();
    void update(float dt) override;
    void draw(SDL_Renderer *renderer);

    //getters
    TankPhysics *getPhysics() const override;
    TankGraphics *getGraphics() const override;
    TankSounds *getSounds() const override;

    //observer
    void onHit() override;

    void shoot();
    bool isAlive() const override;

    //move
    void rotateLeft();
    void rotateRight();
    void stopRotate();

    void moveForward();
    void moveBack();
    void stopMove();

    ~Tank();
};



class Bullet : public Entity, public Observer {
    BulletPhysics *physics;
    BulletGraphics *graphics;

    int hp = 1;
public:
    Bullet(World *world, const double2 &center, double angle, double length, double width);
    void initGraphics(const std::string &id);
    void update(float dt) override;
    void draw(SDL_Renderer *renderer);

    BulletPhysics *getPhysics() const override;
    BulletGraphics *getGraphics() const override;

    void onHit() override;
    bool isAlive() const override;

    ~Bullet();
};



class Brick : public Entity, public Observer {
    BrickPhysics *physics = nullptr;
    BrickGraphics *graphics = nullptr;  
    BrickSounds *sound = nullptr;

    int hp = 3;
public:
    Brick(World *world, const double2 &center, double size); 
    void initGraphics(const std::string &id, int frames_count);
    void initSounds();
    void update(float dt) override;
    void draw(SDL_Renderer *renderer);

    BrickPhysics *getPhysics() const override;
    BrickGraphics *getGraphics() const override;
    BrickSounds *getSounds() const override;

    void onHit() override;
    bool isAlive() const override;
    ~Brick();
};
