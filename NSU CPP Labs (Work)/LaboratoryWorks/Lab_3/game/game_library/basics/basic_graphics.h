#pragma once 
#include "basic_physics.h"

void drawFrame(SDL_Renderer *renderer, double2 center, double angle, double width, double height, 
    const std::string &texture_id, int current_frame); 

class BasicGraphics : public GraphicObject {
    const BasicPhysics *physics;
    const std::string texture_id;
public:
    BasicGraphics(const std::string &id_, const BasicPhysics *physics_);
    void update(float dt) override;
    void draw(SDL_Renderer *renderer) override;

    const BasicPhysics *getPhysics() const;
};


class AnimatedBasicGraphics : public Animation {
    const BasicPhysics *physics;
    const std::string texture_id;
public:
    AnimatedBasicGraphics(const std::string &id_, const BasicPhysics *physics_, 
        double framerate_, int frames_count_, bool repeat_);
    void draw(SDL_Renderer *renderer) override;

    const BasicPhysics *getPhysics() const;
};