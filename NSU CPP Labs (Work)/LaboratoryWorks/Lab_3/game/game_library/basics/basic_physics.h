#pragma once
#include "../../framework/framework.h"

#define PI 3.14159265359

extern b2World collide_world;


using namespace linalg::aliases;

class BasicPhysics : public PhysicalObject {
    b2Body *body;

    double length;
    double width;
public:
    BasicPhysics(const double2 &center_, double angle_, double length_, double width_, const b2BodyType &type_)  
    : length(length_), width(width_) {
        b2PolygonShape shape;
        shape.SetAsBox(length / 2.0, width / 2.0);

        b2BodyDef bdef; 
        bdef.position.Set(center_.x, center_.y);
        bdef.angle = angle_;
        bdef.type = type_;
        bdef.bullet = false;

        body = collide_world.CreateBody(&bdef);
        body->CreateFixture(&shape, 1); 
        body->SetUserData(this);
    }

    void checkCollide(PhysicalObject *other) override {
        for (b2ContactEdge* edge = this->getBody()->GetContactList(); edge; edge = edge->next) {
            if (edge->contact->IsTouching()) {
                b2Fixture* fixtureA = edge->contact->GetFixtureA();
                b2Fixture* fixtureB = edge->contact->GetFixtureB();    
                b2Body* bodyA = fixtureA->GetBody();
                b2Body* bodyB = fixtureB->GetBody();

                if (bodyA->GetUserData() == this && bodyB->GetUserData() == other ||
                    bodyB->GetUserData() == this && bodyA->GetUserData() == other) {
                    auto collide_handler = CollisionMap::getInstance().lookup(this, other);
                    if (collide_handler) {
                        collide_handler(this, other);
                    }
                }
            }
        }
    }

    const b2Body* getBody() const {
        return body;
    }

    b2Body* getBody() {
        return body;
    }

    double getLength() const {
        return length;
    }

    double getWidth() const {
        return width;
    }

    void destroyBody() {
        body->GetWorld()->DestroyBody(body);
        body = nullptr;
    }

    ~BasicPhysics() {
        if (body)
            destroyBody();
    }
};
