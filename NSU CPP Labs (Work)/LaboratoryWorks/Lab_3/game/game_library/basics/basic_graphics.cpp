#include "basic_graphics.h" 


void drawFrame(SDL_Renderer *renderer, double2 center, double angle, double width, double height, 
    const std::string &texture_id, int current_frame) {
    Texture texture = TextureManager::getInstance()->get(texture_id);
    int frame_width = texture.getWidth();
    int frame_height = texture.getHeight();

    int on_screen_width = convert(renderer, width);
    int on_screen_height = convert(renderer, height);
    angle *= 180.0 / PI;

    SDL_Point up_left_corner = convert(renderer, { 
        static_cast<int>(center.x - width / 2.0), 
        static_cast<int>(center.y - height / 2.0) 
    });

    SDL_Rect src_rect { 
        current_frame * frame_width, 
        0, frame_width, frame_height 
    };

    SDL_Rect dest_rect {
        up_left_corner.x, up_left_corner.y,
        on_screen_width, on_screen_height 
    };

    SDL_RenderCopyEx(
        renderer, texture, 
        &src_rect, &dest_rect, angle, 
        NULL, SDL_FLIP_NONE
    );
} 

BasicGraphics::BasicGraphics(const std::string &id_, const BasicPhysics *physics_) 
 : texture_id(id_), physics(physics_) {} 

void BasicGraphics::draw(SDL_Renderer *renderer)  {
    b2Vec2 tmp = physics->getBody()->GetPosition();
    drawFrame(
        renderer, 
        {tmp.x, tmp.y},
        physics->getBody()->GetAngle(),
        physics->getLength(),
        physics->getWidth(), 
        texture_id, 0
    );
}

void BasicGraphics::update(float dt) {
    /* Nothing */
}

const BasicPhysics* BasicGraphics::getPhysics() const {
    return physics;
}


AnimatedBasicGraphics::AnimatedBasicGraphics(const std::string &id_, const BasicPhysics *physics_, 
double framerate_, int frames_count_, bool repeat_) 
 : Animation(framerate_, frames_count_, repeat_), physics(physics_), texture_id(id_) {}

 void AnimatedBasicGraphics::draw(SDL_Renderer *renderer)  {
    b2Vec2 tmp = physics->getBody()->GetPosition();
    drawFrame(
        renderer, 
        {tmp.x, tmp.y},
        physics->getBody()->GetAngle(),
        physics->getLength(),
        physics->getWidth(), 
        texture_id, 
        getCurrentFrame()
    );
}

const BasicPhysics* AnimatedBasicGraphics::getPhysics() const {
    return physics;
}
