#pragma once
#include "../../framework/framework.h"
#include "../basics/basic_graphics.h"

using namespace linalg::aliases;


class AnimationPhysics {
    double2 center;
    double angle;
    double length;
    double width;
public:
    AnimationPhysics(double2 center_, double angle_, double length_, double width_)
    : center(center_), angle(angle_), length(length_), width(width_) {}

    const double2& getCenter() const {
        return center;
    }

    double getAngle() const {
        return angle;
    }

    double getLength() const {
        return length;
    }

    double getWidth() const {
        return width;
    }
};

class BasicAnimation : public Animation {
    AnimationPhysics physics;
    const std::string texture_id;
public:
    BasicAnimation(const std::string &texture_id_, double2 center, double angle, 
        double length, double width, double framerate, int frames_count) 
    : Animation(framerate, frames_count), physics(center, angle, length, width), texture_id(texture_id_) {}

    void draw(SDL_Renderer *renderer) override {
         drawFrame(
            renderer, 
            physics.getCenter(),
            physics.getAngle(),
            physics.getLength(),
            physics.getWidth(), 
            texture_id, 
            getCurrentFrame());
    }
};

class Explosion : public BasicAnimation {
public:
    Explosion(double2 center, double angle, double length, double width) 
    : BasicAnimation("explosion", center, angle, length, width, 15.0, 8) {}
};

class Shoot : public BasicAnimation {
public:
    Shoot(double2 center, double angle, double length, double width) 
    : BasicAnimation("shoot", center, angle, length, width, 20.0, 2) {}
};
