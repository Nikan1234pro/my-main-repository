#pragma once 
#include "../entities/entities.h"

class Player : public InputHandler {
    Tank* tank_to_control;
public:
    Player(Tank *tank) 
    : tank_to_control(tank) {}

    void onKeyDown(const SDL_Keysym &keysym) {
        switch (keysym.sym) {
        case SDLK_d:
            tank_to_control->rotateRight();
            break;
        case SDLK_a:
            tank_to_control->rotateLeft();
            break;
        case SDLK_w:
            tank_to_control->moveForward();
            break;
        case SDLK_s:
            tank_to_control->moveBack();
            break;
        case SDLK_SPACE:
            tank_to_control->shoot();
            break;
       }
    }

    void onKeyUp(const SDL_Keysym &keysym) {
        switch (keysym.sym) {
        case SDLK_d:
            tank_to_control->stopRotate();
            break;
        case SDLK_a:
            tank_to_control->stopRotate();
            break;
        case SDLK_w:
            tank_to_control->stopMove();
            break;
        case SDLK_s:
            tank_to_control->stopMove();
            break;
        }
    }
};

