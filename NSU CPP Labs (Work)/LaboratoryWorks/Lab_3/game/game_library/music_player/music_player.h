#pragma once
#include "../../framework/framework.h"

class MusicPlayer  {
public:
    explicit MusicPlayer(); 
    static void allocateChannels(int numchans);
	static int getNumChannels();
	void performEffect(const std::string& name, int channel = -1, int loops = 0);
    void haltChannel(int channel);
    void setVolume(int channel, int volume);
    int getVolume(int channel);
};