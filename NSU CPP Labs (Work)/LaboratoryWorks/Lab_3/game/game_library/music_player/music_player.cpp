#include "music_player.h"

MusicPlayer::MusicPlayer() {
}

void MusicPlayer::allocateChannels(int numchans) {
    Mix_AllocateChannels(numchans);
}
	
int MusicPlayer::getNumChannels() {
    return Mix_AllocateChannels(-1);
}
	
void MusicPlayer::performEffect(const std::string& name, int channel, int loops) {
    Mix_Chunk* chunk = SoundManager<Mix_Chunk>::getInstance()->get(name);
	Mix_PlayChannel(channel, chunk, loops);
}

void MusicPlayer::haltChannel(int channel) {
    Mix_HaltChannel(channel);
}

void MusicPlayer::setVolume(int channel, int volume) {
    Mix_Volume(channel, volume);
}

int MusicPlayer::getVolume(int channel) {
    return Mix_Volume(channel, -1);
}