#pragma once 
#include "../../framework/framework.h"
#include "../player/player.h"
#include "generator.h"


extern b2World collide_world;
using namespace linalg::aliases;

class Background {
public:
    void draw(SDL_Renderer *renderer) {
        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 0);
        SDL_Rect rect {
            convert(renderer, SDL_Point{0, 0}).x, 
            convert(renderer, SDL_Point{0, 0}).y, 
            convert(renderer, FIELD_WIDTH),
            convert(renderer, FIELD_HEIGHT)
        };
        SDL_RenderFillRect(renderer, &rect);
    } 
};

class Level_2 : public Level {
    std::unique_ptr<Player> player;
    Background background;
public:
    Level_2() {
        auto tank = createEntity<Tank>(getWorld(), double2{50.0, FIELD_HEIGHT / 2.0}, 0, 70, 60);
        tank->initGraphics("tank", 2, 15.0);
        tank->initSounds();
        player.reset(new Player(tank.get()));
        getWorld()->addEntity(std::move(tank));
        generateField(getWorld());
    }

    void preload(SDL_Renderer *renderer) override {
        TextureManager::getInstance()->setDefaultPath("textures");
        TextureManager::getInstance()->load(renderer, "tank", "tank.png", 175, 144);
        TextureManager::getInstance()->load(renderer, "brick", "brick.png", 16, 16);
        TextureManager::getInstance()->load(renderer, "bullet", "bullet.png", 14, 8);
        TextureManager::getInstance()->load(renderer, "explosion", "explosion.png", 20, 20);
        TextureManager::getInstance()->load(renderer, "shoot", "shoot.png", 60, 40);

        SoundManager<Mix_Chunk>::getInstance()->setDefaultPath("sounds");
        SoundManager<Mix_Chunk>::getInstance()->load("shoot", "shoot.ogg");
        SoundManager<Mix_Chunk>::getInstance()->load("engine", "engine.ogg");
        SoundManager<Mix_Chunk>::getInstance()->load("explosion", "explosion.ogg");
    }

    void clear() override {
    }

    void update(float dt) override {
        collide_world.Step(dt, 1, 1);
        Level::update(dt);
    }

    void onEvents(const SDL_Event &event) override {
        if (event.type == SDL_KEYDOWN)
            if (event.key.keysym.sym == SDLK_p)
                quit();
        player->onEvents(event);
    }

    void draw(SDL_Renderer *renderer) {
        background.draw(renderer);
        Level::draw(renderer);
    }
};

class Level_3 : public Level_2 {};

class Level_4 : public Level_3 {};


