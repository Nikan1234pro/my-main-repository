#include "generator.h"
#include "../entities/entities.h"
#include <cstdlib>
#include <ctime>
#include <vector>

extern b2World collide_world;

void setBorder(double x, double y, double width, double height) {
    b2PolygonShape shape;
    shape.SetAsBox(width, height);

    b2BodyDef bdef; 
    bdef.position.Set(x, y);
    bdef.type = b2_staticBody;

    b2Body *body = collide_world.CreateBody(&bdef);
    body->CreateFixture(&shape, 100); 
    body->SetUserData(nullptr);
}

const double BRICK_SIZE = 40; 

void generateField(World *world) {
    //Set world border
    const double DEPTH = 10.0;
    setBorder(FIELD_WIDTH / 2.0, -DEPTH, FIELD_WIDTH / 2.0, DEPTH);
    setBorder(FIELD_WIDTH / 2.0, FIELD_HEIGHT + DEPTH, FIELD_WIDTH / 2.0, DEPTH);
    setBorder(-DEPTH, FIELD_HEIGHT/ 2.0, DEPTH, FIELD_HEIGHT/ 2.0);
    setBorder(FIELD_WIDTH + DEPTH, FIELD_HEIGHT/ 2.0, DEPTH, FIELD_HEIGHT/ 2.0);
    //Generate walls
    std::srand(std::time(NULL));
    const int matrix_w = FIELD_WIDTH / BRICK_SIZE;
    const int matrix_h = FIELD_HEIGHT / BRICK_SIZE;
    const int space = matrix_w / 10;										
	const int count_of_walls = matrix_w * 2;	

    std::vector<std::vector<int>> matrix;								

	matrix.resize(matrix_w);
	for (int i = 0; i < matrix_w; i++)
		matrix[i].resize(matrix_h, 0);

	for (int i = 0; i < count_of_walls; i++) {
		int x = std::rand() % (matrix_w - 2 * space) + space;		
		int y = std::rand() % (matrix_h);
		int w = std::rand() % 5;								
		int h = std::rand() % 5;

		if (y + h >= matrix_h)									
			h = matrix_h - y;

		if (x + w >= matrix_w - space)								
			w = matrix_w - space - x;

		for (int j = 0; j < w; j++) {
            for (int k = 0; k < h; k++) {
                if (!matrix[x + j][y + k]) {
                    matrix[x + j][y + k] = 1;
                    double center_x = static_cast<double>(x + j) * 
                        BRICK_SIZE + 0.5 * BRICK_SIZE;
                    double center_y = static_cast<double>(y + k) * 
                        BRICK_SIZE + 0.5 * BRICK_SIZE;
                    auto brick = createEntity<Brick>(world, double2{center_x, center_y}, BRICK_SIZE);
                    brick->initGraphics("brick", 3);
                    brick->initSounds();
                    world->addEntity(std::move(brick));
                }	
            }
        }		
	}
}