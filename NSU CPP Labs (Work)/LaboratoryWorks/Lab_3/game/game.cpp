#include "game.h"
#include <iostream>
#include "game_library/loading_screen/loading_screen.h"

Game::Game() : 
        BaseGame(), fps_counter(60.0f), 
        level_manager(LevelManager::getInstance()),
        collisions(CollisionMap::getInstance()) {

    collisions.addCollisionHandler(tankBrick);
    collisions.addCollisionHandler(tankBullet);
    collisions.addCollisionHandler(tankTank);

    collisions.addCollisionHandler(bulletBrick);
    collisions.addCollisionHandler(bulletBullet);
    collisions.addCollisionHandler(bulletTank);

    collisions.addCollisionHandler(bulletBullet);
    collisions.addCollisionHandler(bulletTank);


    LevelChangeStrategy *strategy = new LevelChangeStrategy{};
    strategy->setStrategy(
        [](const SDL_Event &event) { 
            if (event.type == SDL_KEYDOWN)
                if (event.key.keysym.sym == SDLK_z)
                    return true;
            return false;
        }
    );

    strategy->setStrategy(
        [](float dt) { 
            static float seconds = 0;
            seconds += dt;
            if (seconds > 10) {
                seconds = 0;
                return true;
            }
            return false;
        }
    );

    level_manager.addLevel<Level_2>();

    level_manager.setLevelChangeStrategy(strategy);
    level_manager.setLoadingScreen(new MyScreen);
}

void Game::preload() {
    level_manager.start(getRenderer());
}

void Game::render() {
    if (!fps_counter.accept())
        return;

    SDL_Renderer *renderer = getRenderer();
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    
    level_manager.render();

    SpriteManager::getInstance()->draw(renderer);
    SDL_RenderPresent(renderer);
}

void Game::handleEvents() {

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        level_manager.onEvents(event);
        onEvents(event);
    }
}

void Game::update(float dt) {
    static constexpr float freq  = 120.0f;
    static float time_accumulator = 0.0f;

    if (!level_manager.isRunning())
        quit();

    float ifreq = 1.0f / freq;    
    time_accumulator += dt;

    while (time_accumulator > ifreq) {
        time_accumulator -= ifreq;

        level_manager.update(ifreq);
        SpriteManager::getInstance()->update(ifreq);
    }    
}

void Game::onKeyDown(const SDL_Keysym &sym) {
    if (sym.sym == SDLK_ESCAPE)
        onQuit();
}

void Game::onQuit() {
    quit();
}

Game::~Game() {}