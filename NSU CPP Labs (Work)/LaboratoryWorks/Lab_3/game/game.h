#pragma once
#include "../framework/framework.h"
#include "game_library/entities/entities.h"
#include "game_library/levels/levels.h"

class Game : public BaseGame {
    FpsCounter fps_counter;
    LevelManager& level_manager;
    CollisionMap& collisions;
public:
    Game();
    void preload();
    void render() override;
    void handleEvents() override;
    void update(float dt) override;

    //eventHandlers
    void onKeyDown(const SDL_Keysym &sym);
    void onQuit();
    ~Game();
};