#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <memory>
#include <unordered_map>

#include "../socket_data/socketdata.h"

class Server : public QObject {

    static const int listen_port = 5000;
    std::unique_ptr<QTcpServer> server_socket;
    std::unordered_map<qintptr, SocketData> clients;

    Q_OBJECT
public:
    Server();
    virtual ~Server();

public slots:
    void clientReady();
    void clientConnected();
    void clientDisconnected();
};

#endif // SERVER_H
