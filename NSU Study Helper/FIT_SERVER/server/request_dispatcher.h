#ifndef REQUESTDISPATCHER_H
#define REQUESTDISPATCHER_H

#include <QtGlobal>
#include <QJsonDocument>

class RequestDispatcher {
    enum Types : qint8 {
        HistoryRequest = 0x00,
        SubjectInfoRequest = 0x01,
        TeacherInfoRequest = 0x02,
        Authentication = 0x03
    };

public:
    RequestDispatcher();
    QJsonDocument dispach(qint8 type, qint8 command,
                          const QJsonDocument &request);
};

#endif // REQUESTDISPATCHER_H
