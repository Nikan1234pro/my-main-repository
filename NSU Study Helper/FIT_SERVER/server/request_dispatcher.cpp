#include "request_dispatcher.h"
#include "../database/historydatabase.h"
#include "../database/authentication_service.h"
#include "../database/subjectdatabase.h"

RequestDispatcher::RequestDispatcher() {}

QJsonDocument RequestDispatcher::dispach(qint8 type, qint8 command,
                                         const QJsonDocument &request) {
    switch (type) {
    case HistoryRequest     : return HistoryDatabase::getInstance().handle(command, request);
    case SubjectInfoRequest : return SubjectDatabase::getInstance().handle(command, request);
    case Authentication     : return AuthenticationService::getInstance().handle(command, request);
    }
    return QJsonDocument{};
}
