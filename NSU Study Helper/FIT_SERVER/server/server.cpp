#include "server.h"
#include <stdexcept>
#include <QDebug>
#include <type_traits>

Server::Server() {
    server_socket = std::make_unique<QTcpServer>();
    if (!server_socket->listen(QHostAddress::Any, listen_port))
        throw std::runtime_error("Couldn't bind");

    connect(server_socket.get(),
            &QTcpServer::newConnection,
            this, &Server::clientConnected);
}

Server::~Server() {}


void Server::clientReady() {
    QTcpSocket* client = static_cast<QTcpSocket*>(sender());
    auto& socket_data = clients[client->socketDescriptor()];

    socket_data.handle();
    if (socket_data.isExpired()) {
        client->close();
    }
}

void Server::clientConnected() {
    QTcpSocket *client = server_socket->nextPendingConnection();
    connect(client, &QTcpSocket::readyRead, this, &Server::clientReady);
    qintptr desc = client->socketDescriptor();
    clients[desc] = SocketData(client);
}

void Server::clientDisconnected(){
    QTcpSocket* client = static_cast<QTcpSocket*>(sender());
    qintptr desc = client->socketDescriptor();
    clients.erase(desc);

    client->close();
    client->deleteLater();
}


