#include <QCoreApplication>
#include <QSqlDatabase>
#include <iostream>
#include "server/server.h"
#include "database/database.h"

#include <QJsonObject>
#include <QJsonDocument>

#include "mail_service/mail_service.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    try {
        DatabaseInterface::initDatabase();
    }
    catch(DatabaseUnavailableException &e) {
        qDebug() << e.what();
        return EXIT_FAILURE;
    }
    Server server;
    a.exec();
}
