#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QJsonDocument>
#include <QSqlQuery>
#include <QJsonObject>
#include <QDebug>
#include <stdexcept>
#include <string>
#include <memory>
#include <map>

#include "json_parser.h"

QByteArray intToBytes(int number);
int bytesToInt(const QByteArray& array);

class DatabaseUnavailableException : public std::exception {
    const std::string message_;
public:
    DatabaseUnavailableException(const std::string& message);

    const char * what() const
    _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override;
};


class Command {
    const QString RESULT        = "result";
    const QString ERROR_MESSAGE = "error_message";
public:
    Command() = default;
    QJsonDocument sqlQueryToJson(QSqlQuery& sqlquery);

    QJsonDocument getErrorMessage(const QString &cause) const;
    QJsonDocument getErrorMessage() const;
    QJsonDocument getSuccessMessage() const;

    virtual QJsonDocument handle(const QJsonDocument &request) = 0;
    virtual ~Command() = default;
};

template <class KeyType>
class CommandFactory {
    std::unordered_map<KeyType, std::unique_ptr<Command>> creators;

public:
    template<class CommandType>
    void addCommand(const KeyType &key) {
        creators[key] = std::make_unique<CommandType>();
    }

    Command* getCommand(const KeyType &key) const {
        auto it = creators.find(key);
        if (it == creators.end()) {
            return nullptr;
        }
        return it->second.get();
    }
};

class DatabaseInterface {
    static constexpr char DATABASE_TYPE[] = "QPSQL";
    static constexpr char DATABASE_NAME[] = "first_test";
    static constexpr char DATABASE_USERNAME[] = "postgres";
    static constexpr char DATABASE_PASSWORD[] = "140508";
    static constexpr char DATABASE_HOSTNAME[] = "127.0.0.1";
    static constexpr int  DATABASE_PORT = 5432;
public:
    DatabaseInterface();
    virtual ~DatabaseInterface();

    static void initDatabase();

    QJsonDocument handle(qint8 command,
                         const QJsonDocument& document) const;

protected:
    CommandFactory<qint8> factory;
};

inline QString toDomain(const QString &name) {
    return ":" + name;
}






















#endif // DATABASE_H
