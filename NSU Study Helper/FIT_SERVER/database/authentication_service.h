#ifndef AUTHENTICATIONSERVICE_H
#define AUTHENTICATIONSERVICE_H

#include <QtGlobal>
#include <QString>
#include <unordered_map>
#include <QJsonDocument>

#include "database.h"

namespace auth {
    extern const QString USER;

    extern const QString LOGIN;
    extern const QString PASSWORD;
    extern const QString ID;
    extern const QString TOKEN;
    extern const QString ADMIN;
    extern const QString PINCODE;
    extern const QString IS_ADMIN;
}

class AuthenticationService : public DatabaseInterface {
    AuthenticationService();

    enum Commands : qint8 {
        LOGIN = 0x00,
        REGISTER = 0x01,
        SEND_PIN = 0x02,
        CHANGE_PASS = 0x03
    };

    using IdType = int;
    using TokenType = int;
    using LoginType = QString;
    using PinCodeType = QString;

    std::unordered_map<IdType, TokenType> tokens;
    std::map<LoginType, PinCodeType> pin_codes;

    int MAX_TOKENS = 1000000;
public:
    using TokenStorage = decltype (tokens);
    using PinCodeStorage = decltype (pin_codes);

    static AuthenticationService& getInstance();

    static QString generatePinCode();
    static bool validateEmail(const QString &email);

    bool handlePinCode(const QString &login,
                       const QString &pin_code);

    bool checkToken(const IdType &user_id,
                    const TokenType &token) const;

    bool checkToken(const QJsonObject &user_json) const;

    TokenType generateToken(const IdType &id);

    PinCodeStorage& getPinCodes();
    TokenStorage& getTokens();

};


class Login : public Command {
    QSqlQuery query;

    const QString command   = "SELECT user_pass, user_id, is_admin "
                              "FROM USERS "
                              "WHERE user_login = :user_login;";

    const QString error_msg = QString::fromUtf8("Неверный пароль");
public:
    Login();
    QJsonDocument handle(const QJsonDocument &request) override;
};


class SendPinCode : public Command {
    QString error_msg = "Почта не принадлежит домену g.nsu";
public:
    SendPinCode();
    QJsonDocument handle(const QJsonDocument &request) override;
};


class Register : public Command {
    QSqlQuery register_query;
    QSqlQuery check_query;

    const QString register_command =
            "INSERT INTO USERS (user_id, user_login, user_pass, is_admin) "
            "VALUES(nextval('user_id_sequence'), :user_login, :user_pass, :is_admin);";

    const QString check_command =
            "SELECT user_id "
            "FROM USERS "
            "WHERE user_login = :user_login;";

    const QString reg_err = "Такой пользователь уже зарегистрирован";
    const QString pin_err = "Неверный ПИН-код";

    bool isNotRegistered(const QString login);
public:
    Register();
    QJsonDocument handle(const QJsonDocument &request) override;
};

#endif // AUTHENTICATIONSERVICE_H








