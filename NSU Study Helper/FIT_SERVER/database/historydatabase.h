#ifndef HISTORYDATABASE_H
#define HISTORYDATABASE_H

#include <unordered_map>
#include <QSqlQuery>
#include <QString>
#include <array>

#include "database.h"

namespace history {
    extern const QString YEAR;
    extern const QString SEMESTER;
}

class HistoryDatabase : public DatabaseInterface {
    enum Commands : qint8 {
        GET_YEAR_LIST = 0x00,
        GET_SEMESTER_LIST = 0x01,
        GET_SUBJECT_LIST = 0x02
    };

    HistoryDatabase();
public: 
    static HistoryDatabase& getInstance();
};


class GetYearList : public Command {
    QSqlQuery query;

    const QString command = "SELECT entry_year "
                            "FROM YEARS ORDER BY 1;";
public:
    GetYearList();
    QJsonDocument handle(const QJsonDocument &request) override;
};


class GetSemesterList : public Command {
    QSqlQuery query;

    const QString command = "SELECT semester FROM SEMESTERS "
                            "WHERE entry_year = :entry_year "
                            "ORDER BY 1;";
public:
    GetSemesterList();
    QJsonDocument handle(const QJsonDocument &request) override;
};


class GetSubjectList : public Command {
    QSqlQuery query;

    const QString command = "SELECT subject_id, subject_name FROM SUBJECTS "
                            "WHERE entry_year = :entry_year "
                            "AND semester = :semester "
                            "ORDER BY 1;";
public:
    GetSubjectList();
    QJsonDocument handle(const QJsonDocument &request) override;
};

#endif // HISTORYDATABASE_H
