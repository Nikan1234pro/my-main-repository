#ifndef SUBJECTDATABASE_H
#define SUBJECTDATABASE_H

#include "database.h"

namespace subject {
    extern const QString SUBJECT;
    extern const QString ID;
    extern const QString MARK;

    namespace comment {
    extern const QString COMMENT;
    extern const QString TEXT;
    extern const QString STATE;
    extern const QString IS_ACCEPTED;
    }
}

class SubjectDatabase : public DatabaseInterface {
    SubjectDatabase();

    enum Commands : qint8 {
        GET_MARK_LIST = 0x00,
        SEND_MARK = 0x01,
        GET_COMMENT_LIST = 0x02,
        SEND_COMMENT = 0x03,
        GET_COMMENT_STATE = 0x04
    };

public:
    static SubjectDatabase& getInstance();
};



class GetBySubject : public Command {
protected:
    QSqlQuery query;

public:
    GetBySubject();
    QJsonDocument handle(const QJsonDocument &request) override;
};



class GetMarkList : public GetBySubject {
    const QString command =
            "SELECT user_id, user_login, mark "
            "FROM MARKS INNER JOIN USERS USING(user_id) "
            "WHERE subject_id = :subject_id;";
public:
    GetMarkList();
};



class GetComments : public GetBySubject {
    const QString command =
            "SELECT user_id, user_login, comment_text "
            "FROM SUBJECT_COMMENTS INNER JOIN USERS "
            "USING(user_id) "
            "WHERE subject_id = :subject_id "
            "AND is_accepted = true;";
public:
    GetComments();
};



class GetCommentState : public GetBySubject {
    const QString command =
            "SELECT is_accepted "
            "FROM SUBJECT_COMMENTS "
            "WHERE user_id = :user_id "
            "AND subject_id = :subject_id;";

    enum CommentState : qint8 {
        NOT_FOUND = 0x00,
        ON_MODERATION = 0x01,
        PUBLISHED = 0x02
    };

    QJsonDocument sendAnswer(qint8 answer);

public:
    GetCommentState();
    QJsonDocument handle(const QJsonDocument &request) override;
};



class SendMark : public Command {
    QSqlQuery insert_query;
    QSqlQuery clean_query;

    const QString insert_command =
            "INSERT INTO MARKS(mark_id, user_id, subject_id, mark) "
            "VALUES (nextval('mark_id_sequence'), :user_id, :subject_id, :mark);";

    const QString clean_command =
            "DELETE FROM MARKS "
            "WHERE user_id = :user_id "
            "AND subject_id = :subject_id;";
public:
    SendMark();
    QJsonDocument handle(const QJsonDocument &request) override;
};



class SendComment : public Command {
    QSqlQuery query;

    const QString command =
            "INSERT INTO SUBJECT_COMMENTS (comment_id, comment_text, user_id, subject_id, is_accepted) "
            "VALUES (nextval('comment_id_sequence'), :comment_text, :user_id, :subject_id, false);";

public:
    SendComment();
    QJsonDocument handle(const QJsonDocument &request) override;
};

#endif // SUBJECTDATABASE_H
