#include "historydatabase.h"
#include <QJsonObject>
#include <QVariant>
#include <QDebug>

namespace history {
    const QString YEAR     = "entry_year";
    const QString SEMESTER = "semester";
}

HistoryDatabase::HistoryDatabase() {
    factory.addCommand<GetYearList>(GET_YEAR_LIST);
    factory.addCommand<GetSemesterList>(GET_SEMESTER_LIST);
    factory.addCommand<GetSubjectList>(GET_SUBJECT_LIST);
}

HistoryDatabase& HistoryDatabase::getInstance() {
    static HistoryDatabase instance;
    return instance;
}


/* Commands */
GetYearList::GetYearList() {
    query.prepare(command);
}

QJsonDocument GetYearList::handle(const QJsonDocument &) {
    return Command::sqlQueryToJson(query);
}


GetSemesterList::GetSemesterList() {
    query.prepare(command);
}

QJsonDocument GetSemesterList::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return QJsonDocument{};

    QJsonObject object = request.object();
    auto result = JsonParser<int>::parse(object, {history::YEAR});
    if (result == std::nullopt) {
        return QJsonDocument{};
    }
    int year = std::get<0>(result.value());
    if (year == extractor<int>::PARSE_ERROR)
        return QJsonDocument{};

    query.bindValue(toDomain(history::YEAR), year);
    return Command::sqlQueryToJson(query);
}


GetSubjectList::GetSubjectList() {
    query.prepare(command);
}

QJsonDocument GetSubjectList::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return QJsonDocument();

    QJsonObject object = request.object();
    /* Get year */
    auto result = JsonParser<int, int>::parse(object, {history::YEAR, history::SEMESTER});
    if (result == std::nullopt)
        return QJsonDocument{};
    auto tuple = result.value();

    int year = std::get<0>(tuple);
    int semester = std::get<1>(tuple);

    if (year == extractor<int>::PARSE_ERROR ||
        semester == extractor<int>::PARSE_ERROR)
        return QJsonDocument{};

    query.bindValue(toDomain(history::YEAR), year);
    query.bindValue(toDomain(history::SEMESTER), semester);
    return Command::sqlQueryToJson(query);
}


