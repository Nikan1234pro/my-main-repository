#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QDebug>
#include <optional>
#include <tuple>

/* Extractors for useful types */
template<class DataType>
struct extractor {
    static DataType extract(const QJsonValue&) {
        return DataType{};
    }
};

template<>
struct extractor<int> {
    static constexpr int PARSE_ERROR = -1;

    static int extract(const QJsonValue &val) {
        return val.toInt(PARSE_ERROR);
    }
};

template<>
struct extractor<bool> {
    static bool extract(const QJsonValue &val) {
        return val.toBool(false);
    }
};

template<>
struct extractor<QString> {
    static QString extract(const QJsonValue &val) {
        return val.toString();
    }
};

template<>
struct extractor<QJsonObject> {
    static QJsonObject extract(const QJsonValue &val) {
        return val.toObject();
    }
};


template <class DataType>
struct JsonDataGetter {
static bool getData(DataType &fill_place,
                    const QJsonObject &object,
                    const QString &key) {
        auto it = object.find(key);
        if (it == object.end())
            return false;

        fill_place = extractor<DataType>::extract(*it);
        return true;
    }
};

template<std::size_t Cur, class... Args>
struct TupleFiller {
    static bool fill(std::tuple<Args...> &tuple,
                     const QJsonObject &object,
                     const QString(&keys)[sizeof...(Args)]) {

        if (!TupleFiller<Cur - 1, Args...>::fill(tuple, object, keys))
            return false;

        return JsonDataGetter<std::remove_reference_t<decltype (std::get<Cur>(tuple))>>::getData(
                    std::get<Cur>(tuple), object, keys[Cur]);
    }
};

template<class... Args>
struct TupleFiller<0, Args...> {
    static bool fill(std::tuple<Args...> &tuple,
                     const QJsonObject &object,
                     const QString(&keys)[sizeof...(Args)]) {

        return JsonDataGetter<std::remove_reference_t<decltype (std::get<0>(tuple))>>::getData(
                    std::get<0>(tuple), object, keys[0]);
    }
};

template <class... Args>
struct JsonParser {
    static std::optional<std::tuple<Args...>> parse(const QJsonObject &object,
                                                    const QString(&keys)[sizeof...(Args)]) {
        std::tuple<Args...> tuple;
        if (!TupleFiller<sizeof...(Args) - 1, Args...>::fill(tuple, object, keys))
           return std::nullopt;
        return tuple;
    }
};

#endif // JSON_PARSER_H
