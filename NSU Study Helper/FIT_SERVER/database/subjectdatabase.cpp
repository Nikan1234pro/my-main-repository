#include "subjectdatabase.h"
#include "authentication_service.h"

#include <QJsonArray>

namespace subject {
    const QString SUBJECT = "subject";
    const QString ID      = "subject_id";
    const QString MARK    = "mark";

    namespace comment {
        const QString COMMENT     = "comment";
        const QString TEXT        = "comment_text";
        const QString STATE       = "comment_state";
        const QString IS_ACCEPTED = "is_accepted";
    }
}

SubjectDatabase::SubjectDatabase() {
    factory.addCommand<GetMarkList>(GET_MARK_LIST);
    factory.addCommand<GetComments>(GET_COMMENT_LIST);
    factory.addCommand<SendMark>(SEND_MARK);
    factory.addCommand<SendComment>(SEND_COMMENT);
    factory.addCommand<GetCommentState>(GET_COMMENT_STATE);
}

SubjectDatabase& SubjectDatabase::getInstance() {
    static SubjectDatabase instance;
    return instance;
}



GetBySubject::GetBySubject(){};

QJsonDocument GetBySubject::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    /* Get info about subject */
    auto opt = JsonParser<int>::parse(
                request.object(), { subject::ID });
    if (opt == std::nullopt)
        return getErrorMessage();
    int subject_id = std::get<0>(opt.value());

    /* Get and send marks */
    query.bindValue(toDomain(subject::ID), subject_id);
    return sqlQueryToJson(query);
}



GetMarkList::GetMarkList() {
    query.prepare(command);
}



GetComments::GetComments() {
    query.prepare(command);
}



GetCommentState::GetCommentState() {
    query.prepare(command);
}

QJsonDocument GetCommentState::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    auto opt = JsonParser<QJsonObject, QJsonObject>::parse(
                request.object(), {auth::USER, subject::SUBJECT});
    if (opt == std::nullopt)
        return getErrorMessage();
    auto tuple = opt.value();

    auto user    = std::get<0>(tuple);
    auto subject = std::get<1>(tuple);


    /* Check user */
    if (!AuthenticationService::getInstance().checkToken(user))
        return getErrorMessage();

    /* Get user id and subject id */
    auto user_opt = JsonParser<int>::parse(user, {auth::ID});
    if (user_opt == std::nullopt)
        return getErrorMessage();

    auto subject_opt = JsonParser<int>::parse(subject, {subject::ID});
    if (subject_opt == std::nullopt)
        getErrorMessage();

    int user_id = std::get<0>(user_opt.value());
    int subject_id = std::get<0>(subject_opt.value());

    query.bindValue(toDomain(auth::ID), user_id);
    query.bindValue(toDomain(subject::ID), subject_id);

    auto data = sqlQueryToJson(query);
    if (data.isNull()) {
        return getErrorMessage();
    }

    /* No comments from this user */
    if (data.array().size() == 0)
        return sendAnswer(NOT_FOUND);

    auto comment_opt = JsonParser<bool>::parse(
                data.array().first().toObject(), {subject::comment::IS_ACCEPTED});
    if (comment_opt == std::nullopt)
        return getErrorMessage();

    bool is_accepted = std::get<0>(comment_opt.value());
    qDebug() << is_accepted;
    return sendAnswer((is_accepted) ? PUBLISHED : ON_MODERATION);
}

QJsonDocument GetCommentState::sendAnswer(qint8 answer) {
    QJsonObject object;
    object.insert(subject::comment::STATE, answer);

    QJsonDocument doc;
    doc.setObject(object);

    return doc;
}



SendMark::SendMark() {
    insert_query.prepare(insert_command);
    clean_query.prepare(clean_command);
}

QJsonDocument SendMark::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    /* Get json data */
    auto opt = JsonParser<QJsonObject, QJsonObject, int>::parse(
                request.object(), {auth::USER, subject::SUBJECT, subject::MARK});
    if (opt == std::nullopt)
        return QJsonDocument{};
    auto tuple = opt.value();

    auto user    = std::get<0>(tuple);
    auto subject = std::get<1>(tuple);
    int  mark    = std::get<2>(tuple);

    /* Get and check token */
    auto user_opt = JsonParser<int, int>::parse(user, { auth::ID, auth::TOKEN });
    if (user_opt == std::nullopt)
        return getErrorMessage();
    auto user_tuple = user_opt.value();

    int user_id = std::get<0>(user_tuple);
    int token   = std::get<1>(user_tuple);
    if (!AuthenticationService::getInstance().checkToken(user_id, token))
        return getErrorMessage();

    /* Get subject info */
    auto subject_opt = JsonParser<int>::parse(subject, { subject::ID });
    if (subject_opt == std::nullopt)
        return getErrorMessage();

    int subject_id = std::get<0>(subject_opt.value());

    /* Clean previous user mark for this subject */
    clean_query.bindValue(toDomain(auth::ID), user_id);
    clean_query.bindValue(toDomain(subject::ID), subject_id);
    clean_query.exec();

    /* Add mark */
    insert_query.bindValue(toDomain(auth::ID), user_id);
    insert_query.bindValue(toDomain(subject::ID), subject_id);
    insert_query.bindValue(toDomain(subject::MARK), mark);
    insert_query.exec();

    return getSuccessMessage();
}



SendComment::SendComment() {
    query.prepare(command);
}

QJsonDocument SendComment::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    /* Get data from json */
    auto opt = JsonParser<QJsonObject, QJsonObject>::parse(
                request.object(), { auth::USER,  subject::comment::COMMENT });
    if (opt == std::nullopt)
        return getErrorMessage();
    auto tuple  = opt.value();

    auto user    = std::get<0>(tuple);
    auto comment = std::get<1>(tuple);


   /* Get info about user and check it */
    auto user_opt = JsonParser<int, int>::parse(user, { auth::ID, auth::TOKEN });
    if (user_opt == std::nullopt)
        return getErrorMessage();
    auto user_tuple = user_opt.value();

    int user_id    = std::get<0>(user_tuple);
    int user_token = std::get<1>(user_tuple);
    if (!AuthenticationService::getInstance().checkToken(user_id, user_token)) {
        return getErrorMessage();
    }

    /* Get comment text */
    auto comment_opt = JsonParser<QString, QJsonObject>::parse(
                comment, { subject::comment::TEXT, subject::SUBJECT});
    if (comment_opt == std::nullopt)
        return getErrorMessage();
    auto text = std::get<0>(comment_opt.value());

    /* Get subject id */
    auto subject_opt = JsonParser<int>::parse(
                std::get<1>(comment_opt.value()), {subject::ID});
    if (subject_opt == std::nullopt)
        return getErrorMessage();

    int subject_id = std::get<0>(subject_opt.value());
    query.bindValue(toDomain(subject::comment::TEXT), text);
    query.bindValue(toDomain(subject::ID), subject_id);
    query.bindValue(toDomain(auth::ID), user_id);
    if (!query.exec())
        return getErrorMessage();

    return getSuccessMessage();
}














