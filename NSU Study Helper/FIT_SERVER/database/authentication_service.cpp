#include "authentication_service.h"
#include "../mail_service/mail_service.h"

#include <QSqlRecord>
#include <QJsonObject>
#include <QSqlError>
#include <QJsonArray>
#include <QCryptographicHash>

namespace auth {
    const QString USER          = "user";

    const QString LOGIN         = "user_login";
    const QString PASSWORD      = "user_pass";
    const QString ID            = "user_id";
    const QString TOKEN         = "user_token";
    const QString ADMIN         = "is_admin";
    const QString PINCODE       = "pin_code";
    const QString IS_ADMIN      = "is_admin";
}


AuthenticationService::AuthenticationService() {
    factory.addCommand<Login>(LOGIN);
    factory.addCommand<SendPinCode>(SEND_PIN);
    factory.addCommand<Register>(REGISTER);
};

AuthenticationService& AuthenticationService::getInstance() {
    static AuthenticationService instance;
    return instance;
}

AuthenticationService::PinCodeType AuthenticationService::generatePinCode() {
    constexpr int PINCODE_LENGTH = 6;

    PinCodeType result;
    std::srand(static_cast<uint>(std::time(nullptr)));
    for (int i = 0; i < PINCODE_LENGTH; ++i) {
        int n = std::rand() % 10;
        result += PinCodeType::number(n);
    }
    return result;
}

bool AuthenticationService::validateEmail(const QString &email) {
    return email.contains("@g.nsu.ru");
}

bool AuthenticationService::handlePinCode(const QString &login,
                                          const QString &pin_code) {
    auto required_pin = pin_codes.find(login);
    if (required_pin == pin_codes.end())
        return false;

    if (pin_code != required_pin->second)
        return false;

    pin_codes.erase(required_pin);
    return true;
}

bool AuthenticationService::checkToken(const IdType &user_id,
                                       const TokenType &token) const {
    auto it = tokens.find(user_id);
    if (it == tokens.end())
        return false;
    return it->second == token;
}

bool AuthenticationService::checkToken(const QJsonObject &user_json) const {
    auto user_opt = JsonParser<IdType, TokenType>::parse(
                user_json, { auth::ID, auth::TOKEN });

    if (user_opt == std::nullopt)
        return false;
    auto user_tuple = user_opt.value();

    auto user_id = std::get<0>(user_tuple);
    auto token   = std::get<1>(user_tuple);

    return checkToken(user_id, token);
}

AuthenticationService::TokenType AuthenticationService::generateToken(const IdType &id) {
    std::srand(static_cast<uint>(std::time(nullptr)));
    int token =  std::rand() % MAX_TOKENS;
    tokens[id] = token;
    return token;
}

AuthenticationService::PinCodeStorage& AuthenticationService::getPinCodes() {
    return pin_codes;
}

AuthenticationService::TokenStorage& AuthenticationService::getTokens() {
    return tokens;
}


Login::Login() {
    query.prepare(command);
}

QJsonDocument Login::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    /* Get login and password from request */
    QJsonObject object = request.object();
    auto opt = JsonParser<QJsonObject>::parse(object, { auth::USER });
    if (opt == std::nullopt) {
        return getErrorMessage();
    }

    auto user_opt = JsonParser<QString, QString>::parse(
                std::get<0>(opt.value()), { auth::LOGIN, auth::PASSWORD});
    auto tuple = user_opt.value();
    QString login = std::get<0>(tuple);
    QString password = std::get<1>(tuple);

    /* Execute */
    query.bindValue(toDomain(auth::LOGIN), login);
    auto data = sqlQueryToJson(query);
    if (data.isNull())
        return getErrorMessage();

    /* Result must contains only one user */
    if (data.array().size() > 1)
        return getErrorMessage();

    QJsonObject user_data = data.array().first().toObject();
    auto expect_pass = JsonParser<QString>::parse(user_data, {auth::PASSWORD});
    if (expect_pass == std::nullopt)
        return getErrorMessage();

    QString encrypted_password = QCryptographicHash::hash(password.toUtf8(),
                                                          QCryptographicHash::Sha256).toHex();
    /* Accept */
    if (encrypted_password == std::get<0>(expect_pass.value())) {
        int id = user_data[auth::ID].toInt();
        int token = AuthenticationService::getInstance().generateToken(id);

        /* Register token */
        AuthenticationService::getInstance().getTokens()[id] = token;

        QJsonObject   answer_object = getSuccessMessage().object();
        QJsonObject   user_info_object;
        user_info_object.insert(auth::ID, id);
        user_info_object.insert(auth::LOGIN, login);
        user_info_object.insert(auth::TOKEN, token);
        user_info_object.insert(auth::ADMIN, user_data[auth::ADMIN]);

        answer_object.insert(auth::USER, user_info_object);
        QJsonDocument answer;
        answer.setObject(answer_object);
        return answer;
    }
    return getErrorMessage(error_msg);
}


SendPinCode::SendPinCode() = default;

QJsonDocument SendPinCode::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    auto opt = JsonParser<QJsonObject>::parse(request.object(), {auth::USER});
    if (opt == std::nullopt)
        return getErrorMessage();

    auto login_opt = JsonParser<QString>::parse(std::get<0>(opt.value()), {auth::LOGIN});
    if (login_opt == std::nullopt)
        return getErrorMessage();

    QString login = std::get<0>(login_opt.value());
    if (!AuthenticationService::validateEmail(login))
        return getErrorMessage(error_msg);

    auto pinCode = AuthenticationService::generatePinCode();
    AuthenticationService::getInstance().getPinCodes()[login] = pinCode;

    MailService{}.sendPinCode(login, pinCode);
    return getSuccessMessage();
}


Register::Register() {
    register_query.prepare(register_command);

    check_query.prepare(check_command);
}

QJsonDocument Register::handle(const QJsonDocument &request) {
    if (!request.isObject())
        return getErrorMessage();

    auto opt = JsonParser<QJsonObject>::parse(request.object(), {auth::USER});
    if (opt == std::nullopt)
        return getErrorMessage();

    auto user_opt = JsonParser<QString, QString, QString>::parse(
                std::get<0>(opt.value()), {auth::LOGIN, auth::PINCODE, auth::PASSWORD});
    auto tuple = user_opt.value();
    QString login    = std::get<0>(tuple);
    QString pin      = std::get<1>(tuple);
    QString password = std::get<2>(tuple);

    /* Check is there a user with the same login */
    if (!isNotRegistered(login))
        return getErrorMessage(reg_err);

    /* Find required pin code and match */
    if (!AuthenticationService::getInstance().handlePinCode(login, pin))
        return getErrorMessage(pin_err);

    QString encrypted_password = QCryptographicHash::hash(password.toUtf8(),
                                                          QCryptographicHash::Sha256).toHex();

    register_query.bindValue(toDomain(auth::LOGIN), login);
    register_query.bindValue(toDomain(auth::PASSWORD), encrypted_password);
    register_query.bindValue(toDomain(auth::IS_ADMIN), false);
    if (register_query.exec())
        return getSuccessMessage();

    return getErrorMessage();
}

bool Register::isNotRegistered(const QString login) {
    check_query.bindValue(toDomain(auth::LOGIN), login);
    check_query.exec();

    return !check_query.next();
}









