#include "database.h"

#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlRecord>
#include <QVariant>
#include <QDebug>

/* Big endian array */
QByteArray intToBytes(int number) {
    QByteArray array;
    array.append(static_cast<char>((number) >> 24));
    array.append(static_cast<char>((number) >> 16));
    array.append(static_cast<char>((number) >> 8));
    array.append(static_cast<char>((number) >> 0));
    return array;
}

int bytesToInt(const QByteArray& array)  {
    return static_cast<int>(
                (static_cast<uint>(array[0] << 24) & 0xFF000000) |
                (static_cast<uint>(array[1] << 16) & 0x00FF0000) |
                (static_cast<uint>(array[2] << 8)  & 0x0000FF00) |
                (static_cast<uint>(array[3] << 0)  & 0x000000FF));
}


DatabaseUnavailableException::DatabaseUnavailableException(const std::string& message)
    : message_(message) {}

const char * DatabaseUnavailableException::what() const
_GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT {
    return message_.c_str();
}

DatabaseInterface::DatabaseInterface() = default;
DatabaseInterface::~DatabaseInterface() = default;

void DatabaseInterface::initDatabase() {
    static QSqlDatabase database =
            QSqlDatabase::addDatabase(DATABASE_TYPE);

    database.setDatabaseName(DATABASE_NAME);
    database.setHostName(DATABASE_HOSTNAME);
    database.setPort(DATABASE_PORT);
    database.setUserName(DATABASE_USERNAME);
    database.setPassword(DATABASE_PASSWORD);
    if (!database.open())
        throw DatabaseUnavailableException(
                "Couldn't connect to database");
}

QJsonDocument DatabaseInterface::handle(qint8 command, const QJsonDocument& document) const {
    auto handler = factory.getCommand(command);
    if (!handler)
        return QJsonDocument{};

    return handler->handle(document);
}


QJsonDocument Command::sqlQueryToJson(QSqlQuery& sqlquery) {
    if (!sqlquery.exec())
        return QJsonDocument{};

    QJsonDocument  json;
    QJsonArray     recordsArray;

    while (sqlquery.next()) {
        QJsonObject recordObject;
        for (int x = 0; x < sqlquery.record().count(); x++) {
             recordObject.insert(sqlquery.record().fieldName(x),
                                 QJsonValue::fromVariant(sqlquery.value(x)));
        }
        recordsArray.push_back(recordObject);
    }
    json.setArray(recordsArray);
    return json;
}

QJsonDocument Command::getErrorMessage() const {
    static QString err_msg = "Возникла ошибка...";
    QJsonDocument document;
    QJsonObject object;
    object.insert(RESULT, false);
    object.insert(ERROR_MESSAGE, err_msg);

    document.setObject(object);
    return document;
}

QJsonDocument Command::getErrorMessage(const QString &cause) const {
    QJsonDocument document;
    QJsonObject object;
    object.insert(RESULT, false);
    object.insert(ERROR_MESSAGE, cause);

    document.setObject(object);
    return document;
}

QJsonDocument Command::getSuccessMessage() const {
    QJsonDocument document;
    QJsonObject object;
    object.insert(RESULT, true);
    document.setObject(object);
    return document;
}





