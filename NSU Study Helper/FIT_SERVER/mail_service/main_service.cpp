#include "mail_service.h"

MailService::MailService() :
    smtp(mailbox_address,
         mailbox_port,
         SmtpClient::SslConnection) {
    smtp.setUser(sender_login);
    smtp.setPassword(sender_password);
}

bool MailService::sendPinCode(const QString &receiver, const QString &pin_code) {
    MimeMessage message;

    message.setSender(new EmailAddress(sender_login, sender_name));
    message.addRecipient(new EmailAddress(receiver));
    message.setSubject(message_subject);

    MimeText text;

    text.setText(message_body + pin_code);
    message.addPart(&text);

    smtp.connectToHost();
    smtp.login();
    return smtp.sendMail(message);
}


MailService::~MailService() {
    smtp.quit();
}
