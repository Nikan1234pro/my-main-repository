/*
  Copyright (c) 2011-2012 - Tőkés Attila

  This file is part of SmtpClient for Qt.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  See the LICENSE file for more details.
*/

#ifndef SMTPMIME_H
#define SMTPMIME_H

#include "smtpclient.h"
#include "mimepart.h"
#include "mimehtml.h"
#include "mimeattachment.h"
#include "mimemessage.h"
#include "mimetext.h"
#include "mimeinlinefile.h"
#include "mimefile.h"

class MailService {
    const QString sender_name     = "NSU Study Helper Team";
    const QString sender_login    = "fit.nsu.studyhelper@gmail.com";
    const QString sender_password = "hello_there2007";

    const QString mailbox_address = "smtp.gmail.com";
    const int mailbox_port = 465;

    SmtpClient smtp;


    const QString message_subject = "Your PinCode";
    const QString message_body = "Hello, your pincode is:\n";
public:
    MailService();
    bool sendPinCode(const QString &receiver, const QString &pin_code);
    ~MailService();
};

#endif // SMTPMIME_H
