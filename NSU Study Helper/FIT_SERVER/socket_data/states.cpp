#include "../server/request_dispatcher.h"
#include "../database/database.h"
#include "states.h"

#include <QDebug>

ConnectionState::ConnectionState() = default;

ConnectionState::~ConnectionState() = default;


NotReady::NotReady(QTcpSocket *socket_)
    : socket(socket_) {}

NotReady::~NotReady() = default;

std::unique_ptr<ConnectionState> NotReady::handle() {
    buffer.append(socket->readAll());
    if (buffer.size() < MIN_SIZE)
        return std::make_unique<NotReady>(*this);

    if (buffer.at(WITH_ARGS_POS)) {
        return std::make_unique<WaitingForArgument>(
                    socket, buffer)->handle();
    }
    return std::make_unique<ProcessingWithoutArgument>(socket,
                buffer.at(DATABASE_TYPE_POS),
                buffer.at(REQUEST_TYPE_POS))->handle();
}


ProcessingWithoutArgument::ProcessingWithoutArgument(QTcpSocket *socket_,
                                                      qint8 type_, qint8 command_)
    : socket(socket_) {
    type = type_;
    command = command_;
}

ProcessingWithoutArgument::~ProcessingWithoutArgument() = default;

std::unique_ptr<ConnectionState> ProcessingWithoutArgument::handle() {
    QJsonDocument answer = RequestDispatcher{}.dispach(type, command, QJsonDocument{});
    QByteArray bytes = answer.toJson();
    socket->write(intToBytes(bytes.size()));
    socket->write(bytes);
    return nullptr;
}


WaitingForArgument::WaitingForArgument(QTcpSocket *socket_,
                                       const QByteArray& buffer_)
    : socket(socket_), buffer(buffer_) {
    type = buffer.at(DATABASE_TYPE_POS);
    command = buffer.at(REQUEST_TYPE_POS);

    buffer.remove(DATABASE_TYPE_POS, MIN_SIZE);
}

WaitingForArgument::~WaitingForArgument() = default;

std::unique_ptr<ConnectionState> WaitingForArgument::handle() {
    buffer.append(socket->readAll());

    /* First of all - get Json document size */
    if (!got_size) {
        if (buffer.size() < static_cast<int>(sizeof(int)))
            return std::make_unique<WaitingForArgument>(*this);

        document_size = bytesToInt(buffer);
        buffer.remove(0, static_cast<int>(sizeof(int)));
        got_size = true;
    }

    /* Get document */
    if (got_size) {
        if (buffer.size() == document_size) {
            QJsonParseError error;
            QJsonDocument doc = QJsonDocument::fromJson(buffer, &error);
            if (error.errorString().toInt() != QJsonParseError::NoError)
                return nullptr;

            return std::make_unique<ProcessingWithArgument>(socket,
                        type, command, doc)->handle();
        }
        else if (buffer.size() > document_size)
            return nullptr;
    }
    return std::make_unique<WaitingForArgument>(*this);
}

ProcessingWithArgument::ProcessingWithArgument(QTcpSocket *socket_,
                                               qint8 type_, qint8 command_,
                                               const QJsonDocument& json_)
    : socket(socket_), type(type_), command(command_), json(json_) {}

ProcessingWithArgument::~ProcessingWithArgument() = default;

std::unique_ptr<ConnectionState> ProcessingWithArgument::handle() {
    QJsonDocument answer = RequestDispatcher{}.dispach(type, command, json);
    QByteArray bytes = answer.toJson();
    socket->write(intToBytes(bytes.size()));
    socket->write(bytes);
    return nullptr;
}
















