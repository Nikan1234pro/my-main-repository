#include "socketdata.h"

SocketData::SocketData() {}

SocketData::SocketData(QTcpSocket *socket_)
    : state(new NotReady(socket_)) {}

void SocketData::handle() {
    state = state->handle();
}

bool SocketData::isExpired() const {
    return state == nullptr;
}
