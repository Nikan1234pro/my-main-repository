#ifndef SOCKETDATA_H
#define SOCKETDATA_H

#include <QTcpSocket>
#include <QByteArray>
#include <memory>

#include "states.h"

class SocketData {
    std::unique_ptr<ConnectionState> state;
public:
    SocketData();
    SocketData(QTcpSocket *socket_);

    void handle();
    bool isExpired() const;
};

#endif // SOCKETDATA_H
