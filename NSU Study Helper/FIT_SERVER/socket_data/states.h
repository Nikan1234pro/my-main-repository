#ifndef ISTATE_H
#define ISTATE_H

#include <QByteArray>
#include <QTcpSocket>
#include <QJsonDocument>
#include <memory>

class ConnectionState {
protected:
    static constexpr int DATABASE_TYPE_POS = 0;
    static constexpr int REQUEST_TYPE_POS = 1;
    static constexpr int WITH_ARGS_POS = 2;

    static constexpr int MIN_SIZE = 3;
public:
    ConnectionState();

    virtual std::unique_ptr<ConnectionState> handle() = 0;
    virtual ~ConnectionState();
};


class NotReady : public ConnectionState {
    QTcpSocket *socket;
    QByteArray  buffer;
public:
    NotReady(QTcpSocket *socket_);
    std::unique_ptr<ConnectionState> handle() override;
    ~NotReady() override;
};


class ProcessingWithoutArgument : public ConnectionState {
    QTcpSocket *socket;
    QByteArray  buffer;

    qint8 type;
    qint8 command;
public:
    ProcessingWithoutArgument(QTcpSocket *socket_,
                              qint8 type_, qint8 command_);
    std::unique_ptr<ConnectionState> handle() override;
    ~ProcessingWithoutArgument() override;
};


class WaitingForArgument : public ConnectionState {
    QTcpSocket *socket;
    QByteArray  buffer;

    int document_size = 0;
    bool got_size = false;

    qint8 type;
    qint8 command;
public:
    WaitingForArgument(QTcpSocket *socket_, const QByteArray& buffer_);
    std::unique_ptr<ConnectionState> handle() override;
    ~WaitingForArgument() override;
};


class ProcessingWithArgument : public ConnectionState {
    QTcpSocket *socket;

    qint8 type;
    qint8 command;

    QJsonDocument json;
public:
    ProcessingWithArgument(QTcpSocket *socket_,
                           qint8 type_, qint8 command_,
                           const QJsonDocument& json);

    std::unique_ptr<ConnectionState> handle() override;
    ~ProcessingWithArgument() override;
};

#endif // ISTATE_H
