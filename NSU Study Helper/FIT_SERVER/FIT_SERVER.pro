QT -= gui
QT += network sql

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        database/authentication_service.cpp \
        database/database.cpp \
        database/historydatabase.cpp \
        database/subjectdatabase.cpp \
        mail_service/emailaddress.cpp \
        mail_service/main_service.cpp \
        mail_service/mimeattachment.cpp \
        mail_service/mimecontentformatter.cpp \
        mail_service/mimefile.cpp \
        mail_service/mimehtml.cpp \
        mail_service/mimeinlinefile.cpp \
        mail_service/mimemessage.cpp \
        mail_service/mimemultipart.cpp \
        mail_service/mimepart.cpp \
        mail_service/mimetext.cpp \
        mail_service/quotedprintable.cpp \
        mail_service/smtpclient.cpp \
        main.cpp \
        server/request_dispatcher.cpp \
        server/server.cpp \
        socket_data/socketdata.cpp \
        socket_data/states.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    database/authentication_service.h \
    database/database.h \
    database/historydatabase.h \
    database/json_parser.h \
    database/subjectdatabase.h \
    mail_service/emailaddress.h \
    mail_service/mail_service.h \
    mail_service/mimeattachment.h \
    mail_service/mimecontentformatter.h \
    mail_service/mimefile.h \
    mail_service/mimehtml.h \
    mail_service/mimeinlinefile.h \
    mail_service/mimemessage.h \
    mail_service/mimemultipart.h \
    mail_service/mimepart.h \
    mail_service/mimetext.h \
    mail_service/quotedprintable.h \
    mail_service/smtpclient.h \
    server/request_dispatcher.h \
    server/server.h \
    socket_data/socketdata.h \
    socket_data/states.h
