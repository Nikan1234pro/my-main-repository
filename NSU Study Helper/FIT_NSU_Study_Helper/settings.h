#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>

constexpr int WINDOW_WIDTH = 830;
constexpr int WINDOW_HEIGHT = 580;

constexpr int DIALOG_WIDTH = 500;
constexpr int DIALOG_HEIGHT = 300;

extern const QString scrollbar_style;
extern const QString area_style;
extern const QString box_style;

extern const QString subtitle_style;
extern const QString title_style;
extern const QString text_style;
extern const QString button_style;
extern const QString text_edit_style;

extern const QString linked_text_in;
extern const QString linked_text_out;

extern const QString text_default_font;
extern const QString button_default_font;

extern const QString error_header;

#endif // SETTINGS_H
