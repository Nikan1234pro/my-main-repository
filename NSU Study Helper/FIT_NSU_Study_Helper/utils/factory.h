/////////////////////////////////////////////// 
//	     Singletone Template Factory		 //
//											 //
///////////////////////////////////////////////
//	BaseType - type of Abstract Base Class	 //
//	DervType - type of Derived Class		 //
//	Key - type of identificator key			 //
///////////////////////////////////////////////

#pragma once
#include <map>
#include <memory>

template <typename BaseType, typename... Args>
class AbstractCreator {
public:
	virtual BaseType* create(Args...) const = 0;
};

template <typename BaseType, typename DervType, typename... Args>
class Creator : public AbstractCreator<BaseType, Args...> {
public:
    BaseType* create(Args... args) const {
		return new DervType(args...);
	}
};

template <typename Key, typename BaseType, typename... Args>
class Factory {
private:
	using FactoryMap = std::map<Key, std::unique_ptr<AbstractCreator<BaseType, Args...> > >;
	FactoryMap _factory;
public:
	Factory() = default;

	template <typename DervType>
    void add(const Key &key) {
        std::unique_ptr<AbstractCreator<BaseType, Args...> > uptr(
                    new Creator<BaseType, DervType, Args...>{});

        _factory[key] = std::move(uptr);
	}

    BaseType *create(const Key &key, Args... args) const {
		auto iter = _factory.find(key);
		if (iter == _factory.end())
            return nullptr;
		return iter->second->create(args...);
	}

	~Factory()
	{}
};
