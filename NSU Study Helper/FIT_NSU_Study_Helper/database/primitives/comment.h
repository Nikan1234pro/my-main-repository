#ifndef COMMENT_H
#define COMMENT_H

#include <QString>
#include "user.h"
#include "serializable.h"

namespace database {
namespace primitives {

class Comment : public SerializableObject {
    User user_;
    QString text_;

public:
    Comment(const User& user, const QString &text);

    const QString& getText() const;
    const User& getUser() const;
    QJsonObject toJson() const override;
};
}

}

#endif // COMMENT_H
