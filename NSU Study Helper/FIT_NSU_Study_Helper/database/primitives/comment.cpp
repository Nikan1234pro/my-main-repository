#include "comment.h"
#include "../../network/host_socket.h"
#include "config.h"

#include <QJsonObject>

namespace database {
namespace primitives {

using namespace network;

Comment::Comment(const User& user, const QString &text)
    : user_(user), text_(text) {}

const QString& Comment::getText() const {
    return text_;
}

const User& Comment::getUser() const {
    return user_;
}


QJsonObject Comment::toJson() const {
    QJsonObject object;
    object.insert(domains::COMMENT_TEXT, text_);
    return object;
}


}
}
