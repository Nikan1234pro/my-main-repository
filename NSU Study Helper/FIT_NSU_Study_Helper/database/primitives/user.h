#ifndef USER_H
#define USER_H

#include <QString>
#include "serializable.h"

namespace database {
namespace primitives {

class User : public SerializableObject {
    int id_;
    QString nickname_;
public:
    User(int id, const QString &nickname);
    User(const User &other);
    bool operator ==(const User &other) const;
    bool operator !=(const User &other) const;
    int getId() const;
    const QString& getName() const;
    QJsonObject toJson() const override;
};
}
}

namespace std {

template<>
struct hash<database::primitives::User> {
    size_t operator()(const database::primitives::User &user) const {
        std::hash<int> hash;
        return hash(user.getId());
    }
};
}


#endif // USER_H
