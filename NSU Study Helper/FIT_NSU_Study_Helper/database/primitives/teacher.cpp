#include "teacher.h"
#include "../../network/host_socket.h"

namespace database {
namespace primitives {

using namespace network;

Teacher::Teacher(int id,
                 const QString& first_name,
                 const QString& middle_name,
                 const QString& last_name)
    : id_(id),
      first_name_(first_name),
      middle_name_(middle_name),
      last_name_(last_name) {}

QJsonObject Teacher::toJson() const {
    return QJsonObject{};
}

}
}
