#include "subject.h"
#include "../../network/host_socket.h"

#include <QDataStream>
#include <QJsonObject>
#include "config.h"

namespace database {
namespace primitives {

using namespace network;

Subject::Subject(int id, int year, int semester, const QString &name)
    : id_(id), year_(year), semester_(semester), name_(name) {
}

int Subject::getId() const {
    return id_;
}

const QString& Subject::getName() const {
    return name_;
}

QJsonObject Subject::toJson() const {
    QJsonObject object;
    object.insert(domains::SUBJECT_ID, id_);
    return object;
}

int Subject::getYear() const {
    return year_;
}

int Subject::getSemester() const {
    return semester_;
}

}
}
