#ifndef TEACHER_H
#define TEACHER_H

#include <QString>
#include "serializable.h"

namespace database {
namespace primitives {

class Teacher : public SerializableObject {
    int id_;
    QString first_name_;
    QString middle_name_;
    QString last_name_;
public:
    Teacher(int id,
            const QString& first_name,
            const QString& middle_name,
            const QString& last_name);
    QJsonObject toJson() const override;
};

}
}

#endif // TEACHER_H
