#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include <QJsonObject>

namespace database {
namespace primitives {

constexpr int BYTE = 8;

class SerializableObject {
public:
    virtual QJsonObject toJson() const = 0;
    virtual ~SerializableObject() = default;
};

}
}
#endif // SERIALIZABLE_H
