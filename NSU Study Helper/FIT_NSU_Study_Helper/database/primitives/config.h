#ifndef INITIALIZER_H
#define INITIALIZER_H
#include <stdexcept>
#include <QTcpSocket>
#include <Qt>

namespace database {


namespace domains {
    extern const QString YEAR;
    extern const QString SEMESTER;

    extern const QString SUBJECT;
    extern const QString SUBJECT_ID;
    extern const QString SUBJECT_NAME;

    extern const QString COMMENT;
    extern const QString COMMENT_TEXT;
    extern const QString COMMENT_STATE;
    extern const QString MARK;

    extern const QString USER;
}

class DatabaseController {
public:
    enum class DataBaseType : qint8 {
        HISTORY = 0x00,
        SUBJECT_STATISTICS = 0x01
    };

    explicit DatabaseController(DataBaseType type);

    const DataBaseType& getType() const;

private:
    DataBaseType type_;
};

template <class E>
constexpr auto toInt(E enumerator) {
    return static_cast<std::underlying_type_t<E>>(enumerator);
}


class DataBaseControllerException : public std::exception {
    QString message_;
public:
    DataBaseControllerException(const QString& message);
    virtual const char* what()
    const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override final;
};

}

#endif // INITIALIZER_H
