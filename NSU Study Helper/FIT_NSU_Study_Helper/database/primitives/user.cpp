#include "user.h"
#include "../../network/host_socket.h"

namespace database {
namespace primitives {

using namespace network;

User::User(int id, const QString &nickname)
    : id_(id), nickname_(nickname) {}

User::User(const User &other)
    : id_(other.id_), nickname_(other.nickname_) {}

bool User::operator ==(const User &other) const {
    return id_ == other.id_;
}

bool User::operator !=(const User &other) const {
    return !(*this == other);
}

int User::getId() const {
    return id_;
}

const QString& User::getName() const {
    return nickname_;
}

QJsonObject User::toJson() const {
    return QJsonObject{};
}

}
}
