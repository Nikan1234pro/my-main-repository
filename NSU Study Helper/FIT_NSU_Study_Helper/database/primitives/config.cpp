#include "config.h"
#include <QSql>
#include <QSqlDatabase>

namespace database {

namespace domains {
    const QString YEAR          = "entry_year";
    const QString SEMESTER      = "semester";

    const QString SUBJECT       = "subject";
    const QString SUBJECT_ID    = "subject_id";
    const QString SUBJECT_NAME  = "subject_name";

    const QString COMMENT       = "comment";
    const QString COMMENT_TEXT  = "comment_text";
    const QString COMMENT_STATE = "comment_state";
    const QString MARK          = "mark";

    const QString USER          = "user";
}

DatabaseController::DatabaseController(DataBaseType type)
    : type_(type) {}

const DatabaseController::DataBaseType& DatabaseController::getType() const {
    return type_;
}


DataBaseControllerException::DataBaseControllerException(const QString& message)
    : message_(message) {}

const char* DataBaseControllerException::what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT {
    return message_.toUtf8();
}

}
