#ifndef SUBJECT_H
#define SUBJECT_H

#include <QString>
#include "serializable.h"

namespace database {
namespace primitives {

class Subject : public SerializableObject {
    int id_;
    int year_;
    int semester_;

    QString name_;
public:
    Subject(int id, int year, int semester, const QString &name);

    int getId() const;
    const QString& getName() const;

    int getYear() const;
    int getSemester() const;

    QJsonObject toJson() const override;
};
}
}

#endif // SUBJECT_H
