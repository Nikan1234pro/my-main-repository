#include "history_controller.h"
#include "primitives/config.h"
#include "settings.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QJsonArray>
#include <QJsonObject>
#include <QTcpSocket>

#include "json_parser.h"
#include "../network/server_access_manager.h"

namespace database {

using namespace primitives;

HistoryDatabaseController::HistoryDatabaseController()
    : DatabaseController(DataBaseType::HISTORY) {}


std::vector<int> HistoryDatabaseController::getYearList() const {
    QByteArray request;
    request.append(toInt(getType()));
    request.append(toInt(REQUEST_TYPE::GET_YEAR_LIST));
    request.append(static_cast<char>(0));

    auto document = network::ServerAccessManager::getInstance().getFromServer(request);
    return getNumberList(document, domains::YEAR);
}


std::vector<int> HistoryDatabaseController::getSemesterList(int year) const {
    QJsonDocument payload;
    QJsonObject object;
    object.insert(domains::YEAR, QJsonValue(year));
    payload.setObject(object);
    QByteArray payload_bytes = payload.toJson();

    QByteArray request;
    request.append(toInt(getType()));
    request.append(toInt(REQUEST_TYPE::GET_SEMESTER_LIST));
    request.append(static_cast<char>(1));
    request.append(network::intToBytes(payload_bytes.size()));
    request.append(payload_bytes);

    auto document = network::ServerAccessManager::getInstance().getFromServer(request);
    return getNumberList(document, domains::SEMESTER);
}


std::vector<Subject> HistoryDatabaseController::getSubjectList(int year,
                                                               int semester) const {
    QJsonDocument payload;
    QJsonObject object;
    object.insert(domains::YEAR, QJsonValue(year));
    object.insert(domains::SEMESTER, QJsonValue(semester));
    payload.setObject(object);
    QByteArray payload_bytes = payload.toJson();

    QByteArray request;
    request.append(toInt(getType()));
    request.append(toInt(REQUEST_TYPE::GET_SUBJECT_LIST));
    request.append(static_cast<char>(1));
    request.append(network::intToBytes(payload_bytes.size()));
    request.append(payload_bytes);

    auto document = network::ServerAccessManager::getInstance().getFromServer(request);
    std::vector<Subject> result;
    if (document.isNull())
        throw DataBaseControllerException(error_message);

    if (document.isArray()) {
        auto doc_array = document.array();
        for (auto it = doc_array.begin(); it != doc_array.end(); ++it) {
            auto opt = JsonParser<int, QString>::parse(
                        (*it).toObject(), {domains::SUBJECT_ID, domains::SUBJECT_NAME});
            if (opt == std::nullopt)
                throw  DataBaseControllerException(error_message);

            auto tuple = opt.value();
            result.emplace_back(std::get<0>(tuple), year, semester,
                                std::get<1>(tuple).simplified());
        }
    }
    else
        throw DataBaseControllerException(error_message);
    return result;
}

std::vector<int> HistoryDatabaseController::getNumberList(const QJsonDocument& document,
                                                          const QString& key) const {
    std::vector<int> result{};

    if (document.isNull())
        throw DataBaseControllerException(error_message);

    if (document.isArray()) {
        auto doc_array = document.array();
        for (auto it = doc_array.begin(); it != doc_array.end(); ++it) {
            auto opt = JsonParser<int>::parse((*it).toObject(), {key});
            if (opt == std::nullopt)
                throw DataBaseControllerException(error_message);
            result.push_back(std::get<0>(opt.value()));
        }
    }
    else
        throw DataBaseControllerException(error_message);
    return result;
}

std::vector<Teacher> HistoryDatabaseController::getTeacherList(const Subject& subject) const {
    return std::vector<Teacher>();
}

}






