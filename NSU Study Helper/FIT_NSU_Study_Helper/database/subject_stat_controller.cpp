#include "subject_stat_controller.h"
#include "../network/server_access_manager.h"
#include "../auth/authservice.h"
#include "primitives/config.h"
#include "json_parser.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QVariant>

namespace database {

using namespace primitives;

SubjectStatisticsController::SubjectStatisticsController(const Subject& subject)
    : DatabaseController(DataBaseType::SUBJECT_STATISTICS),
      subject_(subject) {}


SubjectStatisticsController::MarkList SubjectStatisticsController::getMarks() const {
    QJsonDocument doc;
    doc.setObject(subject_.toJson());
    QByteArray payload = doc.toJson();

    QByteArray request;
    request.append(toInt(getType()));
    request.append(toInt(REQUEST_TYPE::GET_MARK_LIST));
    request.append(static_cast<char>(1));
    request.append(network::intToBytes(payload.size()));
    request.append(payload);

    auto document = network::ServerAccessManager::getInstance().getFromServer(request);

    MarkList marks{};

    if (!document.isArray())
        throw DataBaseControllerException(error_message);

    auto doc_array = document.array();
    marks.reserve(static_cast<std::size_t>(doc_array.size()));

    for (auto it = doc_array.begin(); it != doc_array.end(); ++it) {
        auto opt = JsonParser<int, QString, int>::parse(
                    (*it).toObject(), {auth::ID, auth::LOGIN, domains::MARK});
        if (opt == std::nullopt)
            throw DataBaseControllerException(error_message);

        auto tuple = opt.value();

        marks.insert({User(std::get<0>(tuple),std::get<1>(tuple)),
                      std::get<2>(tuple)});
    }

    return marks;

}

SubjectStatisticsController::CommentList SubjectStatisticsController::getComments() const {
    QJsonDocument doc;
    doc.setObject(subject_.toJson());
    QByteArray payload = doc.toJson();

    QByteArray request;
    request.append(toInt(getType()));
    request.append(toInt(REQUEST_TYPE::GET_COMMENT_LIST));
    request.append(static_cast<char>(1));
    request.append(network::intToBytes(payload.size()));
    request.append(payload);

    auto document = network::ServerAccessManager::getInstance().getFromServer(request);
    CommentList list{};

    if (!document.isArray())
        throw DataBaseControllerException(error_message);

    auto doc_array = document.array();
    list.reserve(static_cast<std::size_t>(doc_array.size()));

    for (auto it = doc_array.begin(); it != doc_array.end(); ++it) {
        auto opt = JsonParser<int, QString, QString>::parse(
                    (*it).toObject(), {auth::ID, auth::LOGIN, domains::COMMENT_TEXT});
        if (opt == std::nullopt)
            throw DataBaseControllerException(error_message);

        auto tuple = opt.value();
        list.push_back(Comment(User(std::get<0>(tuple),
                                    std::get<1>(tuple)),
                               std::get<2>(tuple)));
    }
    return list;
}

bool SubjectStatisticsController::sendMark(int mark) {
    if (!auth::AuthService::getCurrentUser())
        return false;

    QJsonObject user = auth::AuthService::getCurrentUser()->toJson();
    QJsonObject subject = subject_.toJson();

    QJsonObject object;
    object.insert(domains::USER, user);
    object.insert(domains::SUBJECT, subject);
    object.insert(domains::MARK, mark);

    QJsonDocument payload;
    payload.setObject(object);
    auto payload_bytes = payload.toJson();

    QByteArray array;
    array.append(toInt(getType()));
    array.append(database::toInt(REQUEST_TYPE::SEND_MARK));
    array.append(0x01);
    array.append(network::intToBytes(payload_bytes.size()));
    array.append(payload_bytes);

    network::ServerAccessManager::getInstance().getFromServer(array);
    return true;
}

bool SubjectStatisticsController::sendComment(const QString& comment_text) {
    if (!auth::AuthService::getCurrentUser())
        return false;

    QJsonObject user = auth::AuthService::getCurrentUser()->toJson();
    QJsonObject comment;
    comment.insert(domains::SUBJECT, subject_.toJson());
    comment.insert(domains::COMMENT_TEXT, comment_text);

    QJsonObject payload_object;
    payload_object.insert(domains::USER, user);
    payload_object.insert(domains::COMMENT, comment);

    QJsonDocument payload;
    payload.setObject(payload_object);
    auto payload_bytes = payload.toJson();

    QByteArray array;
    array.append(toInt(getType()));
    array.append(database::toInt(REQUEST_TYPE::SEND_COMMENT));
    array.append(0x01);
    array.append(network::intToBytes(payload_bytes.size()));
    array.append(payload_bytes);

    auto result = network::ServerAccessManager::getInstance().getFromServer(array);
    return true;
}

qint8 SubjectStatisticsController::checkCommentState() const {
    if (!auth::AuthService::getCurrentUser())
        return false;

    QJsonObject user = auth::AuthService::getCurrentUser()->toJson();
    QJsonObject subject = subject_.toJson();

    QJsonObject payload_object;
    payload_object.insert(domains::USER, user);
    payload_object.insert(domains::SUBJECT, subject);

    QJsonDocument payload;
    payload.setObject(payload_object);
    auto payload_bytes = payload.toJson();

    QByteArray array;
    array.append(toInt(getType()));
    array.append(database::toInt(REQUEST_TYPE::GET_COMMENT_STATE));
    array.append(0x01);
    array.append(network::intToBytes(payload_bytes.size()));
    array.append(payload_bytes);

    auto result = network::ServerAccessManager::getInstance().getFromServer(array);

    if (!result.isObject())
        throw DataBaseControllerException(error_message);

    auto  opt = JsonParser<int>::parse(result.object(), { domains::COMMENT_STATE });
    if (opt == std::nullopt)
        throw DataBaseControllerException(error_message);

    return static_cast<qint8>(std::get<0>(opt.value()));
}
}













