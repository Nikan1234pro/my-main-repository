#ifndef STATISTICSDATABASECONTROLLER_H
#define STATISTICSDATABASECONTROLLER_H

#include "primitives/config.h"
#include "primitives/user.h"
#include "primitives/subject.h"
#include "primitives/comment.h"
#include <unordered_map>

namespace database {

class SubjectStatisticsController : public DatabaseController {
    primitives::Subject subject_;

    enum class REQUEST_TYPE : qint8 {
        GET_MARK_LIST = 0x00,
        SEND_MARK = 0x01,
        GET_COMMENT_LIST = 0x02,
        SEND_COMMENT = 0x03,
        GET_COMMENT_STATE = 0x04
    };

    const QString error_message = QString::fromUtf8(
                "Сервер отправил неверные данные...");
public:
    using MarkList = std::unordered_map<primitives::User, int>;
    using CommentList = std::vector<primitives::Comment>;

    explicit SubjectStatisticsController(const primitives::Subject &subject);

    MarkList getMarks() const;

    CommentList getComments() const;

    bool sendComment(const QString& comment_text);

    bool sendMark(int mark);

    qint8 checkCommentState() const;
};

}

#endif // STATISTICSDATABASECONTROLLER_H
