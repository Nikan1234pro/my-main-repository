#ifndef DATABASECONTROLLER_H
#define DATABASECONTROLLER_H

#include <QSql>
#include <QSqlDatabase>
#include <QJsonDocument>
#include <QDebug>
#include <vector>
#include <stdexcept>
#include <string>

#include "primitives/subject.h"
#include "primitives/teacher.h"
#include "primitives/config.h"

namespace database {

class HistoryDatabaseController : public DatabaseController {
    enum class REQUEST_TYPE : qint8 {
        GET_YEAR_LIST = 0x00,
        GET_SEMESTER_LIST = 0x01,
        GET_SUBJECT_LIST = 0x02
    };

    const QString error_message = QString::fromUtf8(
                "Сервер отправил неверные данные...");

    std::vector<int> getNumberList(const QJsonDocument& doc, const QString& key) const;
public:
    HistoryDatabaseController();
    std::vector<int> getYearList() const;
    std::vector<int> getSemesterList(int year) const;
    std::vector<primitives::Subject> getSubjectList(int year, int semester) const;
    std::vector<primitives::Teacher> getTeacherList(const primitives::Subject& subject) const;
};

}

#endif // DATABASECONTROLLER_H
