#ifndef SERVERACCESSMANAGER_H
#define SERVERACCESSMANAGER_H

#include "host_socket.h"

namespace network {

constexpr char server_ip[] = "127.0.0.1";
constexpr quint16 server_port = 5000;
constexpr quint16 host_port = 4096;

class ServerAccessManager {
    HostSocket socket;
public:
    ServerAccessManager();
    static ServerAccessManager& getInstance();
    QJsonDocument getFromServer(const QByteArray &request);
};

}

#endif // SERVERACCESSMANAGER_H
