#ifndef HOST_SOCKET_H
#define HOST_SOCKET_H
#include <QTcpSocket>
#include <QJsonDocument>

namespace network {

class ServerUnavailableException : public std::exception {
       std::string message_;
   public:
       ServerUnavailableException(const std::string& message);
       virtual const char* what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT;
};

class HostSocket : public QObject {
    Q_OBJECT

    static const int max_timeout = 10000u;

public:
    QTcpSocket socket;
    HostSocket();
    HostSocket(quint16 port);
    void bind(quint16 port);
    void connectToHost(const QString &ip, quint16 port);

    QByteArray readNBytes(int n);
    void write(const QByteArray &data);
    void disconnectFromHost();
};

QByteArray intToBytes(int number);

int bytesToInt(const QByteArray& bytes);

}

#endif
