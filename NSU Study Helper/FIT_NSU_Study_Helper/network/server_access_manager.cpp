#include "server_access_manager.h"

namespace network {

ServerAccessManager::ServerAccessManager() : socket(host_port) {}

ServerAccessManager& ServerAccessManager::getInstance() {
    static ServerAccessManager instance;
    return instance;
}

QJsonDocument ServerAccessManager::getFromServer(const QByteArray &request) {
    try {
        socket.connectToHost(server_ip, server_port);
        socket.write(request);

        QByteArray buffer{};
        buffer = socket.readNBytes(static_cast<int>(sizeof(int)));

        int doc_len = bytesToInt(buffer);

        buffer.clear();
        buffer = socket.readNBytes(doc_len);

        QJsonParseError error;
        QJsonDocument doc = QJsonDocument::fromJson(buffer, &error);
        if (error.errorString().toInt() != QJsonParseError::NoError)
            QJsonDocument();    //empty json file

        socket.disconnectFromHost();
        return doc;
    }
    catch (ServerUnavailableException &e) {
        socket.disconnectFromHost();
        throw e;
    }
}

}
