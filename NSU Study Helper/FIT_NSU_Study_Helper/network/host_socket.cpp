#include "host_socket.h"
#include <QString>
#include <QEventLoop>
#include <QTimer>

namespace network {

/* Big endian array */
QByteArray intToBytes(int number) {
    QByteArray array;
    array.append(static_cast<char>((number) >> 24));
    array.append(static_cast<char>((number) >> 16));
    array.append(static_cast<char>((number) >> 8));
    array.append(static_cast<char>((number) >> 0));
    return array;
}

int bytesToInt(const QByteArray& array)  {
    return static_cast<int>(
                (static_cast<uint>(array[0] << 24) & 0xFF000000) |
                (static_cast<uint>(array[1] << 16) & 0x00FF0000) |
                (static_cast<uint>(array[2] << 8)  & 0x0000FF00) |
                (static_cast<uint>(array[3] << 0)  & 0x000000FF));
}

ServerUnavailableException::ServerUnavailableException(const std::string& message)
    : message_(message) {
}

const char* ServerUnavailableException::what()
const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT {
    return message_.c_str();
}

HostSocket::HostSocket() = default;

HostSocket::HostSocket(quint16 port) {
    socket.bind(port);
}

void HostSocket::bind(quint16 port) {
    socket.bind(port);
}

void HostSocket::connectToHost(const QString &ip, quint16 port) {
    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);

    connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    connect(&socket, &QTcpSocket::connected, &loop, &QEventLoop::quit);
    socket.connectToHost(ip, port);

    timer.start(max_timeout);
    loop.exec();

    if(!timer.isActive()) {
        throw ServerUnavailableException(
                    "Сервер недоступен...");
    }
    timer.stop();
}

QByteArray HostSocket::readNBytes(int n) {
    QByteArray buffer;

    QEventLoop loop;
    QTimer timer;
    connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    connect(&socket, &QTcpSocket::readyRead, &loop, &QEventLoop::quit);

    while (buffer.size() != n) {
        if (socket.bytesAvailable() == 0) {
            timer.start(max_timeout);
            loop.exec();
            if(!timer.isActive()) {
                throw ServerUnavailableException(
                            "Сервер недоступен...");
            }
            timer.stop();
        }
        buffer.append(socket.read(n - buffer.size()));
    }
    return buffer;
}

void HostSocket::write(const QByteArray &data) {
    QEventLoop loop;
    QTimer timer;

    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    connect(&socket, &QTcpSocket::bytesWritten, &loop, &QEventLoop::quit);
    socket.write(data);

    timer.start(max_timeout);
    loop.exec();

    if(!timer.isActive()) {
        throw ServerUnavailableException(
                    "Сервер недоступен...");
    }
    timer.stop();
}

void HostSocket::disconnectFromHost() {
    socket.disconnectFromHost();
}

}
