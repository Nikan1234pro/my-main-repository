#ifndef AUTHSERVICE_H
#define AUTHSERVICE_H
#include <memory>
#include <QJsonDocument>

#include "authorizeduser.h"

namespace auth {

class AuthService {
    static std::unique_ptr<AuthorizedUser> current_user;
    QString last_error;

    static constexpr qint8 AUTH_SERVICE_CODE = 0x03;

    enum class COMMANDS : qint8 {
        LOGIN = 0x00,
        REGISTER = 0x01,
        SEND_PIN = 0x02
    };

    bool checkResult(const QJsonDocument &document);

public:
    AuthService() noexcept;
    static AuthorizedUser *getCurrentUser();

    bool login(const QString &nickname,
               const QString &password);

    bool registerUser(const QString &nickname,
                      const QString &password,
                      const QString &pin_code);

    bool sendPinCode(const QString &nickname);

    QString getLastError() const;
};

}

#endif // AUTHSERVICE_H
