#ifndef AUTHORIZEDUSER_H
#define AUTHORIZEDUSER_H


#include <QtGlobal>
#include "../database/primitives/user.h"
namespace auth {

extern const QString LOGIN;
extern const QString PASSWORD;
extern const QString ID;
extern const QString TOKEN;
extern const QString ADMIN;
extern const QString PINCODE;
extern const QString RESULT;
extern const QString ERROR_MESSAGE;

class AuthorizedUser : public database::primitives::User {
    int token_;
    bool is_admin_;
public:
    AuthorizedUser(int id, const QString &nickname,
                   int token, bool is_admin);
    QJsonObject toJson() const override;
    static AuthorizedUser formJson(const QJsonObject &object);
};

}
#endif // AUTHORIZEDUSER_H
