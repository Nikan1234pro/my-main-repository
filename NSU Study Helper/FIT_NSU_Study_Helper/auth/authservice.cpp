#include "authservice.h"
#include "../database/primitives/config.h"
#include "../network/server_access_manager.h"

#include <QJsonObject>

namespace auth {

AuthService::AuthService() noexcept {}

AuthorizedUser* AuthService::getCurrentUser() {
    return current_user.get();
}

bool AuthService::login(const QString &nickname,
                        const QString &password) {
    QJsonObject user_object;
    user_object.insert(LOGIN, nickname);
    user_object.insert(PASSWORD, password);

    QJsonObject payload_object;
    payload_object.insert(database::domains::USER, user_object);

    QJsonDocument payload;
    payload.setObject(payload_object);
    QByteArray payload_bytes = payload.toJson();

    QByteArray array;
    array.append(AUTH_SERVICE_CODE);
    array.append(database::toInt(COMMANDS::LOGIN));
    array.append(0x01);
    array.append(network::intToBytes(payload_bytes.size()));
    array.append(payload_bytes);

    QJsonDocument document = network::ServerAccessManager::getInstance().getFromServer(array);
    if (!checkResult(document))
        return false;

    current_user = std::make_unique<AuthorizedUser>(
                AuthorizedUser::formJson(document.object()
                                         [database::domains::USER].toObject()));
    return true;
}

bool AuthService::registerUser(const QString &nickname,
                               const QString &password,
                               const QString &pin_code) {
    QJsonObject user_object;
    user_object.insert(LOGIN, nickname);
    user_object.insert(PASSWORD, password);
    user_object.insert(PINCODE, pin_code);

    QJsonObject payload_object;
    payload_object.insert(database::domains::USER, user_object);

    QJsonDocument payload;
    payload.setObject(payload_object);
    QByteArray payload_bytes = payload.toJson();

    QByteArray array;
    array.append(AUTH_SERVICE_CODE);
    array.append(database::toInt(COMMANDS::REGISTER));
    array.append(0x01);
    array.append(network::intToBytes(payload_bytes.size()));
    array.append(payload_bytes);

    QJsonDocument document = network::ServerAccessManager::getInstance().getFromServer(array);
    return checkResult(document);
}

bool AuthService::sendPinCode(const QString &nickname) {
    QJsonObject user_object;
    user_object.insert(LOGIN, nickname);

    QJsonObject payload_object;
    payload_object.insert(database::domains::USER, user_object);

    QJsonDocument payload;
    payload.setObject(payload_object);
    QByteArray payload_bytes = payload.toJson();

    QByteArray array;
    array.append(AUTH_SERVICE_CODE);
    array.append(database::toInt(COMMANDS::SEND_PIN));
    array.append(0x01);
    array.append(network::intToBytes(payload_bytes.size()));
    array.append(payload_bytes);

    QJsonDocument document = network::ServerAccessManager::getInstance().getFromServer(array);
    return checkResult(document);
}

bool AuthService::checkResult(const QJsonDocument &document) {
    if (!document.isObject())
        return false;

    QJsonObject answer_object = document.object();
    if (!answer_object[RESULT].toBool()) {
        last_error = answer_object[ERROR_MESSAGE].toString();
        return false;
    }
    return true;
}

QString AuthService::getLastError() const {
    return last_error;
}


std::unique_ptr<AuthorizedUser> AuthService::current_user;

}


