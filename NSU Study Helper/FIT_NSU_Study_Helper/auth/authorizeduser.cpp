#include "authorizeduser.h"
#include "../database/primitives/config.h"

#include <QJsonObject>

namespace auth {

const QString LOGIN         = "user_login";
const QString PASSWORD      = "user_pass";
const QString ID            = "user_id";
const QString TOKEN         = "user_token";
const QString ADMIN         = "is_admin";
const QString PINCODE       = "pin_code";
const QString RESULT        = "result";
const QString ERROR_MESSAGE = "error_message";

AuthorizedUser::AuthorizedUser(int id, const QString &nickname, int token, bool is_admin)
    : database::primitives::User(id, nickname), token_(token), is_admin_(is_admin) {
}

QJsonObject AuthorizedUser::toJson() const {
    QJsonObject object;
    object.insert(ID, getId());
    object.insert(LOGIN, getName());
    object.insert(TOKEN, token_);
    return object;
}

AuthorizedUser AuthorizedUser::formJson(const QJsonObject &object) {
    return AuthorizedUser(object.find(ID)->toInt(),
                          object.find(LOGIN)->toString(),
                          object.find(TOKEN)->toInt(),
                          object.find(ADMIN)->toBool());
}

}
