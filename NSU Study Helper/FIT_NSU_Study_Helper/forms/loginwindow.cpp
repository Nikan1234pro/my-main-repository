#include "loginwindow.h"
#include "ui_loginwindow.h"
#include "../settings.h"
#include "../auth/authservice.h"
#include "../network/host_socket.h"
#include "registerwindow.h"

#include <QDebug>
#include <QMessageBox>

LoginWindow::LoginWindow(WindowManager *window_manager,
                         QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginWindow),
    window_manager_(window_manager) {

    ui->setupUi(this);
    ui->error->clear();
    ui->password->setEchoMode(QLineEdit::Password);
}

LoginWindow::~LoginWindow() {
    delete ui;
}

void LoginWindow::on_exitButton_clicked() {
    emit firstWindow();
}

void LoginWindow::on_loginButton_clicked() {
    authorize(ui->login->text(),
                     ui->password->text());
}

void LoginWindow::on_registerButton_clicked() {
    auto* register_window = new RegisterWindow(window_manager_);
    connect(register_window, &RegisterWindow::firstWindow,
            this, &LoginWindow::showThis);
    connect(register_window, &RegisterWindow::authorize,
            this, &LoginWindow::showThisAndAuth);

    window_manager_->nextScene(register_window);
}

void LoginWindow::showThis() {
    window_manager_->prevScene();
}

void LoginWindow::showThisAndAuth(const QString &login,
                                  const QString &password) {
    window_manager_->prevScene();
    authorize(login, password);
}

void LoginWindow::authorize(const QString &login,
                            const QString &password) {
    ui->error->clear();
    try {
        auth::AuthService auth;
        if (!auth.login(login, password)) {
            ui->error->setText(error_msg);
            ui->password->clear();
            return;
        }
        emit firstWindow();
    }
    catch(network::ServerUnavailableException &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}


