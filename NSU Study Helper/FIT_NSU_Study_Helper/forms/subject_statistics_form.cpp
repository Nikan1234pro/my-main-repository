#include "subject_statistics_form.h"
#include "ui_subject_statistics_form.h"
#include "../settings.h"

#include <QDebug>

using namespace statistics;
using namespace database;
using namespace database::primitives;

SubjectStatisticsForm::SubjectStatisticsForm(WindowManager *window_manager,
                                             const SubjectStatistics& statistics,
                                             QWidget *parent)
    : QDialog(parent),
      ui(new Ui::SubjectStatisticsForm),
      window_manager_(window_manager),
      subject_statistics(statistics) {

    ui->setupUi(this);

    auto subject = subject_statistics.getSubject();
    ui->label->setText(subject.getName() +
                      QString::fromUtf8("\n Год поступления: ") + QString::number(subject.getYear()) +
                      QString::fromUtf8(".\t Семестр: ") + QString::number(subject.getSemester()) + ".");

    /* Diagram */
    QRect subject_ui_geometry(SPACE, ui->label->height(),
                              SUBJECT_UI_WIDTH, SUBJECT_UI_HEIGHT);
    subject_ui = std::make_unique<SubjectStatisticsUI>(ui->frame, &subject_statistics);
    subject_ui->setGeometry(subject_ui_geometry);
    subject_ui->display();

    /* Comments list */
    QRect comments_ui_geometry(QPoint(subject_ui_geometry.right() + COLUMN_WIDTH,
                                      subject_ui_geometry.y()),
                               QPoint(geometry().width() - SPACE,
                                      geometry().height() - SPACE));
    comments = std::make_unique<CommentsViewer>(ui->frame, &subject_statistics);
    comments->setGeometry(comments_ui_geometry);
    comments->display();
}

SubjectStatisticsForm::~SubjectStatisticsForm() {
    delete ui;
}

void SubjectStatisticsForm::on_exitButton_clicked() {
    emit firstWindow();
}
