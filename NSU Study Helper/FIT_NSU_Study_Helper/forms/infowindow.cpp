#include "infowindow.h"
#include "ui_infowindow.h"
#include "../settings.h"

#include "../database/subject_stat_controller.h"
#include "../network/host_socket.h"

InfoWindow::InfoWindow(WindowManager *window_manager,
                       QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoWindow),
    window_manager_(window_manager) {

    ui->setupUi(this);
}

InfoWindow::~InfoWindow() {
    delete ui;
}

void InfoWindow::on_exitButton_clicked() {
    emit firstWindow();
}
