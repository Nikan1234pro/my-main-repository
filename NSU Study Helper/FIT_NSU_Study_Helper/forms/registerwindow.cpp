#include "registerwindow.h"
#include "ui_registerwindow.h"
#include "../network/host_socket.h"
#include "../settings.h"

#include <QMessageBox>


RegisterWindow::RegisterWindow(WindowManager *window_manager,
                               QWidget *parent) :
    QDialog(parent),
    window_manager_(window_manager),
    ui(new Ui::RegisterWindow) {
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    ui->password->setEchoMode(QLineEdit::Password);
}

RegisterWindow::~RegisterWindow() {
    delete ui;
}

void RegisterWindow::on_exitButton_clicked() {
    emit firstWindow();
}

void RegisterWindow::on_sendPinCodeButton_clicked() {
    try {
        ui->emailError->clear();

        email = ui->email->text();
        password = ui->password->text();

        if (!auth.sendPinCode(email)) {
            ui->emailError->setText(auth.getLastError());
            return;
        }
        auto *w = ui->stackedWidget;
        w->setCurrentIndex(w->currentIndex() + 1);
    }
    catch(network::ServerUnavailableException &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}

void RegisterWindow::on_registerButton_clicked() {
    try {
        if (!auth.registerUser(email, password, ui->pinCode->text())) {
            ui->registerError->setText(auth.getLastError());
            return;
        }
        auto *w = ui->stackedWidget;
        w->setCurrentIndex(w->currentIndex() + 1);
    }
    catch(network::ServerUnavailableException &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}


void RegisterWindow::on_startButton_clicked() {
    emit authorize(email, password);
}
