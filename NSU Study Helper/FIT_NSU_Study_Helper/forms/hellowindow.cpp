#include "hellowindow.h"
#include "ui_hellowindow.h"

#include "loginwindow.h"
#include "infowindow.h"
#include "entrancewindow.h"

#include "../auth/authservice.h"
#include "../network/host_socket.h"
#include "../settings.h"

#include <QMessageBox>

HelloWindow::HelloWindow(WindowManager *window_manager,
                         QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelloWindow),
    window_manager_(window_manager) {
    ui->setupUi(this);

    addAnnouncement();
}

HelloWindow::~HelloWindow() {
    delete ui;
}

void HelloWindow::on_login_button_clicked() {
    auto* login_window = new LoginWindow(window_manager_);
    connect(login_window, &LoginWindow::firstWindow,
            this, &HelloWindow::showThis);

    window_manager_->nextScene(login_window);
}

void HelloWindow::on_entrance_button_clicked() {
    try {
        auto* entrance_window = new EntranceWindow(window_manager_);
        connect(entrance_window, &EntranceWindow::firstWindow,
                this, &HelloWindow::showThis);

        window_manager_->nextScene(entrance_window);
    }
    catch(network::ServerUnavailableException &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}

void HelloWindow::on_info_button_clicked() {
    auto* info_window = new InfoWindow(window_manager_);
    connect(info_window, &InfoWindow::firstWindow,
            this, &HelloWindow::showThis);

    window_manager_->nextScene(info_window);
}

void HelloWindow::showThis() {
    addAnnouncement();
    window_manager_->prevScene();
}

void HelloWindow::addAnnouncement() {
    if (auth::AuthService::getCurrentUser())
        ui->user->setText(QString::fromUtf8("Добро пожаловать, ") +
                          auth::AuthService::getCurrentUser()->getName());
    else
        ui->user->clear();
}
