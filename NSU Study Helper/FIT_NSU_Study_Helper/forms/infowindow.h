#ifndef INFOWINDOW_H
#define INFOWINDOW_H

#include <QDialog>
#include "window_manager.h"

namespace Ui {
class InfoWindow;
}

class InfoWindow : public QDialog {
    Q_OBJECT

public:
    explicit InfoWindow(WindowManager *window_manager,
                        QWidget *parent = nullptr);
    ~InfoWindow();

signals:
    void firstWindow();

private slots:
    void on_exitButton_clicked();

private:
    Ui::InfoWindow *ui;
    WindowManager *window_manager_;
};

#endif // INFOWINDOW_H
