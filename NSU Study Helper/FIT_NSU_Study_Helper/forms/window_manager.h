#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

#include <QStackedWidget>
#include <QWidget>
#include <stack>

class WindowManager {
    QStackedWidget *window_manager;
    std::stack<QWidget*> widgets;
public:
    WindowManager(QWidget *parent);
    void nextScene(QWidget *scene);
    void update();
    void prevScene();
};

#endif // WINDOWMANAGER_H
