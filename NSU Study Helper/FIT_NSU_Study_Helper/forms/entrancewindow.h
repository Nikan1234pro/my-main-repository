#ifndef ENTRANCEWINDOW_H
#define ENTRANCEWINDOW_H

#include <memory>
#include <QDialog>
#include <QStackedWidget>
#include "../database/primitives/subject.h"
#include "window_manager.h"

namespace Ui {
class EntranceWindow;
}

class EntranceWindow : public QDialog {
    Q_OBJECT

public:
    explicit EntranceWindow(WindowManager *window_manager,
                            QWidget *parent = nullptr);
    ~EntranceWindow();

signals:
    void firstWindow();

private slots:
    void on_exitButton_clicked();

    void on_yearComboBox_activated(const QString&);
    void on_semesterComboBox_activated(const QString&);
    void on_subjectComboBox_activated(const QString&);

    void on_nextButton_clicked();

    void showThis();

private:
    Ui::EntranceWindow *ui;
    std::vector<database::primitives::Subject> subjects_;

    WindowManager *window_manager_;

    const QString select_year_title     = QString::fromUtf8("Выберите год");
    const QString select_semester_title = QString::fromUtf8("Выберите семестр");
    const QString select_subject_title  = QString::fromUtf8("Выберите предмет");
};

#endif // ENTRANCEWINDOW_H
