#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QStackedWidget>
#include <QDialog>
#include "window_manager.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QDialog {
    Q_OBJECT

public:
    explicit LoginWindow(WindowManager *window_manager,
                         QWidget *parent = nullptr);
    ~LoginWindow();

signals:
    void firstWindow();

private slots:
    void on_exitButton_clicked();

    void on_loginButton_clicked();

    void on_registerButton_clicked();

private slots:
    void showThis();

    void showThisAndAuth(const QString &login,
                         const QString &password);

private:
    void authorize(const QString &login,
                   const QString &password);

    Ui::LoginWindow *ui;
    WindowManager *window_manager_;

    const QString error_msg = QString::fromUtf8(
                "Неверный логин или пароль");
};

#endif // LOGINWINDOW_H
