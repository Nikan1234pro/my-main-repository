#include <QtDebug>
#include <QtSql>
#include <QLayout>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hellowindow.h"
#include "settings.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow) {

    ui->setupUi(this);
    window_manager = new WindowManager(this);
    window_manager->nextScene(new HelloWindow(window_manager));
}

MainWindow::~MainWindow() {
    delete ui;
}
