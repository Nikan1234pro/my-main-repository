#include "window_manager.h"
#include "../settings.h"

#include <QDebug>

WindowManager::WindowManager(QWidget *parent)
    : window_manager(new QStackedWidget(parent)) {

    window_manager->setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT);
}

void WindowManager::nextScene(QWidget *scene) {
    widgets.push(scene);

    window_manager->addWidget(scene);

    window_manager->setCurrentIndex(
                static_cast<int>(widgets.size() - 1));
    window_manager->show();
}

void WindowManager::update() {
    window_manager->show();
}

void WindowManager::prevScene() {
    window_manager->removeWidget(widgets.top());
    widgets.pop();
}
