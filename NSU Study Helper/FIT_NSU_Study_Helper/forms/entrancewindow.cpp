#include "entrancewindow.h"
#include "ui_entrancewindow.h"
#include "../database/primitives/config.h"
#include "../database/history_controller.h"
#include "../settings.h"
#include "../forms/subject_statistics_form.h"

#include <iostream>
#include <QString>
#include <QDebug>
#include <QMessageBox>

using namespace database;

EntranceWindow::EntranceWindow(WindowManager *window_manager,
                               QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntranceWindow),
    window_manager_(window_manager) {

    ui->setupUi(this);

    std::vector<int> years{};

    /* May throw exception */
    years = HistoryDatabaseController{}.getYearList();

    ui->yearComboBox->addItem(select_year_title);
    for (auto& year : years)
        ui->yearComboBox->addItem(QString::number(year));
}


EntranceWindow::~EntranceWindow() {
    delete ui;
}


void EntranceWindow::on_exitButton_clicked() {
    emit firstWindow();
}


void EntranceWindow::on_yearComboBox_activated(const QString&) {
    ui->semesterComboBox->clear();
    ui->semesterComboBox->addItem(select_year_title);

    ui->subjectComboBox->clear();
    subjects_.clear();

    std::vector<int> semesters{};
    HistoryDatabaseController controller{};
    try {
        semesters = controller.getSemesterList(
                    ui->yearComboBox->currentText().toInt());
    }
    catch(std::exception &e) {
        QMessageBox::critical(this, error_header, e.what());
        return;
    }
    for (auto& semester : semesters)
        ui->semesterComboBox->addItem(QString::number(semester));
}


void EntranceWindow::on_semesterComboBox_activated(const QString&) {
    ui->subjectComboBox->clear();
    ui->subjectComboBox->addItem(select_semester_title);
    subjects_.clear();

    HistoryDatabaseController controller{};
    try {
        subjects_ = controller.getSubjectList(
                    ui->yearComboBox->currentText().toInt(),
                    ui->semesterComboBox->currentText().toInt());
    }
    catch(std::exception &e) {
        QMessageBox::critical(this, error_header, e.what());
        return;
    }
    for (auto& subject : subjects_)
        ui->subjectComboBox->addItem(subject.getName());
}


void EntranceWindow::on_subjectComboBox_activated(const QString &) {}


void EntranceWindow::on_nextButton_clicked() {
    int index = ui->subjectComboBox->currentIndex() - 1;
    if (index < 0) {
        return;
    }

    database::primitives::Subject subject = subjects_.at(static_cast<std::size_t>(index));

    try {
        statistics::SubjectStatistics statistics(subject);

        auto *subject_window = new SubjectStatisticsForm(
                    window_manager_, statistics);
        connect(subject_window, &SubjectStatisticsForm::firstWindow,
                this, &EntranceWindow::showThis);

        window_manager_->nextScene(subject_window);
    }
    catch(std::exception &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}

void EntranceWindow::showThis() {
    window_manager_->prevScene();
}
