#ifndef SUBJECTSTATISTICSFORM_H
#define SUBJECTSTATISTICSFORM_H

#include <QDialog>
#include <memory>

#include "window_manager.h"
#include "../statistics/subject_statistics_ui.h"
#include "../statistics/commentsviewer.h"

namespace Ui {
class SubjectStatisticsForm;
}

class SubjectStatisticsForm : public QDialog {
    Q_OBJECT

public:
    explicit SubjectStatisticsForm(WindowManager *window_manager,
                                   const statistics::SubjectStatistics& statistics,
                                   QWidget *parent = nullptr);
    ~SubjectStatisticsForm();

signals:
    void firstWindow();

private slots:
    void on_exitButton_clicked();

private:
    Ui::SubjectStatisticsForm *ui;
    WindowManager *window_manager_;

    statistics::SubjectStatistics subject_statistics;
    std::unique_ptr<statistics::SubjectStatisticsUI> subject_ui;
    std::unique_ptr<statistics::CommentsViewer> comments;

    constexpr static int SPACE = 10;
    constexpr static int COLUMN_WIDTH = 40;
    constexpr static int SUBJECT_UI_WIDTH = 300;
    constexpr static int SUBJECT_UI_HEIGHT = 300;

    constexpr static int COMMENT_LIST_HEIGHT = 350;

    constexpr static int EXIT_BUTTON_WIDTH = 130;
    constexpr static int EXIT_BUTTON_HEIGHT = 50;
};

#endif // SUBJECTSTATISTICSFORM_H
