#ifndef REGISTERWINDOW_H
#define REGISTERWINDOW_H

#include <QDialog>
#include "window_manager.h"
#include "../auth/authservice.h"

namespace Ui {
class RegisterWindow;
}

class RegisterWindow : public QDialog
{
    Q_OBJECT

public:
    explicit RegisterWindow(WindowManager *window_manager,
                            QWidget *parent = nullptr);
    ~RegisterWindow();

signals:
    void firstWindow();
    void authorize(const QString &login,
                   const QString &password);

private slots:
    void on_exitButton_clicked();

    void on_sendPinCodeButton_clicked();

    void on_registerButton_clicked();

    void on_startButton_clicked();

private:
    QString email;
    QString password;

    auth::AuthService auth;
    WindowManager *window_manager_;
    Ui::RegisterWindow *ui;
};

#endif // REGISTERWINDOW_H
