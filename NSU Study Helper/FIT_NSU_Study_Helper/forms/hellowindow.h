#ifndef HELLOWINDOW_H
#define HELLOWINDOW_H

#include <QDialog>
#include <QStackedWidget>
#include "window_manager.h"

namespace Ui {
class HelloWindow;
}

class HelloWindow : public QDialog
{
    Q_OBJECT

public:
    explicit HelloWindow(WindowManager *window_manager,
                         QWidget *parent = nullptr);
    ~HelloWindow();

private slots:
    void on_login_button_clicked();

    void on_entrance_button_clicked();

    void on_info_button_clicked();

private:
    void showThis();
    void addAnnouncement();

    Ui::HelloWindow *ui;
    WindowManager  *window_manager_;
};

#endif // HELLOWINDOW_H
