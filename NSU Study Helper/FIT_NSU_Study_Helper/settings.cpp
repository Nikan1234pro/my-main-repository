#include "settings.h"

const QString scrollbar_style = "QScrollBar:vertical {"
                                "    border: 0px solid #999999;"
                                "    background:transparent;"
                                "    width:0px;    "
                                "    margin: 0px 0px 0px 0px;"
                                "}";

const QString area_style      = "QScrollArea { "
                                "background-color:transparent;"
                                "border: none; }";

const QString box_style       = "QGroupBox{ "
                                "background-color:transparent; "
                                "border: none; }";

const QString subtitle_style  = "*{background-color : transparent; "
                                "color : rgb(231, 146, 89); border-style: solid; "
                                "border-width: 1px; border-color: transparent;"
                                "font-weight: bold; }";

const QString title_style     = "*{background-color : transparent; "
                                "color : rgb(208, 208, 208); border-style: solid; "
                                "border-width: 5px; border-color: transparent;}";

const QString text_style      = "*{background-color : transparent; "
                                "color : rgb(208, 208, 208); border-style: solid; "
                                "border-width: 1px; border-color: transparent;}";

const QString linked_text_out = "*{background-color : transparent; "
                                "color : rgb(231, 146, 89); border-style: solid;}";

const QString linked_text_in  = "*{background-color : transparent; "
                                "color : rgb(12, 112, 255); border-style: solid;}";

const QString button_style    = "QPushButton{background-color : rgb(231, 146, 89);"
                                "color : white; border:none; }";

const QString text_edit_style = "*{background-color : transparent;"
                                "color : rgb(208, 208, 208);"
                                "border: 1px solid;"
                                "border-color : rgb(231, 146, 89);"
                                "}";

const QString text_default_font = "Century Gothic";
const QString button_default_font = "Century Gothic";

const QString error_header = QString::fromUtf8("Ошибка");
