#ifndef COMMENTED_OBJECT_H
#define COMMENTED_OBJECT_H

#include "../../database/primitives/comment.h"
#include "observable.h"
#include <vector>

class CommentedObject : public Observable {
protected:
    using CommentList = std::vector<database::primitives::Comment>;
public:
    enum CommentState : qint8 {
        NOT_FOUND = 0x00,
        ON_MODERATION = 0x01,
        PUBLISHED = 0x02
    };

    CommentedObject() = default;
    virtual ~CommentedObject() = default;

    virtual const CommentList& getComments() const = 0;
    virtual bool sendComment(const QString &comment_text) = 0;
    virtual CommentState getCommentState() const = 0;
};

#endif // COMMENTED_OBJECT_H
