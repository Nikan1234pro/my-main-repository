#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include "../observer.h"

class Observable {
public:
    virtual void addObserver(Observer *o) = 0;

    virtual ~Observable() = default;
};

#endif // OBSERVABLE_H
