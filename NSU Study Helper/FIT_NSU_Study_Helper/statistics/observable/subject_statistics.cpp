#include "subject_statistics.h"
#include "../../auth/authservice.h"
#include "../../network/host_socket.h"
#include "../../settings.h"

#include "QMessageBox"

namespace statistics {

using namespace database;
using namespace database::primitives;

SubjectStatistics::SubjectStatistics(const Subject& subject)
    : subject_(subject),
      statistics_controller(subject) {
    update();
}

void SubjectStatistics::addObserver(Observer *observer) {
    observers.push_back(observer);
}

bool SubjectStatistics::sendComment(const QString &comment_text) {
    if (!statistics_controller.sendComment(comment_text))
        return false;

    update();
    return true;
}

const CommentedObject::CommentList& SubjectStatistics::getComments() const {
    return comments_;
}

bool SubjectStatistics::sendMark(int mark) {
    if (!statistics_controller.sendMark(mark))
        return false;

    /* Update info from server */
    update();
    return true;
}

CommentedObject::CommentState SubjectStatistics::getCommentState() const {
    return comment_state;
}

const SubjectStatistics::MarksList& SubjectStatistics::getMarks() const {
    return marks_;
}

void SubjectStatistics::update() {
    marks_ = statistics_controller.getMarks();
    comments_ = statistics_controller.getComments();
    comment_state = static_cast<CommentState>(
                statistics_controller.checkCommentState());

    for (auto *o : observers)
        o->notify();
}

const database::primitives::Subject& SubjectStatistics::getSubject() const {
    return subject_;
}

}
