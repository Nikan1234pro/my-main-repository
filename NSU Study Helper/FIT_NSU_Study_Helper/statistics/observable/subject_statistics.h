#ifndef SUBJECTSTATISTICS_H
#define SUBJECTSTATISTICS_H

#include "../../database/subject_stat_controller.h"
#include "../../database/primitives/comment.h"
#include "../../database/primitives/subject.h"
#include "../../database/primitives/user.h"
#include "../observable/commented_object.h"

#include <unordered_map>
#include <vector>

namespace statistics {

class SubjectStatistics : public CommentedObject {
    using MarksList = std::unordered_map<database::primitives::User, int>;
    MarksList marks_;

    database::primitives::Subject subject_;
    database::SubjectStatisticsController statistics_controller;

    std::vector<Observer *> observers;

    CommentedObject::CommentList comments_;
    CommentState comment_state;
public:
    SubjectStatistics(const database::primitives::Subject& subject);
    void addObserver(Observer* observer) override;

    bool sendComment(const QString &comment_text) override;
    const CommentedObject::CommentList& getComments() const override;
    CommentState getCommentState() const override;

    bool sendMark(int mark);
    const MarksList& getMarks() const;
    void update();

    const database::primitives::Subject& getSubject() const;
};

}

#endif // SUBJECTSTATISTICS_H
