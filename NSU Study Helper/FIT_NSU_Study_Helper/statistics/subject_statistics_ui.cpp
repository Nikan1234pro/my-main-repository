#include <algorithm>
#include <QDebug>
#include <QMessageBox>
#include <cmath>

#include "subject_statistics_ui.h"
#include "labels/data_clickable_label.h"
#include "labels/colorized_clickable_label.h"
#include "dialogs/statistics_dialog.h"
#include "dialogs/choose_mark_dialog.h"
#include "../network/host_socket.h"
#include "../settings.h"
#include "../auth/authservice.h"

namespace statistics {

using namespace database::primitives;

SubjectStatisticsUI::SubjectStatisticsUI(QWidget *parent,
                                         SubjectStatistics *observarable)
    : observarable_(observarable), parent_(parent) {

    observarable_->addObserver(this);

    schema_layout = new QVBoxLayout();
    schema_layout->setMargin(0);

    box = new QGroupBox(parent_);
    box->setLayout(schema_layout);
    box->setStyleSheet(box_style);

    for (std::size_t i = 0; i < std::size(mark_types); ++i)
        mark_to_index[mark_types[i]] = i;
}

void SubjectStatisticsUI::setGeometry(const QRect& rect) {
    box->setGeometry(rect);
}

void SubjectStatisticsUI::notify() {
    display();
}


void SubjectStatisticsUI::prepare() {
    /* Do clean up */
    qDeleteAll(box->findChildren<QLabel*>());
    qDeleteAll(box->findChildren<QGroupBox*>());

    std::for_each(users.begin(), users.end(),
                  [](auto& v) -> void { v.clear(); }
    );

    std::for_each(
        observarable_->getMarks().begin(),
        observarable_->getMarks().end(),
        [&](auto& value) -> void {
            auto it = mark_to_index.find(value.second);
            if (it != mark_to_index.end())
                users[(*it).second].push_back(value.first);
    });
}


void SubjectStatisticsUI::addHeader() {
    /* Header */
    QLabel *title = new QLabel(title_text);
    title->setStyleSheet(title_style);
    title->setMinimumHeight(TITLE_HEIGTH);
    title->setMaximumHeight(TITLE_HEIGTH);

    QFont title_font(text_default_font, title_size, QFont::Thin);
    title_font.setItalic(true);
    title->setFont(title_font);

    schema_layout->addWidget(title);
}


void SubjectStatisticsUI::addDiagram(int user_mark) {
    /* Paint diagram */
    std::size_t marks_count = observarable_->getMarks().size();
    int column_max_width = box->width() - PERCENT_DATA_WIDTH;
    double ratio = (marks_count) ?
                   static_cast<double>(column_max_width) / marks_count : 0.0;

    /* Loop through columns */
    for (std::size_t i = 0; i < std::size(mark_types); ++i) {
        int mark = mark_types[i];
        QHBoxLayout *bar_layout = new QHBoxLayout;
        bar_layout->setSpacing(0);

        int bar_width = static_cast<int>(ratio * users[i].size());
        auto *bar = new DataClickableLabel<int>(mark_types[i]);
        connect(bar, &DataClickableLabel<int>::clicked,
                this, &SubjectStatisticsUI::onDiagramBarClicked);

        bar->setMaximumWidth(bar_width);
        bar->setMinimumWidth(bar_width);
        if (user_mark == mark)
            bar->setStyleSheet(chosen_column_style);
        else
            bar->setStyleSheet(column_style);

        bar->setText(QString::number(mark));
        bar_layout->addWidget(bar);

        auto *space = new QLabel();
        space->setMaximumWidth(column_max_width - bar_width);
        space->setMinimumWidth(column_max_width - bar_width);
        space->setStyleSheet(space_style);
        bar_layout->addWidget(space);

        auto *percent = new QLabel(QString::number(std::round((marks_count) ?
                                   100.0 * users[i].size() / marks_count :
                                   0.0)) + "%");

        percent->setMaximumWidth(PERCENT_DATA_WIDTH);
        percent->setMinimumWidth(PERCENT_DATA_WIDTH);
        percent->setStyleSheet(subtitle_style);
        percent->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
        bar_layout->addWidget(percent);

        schema_layout->addLayout(bar_layout);
        schema_layout->setSpacing(SPACE_WIDTN);
    }
}


void SubjectStatisticsUI::addMarkField(int user_mark) {
    /* Mark input */
    int column_max_width = box->width() - PERCENT_DATA_WIDTH;

    QFont text_font(text_default_font, text_size, QFont::Thin);
    text_font.setItalic(true);
    text_font.setBold(true);

    QHBoxLayout *mark_layout = new QHBoxLayout();
    mark_layout->setMargin(0);
    mark_layout->setSpacing(0);

    QGroupBox *mark_box = new QGroupBox();
    mark_box->setMaximumWidth(column_max_width);

    QLabel *mark_label = new QLabel(mark_text);
    mark_label->setWordWrap(true);
    mark_label->setStyleSheet(title_style);
    mark_label->setFont(text_font);
    mark_layout->addWidget(mark_label);


    QString user_mark_text = (user_mark != MARK_NONE) ?
                              QString::number(user_mark) : mark_none;

    ColorizedClickableLabel *mark_edit =
            new ColorizedClickableLabel(user_mark_text);

    connect(mark_edit, &ColorizedClickableLabel::clicked,
            this, &SubjectStatisticsUI::onMarkChangeClicked);

    text_font.setBold(true);
    mark_edit->setWordWrap(true);
    mark_edit->setAlignment(Qt::AlignCenter);
    mark_edit->setFont(text_font);
    mark_layout->addWidget(mark_edit);

    mark_box->setLayout(mark_layout);
    schema_layout->addWidget(mark_box);
}


void SubjectStatisticsUI::addTeachers() {
    QFont text_font(text_default_font, text_size, QFont::Thin);
    text_font.setItalic(true);
    text_font.setBold(true);

    /* Teachers */
    ColorizedClickableLabel *teachers = new ColorizedClickableLabel(teachers_text);
    teachers->setFont(text_font);

    QGroupBox   *teachers_box = new QGroupBox();
    QHBoxLayout *teachers_layout = new QHBoxLayout();
    teachers_layout->addWidget(teachers);
    teachers_box->setLayout(teachers_layout);

    schema_layout->addWidget(teachers_box);
}

void SubjectStatisticsUI::display() {    
    prepare();

    int user_mark = MARK_NONE;
    if (auth::AuthService::getCurrentUser()) {
        auto it = observarable_->getMarks().find(
                    *auth::AuthService::getCurrentUser());

        if (it != observarable_->getMarks().end())
            user_mark = it->second;
    }

    addHeader();
    addDiagram(user_mark);
    addMarkField(user_mark);
    addTeachers();
}

SubjectStatisticsUI::~SubjectStatisticsUI() = default;


void SubjectStatisticsUI::onDiagramBarClicked() {
    auto *label = dynamic_cast<DataClickableLabel<int>*>(sender());
    if (label == nullptr)
        return;

    int mark = label->getData();

    std::vector<QString> items;
    for(auto u : users[mark_to_index[mark]])
        items.push_back(u.getName());

    QString headline = stat_dialog_title + "\t" +
                       QString::number(mark);

    StatDialog *dialog = new StatDialog(parent_, headline, items);
    dialog->setModal(true);
    dialog->exec();
}

void SubjectStatisticsUI::onMarkChangeClicked() {
    if (!auth::AuthService::getCurrentUser()) {
        QMessageBox::warning(nullptr, error_header, warning_message);
    }
    else {
        ChooseMarkDialog *dialog = new ChooseMarkDialog(parent_);
        connect(dialog, &ChooseMarkDialog::sendMark,
                this, &SubjectStatisticsUI::getAndSendMark);
        dialog->setModal(true);
        dialog->exec();
    }
    display();
}

QRect SubjectStatisticsUI::getGeometry() const {
    return box->geometry();
}

void SubjectStatisticsUI::getAndSendMark(int mark) {
    try {
        observarable_->sendMark(mark);
    }
    catch (network::ServerUnavailableException &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}

}









