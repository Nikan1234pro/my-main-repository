#include "statistics_dialog.h"
#include "ui_statistics_dialog.h"
#include "../settings.h"

#include <QScrollBar>


StatDialog::StatDialog(QWidget *parent,
                       const QString &headline,
                       const std::vector<QString> &items) :
    QDialog(parent),
    ui(new Ui::StatDialog) {

    ui->setupUi(this);
    setFixedSize(DIALOG_WIDTH, DIALOG_HEIGHT);

    QGroupBox *screen = new QGroupBox(this);
    screen->setGeometry(0, 0 , DIALOG_WIDTH, DIALOG_HEIGHT - BAR_HEIGHT);
    screen->setStyleSheet(box_style);

    QVBoxLayout *layout = new QVBoxLayout();
    screen->setLayout(layout);

    QLabel *head = new QLabel(headline);
    QFont headline_font(text_default_font, 18, QFont::Thin);
    headline_font.setItalic(true);
    headline_font.setBold(true);
    head->setFont(headline_font);
    head->setStyleSheet(subtitle_style);

    layout->addWidget(head);

    area = new QScrollArea();
    area->verticalScrollBar()->setStyleSheet(scrollbar_style);
    area->setStyleSheet(area_style);

    items_layout = new QVBoxLayout();
    items_layout->setMargin(0);
    QGroupBox *items_box = new QGroupBox();
    items_box->setLayout(items_layout);
    items_box->setStyleSheet(box_style);

    QFont item_font(text_default_font, 14, QFont::Thin);
    for (auto item : items) {
        QLabel *label = new QLabel(item);
        label->setMaximumWidth(screen->width() - SCROLL_WIDTH);
        label->setMinimumWidth(screen->width() - SCROLL_WIDTH);
        label->setStyleSheet(text_style);
        label->setFont(item_font);
        label->setWordWrap(true);

        items_layout->addWidget(label);
        items_layout->setSpacing(0);
    }
    area->setWidget(items_box);

    layout->addWidget(area);
}

StatDialog::~StatDialog() {
    delete ui;
}



