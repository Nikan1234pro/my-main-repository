#ifndef CHOOSEMARKDIALOG_H
#define CHOOSEMARKDIALOG_H

#include <QDialog>

namespace Ui {
class ChooseMarkDialog;
}

class ChooseMarkDialog : public QDialog {
    Q_OBJECT

    const QString excellent    = QString::fromUtf8("Отлично");
    const QString good         = QString::fromUtf8("Хорошо");
    const QString satisfactory = QString::fromUtf8("Удовлетворительно");


    std::vector<std::pair<QString, int>> marks;

signals:
    void sendMark(int mark);

public:
    explicit ChooseMarkDialog(QWidget *parent);
    ~ChooseMarkDialog();

private slots:
    void on_acceptButton_clicked();

private:
    Ui::ChooseMarkDialog *ui;
};

#endif // CHOOSEMARKDIALOG_H
