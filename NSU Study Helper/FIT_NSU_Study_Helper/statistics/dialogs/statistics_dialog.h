#ifndef STATDIALOG_H
#define STATDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QScrollArea>
#include <QGroupBox>
#include <QVBoxLayout>
#include <vector>

namespace Ui {
class StatDialog;
}

class StatDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StatDialog(QWidget *parent,
                        const QString &headline,
                        const std::vector<QString> &items);
    ~StatDialog();

private:
    constexpr static int BAR_HEIGHT = 50;
    constexpr static int ITEM_SPACING = 10;
    constexpr static int SCROLL_WIDTH = 30;

    Ui::StatDialog *ui;
    QScrollArea *area;
    QVBoxLayout *items_layout;
    QGroupBox   *box;
};

#endif // STATDIALOG_H
