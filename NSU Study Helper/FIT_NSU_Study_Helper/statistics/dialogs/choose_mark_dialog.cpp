#include "choose_mark_dialog.h"
#include "ui_choose_mark_dialog.h"
#include "../settings.h"

ChooseMarkDialog::ChooseMarkDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChooseMarkDialog) {
    ui->setupUi(this);
    setFixedSize(DIALOG_WIDTH, DIALOG_HEIGHT);

    marks.emplace_back(satisfactory, 3);
    marks.emplace_back(good, 4);
    marks.emplace_back(excellent, 5);

    for (auto& v : marks)
        ui->comboBox->addItem(v.first);
}

ChooseMarkDialog::~ChooseMarkDialog() {
    delete ui;
}

void ChooseMarkDialog::on_acceptButton_clicked() {
    std::size_t index = static_cast<std::size_t>(
                ui->comboBox->currentIndex());

    close();
    emit sendMark(marks[index].second);
}
