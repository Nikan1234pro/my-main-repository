#ifndef COMMENTSVIEWER_H
#define COMMENTSVIEWER_H
#include <QScrollArea>
#include <QScrollBar>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QPushButton>
#include <QLabel>
#include <memory>
#include <vector>

#include "../utils/factory.h"
#include "../database/primitives/comment.h"
#include "observable/commented_object.h"

namespace statistics {

class TextFieldState {
protected:
    QTextEdit   *text_edit;
    QPushButton *send_button;
    CommentedObject *object;
public:
    TextFieldState(CommentedObject *object_,
                   QTextEdit *text_edit_,
                   QPushButton *send_button_);

    virtual void handle() = 0;

    virtual ~TextFieldState() = default;
};


class NoComment : public TextFieldState {
    const QString message = QString::fromUtf8(
                "Оставьте отзыв...");

public:
    NoComment(CommentedObject *object_,
              QTextEdit *text_edit_,
              QPushButton *send_button_);

    void handle() override;
};


class OnModeration : public TextFieldState {
    const QString message = QString::fromUtf8(
                "Ваш отзыв находится на модерации...");

public:
    OnModeration(CommentedObject *object_,
                        QTextEdit *text_edit_,
                        QPushButton *send_button_);

    void handle() override;
};


class Published : public TextFieldState {
    const QString message = QString::fromUtf8(
                "Ваш отзыв был опубликован.\n"
                "You are breathtaking \\o_o/");

    const QString button_messages[2] = {
        QString::fromUtf8("Править отзыв"),
        QString::fromUtf8("Отправить")
    };

public:
    Published(CommentedObject *object_,
              QTextEdit *text_edit_,
              QPushButton *send_button_);

    void handle() override;
};



class CommentsViewer : public QObject, public Observer {
    Q_OBJECT
public:
    CommentsViewer(QWidget *parent,
                   CommentedObject *commented_object);

    void notify() override;
    void display();
    void setGeometry(const QRect &rect);

private slots:
    void onSendButtonClicked();

private:    
    void addHeader();
    void addInputField();
    QLayout *addComment(const database::primitives::Comment &comment,
                        int comment_width);

    Factory<qint8,
            TextFieldState,
            CommentedObject*,
            QTextEdit*,
            QPushButton*> factory;

    std::unique_ptr<TextFieldState> handler = nullptr;

    CommentedObject *commented_object_;

    QScrollArea *area;
    QVBoxLayout *layout;
    QGroupBox   *screen;

    QTextEdit *text_area;
    QPushButton *send_button;

    constexpr static int TITLE_HEIGTH     = 50;
    constexpr static int TEXT_AREA_HEIGTH = 80;
    constexpr static int BUTTON_HEIGHT    = 30;

    constexpr static int DEFAULT_SPACING  = 10;
    constexpr static int COMMENT_SPACING  = 30;
    constexpr static int SMALL_SPACING    = 2;

    constexpr static int header_size = 16;
    constexpr static int text_size   = 12;



    const QString warning_message = QString::fromUtf8("Сначала войдите в учетную запись!");
    const QString title_text      = QString::fromUtf8("Отзывы");
    const QString send_text       = QString::fromUtf8("Ok");
};

}
#endif // COMMENTSVIEWER_H
