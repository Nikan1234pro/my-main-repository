#include "commentsviewer.h"
#include "../settings.h"
#include "../auth/authservice.h"
#include "../network/host_socket.h"

#include <QDebug>
#include <QFont>
#include <QTextEdit>
#include <QMessageBox>

namespace statistics {

using namespace database::primitives;

TextFieldState::TextFieldState(CommentedObject *object_,
                               QTextEdit *text_edit_,
                               QPushButton *send_button_)
    : text_edit(text_edit_),
      send_button(send_button_),
      object(object_) {}


NoComment::NoComment(CommentedObject *object_,
                     QTextEdit *text_edit_,
                     QPushButton *send_button_)
    : TextFieldState(object_, text_edit_, send_button_) {
    text_edit->append(message);
}

void NoComment::handle() {
    QString text = text_edit->toPlainText();
    object->sendComment(text);
}



OnModeration::OnModeration(CommentedObject *object_,
                           QTextEdit *text_edit_,
                           QPushButton *send_button_)
    : TextFieldState(object_, text_edit_, send_button_) {
    text_edit->setEnabled(false);
    text_edit->setText(message);

    text_edit->show();
}

void OnModeration::handle() {
    /* Nothing */
}



Published::Published(CommentedObject *object_,
                     QTextEdit *text_edit_,
                     QPushButton *send_button_)
    : TextFieldState(object_, text_edit_, send_button_) {
    text_edit->setEnabled(false);
    text_edit->setText(message);

    send_button->setText(button_messages[0]);
}

void Published::handle() {

}



CommentsViewer::CommentsViewer(QWidget *parent,
                               CommentedObject *commented_object)
    : commented_object_(commented_object),
      area(new QScrollArea()),
      layout(new QVBoxLayout()),
      screen(new QGroupBox(parent)) {

    commented_object_->addObserver(this);

    area->setWidgetResizable(false);
    area->setStyleSheet(area_style);
    area->verticalScrollBar()->setStyleSheet(scrollbar_style);

    layout->setSpacing(0);
    layout->setMargin(0);

    screen->setLayout(layout);
    screen->setStyleSheet(box_style);

    addHeader();
    addInputField();

    factory.add<NoComment>(CommentedObject::NOT_FOUND);
    factory.add<OnModeration>(CommentedObject::ON_MODERATION);
    factory.add<Published>(CommentedObject::PUBLISHED);
}

void CommentsViewer::notify() {
    display();
}


void CommentsViewer::setGeometry(const QRect &rect) {
    screen->setGeometry(rect);
}


void CommentsViewer::addHeader() {
    /* Title */
    QFont title_font(text_default_font, 16, QFont::Thin);
    title_font.setItalic(true);

    QLabel *title = new QLabel(title_text);
    title->setStyleSheet(title_style);
    title->setWordWrap(true);
    title->setMinimumHeight(TITLE_HEIGTH);
    title->setMaximumHeight(TITLE_HEIGTH);
    title->setFont(title_font);

    layout->addWidget(title);
    layout->addSpacing(DEFAULT_SPACING);

    layout->addWidget(area);
}


void CommentsViewer::addInputField() {
    /* Comment input */
    QVBoxLayout *comment_layout = new QVBoxLayout();
    comment_layout->setMargin(0);

    QFont text_font(text_default_font, text_size);
    text_area = new QTextEdit();
    text_area->setStyleSheet(text_edit_style);
    text_area->setFont(text_font);

    QFont button_font(button_default_font, text_size);
    button_font.setItalic(true);
    button_font.setBold(true);

    send_button = new QPushButton(send_text);
    send_button->setStyleSheet(button_style);
    send_button->setFont(button_font);

    connect(send_button, &QPushButton::clicked,
            this, &CommentsViewer::onSendButtonClicked);

    comment_layout->addWidget(text_area);
    comment_layout->addSpacing(SMALL_SPACING);
    comment_layout->addWidget(send_button);

    layout->addLayout(comment_layout);
}

QLayout* CommentsViewer::addComment(const database::primitives::Comment &comment,
                                    int comment_width) {
    auto *layout = new QGridLayout{};

    layout->setVerticalSpacing(0);
    layout->setHorizontalSpacing(0);

    QFont font(text_default_font, text_size, QFont::Thin);
    font.setBold(true);

    QLabel *header = new QLabel(comment.getUser().getName());
    header->setFont(font);
    header->setStyleSheet(subtitle_style);

    font.setBold(false);

    QLabel *text = new QLabel(comment.getText());
    text->setFont(font);
    text->setStyleSheet(text_style);
    text->setMinimumWidth(comment_width);
    text->setMaximumWidth(comment_width);
    text->setWordWrap(true);

    layout->addWidget(header, 0, 0);
    layout->addWidget(text,   1, 0);

    return layout;
}


void CommentsViewer::display() {
    qDeleteAll(area->findChildren<QGroupBox*>());

    const int comment_width = screen->width() - DEFAULT_SPACING;

    text_area->setMinimumSize(comment_width, TEXT_AREA_HEIGTH);
    text_area->setMaximumSize(comment_width, TEXT_AREA_HEIGTH);
    text_area->verticalScrollBar()->setStyleSheet(scrollbar_style);

    send_button->setMinimumSize(comment_width, BUTTON_HEIGHT);
    send_button->setMaximumSize(comment_width, BUTTON_HEIGHT);

    QVBoxLayout *items_layout = new QVBoxLayout();
    QGroupBox   *item_box = new QGroupBox();

    items_layout->setMargin(0);
    items_layout->setSpacing(COMMENT_SPACING);

    item_box->setLayout(items_layout);
    item_box->setStyleSheet(box_style);

    for (auto& comment : commented_object_->getComments())
        items_layout->addLayout(addComment(comment, comment_width));

    area->setWidget(item_box);

    auto state = commented_object_->getCommentState();
    handler.reset(factory.create(state, commented_object_, text_area, send_button));
}


void CommentsViewer::onSendButtonClicked() {
    if (!auth::AuthService::getCurrentUser()) {
        QMessageBox::warning(nullptr, error_header, warning_message);
        return;
    }

    try {
        if (handler)
            handler->handle();
    }
    catch (network::ServerUnavailableException &e) {
        QMessageBox::critical(nullptr, error_header, e.what());
    }
}

}

