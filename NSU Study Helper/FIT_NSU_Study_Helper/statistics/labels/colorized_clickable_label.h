#ifndef COLORIZEDCLICKABLELABEL_H
#define COLORIZEDCLICKABLELABEL_H

#include <QLabel>
#include "../../settings.h"

class ColorizedClickableLabel : public QLabel {
    Q_OBJECT

    QString press_style   = linked_text_in;
    QString release_style = linked_text_out;

public:
    ColorizedClickableLabel(const QString &text = "",
                            QWidget* parent = nullptr);

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent *) override;
};

#endif // COLORIZEDCLICKABLELABEL_H
