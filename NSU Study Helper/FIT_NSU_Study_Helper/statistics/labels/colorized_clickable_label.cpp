#include "colorized_clickable_label.h"
#include <QString>

ColorizedClickableLabel::ColorizedClickableLabel(const QString &text,
                                                 QWidget* parent)
    : QLabel(parent) {
    setText(text);
    setStyleSheet(linked_text_out);
}

void ColorizedClickableLabel::mousePressEvent(QMouseEvent *) {
    setStyleSheet(linked_text_in);
    emit clicked();
}

void ColorizedClickableLabel::mouseReleaseEvent(QMouseEvent *) {
    setStyleSheet(linked_text_out);
}
