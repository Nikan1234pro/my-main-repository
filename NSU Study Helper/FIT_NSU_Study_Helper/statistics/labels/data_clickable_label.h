#ifndef DATA_CLICKABLE_LABEL_H
#define DATA_CLICKABLE_LABEL_H
#include "clickable_label.h"

template<class T>
class DataClickableLabel : public ClickableLabel {
    T data_;
public:
    DataClickableLabel(const T& data,
                       const QString& text = "",
                       QWidget *parent = nullptr)
        : ClickableLabel(text, parent), data_(data)  {}

    ~DataClickableLabel() override = default;

    const T& getData() const {
        return data_;
    }
};

#endif // DATA_CLICKABLE_LABEL_H
