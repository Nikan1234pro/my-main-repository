#include "clickable_label.h"

ClickableLabel::ClickableLabel(const QString &text,
                               QWidget* parent)
    : QLabel(parent) {
    setText(text);
}

ClickableLabel::~ClickableLabel() {}

void ClickableLabel::mousePressEvent(QMouseEvent*) {
    emit clicked();
}
