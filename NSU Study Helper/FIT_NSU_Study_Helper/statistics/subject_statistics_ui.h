#ifndef SUBJECTSTATISTICSUI_H
#define SUBJECTSTATISTICSUI_H

#include <QGroupBox>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <memory>
#include <vector>
#include <array>
#include <unordered_map>

#include "observable/subject_statistics.h"
#include "labels/clickable_label.h"

namespace statistics {

class SubjectStatisticsUI : public QObject, public Observer {
    Q_OBJECT

    static constexpr int mark_types[] = { 5, 4, 3 };

    std::array<std::vector<database::primitives::User>,
               std::size(mark_types)> users;

    static constexpr int SPACE_WIDTN = 2;
    static constexpr int TITLE_HEIGTH = 50;
    static constexpr int PERCENT_DATA_WIDTH = 50;

    static constexpr int MARK_NONE = -1;

    std::unordered_map<int, std::size_t> mark_to_index;
    SubjectStatistics *observarable_;

    QWidget *parent_;
    QVBoxLayout *schema_layout;
    QGroupBox   *box;

    const QString column_style        = "QLabel{background-color : rgb(82, 218, 255);"
                                         "color : white; border-style: solid;"
                                         "border-width: 5px;"
                                         "border-color: rgb(82, 218, 255);"
                                         "font-weight: bold; }";

    const QString chosen_column_style = "QLabel{background-color : rgb(48, 148, 255);"
                                         "color : white; border-style: solid;"
                                         "border-width: 5px;"
                                         "border-color: rgb(48, 148, 255);"
                                         "font-weight: bold; }";

    const QString space_style         = "QLabel{background-color : rgba(48, 48, 48, 200);"
                                        "color : white; border:none;"
                                        "font-weight: bold; }";

    const QString user_mark_style     = "QLabel{background-color : white;"
                                        "color : red; border-style: solid;"
                                        "border-width: 5px;"
                                        "border-color: white;"
                                        "font-weight: bold; }";

    const QString mark_text          = QString::fromUtf8("Ваша оценка:");
    const QString mark_none          = QString::fromUtf8("Указать оценку");
    const QString teachers_text      = QString::fromUtf8("Список преподавателей");
    const QString title_text         = QString::fromUtf8("Результаты Экзамена");
    const QString stat_dialog_title  = QString::fromUtf8("Оценка");
    const QString warning_message    = QString::fromUtf8("Сначала войдите в учетную запись!");

    const int title_size = 16;
    const int text_size  = 11;

public:
    SubjectStatisticsUI(QWidget *parent, SubjectStatistics *observarable);
    void setGeometry(const QRect& rect);
    QRect getGeometry() const;

    void notify() override;
    void display();
    ~SubjectStatisticsUI() override;

private slots:
    void getAndSendMark(int mark);

private:
    void prepare();
    void addHeader();
    void addDiagram(int user_mark);
    void addMarkField(int user_mark);
    void addTeachers();

public slots:
    void onDiagramBarClicked();
    void onMarkChangeClicked();
};

}

#endif // SUBJECTSTATISTICSUI_H
