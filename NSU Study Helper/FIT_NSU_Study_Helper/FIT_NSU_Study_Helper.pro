QT       += core gui charts
QT += sql
QT += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    auth/authorizeduser.cpp \
    auth/authservice.cpp \
    database/history_controller.cpp \
    database/primitives/comment.cpp \
    database/primitives/config.cpp \
    database/primitives/subject.cpp \
    database/primitives/teacher.cpp \
    database/primitives/user.cpp \
    database/subject_stat_controller.cpp \
    forms/entrancewindow.cpp \
    forms/hellowindow.cpp \
    forms/infowindow.cpp \
    forms/loginwindow.cpp \
    forms/mainwindow.cpp \
    forms/registerwindow.cpp \
    forms/subject_statistics_form.cpp \
    forms/window_manager.cpp \
    main.cpp \
    network/host_socket.cpp \
    network/server_access_manager.cpp \
    settings.cpp \
    statistics/commentsviewer.cpp \
    statistics/dialogs/choose_mark_dialog.cpp \
    statistics/dialogs/statistics_dialog.cpp \
    statistics/labels/clickable_label.cpp \
    statistics/labels/colorized_clickable_label.cpp \
    statistics/observable/subject_statistics.cpp \
    statistics/subject_statistics_ui.cpp

HEADERS += \
    auth/authorizeduser.h \
    auth/authservice.h \
    database/history_controller.h \
    database/json_parser.h \
    database/primitives/comment.h \
    database/primitives/config.h \
    database/primitives/serializable.h \
    database/primitives/subject.h \
    database/primitives/teacher.h \
    database/primitives/user.h \
    database/subject_stat_controller.h \
    forms/entrancewindow.h \
    forms/hellowindow.h \
    forms/infowindow.h \
    forms/loginwindow.h \
    forms/mainwindow.h \
    forms/registerwindow.h \
    forms/subject_statistics_form.h \
    forms/window_manager.h \
    network/host_socket.h \
    network/server_access_manager.h \
    settings.h \
    statistics/commentsviewer.h \
    statistics/dialogs/choose_mark_dialog.h \
    statistics/dialogs/statistics_dialog.h \
    statistics/labels/clickable_label.h \
    statistics/labels/colorized_clickable_label.h \
    statistics/labels/data_clickable_label.h \
    statistics/observable/commented_object.h \
    statistics/observable/observable.h \
    statistics/observable/subject_statistics.h \
    statistics/observer.h \
    statistics/subject_statistics_ui.h \
    utils/factory.h

FORMS += \
    ui/choose_mark_dialog.ui \
    ui/entrancewindow.ui \
    ui/hellowindow.ui \
    ui/infowindow.ui \
    ui/loginwindow.ui \
    ui/mainwindow.ui \
    ui/registerwindow.ui \
    ui/statistics_dialog.ui \
    ui/subject_statistics_form.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
