#include <iostream>
#include <fstream>
#include "interface.h"
using namespace std;

void LIST_Data(const char *arch_name)
{
	fstream file_in(arch_name, ios::binary | ios::in);
	if (!file_in)
	{
		cout << "Couldn't open Archive!" << endl;
		return;
	}
	char tmp[SGN_size];
	tmp[SGN_size - 1] = '\0';
	file_in.read(tmp, SGN_size - 1);
	if (strcmp(tmp, signature))
	{
		cout << "Signature didn't match!" << endl;
		file_in.close();
		return;
	}
	cout << "Signature matched..." << endl;
	cout << "-------------------------------------" << endl;
	cout << "Files in " << arch_name << " archive:" << endl;

	while (true)
	{
		short int name_size = 0;
		file_in.read(reinterpret_cast<char*>(&name_size), sizeof(short int));
		char *buf = new char[name_size + 1];
		file_in.read(buf, name_size);
		if (!file_in)
			break;
		buf[name_size] = '\0';
		cout << buf << endl;
		skip_file(file_in);
		delete[] buf;
	}
	file_in.close();
}