#include <iostream>
#include <time.h>
#include "interface.h"
using namespace std;

int Analysis_data(int argc, char **argv)
{
	if (!strcmp(argv[1], "-a"))
	{
		return (argc >= 4) ? 1 : -1;
	}
	else if (!strcmp(argv[1], "-e"))
	{
		return (argc >= 4) ? 2 : -1;
	}
	else if (!strcmp(argv[1], "-l"))
	{
		return (argc == 3) ? 3 : -1;
	}
	else
		return -1;
}

void Archiver(int argc, char **argv, int parameter)
{
	cout << "Archiver 2.0 by Nikita Dolgov" << endl << endl;
	if (parameter == 1)
	{
		for (int i = 3; i < argc; i++)
			ZIP_Data(argv[2], argv[i]);
	}
	if (parameter == 2)
	{
		for (int i = 3; i < argc; i++)
			UNZIP_Data(argv[2], argv[i]);
	}
	if (parameter == 3)
		LIST_Data(argv[2]);

	cout << "-------------------------------------" << endl;
}

int main(int argc, char **argv)
{
	if (argc == 1)
		return 1;
	time_t start, end;
	start = clock();
	setlocale(LC_ALL, "RUS");
	int parameter = Analysis_data(argc, argv);
	if (parameter == -1)
	{
		cout << "Incorrect parameters!" << endl;
		system("pause");
		return 1;
	}
	else
		Archiver(argc, argv, parameter);
	end = clock();
	cout << "Done at " << difftime(end, start) << " ms" << endl;
	system("pause");
	return 0;
}