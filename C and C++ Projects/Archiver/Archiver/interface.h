#pragma once
const int Data_Len = 4096;
const int Tree_Len = 320;
const int Table_Len = 256;
const int Byte_Len = 8;
const int SGN_size = 10;
const char signature[SGN_size] = "Nikan1234";

struct Tree
{
	unsigned char symbol;
	char *code;
	unsigned int priority;
	Tree *next;
	Tree *t_left;
	Tree *t_right;
};


void ZIP_Data(const char *arch_name, const char *file_name);
void UNZIP_Data(const char *arch_name, const char *file_name);
void LIST_Data(const char *arch_name);

bool is_empty_file(std::fstream &file);
void skip_file(std::fstream &file_in);
bool find_file(std::fstream &file_in, const char *cur_name);
Tree *init_Tree();

void my_strcpy(char *s1, const char *s2);
unsigned char Current_Bit(unsigned char symbol, char position);

void clear_Codes(char **Codes);
void clear_Tree(Tree *Head);

