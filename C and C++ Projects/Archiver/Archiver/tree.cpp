#include <iostream>
#include <cstring>
#include <fstream>
#include "interface.h"

using namespace std;

void Obhod(Tree *Head)
{
	if (!Head)
		return;
	cout << static_cast<int>(Head->symbol) << ' ';
	Obhod(Head->t_left);
	Obhod(Head->t_right);
}

Tree *init_Tree()
{
	Tree *Head = new Tree;
	Head->priority = 0;
	Head->symbol = 0;
	Head->next = NULL;
	Head->code = NULL;
	Head->t_left = NULL;
	Head->t_right = NULL;
	return Head;
}

void Push_Queue(Tree *Head, const unsigned char symbol,
	const unsigned int priority)
{
	Tree *temp = new Tree;
	temp->code = NULL;
	temp->symbol = symbol;
	temp->priority = priority;
	temp->t_left = NULL;
	temp->t_right = NULL;
	if (Head->next == NULL)
	{
		Head->next = temp;
		temp->next = NULL;
		return;
	}
	while (Head->next->priority < temp->priority)
	{
		Head = Head->next;
		if (Head->next == NULL) break;
	}
	temp->next = Head->next;
	Head->next = temp;
}

void Create_Queue(Tree *Head, const unsigned int *Table)
{
	for (int i = 0; i < Table_Len; i++)
	{
		if (Table[i] != 0)
			Push_Queue(Head, i, Table[i]);
	}
}

void Create_Tree(Tree *Head)
{
	Tree *temp1, *temp2, *tree, *point;
	do
	{
		temp1 = Head->next;
		temp2 = (Head->next->next) ? Head->next->next : NULL;
		tree = new Tree;
		tree->code = NULL;
		tree->next = NULL;
		tree->t_left = temp1;
		tree->t_right = temp2;
		tree->symbol = '\0';
		tree->priority = temp1->priority + ((temp2) ? temp2->priority : 0);
		if (temp2)
		{
			point = Head;
			while (point->next != NULL)
				if (point->next->priority >= tree->priority) break;
				else point = point->next;

			tree->next = point->next;
			point->next = tree;
			Head->next = temp2->next;
			tree = NULL;
		}
		else
			Head->next = tree;

	} while (Head->next->next != NULL);
}

unsigned char Get_Symbol(fstream &file_in, unsigned char *buf, char &position, int &point)
{
	unsigned char symbol = 0;
	for (int i = Byte_Len; i > 0; i--)
	{
		if (!position)
		{
			point++;
			position = Byte_Len;
		}

		symbol = symbol | (Current_Bit(buf[point], position) << (i - 1));
		position--;

	}
	return symbol;
}

Tree* Re_Create_Tree(fstream &file_in, unsigned char *buf, char &position, int &point, short int size_tree)
{
	Tree *Head = NULL;

	if (point == size_tree)
		return Head;
	Head = init_Tree();

	if (!Current_Bit(buf[point], position))
	{
		position--;
		Head->priority = 0;
		Head->symbol = '\0';
	}
	else
	{
		position--;
		Head->priority = 1;
		Head->symbol = Get_Symbol(file_in, buf, position, point);
	}
	if (!position)
	{
		position = Byte_Len;
		point++;
	}
	if (Head->priority == 0)
		Head->t_left = Re_Create_Tree(file_in, buf, position, point, size_tree);
	if (Head->priority == 0)
		Head->t_right = Re_Create_Tree(file_in, buf, position, point, size_tree);
	return Head;
}

void put_symbol(const Tree *Head, unsigned char *buf, unsigned char &code, char &position, short int &count)
{
	if ((Head->t_left == NULL) && (Head->t_right == NULL)) ///If it is a leaf
		code = code | (1 << ((position)-1));
	position--;

	if (!position)
	{
		buf[count] = code;
		count++;
		code = 0;
		position = Byte_Len;
	}
	///Write ASCII code of symbol
	for (int i = Byte_Len; (!Head->t_left) && (!Head->t_right) && (i > 0); i--)
	{
		code = code | (Current_Bit(Head->symbol, i) << (position - 1));
		position--;
		if (!position)
		{
			buf[count] = code;
			count++;
			code = 0;
			position = Byte_Len;
		}
	}
}

void Save_Tree(const Tree *Head, unsigned char *buf, unsigned char &code, char &position, short int &size_tree)
{
	put_symbol(Head, buf, code, position, size_tree);
	if (Head->t_left)
		Save_Tree(Head->t_left, buf, code, position, size_tree);
	if (Head->t_right)
		Save_Tree(Head->t_right, buf, code, position, size_tree);
}