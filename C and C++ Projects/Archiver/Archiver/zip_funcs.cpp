#include <iostream>
#include <fstream>
#include <cstring>
#include "interface.h"
#include "Tree.h"
using namespace std;

unsigned int *Create_Table(fstream &file_in)
{
	unsigned int *Table = NULL;
	Table = new unsigned int[Table_Len];
	for (int i = 0; i < Table_Len; i++)
		Table[i] = 0;

	unsigned char *buf = new unsigned char[Data_Len];
	while (!file_in.eof())
	{
		file_in.read(reinterpret_cast<char*>(buf), Data_Len);
		for (int i = 0; i < file_in.gcount(); i++)
			Table[static_cast<unsigned char>(buf[i])]++;
	}
	cout << "Table succesfully created..." << endl;

	return Table;
}

char **init_Codes()
{
	char **Codes = new char*[Table_Len];
	for (int i = 0; i < Table_Len; i++)
		Codes[i] = NULL;
	return Codes;
}

int SGN_Test(fstream &file_in, fstream &file_out)
{
	if (is_empty_file(file_out))
	{
		cout << "Write signature to file..." << endl;
		file_out.write(signature, SGN_size - 1);
		return 1;
	}
	else
	{
		char buf[SGN_size];
		buf[SGN_size - 1] = '\0';

		file_out.read(buf, SGN_size - 1);

		if (strcmp(buf, signature))
		{
			cout << "Signature didn't match!" << endl;
			file_in.close();
			file_out.close();
			return 0;
		}
		cout << "Signature matched..." << endl;
		file_out.seekp(0, ios::end);
		return 1;
	}
}

unsigned int Count_Text(unsigned int *Table, char **Codes, char &not_empty_bits)
{
	unsigned long long len = 0;
	for (int i = 0; i < Table_Len; i++)
	{
		if (Table[i])
			len += Table[i] * strlen(Codes[i]);
	}
	unsigned int temp = len / Byte_Len;
	not_empty_bits = len % Byte_Len;
	return (not_empty_bits) ? temp + 1 : temp;
}

void Create_Codes(Tree *Head, char **Codes, const char *code_prev, const char bit)
{
	int size = strlen(code_prev) + 2;
	Head->code = new char[size];
	my_strcpy(Head->code, code_prev);
	Head->code[strlen(code_prev)] = bit;
	Head->code[size - 1] = '\0';
	if ((Head->t_left == NULL) && (Head->t_right == NULL))
		Codes[Head->symbol] = Head->code;

	if (Head->t_left != NULL) Create_Codes(Head->t_left, Codes, Head->code, '0');
	if (Head->t_right != NULL) Create_Codes(Head->t_right, Codes, Head->code, '1');
}

void Write_Codes(char **Codes, fstream &file_in, fstream &file_out)
{
	unsigned char *buffer_in = new unsigned char[Data_Len];
	unsigned char *buffer_out = new unsigned char[Data_Len];
	unsigned char code = 0;
	unsigned int cur_place = 0;
	char position = Byte_Len;
	file_in.clear();
	file_in.seekg(0, ios::beg);
	while (file_in)
	{
		file_in.read(reinterpret_cast<char*>(buffer_in), Data_Len);
		for (int i = 0; i < file_in.gcount(); i++)
		{
			unsigned char cur_symbol = buffer_in[i];
			for (int j = 0; Codes[cur_symbol][j]; j++)
			{
				code = code | (Codes[cur_symbol][j] - '0') << (position - 1);
				position--;
				if (!position)
				{
					buffer_out[cur_place++] = code;
					position = Byte_Len;
					code = 0;
				}
				if (cur_place == Data_Len)
				{
					file_out.write(reinterpret_cast<char*>(buffer_out), cur_place);
					cur_place = 0;
				}
			}
		}
	}
	if (position != Byte_Len)
		buffer_out[cur_place++] = code;

	if (cur_place)
		file_out.write(reinterpret_cast<char*>(buffer_out), cur_place);

	delete[] buffer_in;
	delete[] buffer_out;
}

void ZIP_Data(const char *arch_name, const char *file_name)
{
	cout << "-------------------------------------" << endl;
	setlocale(LC_ALL, "RUS");
	fstream file_in(file_name, ios::binary | ios::in); //File
	fstream file_out(arch_name, ios::binary | ios::app | ios::in); //Archive

	if (!file_in || !file_out)
	{
		cout << "File couldn't be opened!" << endl;
		return;
	}
	if (!SGN_Test(file_in, file_out))
		return;

	cout << "Adding " << file_name << " to archive" << endl;
	unsigned int *Table = NULL;
	char **Codes = NULL;
	Tree *Head = NULL;

	Table = Create_Table(file_in);
	Codes = init_Codes();
	Head = init_Tree();
	Create_Queue(Head, Table);  //Build a Queue based on the Table
	if (Head->next == NULL)
		return;
	Create_Tree(Head);
	cout << "Tree succesfully created..." << endl;

	if (Head->next->t_left) Create_Codes(Head->next->t_left, Codes, "\0", '0'); //Walking the Tree and recording Codes
	if (Head->next->t_right) Create_Codes(Head->next->t_right, Codes, "\0", '1');

	unsigned char buf[Tree_Len];
	char position = Byte_Len;
	unsigned char code = 0;
	short int size_tree = 0;
	Save_Tree(Head->next, buf, code, position, size_tree);

	if (position != Byte_Len)
	{
		buf[size_tree] = code;
		size_tree++;
	}
	char not_empty_bits = 0;
	unsigned int size_text = Count_Text(Table, Codes, not_empty_bits);

	//Put information in file
	short int size_name = strlen(file_name);
	file_out.write(reinterpret_cast<char*>(&size_name), sizeof(short int));
	file_out.write(file_name, strlen(file_name));
	file_out.write(reinterpret_cast<char*>(&size_tree), sizeof(short int));
	file_out.write(reinterpret_cast<char*>(buf), size_tree);
	file_out.write(reinterpret_cast<char*>(&size_text), sizeof(unsigned int));
	file_out.write(&not_empty_bits, sizeof(char));
	Write_Codes(Codes, file_in, file_out);

	if (file_out)
		cout << "File " << file_name << " was succesfully added!" << endl;

	delete[] Table;

	clear_Codes(Codes);
	clear_Tree(Head);
	file_in.close();
	file_out.close();
}
