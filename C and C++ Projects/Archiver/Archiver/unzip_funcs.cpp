#include <iostream>
#include <fstream>
#include <cstring>
#include "interface.h"
#include "Tree.h"
using namespace std;

void Decode_Text(Tree *Head, fstream &file_in, fstream &file_out, unsigned int size_text, char empty_bits)
{
	unsigned char bit, symbol;
	unsigned char *buffer_in = new unsigned char[Data_Len];
	unsigned char *buffer_out = new unsigned char[Data_Len];
	unsigned int point = 0, count = 0, cur_place = 0;
	Tree *temp = Head;

	while (size_text)
	{
		if (size_text > Data_Len)
			file_in.read(reinterpret_cast<char*>(buffer_in), Data_Len);
		else
			file_in.read(reinterpret_cast<char*>(buffer_in), size_text);

		unsigned int count = file_in.gcount();
		size_text -= count;
		for (unsigned int j = 0; j < count; j++)
		{
			symbol = buffer_in[j];
			char last_pos = ((!size_text) && (j == count - 1) && (empty_bits != 8)) ? empty_bits : 0;
			for (char position = Byte_Len; position > last_pos; position--)
			{
				bit = Current_Bit(symbol, position);
				temp = (bit) ? temp->t_right : temp->t_left;
				if ((temp->t_left == NULL) && (temp->t_right == NULL))
				{
					buffer_out[cur_place++] = temp->symbol;
					if (cur_place == Data_Len)
					{
						file_out.write(reinterpret_cast<char*>(buffer_out), cur_place);
						cur_place = 0;
					}
					temp = Head;
				}
			}
		}
	}
	if (cur_place)
		file_out.write(reinterpret_cast<char*>(buffer_out), cur_place);

	delete[] buffer_in;
	delete[] buffer_out;
}

void UNZIP_Data(const char *arch_name, const char *file_name)
{
	cout << "-------------------------------------" << endl;
	setlocale(LC_ALL, "RUS");
	fstream file_in(arch_name, ios::binary | ios::in); //Archive
	char tmp[SGN_size];
	tmp[SGN_size - 1] = '\0';
	file_in.read(tmp, SGN_size - 1);
	if (strcmp(tmp, signature))
	{
		cout << "Signature didn't match!" << endl;
		file_in.close();
		return;
	}
	cout << "Signature matched..." << endl;
	fstream file_out(file_name, ios::binary | ios::out); //File
	cout << "Extracting " << file_name << " from archive..." << endl;
	if (!find_file(file_in, file_name))
	{
		cout << "File " << file_name << " doesn't exist!" << endl;
		file_in.close();
		file_out.close();
		return;
	}
	file_in.clear();
	short int size_tree = 0;
	Tree *Head = init_Tree();

	file_in.read(reinterpret_cast<char*>(&size_tree), sizeof(short int));
	unsigned char *buf = new unsigned char[size_tree];
	file_in.read(reinterpret_cast<char*>(buf), size_tree);
	cout << "Re-creating tree..." << endl;

	char position = Byte_Len;
	int point = 0;
	Head->next = Re_Create_Tree(file_in, buf, position, point, size_tree);
	delete[] buf;

	cout << "Decoding text..." << endl;

	unsigned int size_text = 0;
	char not_empty_bits = 0;
	file_in.read(reinterpret_cast<char*>(&size_text), sizeof(unsigned int));
	file_in.read(reinterpret_cast<char*>(&not_empty_bits), sizeof(char));
	Decode_Text(Head->next, file_in, file_out, size_text, Byte_Len - not_empty_bits);
	if (file_out)
		cout << "File " << file_name << " was succesfully extracted!" << endl;

	clear_Tree(Head);
	file_in.close();
	file_out.close();
}