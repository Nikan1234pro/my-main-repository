#include <fstream>
#include <iostream>
#include"interface.h"
using namespace std;

void skip_file(fstream &file_in)
{
	short int tmp = 0;
	file_in.read(reinterpret_cast<char*>(&tmp), sizeof(short int));
	file_in.seekg(tmp, ios::cur);

	unsigned int tmp2 = 0;
	file_in.read(reinterpret_cast<char*>(&tmp2), sizeof(unsigned int));
	file_in.get();
	file_in.seekg(tmp2, ios::cur);
}

bool find_file(fstream &file_in, const char *cur_name)
{
	while (file_in)
	{
		short int name_size = 0;
		file_in.read(reinterpret_cast<char*>(&name_size), sizeof(short int));
		char *buf = new char[name_size + 1];
		buf[name_size] = '\0';
		file_in.read(buf, name_size);
		if (!strcmp(buf, cur_name))
		{
			delete[] buf;
			return true;
		}
		else
		{
			delete[] buf;
			skip_file(file_in);
		}
	}
	return false;
}

void my_strcpy(char *s1, const char *s2)
{
	for (; *s2; *s1++ = *s2++);
	*s1 = '\0';
}

bool is_empty_file(fstream &file)
{
	char ch = file.get();
	file.clear();
	file.seekg(0, ios::beg);
	file.seekp(0, ios::beg);
	return (ch == EOF) ? true : false;
}

unsigned char Current_Bit(unsigned char symbol, char position)
{
	symbol = symbol << (Byte_Len - position);
	symbol = symbol >> (Byte_Len - 1);
	return symbol;
}

void clear_Codes(char **Codes)
{
	for (int i = 0; i < Table_Len; i++)
	{
		if (Codes[i])
			delete[] Codes[i];
	}
	delete[] Codes;
}

void clear_Sub_Tree(Tree *Head)
{
	if (Head->t_left)
		clear_Sub_Tree(Head->t_left);
	if (Head->t_right)
		clear_Sub_Tree(Head->t_right);
	delete Head;
}

void clear_Tree(Tree *Head)
{
	Tree *tmp = NULL;
	while (Head)
	{
		tmp = Head;
		Head = Head->next;
		clear_Sub_Tree(tmp);
	}
}