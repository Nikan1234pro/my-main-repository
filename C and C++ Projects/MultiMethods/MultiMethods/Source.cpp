#include "MultiMethod.h"

#include <iostream>

// ������� ����� ������ (�����������)
struct Shape {
	virtual ~Shape() = default;
};

// �������������
struct Rectangle : Shape {};

// �����������
struct Triangle : Shape {};

// ������� ��� �������� ����������� ���� ���������������
bool is_intersect_r_r(Shape* a, Shape* b) {
	std::cout << "Rectangle x Rectangle" << std::endl;
	return true;
}

// ������� ��� �������� ����������� �������������� � ������������
bool is_intersect_r_t(Shape* a, Shape* b) {
	std::cout << "Rectangle x Triangle" << std::endl;
	return true;
}

bool is_intersect_t_r(Shape* a, Shape* b) {
	std::cout << "Triangle x Rectangle" << std::endl;
	return true;
}

int main() {
	Multimethod2<Shape, bool, false> is_intersect;

	is_intersect.addImpl(typeid(Rectangle), typeid(Rectangle), is_intersect_r_r);
	is_intersect.addImpl(typeid(Triangle), typeid(Rectangle), is_intersect_t_r);
	is_intersect.addImpl(typeid(Rectangle), typeid(Triangle), is_intersect_r_t);

	Shape* t = new Triangle();
	Shape* r = new Rectangle();


	bool res = is_intersect.call(t, r);


	std::list<int> l = { 1,2,3 };
	l.insert(l.begin(), 4);
	l.insert(l.end(), 5);

	auto it = l.begin();
	++it;
	++it;

	l.insert(it, 6);

	auto rit1 = l.rbegin();
	++rit1;
	++rit1;
	l.insert(rit1.base(), 7);

	auto rit2 = l.rbegin();
	++rit2;
	++rit2;
	l.insert(rit2.base(), 8);


	for (auto t : l)
		std::cout << t << "	";

	return EXIT_SUCCESS;
}