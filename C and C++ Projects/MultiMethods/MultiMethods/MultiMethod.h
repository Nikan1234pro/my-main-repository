#pragma once

#include <functional>
#include <stdexcept>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

namespace std {
template <> struct hash<pair<type_index, type_index>> {
  size_t operator()(const pair<type_index, type_index> &p) const {
    auto x = p.first.hash_code();
    auto y = p.second.hash_code();

    return (x + y + 1) * (x + y) / 2 + x;
  }
};
} // namespace std

template <class Base, class Result, bool Commutative> class Multimethod2 {
public:
  using Key_t = std::type_index;
  using Handler_t = std::function<Result(Base *, Base *)>;

  // ������������� ���������� ������������
  // ��� ����� t1 � t2 �������� ����� typeid
  // f - ��� ������� ��� �������������� ������
  // ����������� ��� ��������� �� Base
  // � ������������ �������� ���� Result
  void addImpl(const Key_t &t1, const Key_t &t2, const Handler_t &f) {
    handlers[std::make_pair(t1, t2)] = f;

    if (Commutative == true)
      handlers[std::make_pair(t2, t1)] = [f](Base *t1, Base *t2) -> Result {
        return f(t2, t1);
      };
  }

  // ���������, ���� �� ���������� ������������
  // ��� ����� �������� a � b
  bool hasImpl(Base *a, Base *b) const {
    Key_t t1 = std::type_index(typeid(*a));
    Key_t t2 = std::type_index(typeid(*b));

    return handlers.find(std::make_pair(t1, t2)) != handlers.end();
  }

  // ��������� ����������� � ��������
  // �� ���������� a � b
  Result call(Base *a, Base *b) const {
    Key_t t1 = std::type_index(typeid(*a));
    Key_t t2 = std::type_index(typeid(*b));

    auto it = handlers.find(std::make_pair(t1, t2));
    if (it == handlers.end()) {
      throw std::runtime_error("Handler not found");
    }
    return it->second(a, b);
  }

private:
  std::unordered_map<std::pair<Key_t, Key_t>, Handler_t> handlers;
};
