#include <stdlib.h>
#include <stdio.h>

typedef struct Stack
{
	int value;
	struct Stack *next;
} Stack;

int push(Stack **head, int value)
{
	Stack *tmp = (Stack*)malloc(sizeof(Stack));
	if (NULL == tmp)
		return EXIT_FAILURE;
	tmp->value = value;
	tmp->next = *head;
	*head = tmp;
	return EXIT_SUCCESS;
}

int pop(Stack **head) {
	Stack *tmp = *head;
	int value = tmp->value;
	*head = (*head)->next;
	free(tmp);
	return value;
}

void clear(Stack **head) {
	for (; *head != NULL; *head = (*head)->next)
		pop(head);
}

int main() {
	Stack *head = NULL;
	for (int i = 0; i < 10; i++)
		push(&head, i * 10);

	for (Stack *tmp = head; tmp != NULL; tmp = tmp->next) {
		printf("%d ", tmp->value);
	}

	clear(&head);
	system("pause");
	return 0;
}