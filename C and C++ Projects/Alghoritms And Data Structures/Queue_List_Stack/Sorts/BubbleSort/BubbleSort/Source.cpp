#include <iostream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

template <class T>
void sort(int *arr, int N, const T &cmp) {
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N - 1; j++)
			if (cmp(arr[j], arr[j + 1]))
				std::swap(arr[j], arr[j + 1]);
}

void reverse(char *str) {
	char *begin = str;
	char *end = str + strlen(str) - 1;
	for (; begin < end; begin++, end--)
		std::swap(*begin, *end);
}

int main() {
	char *str = new char[12];
	for (int i = 0; i < 10; i++)
		str[i] = 'a' + i;
	str[10] = '\0';

	reverse(str);
	std::cout << str << std::endl;

	int *arr = new int[12];
	std::srand(std::time(NULL));
	for (int i = 0; i < 12; i++)
		arr[i] = std::rand() % 1000;

	for (int i = 0; i < 12; i++)
		std::cout << arr[i] << "	";
	std::cout << std::endl;

	sort(arr, 12, std::less<>());

	for (int i = 0; i < 12; i++)
		std::cout << arr[i] << "	";

	system("pause");
	return 0;
}