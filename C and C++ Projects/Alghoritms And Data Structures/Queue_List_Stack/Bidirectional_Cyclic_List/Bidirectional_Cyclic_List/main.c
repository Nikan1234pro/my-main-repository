#include <stdlib.h>
#include <stdio.h>

typedef struct List {
	int value;
	struct List *next;
	struct List *prev;
} List;

int push(List **head, int value) {
	List *tmp = (List*)malloc(sizeof(List));
	if (tmp == NULL)
		return EXIT_FAILURE;
	tmp->value = value;

	if (*head == NULL) {
		tmp->next = tmp;
		tmp->prev = tmp;
	}
	else {
		tmp->prev = (*head)->prev;
		tmp->next = *head;
		(*head)->prev->next = tmp;
		(*head)->prev = tmp;
	}
	*head = tmp;
	return EXIT_SUCCESS;
}

int pop(List **head) {
	List *tmp = *head;
	int value = tmp->value;
	if (tmp == tmp->next)
		*head = NULL;
	else {
		tmp->prev->next = tmp->next;
		tmp->next->prev = tmp->prev;
		*head = (*head)->next;
	}
	free(tmp);
	return value;
}

int main() {
	List *list = NULL;
	for (int i = 0; i < 12; i++)
		push(&list, i);
	
	for (List *tmp = list; tmp != list->next; tmp = tmp->prev)
		printf("%d ", tmp->value);
	printf("\n");

	for (int i = 0; i < 12; i++)
		printf("%d ", pop(&list));
	printf("\n");

	system("pause");
	return 0;
}