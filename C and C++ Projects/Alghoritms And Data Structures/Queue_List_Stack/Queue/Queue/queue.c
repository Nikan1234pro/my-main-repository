#include "queue.h"


Queue_t *init() {
	Queue_t *queue = (Queue_t*)malloc(sizeof(Queue_t));
	queue->front = queue->back = NULL;
	queue->size = 0;
	return queue;
}

int push(Queue_t *queue, int value) {
	Node_t *tmp = (Node_t*)malloc(sizeof(Node_t));
	if (NULL == tmp)
		return EXIT_FAILURE;
	tmp->value = value;
	tmp->hext = NULL;
	if (NULL == queue->front) {
		queue->front = tmp;
		queue->back = tmp;
	}
	else {
		queue->front->hext = tmp;
		queue->front = tmp;
	}
	queue->size++;
	return EXIT_SUCCESS;
}

int pop(Queue_t *queue) {
	Node_t *tmp = queue->back;
	if (NULL == tmp)
		return 0;
	int value = tmp->value;
	if (queue->back == queue->front) {
		queue->back = queue->front = NULL;
	}
	else
		queue->back = queue->back->hext;
	queue->size--;
	free(tmp);
	return value;
}

void clear(Queue_t *queue) {
	while (queue->back != NULL) 
		pop(queue);
	free(queue);
}