#include <stdlib.h>
#include <stdio.h>
#include "queue.h"


int main() {
	Queue_t *q = init();
	push(q, 10);
	push(q, 228);
	push(q, 1234);
	printf("%d\n", q->size);
	for (int i = 0; i < 3; i++) {
		printf("%d ", pop(q));
	}
	push(q, 45);
	clear(q);
	system("pause");
	return 0;
}