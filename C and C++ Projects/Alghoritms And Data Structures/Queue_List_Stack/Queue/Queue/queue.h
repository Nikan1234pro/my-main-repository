#pragma once
#include <stdlib.h>

typedef struct Node
{
	int value;
	struct Node *hext;
} Node_t;

typedef struct Queue {
	Node_t *front;
	Node_t *back;
	int size;
} Queue_t;

Queue_t *init();
int push(Queue_t *queue, int value);
int pop(Queue_t *queue);
void clear(Queue_t *queue);