#include <stdlib.h>
#include <stdio.h>

typedef struct List {
	int value;
	struct List *next;
	struct List *prev;
} List_t;

int push(List_t **head, int value) {
	List_t *tmp = (List_t*)malloc(sizeof(List_t));
	if (tmp == NULL)
		return EXIT_FAILURE;
	tmp->value = value;
	if (*head == NULL) {
		tmp->next = NULL;
		tmp->prev = NULL;
	}
	else {
		tmp->next = *head;
		tmp->prev = NULL;
		(*head)->prev = tmp;
	}
	*head = tmp;
	return EXIT_SUCCESS;
}

int pop(List_t **head) {
	List_t *tmp = *head;
	(*head)->prev = NULL;
	int value = tmp->value;
	*head = tmp->next;
	free(tmp);
	return value;
}

List_t* sort(List_t *head) {

	return head;
}

int main() {
	List_t *list = NULL;
	for (int i = 0; i < 10; i++)
		push(&list, i);

	for (List_t *tmp = list; tmp != NULL; tmp = tmp->next)
		printf("%d ", tmp->value);
	printf("\n");

	while (list) {
		printf("%d ", pop(&list));
	};
	printf("\n");

	system("pause");
	return 0;
}