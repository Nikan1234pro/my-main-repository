#include <iostream>
#include <ctime>
#include <cstdlib>
#include "List.h"
#include <vector>


void print(const List<int> &list) {
	List<int>::iterator it = list.begin();

	for (; it != list.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

int main() {
	List<int> list;
	std::srand(std::time(NULL));
	for(int i = 0; i < 10; i++) {
		list.push_back(std::rand() % 1000);
	}
	print(list);

	list.sort();
	print(list);
	
	for (int i = 0; i < 10; i++)
		list.pop_front();
	
	system("pause");
	return EXIT_SUCCESS;
}