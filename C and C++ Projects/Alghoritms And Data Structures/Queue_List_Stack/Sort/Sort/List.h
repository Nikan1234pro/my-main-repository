#pragma once

template <class T>
class Iterator;

template <class T>
class List {
private:
	struct Node {
		T value;
		Node* next;
		Node *prev;
		Node(T value) : value(value), next(nullptr), prev(nullptr) {
		}
	};


	Node *front;
	Node *back;
	size_t _size;
public:
	using iterator = Iterator<T>;

	List() : front(nullptr), back(nullptr), _size(0) {
	}

	void push_front(T value) {
		Node *tmp = new Node(value);
		if (front == nullptr) {
			front = back = tmp;
			return;
		}
		tmp->next = front;
		front->prev = tmp;
		front = tmp;
		_size++;
	}

	void push_back(T value) {
		Node *tmp = new Node(value);
		if (back == nullptr) {
			front = back = tmp;
			return;
		}
		tmp->prev = back;
		back->next = tmp;
		back = tmp;
		_size++;
	}

	void pop_front() {
		if (front == nullptr)
			throw std::out_of_range("List already clear!");
		Node *tmp = front;
		if (front == back)
			front = back = nullptr;
		else
			front = front->next;
		delete tmp;
		_size--;
	}

	void pop_back() {
		if (back == nullptr)
			throw std::out_of_range("List already clear");
		Node *tmp = back;
		if (front == back)
			front = back = nullptr;
		else
			back = back->prev;
		delete tmp;
		_size--;
	}

	int size() const {
		return _size;
	}

	void sort() {
		Node *new_front = nullptr;
		Node *new_back = nullptr;
		while (front != nullptr) {
			Node *node = front;
			front = front->next;

			if (new_front == nullptr || node->value < new_front->value) {
				node->next = new_front;
				node->prev = nullptr;
				if (new_front != nullptr)
					new_front->prev = node;
				else
					new_back = node;
				new_front = node;
			}
			else {
				Node *tmp = new_front;
				while (tmp->next != nullptr && node->value >= tmp->next->value)
					tmp = tmp->next;

				node->next = tmp->next;
				node->prev = tmp;
				if (tmp->next != nullptr)
					tmp->next->prev = node;
				else
					new_back = node;
				tmp->next = node;
			}
		}
		front = new_front;
		back = new_back;
	}

	iterator begin() const {
		return iterator(front);
	}

	iterator end() const {
		return iterator(nullptr);
	}

	~List() {
		while (front)
		{
			Node *tmp = front;
			front = front->next;
			delete tmp;
		}
	}

	friend class Iterator<T>;
};

template <class T>
class Iterator {
	typename List<T>::Node *ptr;
public:
	Iterator(typename List<T>::Node *p) : ptr(p) {
	}

	Iterator operator++(int) {
		Iterator tmp = *this;
		ptr = ptr->next;
		return tmp;
	}

	Iterator& operator++() {
		ptr = ptr->next;
		return *this;
	}

	T operator*() {
		return ptr->value;
	}

	bool operator==(const Iterator &other) {
		return ptr == other.ptr;
	}

	bool operator!=(const Iterator &other) {
		return ptr != other.ptr;
	}
};