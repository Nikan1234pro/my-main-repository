#pragma once

typedef struct Tree {
	int value;
	struct Tree *left;
	struct Tree *right;
} Tree_t;

Tree_t* add(Tree_t *node, int value);
Tree_t *delete(Tree_t *node, int value);
Tree_t *remove_min(Tree_t *node);
Tree_t *remove_max(Tree_t *node);
Tree_t *find_min(Tree_t *node);
Tree_t *find_max(Tree_t *node);

void print(Tree_t *node);
int is_consist(Tree_t *node, int value);