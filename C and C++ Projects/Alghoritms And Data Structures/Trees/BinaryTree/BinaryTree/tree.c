#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

#define true 1
#define false 0

#define param 0

Tree_t *add(Tree_t *node, int value) {
	if (NULL == node) {
		node = (Tree_t*)malloc(sizeof(Tree_t));
		node->value = value;
		node->left = NULL;
		node->right = NULL;
		return node;
	}
	if (value < node->value)
		node->left = add(node->left, value);
	else if (value > node->value)
		node->right = add(node->right, value);
	return node;
}

int is_consist(Tree_t *node, int value) {
	if (node == NULL)
		return false;
	if (node->value == value)
		return true;
	if (value < node->value)
		return is_consist(node->left, value);
	else
		return is_consist(node->right, value);
}

Tree_t *remove_min(Tree_t *node) {
	if (NULL == node)
		return NULL;
	if (NULL == node->left) {
		Tree_t *tmp = node->right;
		free(node);
		return tmp;
	}
	node->left = remove_min(node->left);
	return node;
}

Tree_t *remove_max(Tree_t *node) {
	if (NULL == node)
		return NULL;
	if (NULL == node->right) {
		Tree_t *tmp = node->left;
		free(node);
		return tmp;
	}
	node->right = remove_max(node->right);
	return node;
}

Tree_t *find_min(Tree_t *node) {
	return (node->left) ? find_min(node->left) : node;
}

Tree_t *find_max(Tree_t *node) {
	return (node->right) ? find_max(node->right) : node;
}

#if param
Tree_t *delete(Tree_t *node, int value) {
	if (NULL == node)
		return NULL;
	if (node->value == value) {
		if (NULL == node->left && NULL == node->right) {
			free(node);
			return NULL;
		}
		if (NULL == node->left) {
			Tree_t *tmp = node->right;
			free(node);
			return tmp;
		}
		if (NULL == node->right) {
			Tree_t *tmp = node->left;
			free(node);
			return tmp;
		}
		if (node->left && node->right) {
			Tree_t *local_max = find_max(node->left);
			node->value = local_max->value;
			node->left = remove_max(node->left);
			return node;
		}
	}

	if (value < node->value)
		node->left = delete(node->left, value);
	else
		node->right = delete(node->right, value);
	return node;
}
#else
Tree_t *delete(Tree_t *node, int key) {
	if (node == NULL)
		return NULL;
	if (key < node->value)
		node->left = delete(node->left, key);
	else if (key > node->value)
		node->right = delete(node->right, key);
	else {
		if (node->right == NULL) {
			Tree_t *tmp = node->left;
			free(node);
			return tmp;
		}
		Tree_t *min = find_min(node->right);
		node->value = min->value;
		node->right = remove_min(node->right);
		return node;
	}
	return node;
}
#endif // param

void print(Tree_t *node) {
	if (NULL == node)
		return;
	printf("%d ", node->value);
	print(node->left);
	print(node->right);
}

