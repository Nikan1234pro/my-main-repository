#include <iostream>
#include <cstdlib>
#include "tree.h"

int main() {
	Tree<int> *tree = nullptr;
	for (int i = 0; i < 15; i++)
		tree = add(tree, i + 1);
	print(tree);
	tree = remove(tree, 4);

	std::cout << '\n';
	print(tree);
	clear(tree);
	system("pause");
	return 0;
}