#pragma once
#define MAX(a, b) ((a > b) ? (a) : (b))
#include <iostream>

template <class T>
struct Tree
{
	T value;
	int height;
	Tree *left;
	Tree *right;
	Tree(T value) : value(value), left(nullptr), right(nullptr), height(1) {
	}
};

template <class T>
int height(Tree<T> *node) {
	return (node) ? node->height : 0;
}

template <class T>
int balance_factor(Tree<T> *node) {
	int h1 = height(node->right);
	int h2 = height(node->left);
	return h2 - h1;
}

template <class T>
void recover_height(Tree<T> *node) {
	node->height = MAX(height(node->left), height(node->right)) + 1;
}

template <class T>
Tree<T> *rotate_left(Tree<T> *q) {
	Tree<T> *p = q->right;
	q->right = p->left;
	p->left = q;
	recover_height(q);
	recover_height(p);
	return p;
}

template <class T>
Tree<T> *rotate_right(Tree<T> *p) {
	Tree<T> *q = p->left;
	p->left = q->right;
	q->right = p;
	recover_height(p);
	recover_height(q);
	return q;
}

template <class T>
Tree<T> *balance(Tree<T> *p) {
	recover_height(p);
	if (balance_factor(p) == -2) {
		if (balance_factor(p->right) > 0)
			p->right = rotate_right(p->right);
		return rotate_left(p);
	}
	if (balance_factor(p) == 2) {
		if (balance_factor(p->left) < 0)
			p->left = rotate_left(p->left);
		return rotate_right(p);
	}
	return p;
}


template <class T>
Tree<T> *add(Tree<T> *node, T key) {
	if (node == nullptr) 
		return new Tree<T>(key);
	
	if (key < node->value)
		node->left = add(node->left, key);
	if (key > node->value)
		node->right = add(node->right, key);
	return balance(node);
}

template <class T>
void print(Tree<T> *node) {
	if (nullptr == node)
		return;
	std::cout << node->value << "  ";
	print(node->left);
	print(node->right);
}

template <class T>
int height(Tree<T> node) {
	if (nullptr == node)
		return 0;
	return MAX(height(node->left), height(node->right)) + 1;
}

template <class T>
Tree<T> *find_min(Tree<T> *node) {
	return (node->left) ? find_min(node->left) : node;
}

template <class T>
Tree<T> *remove_min(Tree<T> *node) {
	if (nullptr == node->left)
		return node->right;
	node->left = remove_min(node->left);
	return balance(node);
}

template <class T>
Tree<T> *remove(Tree<T> *node, T key) {
	if (nullptr == node)
		return nullptr;
	if (key < node->value)
		node->left = remove(node->left, key);
	else if (key > node->value)
		node->right = remove(node->right, key);
	else {
		Tree<T> *q = node->left;
		Tree<T> *r = node->right;
		delete node;
		if (nullptr == r)
			return q;
		Tree<T> *min = find_min(r);
		min->right = remove_min(r);
		min->left = q;
		return balance(min);
	}
	return balance(node);
}

template <class T>
void clear(Tree<T> *node) {
	if (nullptr == node)
		return;
	clear(node->left);
	clear(node->right);
	delete node;
}







