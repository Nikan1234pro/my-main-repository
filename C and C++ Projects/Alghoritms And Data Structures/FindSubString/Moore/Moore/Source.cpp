#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>

typedef unsigned char uchar;

const int substring_size = 20;
const int string_size = 4096;

std::vector<int> get_shift_table(char *string) {
  int length = strlen(string);
  std::vector<int> table(256, length);
  for (int i = 0; i < length - 1; i++)
    table[static_cast<uchar>(string[i])] = length - (i + 1);
  return table;
}

void Moore(std::istream &is, char *substring, std::vector<int> table) {
  int substr_len = strlen(substring);
  char *buffer[2];
  buffer[0] = new char[string_size]{};
  buffer[1] = new char[string_size]{};

  int shift = 1; // absolute buffer shift over a text
  int position = 0;

  is.read(buffer[0], string_size - 1);
  if (is.gcount() < substr_len)
    return;
  else
    buffer[0][is.gcount()] = '\0';
  while (true) {
    if (position >= strlen(buffer[0])) {
      int tmp = strlen(buffer[0]);
      std::swap(buffer[0], buffer[1]);
      is.read(buffer[1], substring_size - 1);
      buffer[1][is.gcount()] = '\0';
      position -= tmp;
      shift += tmp;
    }
    if (position + substr_len <= strlen(buffer[0])) {
      for (int i = substr_len - 1; i >= 0; i--) {
        int current = position + i;
        std::cout << shift + position + i << ' ';
        if (substring[i] != buffer[0][current])
          break;
      }
      position +=
          table[static_cast<uchar>(buffer[0][position + substr_len - 1])];
    }
    if (position + substr_len > strlen(buffer[0])) {
      if (buffer[1][0] == '\0') {
        is.read(buffer[1], string_size - 1);
        buffer[1][is.gcount()] = '\0';
      }
      if (position + substr_len >
          strlen(buffer[0]) +
              strlen(buffer[1])) { // if we reached the end of text
        break;
      }
      for (int i = substr_len - 1; i >= 0; i--) {
        int part = (position + i) / (strlen(buffer[0]));
        int current =
            i + ((part == 0) ? position : position - strlen(buffer[0]));
        std::cout << shift + position + i << ' ';
        if (substring[i] != buffer[part][current])
          break;
      }
      position += table[static_cast<uchar>(
          buffer[1][position - strlen(buffer[0]) + substr_len - 1])];
    }
  }
  delete[] buffer[0];
  delete[] buffer[1];
}

int main() {

  char *substring = new char[substring_size];
  std::ifstream file("in.txt");
  if (!file)
    return 1;
  file.getline(substring, substring_size - 1, '\n');

  Moore(file, substring, get_shift_table(substring));
  return 0;
}