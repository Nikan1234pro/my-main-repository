#include <iostream>
#include <cmath>
#include <algorithm>
#include <iomanip>
#define max(a, b) (((a) > (b)) ? (a) : (b))

int NOD(int a, int b){
	while (b) {
		int c = a % b;
		a = b;
		b = c;
	}
	return a & 0x7FFF;
}

int NOK(int a, int b) {
	return a * b / NOD(a, b);
}

double my_sqrt(double n) {
	double x = 1;
	while (true) {
		double nx = (x + n / x) / 2.0;
		if (std::abs(nx - x) / max(x, nx) < DBL_EPSILON)
			return x;
		x = nx;
	}
}

double DBL_EPS() {
	double d = 1;
	while (1 + d / 2 != 1)
		d /= 2.0;
	return d;
}

int my_pow(int x, int n) {
	return (n == 0) ? 1 : (((n & 1 == 1) ? x : 1) * my_pow(x * x, n / 2));
}

int main() {
	std::cout << NOD(625, 25) << std::endl;
	std::cout << NOK(625, 25) << std::endl;
	
	int c = 0;
	for (double d = 2.4567676535; d < 1000; d += 1.43521456) {
		double res1 = my_sqrt(d);
		double res2 = std::sqrt(d);
		if (std::abs(res1 - res2) / max(res1, res2) > DBL_EPSILON) {
			c++;
			std::cout << std::setw(20) << d << " :	FAIL\n";
		}
		else
			std::cout << std::setw(20) << d << " :	SUCCESS\n";
	}
	std::cout << c << std::endl;
	
	std::cout << DBL_EPS() << std::endl;
	std::cout << DBL_EPSILON << std::endl;
	std::cout << my_pow(2, 9) << std::endl;
	system("pause");
	return 0;
}