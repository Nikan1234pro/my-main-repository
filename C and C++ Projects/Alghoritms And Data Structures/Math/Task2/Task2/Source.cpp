#include <iostream>
#include <algorithm>

//ax + by = 1 solution

std::pair<int, int> solve(int a, int b) {
	int E[][2] = { {1, 0}, {0, 1} };
	int r = a % b;
	while (r) {
		int q = a / b;
		int tmp1 = E[0][1];
		int tmp2 = E[0][0] - q * E[0][1];
		int tmp3 = E[1][1];
		int tmp4 = E[1][0] - q * E[1][1];
		E[0][0] = tmp1;
		E[0][1] = tmp2;
		E[1][0] = tmp3;
		E[1][1] = tmp4;
		a = b;
		b = r;
		r = a % b;
	}
	return std::make_pair(E[0][1], E[1][1]);
}



void foo(char (&buf)[4]) {
	for (int i = 0; i < 3; i++) {
		buf[i] = 'd';
	}
}


int main() {
	int a, b;
	std::cin >> a >> b;
	std::pair<int, int> res = solve(a, b);
	std::cout << "x = " << res.first << "	  y = " << res.second << std::endl;

	char buf[] = { 'a', 'a', 'a', 0 };
	foo(buf);



	std::cout << buf << std::endl;
	system("pause");
	return 0;
}