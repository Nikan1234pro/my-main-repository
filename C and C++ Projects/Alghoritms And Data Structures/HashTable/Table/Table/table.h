#pragma once
#include <vector>
#include <list>
#include <algorithm>

template <class Type, class F>
class HashTable;

template <class Type>
class HashFunctor {	
public:
	HashFunctor(int s) {}
	int operator()(Type key) {
		throw 32;
	}
};

template <>
class HashFunctor<int> {
	int max;
	static const int K = 40503;
	static const int S = 6;
public:
	HashFunctor(int m) : max(m) {
	}
	int operator()(int key) {
		
		return ((K * key) >> S) % max;
	}
};


template <class Type, class F = HashFunctor<Type>>
class HashTable {
	std::vector<std::list<Type>> table;
	int size;
	F functor;
public:
	typedef Type value_type;

	HashTable(int s) : size(s), functor(s){
		table.resize(size);
		int *a = new int[10](3);
		for (int i = 0; i < 10; i++)
			std::cout << a[i] << " ";
	}

	void add(Type data) {
		int i = functor(data);
		if (std::find(table[i].begin(), table[i].end(), data) == table[i].end())
			table[i].push_back(data);
	}

	std::list<Type> get(int i) {
		if (i < 0 || i >= size)
			throw std::out_of_range("out of range");
		return table[i];
	}
};