#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include <sstream>
#include <exception>
#include <stdlib.h>
#include <set>
#define SIZE_LIMIT  3

const char separator = ' ';
using item = std::pair<int, std::fstream&>;

class comparator {
public:
	bool operator()(const item &lhs, const item &rhs) noexcept { 
		return lhs.first < rhs.first;  
	}
};

void sort(std::ifstream &file_in) {
	std::vector<std::fstream> file_parts;
	std::vector<int> buffer(SIZE_LIMIT);

	int file_count = 0;
	while (file_in) {
		int size;
		for (size = 0; size < SIZE_LIMIT && file_in; ) {
			file_in >> buffer[size];
			if (file_in)
				size++;
		}
		if (size == 0)
			break;
		std::sort(buffer.begin(), buffer.begin() + size, std::less<int>());
		std::fstream tmp(std::to_string(file_count), std::ios::in | std::ios::out | std::ios::trunc);
		if (!tmp)
			throw std::ios_base::failure("Can't create file");
		file_parts.push_back(std::move(tmp));
		for (int i = 0; i < size; i++)
			file_parts.back() << buffer[i] << separator;
		file_count++;
	}
	std::multiset <item, comparator> selection;
	for (auto &file_ref : file_parts) {
		file_ref.seekg(std::ios::beg);
		int tmp;
		file_ref >> tmp;
		selection.insert(item(tmp, file_ref));
	}

	std::ofstream file_out("out.txt");
	if (!file_out)
		throw std::ios_base::failure("Can't create file");
	while (!selection.empty()) {
		auto &file_ref = selection.begin()->second;
		file_out << selection.begin()->first << separator;
		int tmp;
		file_ref >> tmp;
		selection.erase(selection.begin());
		if (file_ref)
			selection.insert(item(tmp, file_ref));
		else
			file_ref.close();
	}

	for (int i = 0; i < file_count; i++)
		std::remove(std::to_string(i).c_str());
}

int main() {
	std::ifstream file_in("in.txt", std::ios::in);
	if (!file_in)
		return EXIT_FAILURE;
	sort(file_in);
	system("pause");
	return EXIT_SUCCESS;
}