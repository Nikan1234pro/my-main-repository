#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>

template <class T>
void dble(T& x)
{
	x *= 2;
}

template <class Container>
void print(Container &cont)
{
	for (auto it = cont.begin(); it != cont.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class T>
class Functor
{
public:
	void operator()(T& x)
	{
		x *= 2;
	}
};



template <class T, class Cmp>
std::vector<T> split(std::vector<T> a, std::vector<T> b, Cmp cmp){
	std::vector<T> result;
	typename std::vector<T>::iterator it_a = a.begin();
	typename std::vector<T>::iterator it_b = b.begin();
	while (it_a != a.end() || it_b != b.end()){

		if (it_a == a.end()) {
			result.push_back(T(*it_b));
			it_b++;
			continue;
		}
		
		if (it_b == b.end()) {
			result.push_back(T(*it_a));
			it_a++;
			continue;
		}

		if (cmp(*it_a, *it_b)) {
			result.push_back(T(*it_a));
			it_a++;
		}
		else {
			result.push_back(T(*it_b));
			it_b++;
		}
	}
	return result;
}

template <class T>
std::vector<T> split(std::vector<T> a, std::vector<T> b) {
	return split(a, b, std::less<T>());
}

class Int {
public:
	int *n;
	Int(int i) : n((int*)i) {}
};

int& operator*(Int i) {
	return *(int*)i.n;
}

int main()
{

	int a = (int)malloc(sizeof(int));
	Int b = a;
	*b = 6;



	std::srand(std::time(NULL));
	std::vector<int> vect;
	for (int i = 0; i < 10; i++)
		vect.push_back(std::rand() % 1000);

	std::vector<int> vect1;
	for (int i = 0; i < 14; i++)
		vect1.push_back(std::rand() % 1000);

	std::sort(vect.begin(), vect.end(), std::less<int>());
	std::sort(vect1.begin(), vect1.end(), std::less<int>());

	print(vect);
	print(vect1);
	std::vector<int> res = split(vect, vect1, [](int x, int y) -> bool { return (x < y); });
	print(res);


	std::cout << "\n\n";
	std::for_each(vect.begin(), vect.end(), dble<int>);
	print(vect);

	void (*fptr)(int&) = &dble<int>;
	std::for_each(vect.begin(), vect.end(), fptr);
	print(vect);

	std::for_each(vect.begin(), vect.end(), [](int &x) noexcept -> void { x *= 2;});
	print(vect);

	std::for_each(vect.begin(), vect.end(), Functor<int>());
	print(vect);

	system("pause");
	return 0;
}