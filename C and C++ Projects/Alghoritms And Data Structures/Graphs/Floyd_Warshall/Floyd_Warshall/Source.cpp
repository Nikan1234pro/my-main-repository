#include "graph.h"
#include <iostream>
#include <fstream>
#include <iomanip>


#define MIN(a, b) (((a) < (b)) ? (a) : (b))
using uint = unsigned int;

#define Inf INT_MAX

void Floyd(const Graph &graph, uint *W) {
	int N = graph.vertices();
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			for (int k = 0; k < N; k++)
				W[i * N + j] = MIN(W[i * N + j], W[i * N + k] + W[k * N + j]);
}

int main() {
	int N;
	std::ifstream file("in.txt");
	if (!file)
		return 1;
	file >> N;
	WeightedGraph graph(N);
	graph.fill(file);
	uint *W = new uint[N * N];
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++) {
			W[i * N + j] = (graph[i][j]) ? graph[i][j] : Inf;
		}

	Floyd(graph, W);

	std::cout.setf(std::ios::left, std::ios::adjustfield);
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			if (W[i * N + j] < Inf)
				std::cout << std::setw(5) << W[i * N + j];
			else
				std::cout << std::setw(5) << "oo";
		}
		std::cout << std::endl;
	}
	system("pause");
	return 0;
}