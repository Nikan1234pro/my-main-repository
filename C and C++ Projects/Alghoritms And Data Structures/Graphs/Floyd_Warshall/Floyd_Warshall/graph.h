#pragma once

#include <iostream>

class Graph {
protected:
	int *matrix;
	int V;
	int E;
public:
	Graph(int v) : V(v), E(0) {
		if (V == -1)
			throw std::logic_error("Number of vertex must be > 0!");
		matrix = new int[V * V]{};
	}

	void fill(std::istream &is) {
		int vertex1, vertex2;
		is >> E;
		for (int i = 0; i < E; i++) {
			is >> vertex1 >> vertex2;
			if (vertex1 > V || vertex1 <= 0 ||
				vertex2 > V || vertex2 <= 0)
				throw std::logic_error("Wrong input!");
			int u = --vertex1;
			int v = --vertex2;
			(*this)[u][v] = 1;
			(*this)[v][u] = 1;
		}
	}
	int vertices() const {
		return V;
	}

	int edges() const {
		return E;
	}

	int *operator[] (int i) {
		return matrix + i * V;
	}

	const int * operator[] (int i) const {
		return matrix + i * V;
	}

};

class WeightedGraph : public Graph {
public:
	WeightedGraph(int N) : Graph(N) {
	}

	void fill(std::istream &is) {
		int vertex1, vertex2, edge;
		is >> E;
		for (int i = 0; i < E; i++) {
			is >> vertex1 >> vertex2 >> edge;
			if (vertex1 > V || vertex1 <= 0 ||
				vertex2 > V || vertex2 <= 0)
				throw std::logic_error("Wrong input!");
			int u = --vertex1;
			int v = --vertex2;
			(*this)[u][v] = edge;
			(*this)[v][u] = edge;
		}
	}
};