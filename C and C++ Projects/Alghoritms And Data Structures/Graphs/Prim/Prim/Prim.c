#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>


#define MAX_VERTEX 5000
#define BAD_VERTEX -1
#define INFINITY INT_MAX + 1

enum colors{ RED, BLUE };
typedef unsigned int uint;

int DFS(int *matrix, char *is_visited, int u, int N) {
	is_visited[u] = 1;
	int n = 1;
	for (int v = 0; v < N; v++) {
		if (matrix[u * N + v] && is_visited[v] == 0) {
			n += DFS(matrix, is_visited, v, N);
		}
	}
	return n;
}

int is_spanning(int *matrix, int N) {
	char *is_visited = (char*)calloc(sizeof(char), N);
	if (is_visited == NULL)
		return EXIT_FAILURE;
	int answer = DFS(matrix, is_visited, 0, N);
	free(is_visited);
	if (answer != N)
		return EXIT_FAILURE;
	
	return EXIT_SUCCESS;
}

int is_valid(int N, int M) {
	if (N == 0) {
		printf("no spanning tree");
		return EXIT_FAILURE;
	}
	else if (N == 1) {
		return EXIT_FAILURE;
	}
	else if (N < 0 || N > MAX_VERTEX) {
		printf("bad number of vertices");
			return EXIT_FAILURE;
	}
	else if (M < 0 || M > N * (N - 1) / 2) {
		printf("bad number of edges");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int create_matrix(FILE *file, int *matrix, int N, int M) {
	int vertex1, vertex2;
	uint edge;

	for (int i = 0; i < M; i++) {
		if (fscanf(file, "%d %d %u", &vertex1, &vertex2, &edge) != 3) {
			printf("bad number of lines");
			return EXIT_FAILURE;
		}
		if (vertex1 > N || vertex2 > N || vertex1 <= 0 || vertex2 <= 0) { 
			printf("bad vertex"); 
			return EXIT_FAILURE;
		}
		if (edge < 0 || edge > INT_MAX) 
		{ 
			printf("bad length"); 
			return EXIT_FAILURE; 
		}
		int u = --vertex1;
		int v = --vertex2;
		matrix[u * N + v] = edge;
		matrix[v * N + u] = edge;
	}
	return EXIT_SUCCESS;
}

int get_vertex(int *color, int *weight, int N) {
	int vertex = BAD_VERTEX;
	uint min = INFINITY;

	for (int i = 0; i < N; i++) {
		if (color[i] == RED && weight[i] < min) {
			min = weight[i];
			vertex = i;
		}
	}
	return vertex;
}

int Prime(int *matrix, int *tree, int N, int M) {
	enum colors *color = (enum colors*)malloc(sizeof(enum colors) * N);
	uint *weight = (uint*)malloc(sizeof(uint) * N);

	if (color == NULL || weight == NULL) {
		free(color);
		free(weight);
		return EXIT_FAILURE;
	}
	for (int i = 0; i < N; i++) {
		color[i] = RED;
		weight[i] = INFINITY;
	}
	weight[0] = 0;
	tree[0] = BAD_VERTEX;
	for (int i = 0; i < N - 1; i++) {
		int u = get_vertex(color, weight, N);
		if (u == BAD_VERTEX) {
			printf("no spanning tree");
			free(color);
			free(weight);
			return EXIT_FAILURE;
		}
		color[u] = BLUE;
		for (int v = 0; v < N; v++) {
			if (matrix[u * N + v] != 0 && color[v] == RED && 
				matrix[u * N + v] < weight[v]) {
				tree[v] = u;
				weight[v] = matrix[u * N + v];
			}
		}
	}
	free(color);
	free(weight);
	return EXIT_SUCCESS;
}

int main() {
	int N, M;
	FILE *file = fopen("in.txt", "r");
	if (fscanf(file, "%d %d", &N, &M) == EOF) {
		printf("bad number of lines");
		fclose(file);
		return 0;
	}
	if (is_valid(N, M) == EXIT_SUCCESS) {
		int *matrix = (int*)calloc(sizeof(int), N * N);
		int *tree = (int*)malloc(sizeof(int) * N);
		if (matrix == NULL || tree == NULL) {
			free(matrix);
			free(tree);
			fclose(file);
			return EXIT_FAILURE;
		}
		if (create_matrix(file, matrix, N, M) == EXIT_FAILURE) {
			free(matrix);
			free(tree);
			fclose(file);
			return EXIT_FAILURE;
		}
		if (is_spanning(matrix, N) == EXIT_FAILURE) {
			printf("no spanning tree");
			free(matrix);
			free(tree);
			fclose(file);
			return EXIT_FAILURE;
		}
		
		if (Prime(matrix, tree, N, M) == EXIT_SUCCESS) {
			for (int i = 1; i < N; i++) {
				printf("%d %d\n", tree[i] + 1, i + 1);
			}
		}
		free(matrix);
		free(tree);
	}
	fclose(file);
	return EXIT_SUCCESS;
}