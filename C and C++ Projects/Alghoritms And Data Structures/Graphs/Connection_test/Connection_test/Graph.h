#pragma once

#pragma once

#include <iostream>

class Graph {
	int *matrix;
	int N;
public:
	Graph(int N) : N(N) {
		if (N == -1)
			throw std::logic_error("Number of vertex must be > 0!");
		matrix = new int[N * N]{};
	}

	void fill(std::istream &is, int M) {
		int vertex1, vertex2;
		for (int i = 0; i < M; i++) {
			is >> vertex1 >> vertex2;
			if (vertex1 > N || vertex1 <= 0 ||
				vertex2 > N || vertex2 <= 0)
				throw std::logic_error("Wrong input!");
			int u = --vertex1;
			int v = --vertex2;
			(*this)[u][v] = 1;
			(*this)[v][u] = 1;
		}
	}
	int size() const {
		return N;
	}

	int *operator[] (int i) {
		return matrix + i * N;
	}

	const int * operator[] (int i) const {
		return matrix + i * N;
	}

};

class WeightedGraph : public Graph {
public:
	WeightedGraph(int N) : Graph(N) {
	}

	void fill(std::istream &is, int M) {
		int vertex1, vertex2, edge;
		for (int i = 0; i < M; i++) {
			is >> vertex1 >> vertex2 >> edge;
			if (vertex1 > size() || vertex1 <= 0 ||
				vertex2 > size() || vertex2 <= 0)
				throw std::logic_error("Wrong input!");
			int u = --vertex1;
			int v = --vertex2;
			(*this)[u][v] = edge;
			(*this)[v][u] = edge;
		}
	}
};