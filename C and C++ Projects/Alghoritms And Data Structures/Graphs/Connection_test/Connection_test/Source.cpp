#include <iostream>
#include <vector>
#include <fstream>
#include "Graph.h"

//This alghoritm uses DFS. We count number of visited vertices. If it less than N - graph isn't connected


enum colors{ WHITE, GREY, BLACK };
typedef std::vector<colors> vect_t;

int test(int u, const Graph &graph, vect_t &color) {
	color[u] = GREY;
	int n = 1;
	for (int i = 0; i < graph.size(); i++) {
		if (graph[i][u] && color[i] == WHITE) {
			n += test(i, graph, color);
		}
	}
	color[u] = BLACK;
	return n;
}

int main() {
	int N, M;
	std::ifstream fin("in.txt");
	if (!fin)
		return 1;
	fin >> N >> M;
	Graph graph(N);
	graph.fill(fin, M);

	vect_t color(N, WHITE);
	int k = test(0, graph, color);
	if (k == graph.size())
		std::cout << "OK\n";
	else
		std::cout << "BAD\n";

	system("pause");
	return 0;
}