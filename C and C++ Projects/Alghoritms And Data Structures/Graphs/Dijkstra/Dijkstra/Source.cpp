#include <iostream>
#include <fstream>
#include "matrix.h"
#define INFINITY INT_MAX

typedef unsigned int uint;

enum colors {WHITE, BLACK};

template <class T>
void Dijkstra(T &matrix, int N, int start, int end) {
	start -= 1;
	end -= 1;

	colors *color = new colors[N];
	uint *weight = new uint[N];

	for (int i = 0; i < N; i++) {
		weight[i] = INFINITY;
		color[i] = WHITE;
	}
	weight[start] = 0;

	uint min_index;
	uint min_weight;
	do {
		min_index = -1;
		min_weight = INFINITY;
		for (int i = 0; i < N; i++) {
			if (weight[i] < min_weight && color[i] == WHITE) {
				min_index = i;
				min_weight = weight[i];
			}
		}

		if (min_index != -1) {
			for (int i = 0; i < N; i++) {
				if (matrix[min_index][i] != 0) {
					uint tmp = min_weight + matrix[min_index][i];
					if (weight[i] > tmp)
						weight[i] = tmp;
				}
			}
			color[min_index] = BLACK;
		}	
		
	} while (min_index != -1);

	for (int i = 0; i < N; i++) {
		std::cout << start + 1 << " -> " << i + 1 << " = " << weight[i] << std::endl;
	}

	int *path = new int[N];
	int k = 0;
	path[k++] = end + 1;
	int vertex = end;
	while (vertex != start) {
		for (int i = 0; i < N; i++) {
			if (matrix[vertex][i]) {
				if (weight[i] + matrix[vertex][i] == weight[vertex]) {
					path[k++] = i + 1;
					vertex = i;
					break;
				}
			}
		}
	}
	std::cout << "PATH:" << std::endl;
	for (int i = k - 1; i >= 0; i--)
		std::cout << path[i] << "  ";
	std::cout << std::endl;

	delete[] color;
	delete[] weight;
	delete[] path;
}

int main() {
	int N;
	int M;
	int start, end;
	std::fstream file("in.txt");
	if (!file)
		return 1;
	file >> N >> M >> start >> end;
	Matrix matrix(N, N);
	matrix.fill(file, M);

	Dijkstra(matrix, N, start, end);

	system("pause");
	return 0;
}