#pragma once
#include <stdexcept>
#include <istream>

class Matrix {
	int *matrix;
	int N;		//rows
	int M;		//cols

public:
	Matrix(int N, int M) : N(N), M(M) {
		matrix = new int[N * M] {};
	}

	int *operator[](int i) {
		if (i > N - 1)
			throw std::out_of_range("Out of range");
		return matrix + i * M;
	}

	void fill(std::istream &is, int count) {
		int vertex1, vertex2, weight;
		for (int i = 0; i < count; i++) {
			is >> vertex1 >> vertex2 >> weight;
			(*this)[vertex1 - 1][vertex2 - 1] = weight;
			(*this)[vertex2 - 1][vertex1 - 1] = weight;
		}
	}

	~Matrix() {
		delete[] matrix;
	}
};