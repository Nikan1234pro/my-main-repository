#include "Graph.h"
#include <fstream>
#include <queue>
#include <vector>

enum colors { WHITE, GREY, BLACK };
using vect_t = std::vector<colors>;

typedef unsigned int uint;

bool BFS(const Graph &graph, int start, int end) {
	start--;
	end--;
	vect_t color(graph.size(), WHITE);
	std::queue<int> queue;
	queue.push(start);
	while (queue.size()) {
		int vertex = queue.front();
		if (vertex == end)
			return true;

		for (int i = 0; i < graph.size(); i++) {
			if (graph[vertex][i] && color[i] == WHITE) {
				color[i] = GREY;
				queue.push(i);
			}	
		}
		queue.pop();
		color[vertex] = BLACK;
	}
	return false;
}

bool DFS(const Graph &graph, int start, int end, vect_t &color) {
	bool result = false;
	color[start] = GREY;
	if (start == end)
		return true;
	for (int i = 0; i < graph.size(); i++) {
		if (graph[i][start] && color[i] == WHITE)
			result = DFS(graph, i, end, color);
	}
	color[start] == BLACK;
	return result;
}

bool DFS(const Graph &graph, int start, int end) {
	start--;
	end--;
	vect_t color(graph.size(), WHITE);
	return DFS(graph, start, end, color);
}

int main() {
	int N, M;
	std::fstream file("in.txt");
	if (!file)
		return 1;
	file >> N >> M;
	Graph graph(N);
	graph.fill(file, M);
	std::cout << BFS(graph, 4, 4) << std::endl;
	std::cout << DFS(graph, 4, 4) << std::endl;
	system("pause");
	return 0;
}