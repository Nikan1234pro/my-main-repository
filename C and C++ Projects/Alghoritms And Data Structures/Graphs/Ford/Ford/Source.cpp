#include <fstream>
#include <iostream>
#include <vector>

#define inf INT_MAX

struct edge {
	int u;  
	int v;
	int weight;
	edge(int u, int v, int w) : u(u), v(v), weight(w) {
	}
};

using vect_t = std::vector<edge>;
using uint = unsigned int;

void Ford(const vect_t &edges, int N, int M, int s, int e) {
	std::vector<int> dist(N, inf);
	std::vector<int> parent(N, -1);
	std::vector<int> path;
	dist[s] = 0;
	for (int i = 0; i < N - 1; i++) {
		for (int j = 0; j < M; j++)
		{
			if (dist[edges[j].u] < inf)
			{
				if (dist[edges[j].v] > dist[edges[j].u] + edges[j].weight) {
					dist[edges[j].v] = dist[edges[j].u] + edges[j].weight;
					parent[edges[j].v] = edges[j].u;
				}
					
			}
		}
	}	
	for (int i = 0; i < N; i++) {
		std::cout << s + 1 << "->" << i + 1 << "	";
		if (dist[i] < inf)
			std::cout << dist[i];
		else
			std::cout << "oo";
		std::cout << std::endl;
	}

	if (dist[e] == inf) {
		std::cout << "No path" << std::endl;
		return;
	}

	for (int p = e; p != s; p = parent[p])
		path.push_back(p + 1);
	path.push_back(s + 1);
	std::reverse(path.begin(), path.end());

	std::cout << "Path" << std::endl;
	for (int i = 0; i < path.size() - 1; i++)
		std::cout << path[i] << "->";
	std::cout << path[path.size() - 1] << std::endl;
}

int main() {
	int N, M;
	std::ifstream file("in.txt");
	if (!file)
		return 1;
	file >> N >> M;
	vect_t edges;
	for (int i = 0; i < M; i++) {
		int vertex1, vertex2, weight;
		file >> vertex1 >> vertex2 >> weight;
		edges.push_back(edge(vertex1 - 1, vertex2 - 1, weight));
	}
	Ford(edges, N, M, 0, 6);

	system("pause");
	return 0;
}