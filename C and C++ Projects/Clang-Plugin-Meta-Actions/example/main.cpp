#include "test.h"
#include <iostream>
#include <vector>

#include "../parser/interpreter/meta.h"


META(
    for (int i = 0; i < 1000; ++i) {
        std::vector<int> v(10'000'000);
        std::cout << i << std::endl;
    }
    inject(R"(void test_call() {
        std::cout << "HELLO" << std::endl;
    })");

    return;
)

int main() {
    std::cout << "Result is " << foo() << std::endl; 

    META(
        inject("test_call();");
    )

    return 0;
}