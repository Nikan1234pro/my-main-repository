#include "../parser/interpreter/meta.h"
#include <iostream>
#include <sstream>
#include <fstream>

int foo() {
    int result = 0;

    META_BEGIN 
        int n = 0;
        std::cout << "Input n: ";
        std::cin >> n;

        std::stringstream ss;
        ss << "result = " << n << ";";
        inject(ss.str().c_str());
    META_END
    return result;
}