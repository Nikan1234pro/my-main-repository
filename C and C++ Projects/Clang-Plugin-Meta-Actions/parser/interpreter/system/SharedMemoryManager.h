#ifndef INTERPRETER_PROCESS_SHARED_MEMEORY_MANAGER_H
#define INTERPRETER_PROCESS_SHARED_MEMEORY_MANAGER_H

#include <memory>
#include <string>

#include "Types.h"

namespace meta {
namespace sys {

class SharedMemoryManager {
  struct MemoryData;

public:
  SharedMemoryManager(const TSTRING &name, std::size_t size);
  ~SharedMemoryManager();

  void *createSharedMemory();
  void *openSharedMemory();

private:
  const TSTRING name;
  const std::size_t size;

  /* Pointer to incomplete type */
  std::unique_ptr<MemoryData> data;
};

} // namespace sys
} // namespace meta

#endif