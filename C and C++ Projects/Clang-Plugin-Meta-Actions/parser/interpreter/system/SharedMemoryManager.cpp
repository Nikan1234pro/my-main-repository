#include "SharedMemoryManager.h"

#include <memory>
#include <stdexcept>

meta::sys::SharedMemoryManager::SharedMemoryManager(const TSTRING &name,
                                                    std::size_t size)
    : name(name), size(size) {}

meta::sys::SharedMemoryManager::~SharedMemoryManager() = default;

#if defined(unix) || defined(__unix__) || defined(__unix)

#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define HANDLE_ERROR throw std::runtime_error(strerror(errno));

namespace meta {
namespace sys {

struct SharedMemoryManager::MemoryData {
  TSTRING name;
  int handle;

  constexpr static int SHARED_MEMORY_FAIL = -1;

  MemoryData(const TSTRING &name, int handle) : name(name), handle(handle) {}

  ~MemoryData() {
    int result = close(handle);
    if (result == SHARED_MEMORY_FAIL) {
      perror("close");
    }
    result = shm_unlink(name.c_str());
    if (result == SHARED_MEMORY_FAIL) {
      perror("shm_unlink");
    }
  }
};

void *SharedMemoryManager::createSharedMemory() {
  int handle = shm_open(name.c_str(), O_CREAT | O_RDWR, S_IRWXU);
  if (handle == MemoryData::SHARED_MEMORY_FAIL) {
    HANDLE_ERROR
  }
  /* Init RAII object */
  data = std::make_unique<MemoryData>(name, handle);

  int result = ftruncate(handle, size);
  if (result == MemoryData::SHARED_MEMORY_FAIL) {
    HANDLE_ERROR
  }
  void *buffer =
      mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, handle, 0);
  if (!buffer) {
    HANDLE_ERROR;
  }
  return buffer;
}

void *SharedMemoryManager::openSharedMemory() {
  int handle = shm_open(name.c_str(), O_RDONLY, 0);
  if (handle == MemoryData::SHARED_MEMORY_FAIL) {
    HANDLE_ERROR;
  }
  /* Init RAII object */
  data = std::make_unique<MemoryData>(name, handle);

  void *buffer = mmap(NULL, size, PROT_READ, MAP_SHARED, handle, 0);
  if (data == NULL) {
    HANDLE_ERROR;
  }
  return buffer;
}

} // namespace sys
} // namespace meta

#elif defined(_WIN32) || defined(_WIN64)

#include <Windows.h>

namespace meta {
namespace sys {

struct SharedMemoryManager::MemoryData {
  HANDLE handle;
  LPVOID buffer;

  MemoryData(HANDLE handle, LPVOID buffer) : handle(handle), buffer(buffer) {}

  ~MemoryData() {
    UnmapViewOfFile(buffer);
    CloseHandle(handle);
  }
};

void *SharedMemoryManager::createSharedMemory() {

  HANDLE handle = CreateFileMapping(INVALID_HANDLE_VALUE, nullptr,
                                    PAGE_READWRITE, 0, size, name.c_str());
  if (handle == nullptr) {
    throw std::runtime_error("Can't create file mapping");
  }

  LPVOID buffer = MapViewOfFile(handle, FILE_MAP_ALL_ACCESS, 0, 0, size);
  if (buffer == nullptr) {
    CloseHandle(handle);
    throw std::runtime_error("Can't map view of file");
  }
  /* Init RAII object */
  data = std::make_unique<MemoryData>(handle, buffer);
  return buffer;
}

void *SharedMemoryManager::openSharedMemory() {
  HANDLE handle = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, name.c_str());
  if (handle == nullptr) {
    throw std::runtime_error("Can't open file mapping");
  }
  LPVOID buffer = MapViewOfFile(handle, FILE_MAP_ALL_ACCESS, 0, 0, size);
  if (buffer == nullptr) {
    CloseHandle(handle);
    throw std::runtime_error("Can't map view of file");
  }
  /* Init RAII object */
  data = std::make_unique<MemoryData>(handle, buffer);
  return buffer;
}

} // namespace sys
} // namespace meta

#endif