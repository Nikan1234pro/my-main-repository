#include "DataReceiver.h"

#if defined(unix) || defined(__unix__) || defined(__unix)

#elif defined(_WIN32) || defined(_WIN64)

#include <cassert>
#include <future>
#include <stdexcept>

#define INSTANCES 1
#define EVENT_COUNT 2
#define CANCEL_EVENT_INDEX (EVENT_COUNT - 1)

namespace {
/* Don't use exceptions in child thread function */
template <class OutIter>
void dataReceiver(OutIter iter, const std::string &pipe_name,
                  HANDLE cancel_event) {
  HANDLE_WRAPPER connect_event =
      make_wrapper(CreateEvent(NULL, TRUE, TRUE, NULL));
  if (!connect_event) {
    throw std::runtime_error("Can't create events");
    return;
  }
  HANDLE events[EVENT_COUNT] = {connect_event.get(), cancel_event};

  /* Create pipe */

  HANDLE_WRAPPER pipe = make_wrapper(CreateNamedPipeA(
      pipe_name.c_str(), PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
      PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, INSTANCES,
      RECEIVER_BUFSIZE, RECEIVER_BUFSIZE, NMPWAIT_USE_DEFAULT_WAIT, NULL));
  if (pipe.get() == INVALID_HANDLE_VALUE) {
    throw std::runtime_error("Can't create pipe");
  }
  OVERLAPPED overlap = {0};
  overlap.hEvent = connect_event.get();

  while (true) {
    if (ConnectNamedPipe(pipe.get(), &overlap)) {
      throw std::runtime_error("Connect failed");
    }
    /* Wait for connection on cancellation */
    auto dd_wait = WaitForMultipleObjects(EVENT_COUNT, events, FALSE, INFINITE);
    int idx = dd_wait - WAIT_OBJECT_0;
    if (idx < 0 || idx >= EVENT_COUNT) {
      throw std::runtime_error("Unknown event");
    }

    /* Check cancellation */
    if (idx == CANCEL_EVENT_INDEX) {

      return;
    }
    /* Receive data */
    std::string result;

    char buffer[RECEIVER_BUFSIZE] = {0};
    DWORD bytes = 0;
    while (ReadFile(pipe.get(), buffer, RECEIVER_BUFSIZE, &bytes, NULL)) {
      result.insert(result.end(), buffer, buffer + bytes);
    }
    /* Reading until end of pipe */
    if (GetLastError() == ERROR_BROKEN_PIPE) {
      *iter++ = result;
    } else {
      throw std::runtime_error("Can't read data");
    }
    /* Disconnect client */
    if (!DisconnectNamedPipe(pipe.get())) {
      throw std::runtime_error("Can't disconnect");
    }
  }
}

} // namespace

namespace meta {
namespace sys {

class DataReceiver::ThreadWrapper {
  std::string name;
  std::future<void> future;
  HANDLE_WRAPPER cancel_event;

public:
  explicit ThreadWrapper(const char *name)
      : name(name), future(), cancel_event(make_wrapper()) {}

  template <class Iter> void run(Iter iter) {
    cancel_event = make_wrapper(CreateEvent(NULL, TRUE, FALSE, NULL));
    if (!cancel_event) {
      throw std::runtime_error("Can't run receiver");
    }
    future = std::async(std::launch::async, dataReceiver<Iter>, iter,
                        std::cref(name), cancel_event.get());
  }

  void shutdown() {
    if (!cancel_event) {
      return;
    }

    /* Cancel work */
    SetEvent(cancel_event.get());

    /* Get exceptions */
    future.get();
  }

  ~ThreadWrapper() { SetEvent(cancel_event.get()); }
};

DataReceiver::DataReceiver(const char *name)
    : thread_wrapper(new ThreadWrapper(name)) {}

void DataReceiver::run() {
  auto iter = std::back_inserter(data);
  thread_wrapper->run(iter);
}

DataReceiver::data_list DataReceiver::shutdown() {
  thread_wrapper->shutdown();
  return std::move(data);
}

DataReceiver::~DataReceiver() = default;

} // namespace sys
} // namespace meta

#endif