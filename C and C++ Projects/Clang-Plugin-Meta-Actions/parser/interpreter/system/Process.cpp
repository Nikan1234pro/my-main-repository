#include "Process.h"

#include <stdexcept>

#if defined(unix) || defined(__unix__) || defined(__unix)

#include <algorithm>
#include <cstring>
#include <sys/wait.h>

namespace meta {
namespace sys {

#define CHILD_PID 0
#define SYSTEM_FAIL -1

#define HANDLE_PROCESS_ERROR throw std::runtime_error(strerror(errno));

Process::Process(const TSTRING &pathname, const std::vector<TSTRING> &args) {
  std::vector<TCHAR *> execute_args;
  execute_args.reserve(args.size() + 1);
  execute_args.push_back(const_cast<TCHAR *>(pathname.c_str()));

  std::transform(
      std::begin(args), std::end(args), std::back_inserter(execute_args),
      [](const std::string &arg) { return const_cast<char *>(arg.c_str()); });

  process = fork();
  switch (process) {
  case CHILD_PID: {
    execve(execute_args.front(), execute_args.data(), nullptr);
    /* Newer return... */
    HANDLE_PROCESS_ERROR
  }
  case SYSTEM_FAIL:
    HANDLE_PROCESS_ERROR
  }
}

void Process::waitProcess() const {
  if (waitpid(process, NULL, WUNTRACED | WCONTINUED) == SYSTEM_FAIL) {
    HANDLE_PROCESS_ERROR
  }
}

Process::~Process() = default;

} // namespace sys
} // namespace meta

#elif defined(_WIN32) || defined(_WIN64)

#include <sstream>

namespace meta {
namespace sys {

Process::Process(const TSTRING &pathname, const std::vector<TSTRING> &args) {
  ZeroMemory(&process, sizeof(process));

  STARTUPINFO startup;
  ZeroMemory(&startup, sizeof(startup));
  startup.cb = sizeof(startup);

  std::basic_stringstream<TCHAR> wstream;
  wstream << "\"" << pathname.c_str() << "\"";

  std::size_t i = 0;
  constexpr wchar_t sep = L' ';
  for (auto &arg : args) {
    wstream << sep << arg.c_str();
  }

  TSTRING command_line = wstream.str();
  if (!CreateProcess(nullptr, const_cast<TCHAR *>(command_line.c_str()),
                     nullptr, nullptr, FALSE, 0, nullptr, nullptr, &startup,
                     &process)) {
    throw std::runtime_error("Can't create process");
  }
}

int Process::waitProcess() const {
  if (WaitForSingleObject(process.hProcess, INFINITE) != WAIT_OBJECT_0) {
    throw std::runtime_error("Can't wait process");
  }

  DWORD exit_code = 0;
  if (!GetExitCodeProcess(process.hProcess, &exit_code)) {
    throw std::runtime_error("Can't get execution result");
  }
  return static_cast<int>(exit_code);
}

Process::~Process() {
  CloseHandle(process.hProcess);
  CloseHandle(process.hThread);
}

TSTRING getExecutablePath() {
  TCHAR executable_path[MAX_PATH];
  GetModuleFileName(nullptr, executable_path, MAX_PATH);

  return executable_path;
}

} // namespace sys
} // namespace meta

#endif
