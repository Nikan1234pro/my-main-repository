#ifndef INTERPRETER_PROCESS_DATARECEIVER_H
#define INTERPRETER_PROCESS_DATARECEIVER_H

#include "Types.h"

#include <list>
#include <memory>

#define RECEIVER_BUFSIZE 4096

namespace meta {
namespace sys {

class DataReceiver {
public:
  using data_list = std::list<std::string>;

  explicit DataReceiver(const char *name);
  ~DataReceiver();

  void run();
  data_list shutdown();

private:
  class ThreadWrapper;
  std::unique_ptr<ThreadWrapper> thread_wrapper;

  data_list data;
};

} // namespace sys
} // namespace meta

#endif