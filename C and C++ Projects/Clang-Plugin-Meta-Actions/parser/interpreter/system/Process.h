#ifndef INTERPRETER_PROCESS_PROCESS_H
#define INTERPRETER_PROCESS_PROCESS_H

#include "Types.h"

#include <string>
#include <vector>

#if defined(unix) || defined(__unix__) || defined(__unix)
#include <unistd.h>
using process_id = pid_t;
#elif defined(_WIN32) || defined(_WIN64)
#include <Windows.h>
using process_id = PROCESS_INFORMATION;
#else
static_assert(false, "Unsupported platform");
#endif

namespace meta {
namespace sys {

class Process {
public:
  Process(const TSTRING &pathname, const std::vector<TSTRING> &args);
  int waitProcess() const;
  ~Process();

  const process_id &getProcessId() const { return process; }

private:
  process_id process;
};

TSTRING getExecutablePath();

} // namespace sys
} // namespace meta

#endif