#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING

#include "jit/MetaJit.h"

#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/TargetSelect.h"

#include "MetaInterpreter.h"
#include "system/Process.h"
#include "system/SharedMemoryManager.h"

#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sstream>
#include <stdexcept>

#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

using namespace llvm;

#include "Error.h"

#define ENTRY_FUNC_NAME "main"
#define MEM_BUFFER_NAME "MEM_BUF"
#define LINKS_FILE_NAME "links.txt"

#if defined(unix) || defined(__unix__) || defined(__unix)
#define META_LIBRARY_NAME "Meta.so"
#elif defined(_WIN32) || defined(_WIN64)
#define META_LIBRARY_NAME "Meta.dll"
#endif

namespace {
template <class T> T fromString(std::string const &s) {
  std::istringstream stream{s};
  T value;
  stream >> std::noskipws >> value;

  if (stream.fail()) {
    throw std::runtime_error("Wrong input");
  }

  stream.peek();
  if (!stream.eof()) {
    throw std::runtime_error("Wrong input");
  }
  return value;
}

std::array<const char *, 2> META_ARGS = {"a.out", PIPE_NAME};

} // namespace

#define ARGS_COUNT 4

using namespace meta;
using namespace meta::sys;
using namespace meta::jit;

int main(int argc, char **argv) {
  setlocale(LC_ALL, "RUS");

  InitLLVM X(argc, argv);
  llvm::InitializeNativeTarget();
  llvm::InitializeNativeTargetAsmPrinter();
  llvm::InitializeNativeTargetAsmParser();

  if (argc != ARGS_COUNT) {
    reportError("Wrong args count");
  }
  const char *prog_name = argv[0];

  try {
    /* First arg - shared memory name */
    const char *mem_name_ptr = argv[1];
    TSTRING shared_memory_name(mem_name_ptr,
                               mem_name_ptr + strlen(mem_name_ptr));

    /* Second arg - size of shared memory */
    std::size_t size = fromString<std::size_t>(argv[2]);

    /* Third arg - path to interpreter links file */
    const char *link_file_path = argv[3];

    /* Open shared memory */
    SharedMemoryManager manager(shared_memory_name, size);
    const char *data =
        reinterpret_cast<const char *>(manager.openSharedMemory());

    llvm::StringRef stringRef(data, size);
    llvm::MemoryBufferRef buffer(stringRef, MEM_BUFFER_NAME);

    /* Create JIT */
    auto executable_directory = fs::path(getExecutablePath()).parent_path();

    MetaJit jit;

    /* Add meta library */
    jit.addDynamicLib((executable_directory / META_LIBRARY_NAME).string());

    std::ifstream link_file(link_file_path);
    if (!link_file.is_open()) {
      reportError(prog_name, "links.txt file not found");
    }
    while (true) {
      std::string libpath;
      std::getline(link_file, libpath);
      if (!link_file) {
        break;
      }
      jit.addDynamicLib(libpath);
    }
    /* Add module */
    jit.addModuleFromBuffer(buffer);

    /* Execute module */
    auto entry_function =
        (int (*)(int, const char **))jit.lookup(ENTRY_FUNC_NAME);

    if (!entry_function) {
      reportError(prog_name, "Could't find entry function");
    }
    entry_function(META_ARGS.size(), META_ARGS.data());

  } catch (std::exception &e) {
    reportError(prog_name, e.what());
  }
  exit(EXIT_SUCCESS);
}
