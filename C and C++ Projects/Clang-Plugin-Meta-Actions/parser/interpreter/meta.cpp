#include "meta.h"

#include <stdexcept>

#define MIN_ARGS_COUNT 2

#include "system/DataReceiver.h"

namespace {
std::string pipe_name;
}

extern "C++" META_API void InitMeta(int argc, const char *argv[]) {
  if (argc < MIN_ARGS_COUNT) {
    throw std::runtime_error("Not enough args");
  }
  pipe_name.assign(argv[1]);
}

#if defined(unix) || defined(__unix__) || defined(__unix)

#elif defined(_WIN32) || defined(_WIN64)

#include <fstream>

#pragma comment(linker, "/export:??_7type_info@@6B@")
#pragma comment(                                                               \
    linker,                                                                    \
    "/export:?_Facet_Register@std@@YAXPEAV_Facet_base@1@@Z=?FacetHandler@@YAXPEAV_Facet_base@std@@@Z")
void FacetHandler(std::_Facet_base *base) { std::_Facet_Register(base); }

#define MAX_TRANSFER_SIZE DWORD(RECEIVER_BUFSIZE)
#define PIPE_WAIT_TIMEOUT 5000

extern "C++" META_API void inject(const char *code) {
  HANDLE_WRAPPER pipe = make_wrapper(nullptr);
  while (1) {
    pipe = make_wrapper(CreateFileA(pipe_name.c_str(),
                                    GENERIC_READ | GENERIC_WRITE, 0, NULL,
                                    OPEN_EXISTING, 0, NULL));
    if (pipe.get() != INVALID_HANDLE_VALUE) {
      break;
    }
    if (GetLastError() != ERROR_PIPE_BUSY) {
      throw std::runtime_error("Can't open pipe");
    }
    /* Wait pipe instance for PIPE_WAIT_TIMEOUT ms */
    if (!WaitNamedPipeA(pipe_name.c_str(), PIPE_WAIT_TIMEOUT)) {
      throw std::runtime_error("Could not open pipe: timed out");
    }
  }

  /* Send data */
  DWORD bytes_written = 0;
  DWORD bytes_to_write = strlen(code);

  while (bytes_written < bytes_to_write) {
    DWORD bytes_remained = bytes_to_write - bytes_written;

    /* Don't send more than MAX_TRANSFER_SIZE bytes at once */
    bytes_remained = (bytes_remained > MAX_TRANSFER_SIZE) ? MAX_TRANSFER_SIZE
                                                          : bytes_remained;
    DWORD temp = 0;
    if (!WriteFile(pipe.get(), code + bytes_written, bytes_remained, &temp,
                   nullptr)) {
      throw std::runtime_error("Can't send data");
    }
    bytes_written += temp;
  }
  if (bytes_written != bytes_to_write) {
    throw std::runtime_error("Unknown error");
  }
}

#endif
