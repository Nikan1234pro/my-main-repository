#include "MetaInterpreterDriver.h"
#include "MetaInterpreter.h"

#include "system/DataReceiver.h"
#include "system/Process.h"
#include "system/SharedMemoryManager.h"

#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"

#include <sstream>
#include <stdexcept>

#include "Error.h"


#define SHARED_MEM_NAME TEXT("/meta_plugin_shared")

namespace {
template <class T> TSTRING to_string(T &&value) {
  std::basic_stringstream<TCHAR> stream;
  stream << std::forward<T>(value);
  return stream.str();
}
} // namespace

namespace meta {

MetaInterpreterDriver::MetaInterpreterDriver()
    : shared_memory_name(SHARED_MEM_NAME) {
  auto cur_path = fs::path(sys::getExecutablePath()).parent_path();
  path_to_interpreter = cur_path / INTERPRETER_EXECUTABLE_NAME;
  path_to_link_file = cur_path / INTERPRETER_LINKS_FILE_NAME;
}

void MetaInterpreterDriver::setPathToInterpreter(fs::path path) {
  path_to_interpreter = std::move(path);
}

void MetaInterpreterDriver::setPathToLinkFile(fs::path path) {
  path_to_link_file = std::move(path);
}

MetaInterpreterDriver &MetaInterpreterDriver::getInstance() {
  static MetaInterpreterDriver instance;
  return instance;
}

llvm::Error
MetaInterpreterDriver::execute(MetaBlock &meta,
                               std::unique_ptr<llvm::Module> Module) {
  try {
    TSTRING executable_name = path_to_interpreter.generic_string<TCHAR>();

    /* Dump module to string */
    std::string module_text{};
    llvm::raw_string_ostream stream{module_text};
    stream << *Module;
    stream.flush();

    /* Compute size */
    std::size_t required_size = static_cast<int>(module_text.size() + 1);
    TSTRING required_size_str = to_string(required_size);

    /* Create shared memory and copy data */
    sys::SharedMemoryManager memory_manager{shared_memory_name, required_size};
    void *shared_memory = memory_manager.createSharedMemory();
    std::memcpy(shared_memory, module_text.c_str(), required_size);

    /* Run interpreter process */
    std::vector<TSTRING> args;
    args.push_back(shared_memory_name);
    args.push_back(required_size_str);
    args.push_back(path_to_link_file.generic_string<TCHAR>());

    /* Run receiver before child process creation */
    sys::DataReceiver receiver(PIPE_NAME);
    receiver.run();

    sys::Process process(executable_name, args);
    if (process.waitProcess() != EXIT_SUCCESS) {
      return makeError("Interpreter failed");
    }

    /* Shutdown receiver after child process termination */
    for (auto &code : receiver.shutdown()) {
      meta.addInjectionCode(std::move(code));
    }

  } catch (const std::exception &e) {
    return makeError(e.what());
  }
  return llvm::Error::success();
}

} // namespace meta
