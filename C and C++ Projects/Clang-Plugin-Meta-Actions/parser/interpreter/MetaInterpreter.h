#ifndef INTERPRETER_META_INTERPRETER_H
#define INTERPRETER_META_INTERPRETER_H

#include "Types.h"

#define INTERPRETER_EXECUTABLE_NAME TEXT("MetaInterpreter")
#define INTERPRETER_LINKS_FILE_NAME TEXT("links.txt")
#define PIPE_NAME "\\\\.\\pipe\\meta_plugin_pipe"

#endif