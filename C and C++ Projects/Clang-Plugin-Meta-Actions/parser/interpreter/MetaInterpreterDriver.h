#ifndef INTERPRETER_META_INTERPRETER_DRIVER_H
#define INTERPRETER_META_INTERPRETER_DRIVER_H

#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING

#include <experimental/filesystem>
#include <list>
#include <memory>
#include <string>

#include "llvm/IR/Module.h"
#include "llvm/Support/Error.h"

#include "MetaBlock.h"
#include "Types.h"

namespace meta {

namespace fs = std::experimental::filesystem;

class MetaInterpreterDriver {
  const TSTRING shared_memory_name;

  fs::path path_to_interpreter;
  fs::path path_to_link_file;

  MetaInterpreterDriver();

public:
  static MetaInterpreterDriver &getInstance();
  void setPathToInterpreter(fs::path path);
  void setPathToLinkFile(fs::path path);
  llvm::Error execute(MetaBlock &meta, std::unique_ptr<llvm::Module> Module);
};

} // namespace meta

#endif