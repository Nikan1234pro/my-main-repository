#ifndef INTERPRETER_META_JIT_H
#define INTERPRETER_META_JIT_H

#include "llvm/ExecutionEngine/Orc/LLJIT.h"

namespace meta {
namespace jit {

class MetaJit {
  std::unique_ptr<llvm::orc::LLJIT> jit;
  std::unique_ptr<llvm::TargetMachine> target_machine;

public:
  MetaJit();
  ~MetaJit();

  void addModuleFromBuffer(llvm::MemoryBufferRef buffer);

  void addDynamicLib(const std::string &filename);
  llvm::JITTargetAddress lookup(const std::string &name);
};

} // namespace jit
} // namespace meta

#endif