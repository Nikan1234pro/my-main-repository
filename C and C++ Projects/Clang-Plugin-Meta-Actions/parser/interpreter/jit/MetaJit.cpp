#include "MetaJit.h"
#include "Error.h"

#include "llvm/ExecutionEngine/Orc/JITTargetMachineBuilder.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Target/TargetMachine.h"

#include <thread>

namespace {
unsigned int getNumOfThreads() noexcept {
  return std::thread::hardware_concurrency();
}

void checkLLVMError(llvm::Error error) {
  if (error) {
    meta::reportError(llvm::toString(std::move(error)).c_str());
  }
}

template <typename T> T CHECK_LLVM(llvm::Expected<T> &&E) {
  checkLLVMError(E.takeError());
  return std::move(*E);
}

} // namespace

namespace meta {
namespace jit {

MetaJit::MetaJit() {
  auto machine_builder =
      CHECK_LLVM(llvm::orc::JITTargetMachineBuilder::detectHost());
  target_machine = CHECK_LLVM(machine_builder.createTargetMachine());

  auto data_layout =
      CHECK_LLVM(machine_builder.getDefaultDataLayoutForTarget());

  jit = CHECK_LLVM(llvm::orc::LLJITBuilder()
                       .setJITTargetMachineBuilder(std::move(machine_builder))
                       .setNumCompileThreads(getNumOfThreads())
                       .create());
}

MetaJit::~MetaJit() {}

void MetaJit::addModuleFromBuffer(llvm::MemoryBufferRef buffer) {
  llvm::SMDiagnostic error;

  std::unique_ptr<llvm::LLVMContext> context =
      std::make_unique<llvm::LLVMContext>();
  std::unique_ptr<llvm::Module> owner = llvm::parseIR(buffer, error, *context);

  if (!owner) {
    reportError(error.getMessage().str().c_str());
  }

  jit->addIRModule(
      llvm::orc::ThreadSafeModule(std::move(owner), std::move(context)));
}

void MetaJit::addDynamicLib(const std::string &filename) {
  char prefix = jit->getDataLayout().getGlobalPrefix();
  jit->getMainJITDylib().addGenerator(
      cantFail(llvm::orc::DynamicLibrarySearchGenerator::Load(filename.c_str(),
                                                              prefix)));
}

llvm::JITTargetAddress MetaJit::lookup(const std::string &name) {
  llvm::JITEvaluatedSymbol sym =
      CHECK_LLVM(jit->lookupLinkerMangled(name.c_str()));
  return sym.getAddress();
}

} // namespace jit
} // namespace meta