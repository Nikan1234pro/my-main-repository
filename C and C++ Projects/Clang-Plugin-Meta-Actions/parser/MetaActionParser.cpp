#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Decl.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/StmtVisitor.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Driver/Compilation.h"
#include "clang/Driver/Driver.h"
#include "clang/Driver/Tool.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendDiagnostic.h"
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include "MetaActionParser.h"
#include "interpreter/MetaInterpreterDriver.h"
#include "module/ModuleCompiler.h"
#include "module/ModuleFileBuilder.h"

#include "Error.h"
#include "TemporaryFile.h"

#include "ASTSourceWrapper.h"
#include "SourceWrapper.h"
#include "StringWrapper.h"

#include <fstream>
#include <iostream>

namespace {

using OutputFilesIter = std::back_insert_iterator<std::vector<std::string>>;

const std::string METACLASS_PREFIX = "_META_WRAPPER_";
const std::string METACLASS_ACTION = "_META_ACTION_";

class MetaBlockFinder : public clang::RecursiveASTVisitor<MetaBlockFinder> {

  const meta::ModuleFileBuilder *builder;
  const clang::CXXRecordDecl *record;

  std::unique_ptr<meta::MetaBlock> meta_block;

public:
  MetaBlockFinder(const meta::ModuleFileBuilder *builder,
                  clang::CXXRecordDecl *record)
      : builder(builder), record(record) {
    TraverseCXXRecordDecl(record);
  }

  virtual bool VisitCXXMethodDecl(clang::CXXMethodDecl *method) {
      using namespace meta;

    if (!method->getASTContext().getSourceManager().isInMainFile(
            method->getLocation())) {
      return true;
    }

    if (method->getName().startswith(METACLASS_ACTION)) {
      /* Method body is meta-action */
      meta_block.reset(new MetaBlock(record, method->getBody()));

      /* Handle meta block */
      meta::TemporaryFile file = ExitOnErr(builder->buildFile(*meta_block));
      auto module =
          ExitOnErr(ModuleCompiler::getInstance().compile(file));
      if (module) {
        ExitOnErr(MetaInterpreterDriver::getInstance().execute(
            *meta_block, std::move(module)));
      }
    }
    return true;
  }

  const meta::MetaBlock *getMetaBlock() const { return meta_block.get(); }
};

class ParseMetaStatemetsVisitor
    : public clang::RecursiveASTVisitor<ParseMetaStatemetsVisitor> {

  using SourceList = std::vector<std::unique_ptr<meta::source::SourceWrapper>>;
  SourceList rewritten_file;

  const meta::ModuleFileBuilder *builder;

  clang::SourceLocation begin;
  clang::SourceLocation end;

  clang::ASTContext *context;

  void HandleUnspecifiedCode(const meta::MetaBlock &meta_block) {
    rewritten_file.push_back(std::make_unique<meta::source::ASTSourceWrapper>(
        begin, meta_block.getBeginLocation(), context->getSourceManager()));

    std::for_each(meta_block.getInjectionBegin(), meta_block.getInjectionEnd(),
                  [&](const std::string &code) {
                    rewritten_file.push_back(
                        std::make_unique<meta::source::StringWrapper>(code));
                  });

    begin = meta_block.getBeginLocation();
  }

public:
  explicit ParseMetaStatemetsVisitor(const clang::CompilerInstance &CI,
                                     const meta::ModuleFileBuilder *builder)
      : context(&CI.getASTContext()), builder(builder) {
    auto &sm = context->getSourceManager();
    auto file_id = sm.getMainFileID();
    begin = sm.getLocForStartOfFile(file_id);
    end = sm.getLocForEndOfFile(file_id);
  }

  const SourceList &Finalize() {
    if (begin != end) {
      rewritten_file.push_back(std::make_unique<meta::source::ASTSourceWrapper>(
          begin, end, context->getSourceManager()));
      begin = end;
    }
    return rewritten_file;
  }

  virtual bool VisitCXXRecordDecl(clang::CXXRecordDecl *record) {
    if (record->isClass()) {
      if (record->getName().startswith(METACLASS_PREFIX)) {

        MetaBlockFinder meta_block_finder(builder, record);
        if (const meta::MetaBlock *meta_block =
                meta_block_finder.getMetaBlock()) {
          /* Handle meta block */
          HandleUnspecifiedCode(*meta_block);
        }
      }
    }
    return true;
  }
};

class ParseMetaStatemetsConsumer : public clang::ASTConsumer {
  meta::ModuleFileBuilder builder;
  ParseMetaStatemetsVisitor visitor;

  std::string filename;

  OutputFilesIter output_files;

  llvm::ErrorOr<std::pair<std::ofstream, std::string>>
  makeOutputFile(const meta::fs::path &input) {
    std::string input_filename = input.filename().string();
    meta::fs::path directory = input.parent_path();

    auto pos = input_filename.find_last_of('.');
    if (pos != std::string::npos) {
      std::string output_filename = input_filename.insert(pos, "_gen");
      meta::fs::path output(directory / output_filename);
      return std::make_pair(std::ofstream(output), output.string());
    }
    return llvm::inconvertibleErrorCode();
  }

public:
  ParseMetaStatemetsConsumer(clang::CompilerInstance &Instance,
                             std::string filename, OutputFilesIter output_files)
      : builder(&Instance.getSourceManager()), visitor(Instance, &builder),
        filename(std::move(filename)), output_files(std::move(output_files)) {
    Instance.getPreprocessor().addPPCallbacks(builder.getCallback());
    meta::ModuleCompiler::getInstance().setUserHeaderEntries(
        Instance.getHeaderSearchOpts().UserEntries);
  }

  void HandleTranslationUnit(clang::ASTContext &context) override {
    /* Check errors */
    if (context.getDiagnostics().hasErrorOccurred()) {
      std::string message = "Parsing " + filename + " failed";
      ExitOnErr(llvm::make_error<llvm::StringError>(
          message, llvm::inconvertibleErrorCode()));
    }

    visitor.TraverseDecl(context.getTranslationUnitDecl());

    auto result = makeOutputFile(meta::fs::path(filename));
    if (result.getError()) {
      llvm::errs() << "Couldn't create output file\n";
    }
    auto &output = result.get();
    for (auto &source : visitor.Finalize()) {
      output.first << source->getCharacterData() << std::endl;
    }
    *output_files++ = output.second;
  }
};

class ParseMetaStatemetsAction : public clang::ASTFrontendAction {
  OutputFilesIter output_files;

public:
  ParseMetaStatemetsAction(OutputFilesIter output_files)
      : output_files(std::move(output_files)) {}

  std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance &CI,
                    llvm::StringRef filename) override {
    return std::make_unique<ParseMetaStatemetsConsumer>(CI, filename.str(),
                                                        output_files);
  }
};

class MetaFrontendActionFactory : public clang::tooling::FrontendActionFactory {
  OutputFilesIter output_files;

public:
  explicit MetaFrontendActionFactory(OutputFilesIter output_files)
      : output_files(std::move(output_files)) {}

  std::unique_ptr<clang::FrontendAction> create() override {
    return std::make_unique<ParseMetaStatemetsAction>(output_files);
  }
};

} // namespace

namespace meta {

MetaActionParser::MetaActionParser(int argc, const char **argv)
    : arg_count(argc),
      op(arg_count, argv, tool_category, llvm::cl::ZeroOrMore, nullptr) {

  if (!interpreter_path.empty()) {
    MetaInterpreterDriver::getInstance().setPathToInterpreter(interpreter_path);
  }
  if (!interpreter_links.empty()) {
    MetaInterpreterDriver::getInstance().setPathToLinkFile(interpreter_links);
  }
}

llvm::ErrorOr<std::vector<std::string>> MetaActionParser::parse() {
  auto &input = op.getSourcePathList();

  std::vector<std::string> output;
  output.reserve(input.size());

  auto factory =
      std::make_unique<MetaFrontendActionFactory>(std::back_inserter(output));
  for (auto &filename : input) {
    std::cout << "Parsing " << filename << "..." << std::endl;
    clang::tooling::ClangTool tool(op.getCompilations(), filename);
    if (tool.run(factory.get()) != EXIT_SUCCESS) {
      /* Error occured */
      return llvm::inconvertibleErrorCode();
    }
  }
  return output;
}

llvm::cl::OptionCategory MetaActionParser::tool_category("My tool options");

llvm::cl::opt<std::string> MetaActionParser::interpreter_path(
    "ir-path", llvm::cl::desc("Specify interpreter path"),
    llvm::cl::value_desc("path/to/interpreter"));

llvm::cl::opt<std::string> MetaActionParser::interpreter_links(
    "ir-link", llvm::cl::desc("Specify interpreter links file"),
    llvm::cl::value_desc("path/to/links/file"));

} // namespace meta
