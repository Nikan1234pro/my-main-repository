#ifndef META_FILE_BUILDER
#define META_FILE_BUILDER

#include "Error.h"
#include "MetaBlock.h"
#include "TemporaryFile.h"
#include "primitives/IncludeDirective.h"

#include "clang/AST/Stmt.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Lex/PPCallbacks.h"
#include "clang/Lex/Token.h"
#include "llvm/ADT/StringRef.h"

#include <memory>
#include <vector>

namespace meta {

class ModuleFileBuilder {
  class PPIncludeCallback : public clang::PPCallbacks {
    const clang::SourceManager *manager;
    std::vector<primitives::IncludeDirective> *includes;

  public:
    explicit PPIncludeCallback(const clang::SourceManager *,
                               std::vector<primitives::IncludeDirective> *);

    virtual void InclusionDirective(clang::SourceLocation, const clang::Token &,
                                    clang::StringRef, bool,
                                    clang::CharSourceRange,
                                    const clang::FileEntry *, clang::StringRef,
                                    clang::StringRef, const clang::Module *,
                                    clang::SrcMgr::CharacteristicKind);
  };

  const clang::SourceManager *manager;
  std::vector<primitives::IncludeDirective> includes;

public:
  explicit ModuleFileBuilder(const clang::SourceManager *);

  std::unique_ptr<PPIncludeCallback> getCallback();
  llvm::Expected<TemporaryFile> buildFile(const MetaBlock &meta_block) const;
};

} // namespace meta

#endif