#include "ModuleFileBuilder.h"

#include "clang/AST/PrettyPrinter.h"
#include "clang/Basic/TokenKinds.h"
#include "llvm/Support/raw_ostream.h"

#include <cstdio>
#include <fstream>

#include "Types.h"

#define META_INITIALIZER "InitMeta"
#define META_ACTION "MetaAction"

#include "primitives/Function.h"
#include "primitives/QualifierMaker.h"

namespace meta {

ModuleFileBuilder::ModuleFileBuilder(const clang::SourceManager *manager)
    : manager(manager) {}

std::unique_ptr<ModuleFileBuilder::PPIncludeCallback>
ModuleFileBuilder::getCallback() {
  return std::make_unique<PPIncludeCallback>(manager, &includes);
}

llvm::Expected<TemporaryFile>
ModuleFileBuilder::buildFile(const MetaBlock &meta_block) const {
  using namespace primitives;

  TopLevelDeclaration top;

  /* Add all includes */
  for (auto &include : includes) {
    top.addInclude(include);
  }

  /* Create meta action */
  auto meta_action = FunctionBuilder{}
                         .functionName(META_ACTION)
                         .makeReturnType<BaseType>(TO_STR(void))
                         .body(std::make_unique<StringStatement>(
                             meta_block.getMetaActionCode()))
                         .build();

  std::unique_ptr<CompoundStatement> body(new CompoundStatement());
  body->addStatement(
      std::make_unique<FunctionCall>(META_INITIALIZER, "argc", "argv"));
  body->addStatement(std::make_unique<FunctionCall>(META_ACTION));

  top.addDeclaration(std::move(meta_action));
  top.addDeclaration(
      FunctionBuilder{}
          .functionName("main")
          .makeReturnType<BaseType>(TO_STR(int))
          .addArgument(type<BaseType>(TO_STR(int)).build(), "argc")
          .addArgument(
              type<Pointer>(
                  type<Pointer>(
                      type<BaseType>(TO_STR(char)).isConst(true).build())
                      .build())
                  .build(),
              "argv")
          .body(std::move(body))
          .build());

  auto result = makeTemporaryFile(std::ios_base::out | std::ios_base::trunc);
  if (result.takeError()) {
    return result;
  }

  TemporaryFile &output = result.get();
  output << top.toString();
  return result;
}

ModuleFileBuilder::PPIncludeCallback::PPIncludeCallback(
    const clang::SourceManager *manager,
    std::vector<primitives::IncludeDirective> *includes)
    : manager(manager), includes(includes) {}

void ModuleFileBuilder::PPIncludeCallback::InclusionDirective(
    clang::SourceLocation Loc, const clang::Token &IncludeTok,
    clang::StringRef FileName, bool IsAngled, clang::CharSourceRange,
    const clang::FileEntry *, clang::StringRef, clang::StringRef,
    const clang::Module *, clang::SrcMgr::CharacteristicKind) {

  /* Get includes only from main file */
  if (!manager->isInMainFile(Loc)) {
    return;
  }
  std::string type(manager->getCharacterData(IncludeTok.getLocation()),
                   IncludeTok.getLength());
  includes->emplace_back(type, FileName.str(), IsAngled);
}

} // namespace meta