#ifndef MODULE_COMPILER_H
#define MODULE_COMPILER_H

#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Lex/HeaderSearchOptions.h"
#include "llvm/IR/Module.h"

#include "TemporaryFile.h"

#include <memory>
#include <string>
#include <vector>

namespace meta {

class ModuleCompiler {
  using IncludePathList = std::vector<clang::HeaderSearchOptions::Entry>;

  IncludePathList header_entries;

  std::unique_ptr<clang::CodeGenAction> CodeGenAction;

  ModuleCompiler();

public:
  static ModuleCompiler &getInstance();

  void setUserHeaderEntries(const IncludePathList &);

  llvm::Expected<std::unique_ptr<llvm::Module>>
  compile(const TemporaryFile &file) const;
};

} // namespace meta

#endif