#ifndef PRIMITIVES_TYPE_H
#define PRIMITIVES_TYPE_H

#include "Primitive.h"
#include <memory>

namespace meta {
namespace primitives {

class BaseType : public Primitive {

  std::string typename_;
  bool is_const;
  bool is_volatile;

public:
  explicit BaseType(std::string typename_);

  std::string toString() const override;

  void isConst(bool value) { is_const = value; };
  bool isConst() const { return is_const; };

  void isVolatile(bool value) { is_volatile = value; }
  bool isVolatile() const { return is_volatile; }
};

using TypeWrapper = std::unique_ptr<BaseType>;

class ComplexType : public BaseType {
  TypeWrapper underlying_type;

public:
  explicit ComplexType(TypeWrapper type, std::string name);
  std::string toString() const override;
};

class Pointer : public ComplexType {
public:
  explicit Pointer(TypeWrapper type);
};

class Reference : public ComplexType {
public:
  explicit Reference(TypeWrapper type);
};

class TypeBuilder {
  TypeWrapper type;

public:
  TypeBuilder(TypeWrapper type);

  TypeBuilder &isConst(bool value);
  TypeBuilder &isVolatile(bool value);

  TypeWrapper build();
};

template <class T, class... Args,
          class E = typename std::enable_if<
              std::is_base_of<BaseType, T>::value>::type>
TypeBuilder type(Args &&... args) {
  return {std::make_unique<T>(std::forward<Args>(args)...)};
}

} // namespace primitives
} // namespace meta

#endif