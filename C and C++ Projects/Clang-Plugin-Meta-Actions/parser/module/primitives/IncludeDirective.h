#ifndef PRIMITIVES_INCLUDE_DIRECTIVE_H
#define PRIMITIVES_INCLUDE_DIRECTIVE_H

#include "Primitive.h"

namespace meta {
namespace primitives {

class IncludeDirective : public Primitive {
  const std::string type;
  const std::string filename;
  const bool is_angled;

  const char inclusion_brackets[2][2] = {{'\"', '\"'}, {'<', '>'}};

public:
  IncludeDirective(const std::string &type, const std::string &filename,
                   bool is_angled);

  std::string toString() const override;
};
} // namespace primitives
} // namespace meta

#endif
