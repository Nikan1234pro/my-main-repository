#ifndef PRIMITIVES_PRIMITIVE_H
#define PRIMITIVES_PRIMITIVE_H

#include <string>

namespace meta {
namespace primitives {

class Primitive {
public:
  virtual std::string toString() const = 0;

  virtual ~Primitive() = default;

protected:
  struct Brackets {
    static constexpr char brackets[3][2] = {{'(', ')'}, {'{', '}'}, {'[', ']'}};
    static constexpr unsigned ROUND = 0;
    static constexpr unsigned ANGLED = 1;
    static constexpr unsigned SQUARE = 2;

    static constexpr unsigned LEFT = 0;
    static constexpr unsigned RIGHT = 1;
  };

  struct Separators {
    static constexpr char space = ' ';
    static constexpr char comma = ',';
    static constexpr char endl = '\n';
    static constexpr char decl_end = ';';
  };
};

} // namespace primitives
} // namespace meta

#endif