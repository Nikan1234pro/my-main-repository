#include "Statement.h"

#include <sstream>

namespace meta {
namespace primitives {

CompoundStatement::CompoundStatement(
    std::vector<std::unique_ptr<Statement>> stats) {
  for (auto &s : stats) {
    statements.push_back(std::move(s));
  }
}

void CompoundStatement::addStatement(std::unique_ptr<Statement> statement) {
  statements.push_back(std::move(statement));
}

std::string CompoundStatement::toString() const {
  std::stringstream result;
  for (auto &statement : statements) {
    result << statement->toString() << Separators::endl;
  }
  return result.str();
}

auto CompoundStatement::childBegin() const { return statements.begin(); }

auto CompoundStatement::childEnd() const { return statements.end(); }

StringStatement::StringStatement(std::string statement)
    : statement(std::move(statement)) {}

std::string StringStatement::toString() const { return statement; }

} // namespace primitives
} // namespace meta