#include "Type.h"

#include "QualifierMaker.h"

namespace meta {
namespace primitives {

namespace type_qualifiers {

std::string q_const = MAKE_QUALIFIER_RIGHT(const);
std::string q_volatile = MAKE_QUALIFIER_RIGHT(volatile);

std::string q_pointer = MAKE_QUALIFIER_RIGHT(*);
std::string q_reference = MAKE_QUALIFIER_RIGHT(&);

} // namespace type_qualifiers

BaseType::BaseType(std::string typename_)
    : typename_(std::move(typename_)), is_const(false), is_volatile(false) {}

std::string BaseType::toString() const {
  std::string result(typename_);
  if (isVolatile()) {
    result += type_qualifiers::q_volatile;
  }
  if (isConst()) {
    result += type_qualifiers::q_const;
  }
  return result;
}

ComplexType::ComplexType(TypeWrapper type, std::string name)
    : BaseType(std::move(name)), underlying_type(std::move(type)) {}

std::string ComplexType::toString() const {
  return underlying_type->toString() + BaseType::toString();
}

Pointer::Pointer(TypeWrapper type)
    : ComplexType(std::move(type), type_qualifiers::q_pointer) {}

Reference::Reference(TypeWrapper type)
    : ComplexType(std::move(type), type_qualifiers::q_reference) {}

TypeBuilder::TypeBuilder(TypeWrapper type) : type(std::move(type)) {}

TypeBuilder &TypeBuilder::isConst(bool value) {
  type->isConst(value);
  return *this;
};

TypeBuilder &TypeBuilder::isVolatile(bool value) {
  type->isVolatile(value);
  return *this;
}

TypeWrapper TypeBuilder::build() { return std::move(type); }

} // namespace primitives
} // namespace meta
