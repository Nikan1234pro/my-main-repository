#include "Function.h"
#include "QualifierMaker.h"

#include <sstream>

namespace meta {
namespace primitives {

namespace function_qualifiers {
std::string q_static = MAKE_QUALIFIER_LEFT(static);
std::string q_inline = MAKE_QUALIFIER_LEFT(inline);
} // namespace function_qualifiers

std::string FunctionArgDeclaration::toString() const {
  return type->toString() + Separators::space + arg_name;
}

FunctionDeclaration::FunctionDeclaration(std::string name,
                                         TypeWrapper return_type,
                                         StatementWrapper body, bool is_static,
                                         bool is_inline)
    : name(std::move(name)), return_type(std::move(return_type)),
      body(std::move(body)), is_static(is_static), is_inline(is_inline) {}

FunctionDeclaration::FunctionDeclaration(std::string name,
                                         TypeWrapper return_type,
                                         FunctionArgumentList args,
                                         StatementWrapper body, bool is_static,
                                         bool is_inline)
    : name(std::move(name)), return_type(std::move(return_type)),
      arguments(std::move(args)), body(std::move(body)), is_static(is_static),
      is_inline(is_inline) {}

std::string FunctionDeclaration::toString() const {
  std::stringstream stream;
  if (isStatic()) {
    stream << function_qualifiers::q_static;
  }
  if (isInline()) {
    stream << function_qualifiers::q_inline;
  }
  stream << return_type->toString() << Separators::space << name
         << Brackets::brackets[Brackets::ROUND][Brackets::LEFT];

  /*Add function args */
  if (!arguments.empty()) {
    std::size_t current = 0u;
    while (current < arguments.size() - 1) {
      stream << arguments[current]->toString() << Separators::comma
             << Separators::space;
      ++current;
    }
    stream << arguments[current]->toString();
  }

  stream << Brackets::brackets[Brackets::ROUND][Brackets::RIGHT];

  /* Add function body */
  if (!body) {
    stream << Separators::decl_end;
    return stream.str();
  }
  stream << Separators::endl
         << Brackets::brackets[Brackets::ANGLED][Brackets::LEFT]
         << Separators::endl
         << body->toString() << Separators::endl
         << Brackets::brackets[Brackets::ANGLED][Brackets::RIGHT];
  return stream.str();
}

FunctionBuilder &FunctionBuilder::returnType(TypeWrapper type) {
  ret_type = std::move(type);
  return *this;
}

FunctionBuilder &FunctionBuilder::functionName(const char *name) {
  this->name.assign(name);
  return *this;
}

FunctionBuilder &FunctionBuilder::addArgument(TypeWrapper type,
                                              std::string name) {
  args.push_back(std::make_unique<FunctionArgDeclaration>(std::move(type),
                                                          std::move(name)));
  return *this;
}

FunctionBuilder &FunctionBuilder::isStatic(bool value) {
  is_static = value;
  return *this;
}

FunctionBuilder &FunctionBuilder::isInline(bool value) {
  is_inline = value;
  return *this;
}

FunctionBuilder &FunctionBuilder::body(StatementWrapper body) {
  function_body = std::move(body);
  return *this;
}

std::unique_ptr<FunctionDeclaration> FunctionBuilder::build() {
  if (name.empty()) {
    return nullptr;
  }
  if (!ret_type) {
    return nullptr;
  }
  return std::make_unique<FunctionDeclaration>(
      std::move(name), std::move(ret_type), std::move(args),
      std::move(function_body), is_static, is_inline);
}

std::string FunctionCall::toString() const {
  std::stringstream stream;
  stream << name << Brackets::brackets[Brackets::ROUND][Brackets::LEFT];

  /* Add arg list*/
  if (!arg_names.empty()) {
    std::size_t current = 0u;
    while (current < arg_names.size() - 1) {
      stream << arg_names[current] << Separators::comma << Separators::space;
      ++current;
    }
    stream << arg_names[current];
  }

  stream << Brackets::brackets[Brackets::ROUND][Brackets::RIGHT]
         << Separators::decl_end;
  return stream.str();
}

} // namespace primitives
} // namespace meta
