#include "IncludeDirective.h"

#include <sstream>

namespace meta {
namespace primitives {

IncludeDirective::IncludeDirective(const std::string &type,
                                   const std::string &filename, bool is_angled)
    : type(type), filename(filename), is_angled(is_angled) {}

std::string IncludeDirective::toString() const {

  std::stringstream ss;
  ss << '#' << type << inclusion_brackets[is_angled][Brackets::LEFT] << filename
     << inclusion_brackets[is_angled][Brackets::RIGHT];
  return ss.str();
}
} // namespace primitives
} // namespace meta