#ifndef PRIMITIVES_FUNCTION_H
#define PRIMITIVES_FUNCTION_H

#include "Declaration.h"
#include "Statement.h"

#include "ContainerInserter.h"

#include <type_traits>

namespace meta {
namespace primitives {

class FunctionArgDeclaration : public Declaration {
  TypeWrapper type;
  std::string arg_name;

public:
  explicit FunctionArgDeclaration(TypeWrapper type)
      : type(std::move(type)), arg_name() {}

  FunctionArgDeclaration(TypeWrapper type, std::string name)
      : type(std::move(type)), arg_name(std::move(name)) {}

  std::string toString() const;
};

using FunctionArgumentList =
    std::vector<std::unique_ptr<FunctionArgDeclaration>>;

class FunctionDeclaration : public Declaration {
  std::string name;

  TypeWrapper return_type;
  FunctionArgumentList arguments;

  StatementWrapper body;

  bool is_static;
  bool is_inline;

public:
  FunctionDeclaration(std::string name, TypeWrapper return_type,
                      StatementWrapper body = nullptr, bool is_static = false,
                      bool is_inline = false);

  FunctionDeclaration(std::string name, TypeWrapper return_type,
                      FunctionArgumentList args,
                      StatementWrapper body = nullptr, bool is_static = false,
                      bool is_inline = false);

  std::string toString() const;

  void isStatic(bool value) { is_static = value; }
  bool isStatic() const { return is_static; }

  void isInline(bool value) { is_inline = value; }
  bool isInline() const { return is_inline; }
};

class FunctionBuilder {
  std::string name;

  TypeWrapper ret_type;
  FunctionArgumentList args;

  StatementWrapper function_body;

  bool is_static = false;
  bool is_inline = false;

public:
  FunctionBuilder() = default;

  FunctionBuilder &returnType(TypeWrapper type);

  template <class RetType, class... Args,
            class F = typename std::enable_if<
                std::is_base_of<BaseType, RetType>::value>::type>
  FunctionBuilder &makeReturnType(Args &&... args) {
    ret_type = std::make_unique<RetType>(std::forward<Args>(args)...);
    return *this;
  }

  FunctionBuilder &functionName(const char *name);

  FunctionBuilder &addArgument(TypeWrapper type, std::string name = "");

  FunctionBuilder &isStatic(bool value);
  FunctionBuilder &isInline(bool value);

  FunctionBuilder &body(StatementWrapper body);

  std::unique_ptr<FunctionDeclaration> build();
};

class FunctionCall : public Statement {
  std::string name;
  std::vector<std::string> arg_names;

public:
  template <class... Args>
  FunctionCall(std::string name, Args &&... args) : name(std::move(name)) {
    arg_names.reserve(sizeof...(Args));
    emplace_values(arg_names, std::forward<Args>(args)...);
  }

  std::string toString() const override;
};

} // namespace primitives
} // namespace meta

#endif