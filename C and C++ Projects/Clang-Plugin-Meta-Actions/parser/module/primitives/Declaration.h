#ifndef PRIMITIVES_DECLARATION_H
#define PRIMITIVES_DECLARATION_H

#include "IncludeDirective.h"
#include "Type.h"

#include <list>
#include <vector>

namespace meta {
namespace primitives {

class Declaration : public Primitive {};

using DeclarationWrapper = std::unique_ptr<Declaration>;

class TopLevelDeclaration : public Declaration {
  std::list<DeclarationWrapper> declarations;
  std::list<IncludeDirective> includes;

public:
  TopLevelDeclaration() = default;
  explicit TopLevelDeclaration(std::vector<DeclarationWrapper> decls);

  auto declsBegin() const;
  auto declsEnd() const;

  void addDeclaration(DeclarationWrapper decl);
  void addInclude(const IncludeDirective &include);

  std::string toString() const override;
};

} // namespace primitives
} // namespace meta
#endif