#include "Declaration.h"

#include <sstream>

namespace meta {
namespace primitives {

TopLevelDeclaration::TopLevelDeclaration(
    std::vector<DeclarationWrapper> decls) {
  for (auto &decl : decls) {
    declarations.push_back(std::move(decl));
  }
}

auto TopLevelDeclaration::declsBegin() const { return declarations.begin(); }

auto TopLevelDeclaration::declsEnd() const { return declarations.end(); }

void TopLevelDeclaration::addDeclaration(DeclarationWrapper decl) {
  declarations.push_back(std::move(decl));
}

void TopLevelDeclaration::addInclude(const IncludeDirective &include) {
  includes.push_back(include);
}

std::string TopLevelDeclaration::toString() const {
  std::stringstream result;

  for (auto &include : includes) {
    result << include.toString() << Separators::endl;
  }
  result << Separators::endl;

  for (auto &decl : declarations) {
    result << decl->toString() << Separators::endl;
  }
  return result.str();
}

} // namespace primitives
} // namespace meta