#ifndef PRIMITIVES_STATEMENT_H
#define PRIMITIVES_STATEMENT_H

#include "Primitive.h"

#include <list>
#include <memory>
#include <vector>

namespace meta {
namespace primitives {

class Statement : public Primitive {};

using StatementWrapper = std::unique_ptr<Statement>;

class StringStatement : public Statement {
  std::string statement;

public:
  StringStatement(std::string statement);
  std::string toString() const override;
};

class CompoundStatement : public Statement {
  std::list<std::unique_ptr<Statement>> statements;

public:
  CompoundStatement() = default;
  CompoundStatement(std::vector<std::unique_ptr<Statement>> stats);

  void addStatement(std::unique_ptr<Statement> statement);

  auto childBegin() const;
  auto childEnd() const;

  std::string toString() const override;
};

} // namespace primitives
} // namespace meta

#endif