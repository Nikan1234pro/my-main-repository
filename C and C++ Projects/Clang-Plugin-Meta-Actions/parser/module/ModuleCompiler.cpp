#include "ModuleCompiler.h"

#include "llvm/Linker/Linker.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetMachine.h"

#include "clang/Driver/Compilation.h"
#include "clang/Driver/Driver.h"
#include "clang/Driver/Tool.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendDiagnostic.h"
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Lex/PreprocessorOptions.h"

#include "Error.h"

std::string GetExecutablePath(const char *Argv0, void *MainAddr) {
  return llvm::sys::fs::getMainExecutable(Argv0, MainAddr);
}

using namespace clang;
using namespace clang::driver;

namespace meta {

ModuleCompiler::ModuleCompiler()
    : CodeGenAction(std::make_unique<clang::EmitLLVMOnlyAction>()) {}

ModuleCompiler &ModuleCompiler::getInstance() {
  static ModuleCompiler instance;
  return instance;
}

void ModuleCompiler::setUserHeaderEntries(const IncludePathList &list) {
  header_entries = list;
}

llvm::Expected<std::unique_ptr<llvm::Module>>
ModuleCompiler::compile(const TemporaryFile &file) const {
  const char *error_msg = "Couldn't create module";

  const char *executable = "./a.out";

  // This just needs to be some symbol in the binary; C++ doesn't
  // allow taking the address of ::main however.
  void *MainAddr = (void *)(intptr_t)GetExecutablePath;
  std::string Path = GetExecutablePath(executable, MainAddr);
  IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts = new DiagnosticOptions();
  TextDiagnosticPrinter *DiagClient =
      new TextDiagnosticPrinter(llvm::errs(), &*DiagOpts);

  IntrusiveRefCntPtr<DiagnosticIDs> DiagID(new DiagnosticIDs());
  DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagClient);

  const std::string TripleStr = llvm::sys::getProcessTriple();
  llvm::Triple T(TripleStr);

  ExitOnErr.setBanner("clang interpreter");

  Driver TheDriver(Path, T.str(), Diags);
  TheDriver.setTitle("clang interpreter");
  TheDriver.setCheckInputsExist(false);

  // FIXME: This is a hack to try to force the driver to do something we can
  // recognize. We need to extend the driver library to support this use model
  // (basically, exactly one input, and the operation mode is hard wired).

  SmallVector<const char *, 16> Args;
  Args.emplace_back(executable);
  Args.emplace_back("-x");
  Args.emplace_back("c++");
  Args.emplace_back(file.name().c_str());
  Args.emplace_back("-fsyntax-only");
  Args.emplace_back("-Wno-everything");
  Args.emplace_back("-fno-use-cxa-atexit");

  TheDriver.ParseDriverMode(file.name(), Args);
  std::unique_ptr<Compilation> C(TheDriver.BuildCompilation(Args));

  if (!C)
    return makeError(error_msg);

  // We expect to get back exactly one command job, if we didn't something
  // failed. Extract that job from the compilation.
  const driver::JobList &Jobs = C->getJobs();
  if (Jobs.size() != 1 || !isa<driver::Command>(*Jobs.begin())) {
    SmallString<256> Msg;
    llvm::raw_svector_ostream OS(Msg);
    Jobs.Print(OS, "; ", true);
    Diags.Report(diag::err_fe_expected_compiler_job) << OS.str();
    return makeError(error_msg);
  }

  const driver::Command &Cmd = cast<driver::Command>(*Jobs.begin());
  if (llvm::StringRef(Cmd.getCreator().getName()) != "clang") {
    Diags.Report(diag::err_fe_expected_clang_command);
    return makeError(error_msg);
  }

  // Initialize a compiler invocation object from the clang (-cc1) arguments.
  const llvm::opt::ArgStringList &CCArgs = Cmd.getArguments();
  std::unique_ptr<CompilerInvocation> CI(new CompilerInvocation);
  CompilerInvocation::CreateFromArgs(*CI, CCArgs, Diags);

  auto *langOpts = CI->getLangOpts();
  langOpts->CXXExceptions = 1;
  langOpts->RTTI = 1;
  langOpts->Bool = 1; // <-- Note the Bool option here !
  langOpts->CPlusPlus = 1;

#if defined(_WIN32) || defined(_WIN64)
  /* Add some definitions for MSVC */
  auto &opts = CI->getPreprocessorOpts();
  opts.Macros.emplace_back("_MT", false);
  opts.Macros.emplace_back("_DLL", false);
  opts.Macros.emplace_back("_CRT_SECURE_NO_WARNINGS", false);
  opts.Macros.emplace_back("_ENABLE_DELETE_REPLACEMENT", false);
#endif

  CompilerInstance Clang;
  Clang.setInvocation(std::move(CI));

  if (!header_entries.empty()) {
    Clang.getHeaderSearchOpts().UserEntries = header_entries;
  }

  // Create the compilers actual diagnostics engine.
  Clang.createDiagnostics();
  if (!Clang.hasDiagnostics())
    return nullptr;

  // Create and execute the frontend to generate an LLVM bitcode module.
  if (!Clang.ExecuteAction(*CodeGenAction))
    return nullptr;

  return CodeGenAction->takeModule();
}

} // namespace meta