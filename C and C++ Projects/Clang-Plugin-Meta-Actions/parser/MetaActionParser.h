#ifndef META_ACTION_PARSER_H
#define META_ACTION_PARSER_H

#include "clang/Tooling/CommonOptionsParser.h"
#include "llvm/Support/CommandLine.h"

#include "Error.h"

namespace meta {

class MetaActionParser {
  int arg_count;

  clang::tooling::CommonOptionsParser op;

  static llvm::cl::OptionCategory tool_category;
  static llvm::cl::opt<std::string> interpreter_path;
  static llvm::cl::opt<std::string> interpreter_links;

public:
  MetaActionParser(int argc, const char **argv);
  llvm::ErrorOr<std::vector<std::string>> parse();
};

} // namespace meta

#endif