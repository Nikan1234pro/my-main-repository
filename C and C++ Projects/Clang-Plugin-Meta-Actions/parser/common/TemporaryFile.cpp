#include "TemporaryFile.h"

#include <algorithm>
#include <cstdlib>
#include <string>
#include <unordered_map>

#include <iostream>

namespace meta {

#if defined(unix) || defined(__unix__) || defined(__unix)
#elif defined(_WIN32) || defined(_WIN64)
#include <io.h>
#define __mktemp__ _mktemp_s
#define __mktemp__success__ 0
#endif

namespace {
class TemporaryFileStorage {
  using FileMap = std::unordered_map<std::string, std::fstream>;
  FileMap files_map;

public:
  FileMap::pointer make(const std::string &filename,
                        const std::ios_base::openmode mode) {
    std::fstream stream(filename, mode);

    auto it = files_map.find(filename);
    if (it != files_map.end()) {
      it->second = std::move(stream);
      return &(*it);
    }
    auto result = files_map.insert({filename, std::move(stream)});
    if (!result.second) {
      errno = EEXIST;
      perror("Couldn't create temporary file");
      return nullptr;
    }
    return &(*result.first);
  }

  void remove(const std::string &filename) {
    auto iter = files_map.find(filename);
    if (iter != files_map.end()) {
      /* Close before removing */
      iter->second.close();

      if (std::remove(filename.c_str()) != EXIT_SUCCESS) {
        perror("Couldn't remove temporary file");
      }
      files_map.erase(iter);
    }
  }

  ~TemporaryFileStorage() {
    for (auto &value : files_map) {
      /* Close before removing */
      value.second.close();

      if (std::remove(value.first.c_str()) != EXIT_SUCCESS) {
        perror("Couldn't remove temporary file");
      }
    }
  }
};

TemporaryFileStorage &getFileStorage() {
  static TemporaryFileStorage TEMP_FILE_STORAGE;
  return TEMP_FILE_STORAGE;
}
} // namespace

TemporaryFile::TemporaryFile(const char *filename,
                             std::ios_base::open_mode mode)
    : stream_data(getFileStorage().make(filename, mode)) {}

TemporaryFile::TemporaryFile(TemporaryFile &&other) noexcept
    : stream_data(other.stream_data) {
  other.stream_data = nullptr;
}

TemporaryFile &TemporaryFile::operator=(TemporaryFile &&other) noexcept {
  if (this != &other) {
    stream_data = other.stream_data;
    other.stream_data = nullptr;
  }
  return *this;
}

TemporaryFile::operator bool() const { return stream().operator bool(); }

TemporaryFile::~TemporaryFile() noexcept {
  if (stream_data) {
    /* Remove before it will be removed by Storage */
    getFileStorage().remove(name());
  }
}

llvm::Expected<TemporaryFile> makeTemporaryFile(std::ios_base::open_mode mode) {
  char pattern[] = "tmpfileXXXXXX";
  auto err = __mktemp__(pattern);
  if (err != __mktemp__success__) {
    return makeError("Can't create file");
  }
  return TemporaryFile(pattern, mode);
}

} // namespace meta
