#ifndef COMMON_ERROR_H
#define COMMON_ERROR_H

#include "llvm/Support/Compiler.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/ErrorOr.h"

static llvm::ExitOnError ExitOnErr;

namespace meta {

LLVM_ATTRIBUTE_NORETURN
void reportError(const char *prog_name, const char *message);

LLVM_ATTRIBUTE_NORETURN
void reportError(const char *message);

llvm::Error makeError(const char *message);

} // namespace meta

#endif