#ifndef COMMON_AST_SOURCE_WRAPPER_H
#define COMMON_AST_SOURCE_WRAPPER_H

#include "SourceWrapper.h"

#include "clang/Basic/SourceLocation.h"
#include "clang/Basic/SourceManager.h"

#include <string>

namespace meta {
namespace source {

class ASTSourceWrapper : public SourceWrapper {
  std::string source;

public:
  ASTSourceWrapper(const clang::SourceLocation &begin,
                   const clang::SourceLocation &end,
                   const clang::SourceManager &sm)
      : source(sm.getCharacterData(begin),
               sm.getCharacterData(end) - sm.getCharacterData(begin)) {}

  virtual const char *getCharacterData() const override {
    return source.c_str();
  }
};

} // namespace source
} // namespace meta

#endif