#ifndef COMMON_CONTAINER_INSERTER_H
#define COMMON_CONTAINER_INSERTER_H

#include <vector>

template <class T> void emplace_values(std::vector<T> &) {}

template <class T, class Arg, class... Args>
void emplace_values(std::vector<T> &v, Arg &&arg, Args &&... args) {
  v.emplace_back(std::forward<Arg>(arg));
  emplace_values(v, std::forward<Args>(args)...);
}

#endif