#ifndef INTERPRETER_PROCESS_TYPES_H
#define INTERPRETER_PROCESS_TYPES_H

#if defined(unix) || defined(__unix__) || defined(__unix)

#include <unistd.h>

using TCHAR = char;
#define TEXT(c) c

#elif defined(_WIN32) || defined(_WIN64)

#include <Windows.h>
#include <memory>
#include <type_traits>

using HANDLE_WRAPPER =
    std::unique_ptr<std::remove_pointer<HANDLE>::type, void (*)(void *)>;

inline HANDLE_WRAPPER make_wrapper(HANDLE handle = nullptr) {
  return HANDLE_WRAPPER(handle, [](void *handle) {
    if (handle) {
      CloseHandle(handle);
    }
  });
}

#else

static_assert(false, "Unsupported platform");

#endif

#include <string>

using TSTRING = std::basic_string<TCHAR>;

#endif