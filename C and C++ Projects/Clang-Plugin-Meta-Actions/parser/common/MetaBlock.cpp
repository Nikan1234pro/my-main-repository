#include "MetaBlock.h"

#include "clang/AST/ASTContext.h"
#include "clang/AST/DeclCXX.h"
#include "clang/Basic/SourceManager.h"

namespace meta {

std::pair<clang::SourceLocation, clang::SourceLocation>
MetaBlock::getDeclRange() const {

  auto &context = decl->getASTContext();
  auto &sm = context.getSourceManager();
  auto &lopt = context.getLangOpts();

  clang::SourceLocation B = sm.getFileLoc(decl->getBeginLoc());
  clang::SourceLocation E = sm.getFileLoc(decl->getEndLoc());

  return {B, E};
}

clang::SourceLocation MetaBlock::getBeginLocation() const {
  return getDeclRange().first;
}

clang::SourceLocation MetaBlock::getEndLocation() const {
  return getDeclRange().second;
}

std::string MetaBlock::getMetaActionCode() const {
  std::string text;
  llvm::raw_string_ostream stream{text};
  meta_action->printPretty(stream, nullptr,
                           clang::PrintingPolicy(clang::LangOptions()));
  return text;
}

} // namespace meta
