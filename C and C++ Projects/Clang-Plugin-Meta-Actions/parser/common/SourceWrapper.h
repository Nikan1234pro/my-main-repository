#ifndef COMMON_SOURCE_WRAPPER_H
#define COMMON_SOURCE_WRAPPER_H

namespace meta {
namespace source {

class SourceWrapper {
public:
  virtual const char *getCharacterData() const = 0;
  virtual ~SourceWrapper() = default;
};

} // namespace source
} // namespace meta

#endif