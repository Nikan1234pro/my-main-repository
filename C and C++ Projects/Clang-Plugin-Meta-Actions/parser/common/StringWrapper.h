#ifndef COMMON_STRING_WRAPPER_H
#define COMMON_STRING_WRAPPER_H

#include "SourceWrapper.h"

#include <string>

namespace meta {
namespace source {

class StringWrapper : public SourceWrapper {
  std::string source;

public:
  StringWrapper(std::string src) : source(std::move(src)) {}

  virtual const char *getCharacterData() const override {
    return source.c_str();
  }
};

} // namespace source
} // namespace meta

#endif