#ifndef COMMON_META_BLOCK_H
#define COMMON_META_BLOCK_H

#include "clang/AST/Decl.h"
#include "clang/AST/Stmt.h"

#include <list>
#include <string>

namespace meta {

class MetaBlock {
  const clang::CXXRecordDecl *decl;
  const clang::Stmt *meta_action;

  std::list<std::string> injections;

  std::pair<clang::SourceLocation, clang::SourceLocation> getDeclRange() const;

public:
  MetaBlock(const clang::CXXRecordDecl *decl, const clang::Stmt *stmt)
      : decl(decl), meta_action(stmt) {}

  clang::SourceLocation getBeginLocation() const;
  clang::SourceLocation getEndLocation() const;

  std::string getMetaActionCode() const;

  void addInjectionCode(std::string code) {
    injections.push_back(std::move(code));
  }

  auto getInjectionBegin() const { return injections.begin(); }

  auto getInjectionEnd() const { return injections.end(); }
};

} // namespace meta

#endif