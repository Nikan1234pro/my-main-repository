#ifndef COMMON_TEMPORARY_FILE_H
#define COMMON_TEMPORARY_FILE_H

#include <cstdio>
#include <fstream>

#include "Error.h"

namespace meta {

class TemporaryFile {
  std::pair<const std::string, std::fstream> *stream_data;

  const std::fstream &stream() const { return stream_data->second; }

  std::fstream &stream() {
    return const_cast<std::fstream &>(
        static_cast<const TemporaryFile &>(*this).stream());
  }

public:
  TemporaryFile(const char *filename, std::ios_base::open_mode mode);
  TemporaryFile(TemporaryFile &&other) noexcept;
  TemporaryFile &operator=(TemporaryFile &&other) noexcept;

  const std::string &TemporaryFile::name() const { return stream_data->first; }

  template <class T> TemporaryFile &operator<<(T &&value) {
    auto &s = stream();
    s << std::forward<T>(value);
    s.flush();
    return *this;
  }

  template <class T> TemporaryFile &operator>>(T &&value) {
    stream() >> std::forward<T>(value);
    return *this;
  }

  operator bool() const;

  ~TemporaryFile() noexcept;
};

llvm::Expected<TemporaryFile> makeTemporaryFile(std::ios_base::open_mode mode);

} // namespace meta

#endif