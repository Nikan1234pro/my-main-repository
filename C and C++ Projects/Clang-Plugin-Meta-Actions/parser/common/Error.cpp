#include "Error.h"

#include "llvm/Support/WithColor.h"

namespace meta {

LLVM_ATTRIBUTE_NORETURN
void reportError(const char *prog_name, const char *message) {
  llvm::WithColor::error(llvm::errs(), prog_name) << message << '\n';
  exit(EXIT_FAILURE);
}

LLVM_ATTRIBUTE_NORETURN
void reportError(const char *message) { reportError("System", message); }

llvm::Error makeError(const char *message) {
  return llvm::make_error<llvm::StringError>(message,
                                             llvm::inconvertibleErrorCode());
}

} // namespace meta