#include "compiler/CXXCompiler.h"
#include "parser/MetaActionParser.h"

#include <iostream>

int main(int argc, const char **argv) {
  std::cout << "Step 1: Parse meta actions..." << std::endl;
  meta::MetaActionParser parser(argc, argv);
  auto files = parser.parse();
  if (files.getError()) {
    return EXIT_FAILURE;
  }
  std::cout << "Step 2: Compile..." << std::endl;
  meta::CXXCompiler compiler(argc, argv);
  compiler.compile(files.get());

  return EXIT_SUCCESS;
}