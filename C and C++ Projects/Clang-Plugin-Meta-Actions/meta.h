#ifndef META_H
#define META_H

#define METACLASS_PREFIX _META_WRAPPER_
#define METACLASS_ACTION _META_ACTION_

#define TOKENPASTE(x, y) x##y
#define TOKENPASTE_HANDLER(x, y) TOKENPASTE(x, y)
#define METACLASS_NAME                                                         \
  TOKENPASTE_HANDLER(TOKENPASTE_HANDLER(METACLASS_PREFIX, __LINE__),           \
                     __COUNTER__)

#define META_BEGIN                                                             \
  class METACLASS_NAME {                                                       \
    void METACLASS_ACTION() const {

#define META_END                                                               \
  }                                                                            \
  }                                                                            \
  ;

#define META(META_STMT) META_BEGIN META_STMT META_END

#if defined(unix) || defined(__unix__) || defined(__unix)
#define META_DLLEXPORT
#elif defined(_WIN32) || defined(_WIN64)
#ifdef META_EXPORTS
#define META_API __declspec(dllexport)
#else
#define META_API __declspec(dllimport)
#endif

#if defined(_ENABLE_DELETE_REPLACEMENT)
#include <cstdlib>
/* Add operator delete definition here */
inline void __cdecl operator delete(void* _Block, unsigned __int64) {
    free(_Block);
}

#endif

#endif

extern "C++" META_API void InitMeta(int argc, const char* argv[]);
extern "C++" META_API void inject(const char* code);

#endif
