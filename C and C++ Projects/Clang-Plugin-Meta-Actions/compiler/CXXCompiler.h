#ifndef CXX_COMPILER_H
#define CXX_COMPILER_H

#include <string>
#include <vector>

namespace meta {

class CXXCompiler {
  std::vector<std::string> args;

public:
  CXXCompiler(int argc, const char **argv);
  void compile(const std::vector<std::string> files);
};

} // namespace meta

#endif