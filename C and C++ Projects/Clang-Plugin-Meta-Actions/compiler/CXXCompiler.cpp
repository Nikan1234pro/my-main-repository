#include "CXXCompiler.h"
#include "clang/Driver/Compilation.h"
#include "clang/Driver/Driver.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/Program.h"
#include "llvm/Support/VirtualFileSystem.h"

#include <algorithm>

namespace {
    auto str_transformer = [](const std::string& s) { return s.c_str(); };
}

namespace meta {

CXXCompiler::CXXCompiler(int argc, const char **argv) {
  auto begin = argv;
  auto end = argv + argc;

  auto iter = std::find_if(
      begin, end, [](const char *val) { return std::strcmp(val, "--") == 0; });
  if (iter != end) {
    args.insert(args.end(), ++iter, end);
  }
}

void CXXCompiler::compile(const std::vector<std::string> files) {
  auto result = llvm::sys::findProgramByName("clang++");
  if (result.getError()) {
    llvm::errs() << "Couldn't find clang++\n";
    return;
  }

  std::vector<const char *> compile_args;
  compile_args.reserve(files.size() + args.size() + 1);

  compile_args.push_back(result.get().c_str());
  std::transform(files.begin(), files.end(), std::back_inserter(compile_args),
                 str_transformer);
  std::transform(args.begin(), args.end(), std::back_inserter(compile_args),
                 str_transformer);

  clang::IntrusiveRefCntPtr<clang::DiagnosticOptions> DiagOpts =
      new clang::DiagnosticOptions();
  clang::TextDiagnosticPrinter *DiagClient =
      new clang::TextDiagnosticPrinter(llvm::errs(), &*DiagOpts);

  clang::IntrusiveRefCntPtr<clang::DiagnosticIDs> DiagID(
      new clang::DiagnosticIDs());
  clang::DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagClient);

  const std::string TripleStr = llvm::sys::getProcessTriple();
  clang::driver::Driver TheDriver(compile_args[0], TripleStr, Diags);

  std::unique_ptr<clang::driver::Compilation> C(
      TheDriver.BuildCompilation(compile_args));
  int Res = 0;
  llvm::SmallVector<std::pair<int, const clang::driver::Command *>, 0>
      FailingCommand;
  if (C)
    Res = TheDriver.ExecuteCompilation(*C, FailingCommand);
  if (Res < 0) {
    llvm::errs() << "Compilation failed: " << Res << "\n";
  }
}

} // namespace meta
