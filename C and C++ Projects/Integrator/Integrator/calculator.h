#pragma once
#include <list>
#include <stack>
#include <string>
#include <map>
#include <cstdlib>
#include <exception>
#include <algorithm>
#include <functional>

#define NO_PRIORITY 0
#define MIN_PRIORITY 1
#define MAX_PRIORITY 20

class Calculator {
	struct elem {
		std::string element;
		int priority;
		elem(const std::string &e, int p) : element(e), priority(p) {
		}
	};
	using function = std::function<double(double, double)>;

	std::stack<elem> stack;
	std::list<elem> formula_RPN;
	std::map<std::string, std::pair<int, function>> priorities;

public:
	Calculator(){
		priorities["("] = std::pair<int, function>(MIN_PRIORITY, NULL);
		priorities[")"] = std::pair<int, function>(MAX_PRIORITY, NULL);
		priorities["+"] = std::pair<int, function>(2, [](double x, double y) -> double { return x + y; });
		priorities["-"] = std::pair<int, function>(2, [](double x, double y) -> double { return x - y; });
		priorities["*"] = std::pair<int, function>(3, [](double x, double y) -> double { return x * y; });
		priorities["/"] = std::pair<int, function>(3, [](double x, double y) -> double { return x / y; });
		priorities["^"] = std::pair<int, function>(4, [](double x, double y) -> double { return std::pow(x, y);});
	}
	
	void parse(const std::string &formula) {
		std::string number = "";
		for (int i = 0; i < formula.size(); i++) {
			if (isdigit(formula[i]) || formula[i] == '.')
				number += formula[i];
			else {
				if (number.size()) {
					formula_RPN.push_back(elem(number, NO_PRIORITY));
					number = "";
				}
				std::string operation(1, formula[i]);
				if (priorities.find(operation) == priorities.end())
					throw std::logic_error("Wrong input!");
				else {
					if (priorities[operation].first == MAX_PRIORITY) {
						while (stack.top().priority != MIN_PRIORITY) {
							formula_RPN.push_back(stack.top());
							stack.pop();
							if (stack.size() == 0) 
								throw std::logic_error("Wrong input!");
						}
						stack.pop();
					}
					else {
						if (priorities[operation].first != MIN_PRIORITY)
							while (stack.size() && stack.top().priority != MIN_PRIORITY && 
									priorities[operation].first <= stack.top().priority) {
								formula_RPN.push_back(stack.top());
								stack.pop();
							}
						stack.push(elem(operation, priorities[operation].first));		
					}
				}
			}	
		}
		if (number.size())
			formula_RPN.push_back(elem(number, NO_PRIORITY));
		while (stack.size()) {
			if (stack.top().priority == MIN_PRIORITY)
				throw std::logic_error("Wrong input!");
			formula_RPN.push_back(stack.top());
			stack.pop();
		}
	}

	double calculate() {
		for (auto it = formula_RPN.begin(); it != formula_RPN.end(); it++){
			if (it->priority == NO_PRIORITY)
				stack.push(*it);
			else {
				if (stack.size() < 2)
					throw std::logic_error("Wrong input");
				double y = std::atof(stack.top().element.c_str());
				stack.pop();
				double x = std::atof(stack.top().element.c_str());
				stack.pop();
				double result = priorities[it->element].second(x, y);
				stack.push(elem(std::to_string(result), NO_PRIORITY));
			}
		}
		return std::atof(stack.top().element.c_str());
	}
};