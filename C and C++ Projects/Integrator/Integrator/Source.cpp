#include <iostream>
#include "calculator.h"

int main() {
	std::string formula;
	try {
		std::cin >> formula;
		Calculator calc;
		calc.parse(formula);
		std::cout << calc.calculate() << std::endl;
	}
	catch (std::logic_error &ex) {
		std::cout << ex.what() << std::endl;
	}
	system("pause");
	return 0;
}