#include "visitor.h"
#include "objects/enum.h"

#include <fstream>

SyntaxVisitor::SyntaxVisitor(clang::CompilerInstance *CI)
    : astContext(&(CI->getASTContext())) {

  auto &mgr = astContext->getSourceManager();
  auto file_id = mgr.getMainFileID();
  begin = mgr.getLocForStartOfFile(file_id);
  end = mgr.getLocForEndOfFile(file_id);
}

void SyntaxVisitor::updateNamespace(std::unique_ptr<SyntaxObject> &&o) {
  if (namespace_ == nullptr) {
    auto namespace_uptr = std::make_unique<Namespace>();
    namespace_ = namespace_uptr.get();
    objects.push_back(std::move(namespace_uptr));
  }
  namespace_->addObject(std::move(o));
}

void SyntaxVisitor::handleUnspecifiedCode(const SyntaxObject *o) {
  const char delimiter = ';';
  auto &mgr = astContext->getSourceManager();
  auto file_id = mgr.getMainFileID();

  objects.push_back(std::make_unique<UnspecifiedObject>(
      begin, o->getDeclStart(), astContext->getSourceManager()));

  /* ';' symbol doesn't include in declarations.
     So, find them and set begin after it */
  std::string_view s = mgr.getCharacterData(o->getDeclEnd());
  int pos = s.find(delimiter);
  if (pos == std::string_view::npos)
    begin = o->getDeclEnd();
  else
    begin = o->getDeclEnd().getLocWithOffset(++pos);
}

void SyntaxVisitor::handleEnumTypedef(clang::TypedefDecl *td) {
  auto enum_type = std::make_unique<Enum>(td);

  /* Add all text before typedef declaration */
  handleUnspecifiedCode(enum_type.get());

  objects.push_back(std::move(enum_type));
}

void SyntaxVisitor::handleStructTypedef(clang::TypedefDecl *td) {
  const clang::Type *type = td->getUnderlyingType().getTypePtr();

  auto *struct_record = type->getAsStructureType();
  if (struct_record == nullptr)
    return;

  /* Create class */
  auto class_type = std::make_unique<Class>(td);
  auto class_name = class_type->getName();

  /* Add all text before typedef declaration */
  handleUnspecifiedCode(class_type.get());

  /* Get list of function pointers - virtual functions */
  GetVirtualFunctionList v{};
  v.TraverseDecl(struct_record->getDecl());
  for (auto *func : v.getList()) {
    class_type->addMethod(method_factory.createMethod(class_name, func));
  }

  classes[class_name] = class_type.get();
  objects.push_back(std::move(class_type));
}

bool SyntaxVisitor::VisitTypedefDecl(clang::TypedefDecl *td) {
  if (!astContext->getSourceManager().isInMainFile(td->getLocation()))
    return true;

  /* Find enums and structs by them typedef names */
  const clang::Type *type = td->getUnderlyingType().getTypePtr();

  if (type->isStructureType()) {
    handleStructTypedef(td);
  } else if (type->isEnumeralType()) {
    handleEnumTypedef(td);
  }
  return true;
}

bool SyntaxVisitor::VisitFunctionDecl(clang::FunctionDecl *func) {
  if (!astContext->getSourceManager().isInMainFile(func->getLocation()))
    return true;

  /* Create method */
  auto method_type = method_factory.createMethod(func);

  /* Add all text before function declaration */
  handleUnspecifiedCode(method_type.get());

  auto class_name = method_type->getClassName();

  /* It could be namespace name */
  if (class_name == Namespace::name) {
    updateNamespace(std::move(method_type));
    return true;
  }

  /* Otherwise, we found class method */
  auto class_iter = classes.find(class_name);
  if (class_iter == classes.end()) {
    throw std::runtime_error(
        "Error with method: " + method_type->getMethodName() +
        "\nCouldn't find class: " + class_name);
  }

  class_iter->second->addMethod(std::move(method_type));
  return true;
}

void SyntaxVisitor::Finalize() {
  if (begin != end) {
    objects.push_back(std::make_unique<UnspecifiedObject>(
        begin, end, astContext->getSourceManager()));
    begin = end;
  }
}

const ObjectList &SyntaxVisitor::getResult() const { return objects; }

SyntaxASTConsumer::SyntaxASTConsumer(clang::CompilerInstance *CI,
                                     const std::string &file_path)
    : visitor(CI), input_filename(fs::path(file_path).filename().u8string()) {}

void SyntaxASTConsumer::HandleTranslationUnit(clang::ASTContext &Context) {
  fs::path output_file = settings::output_dir / input_filename;
  {
    std::ofstream output(output_file);
    if (!output)
      throw std::runtime_error("Couldn't create file");

    visitor.TraverseDecl(Context.getTranslationUnitDecl());
    visitor.Finalize();

    for (auto &obj : visitor.getResult()) {
      output << obj->toString() << std::endl;
    }
  }
  /* Use clang-format */
  std::string command =
      settings::clang_format_path + " -i " + output_file.u8string();
  std::system(command.c_str());
}

SyntaxFrontendAction::SyntaxFrontendAction() {}

std::unique_ptr<clang::ASTConsumer>
SyntaxFrontendAction::CreateASTConsumer(clang::CompilerInstance &CI,
                                        llvm::StringRef file_path) {
  return std::make_unique<SyntaxASTConsumer>(&CI, file_path);
}