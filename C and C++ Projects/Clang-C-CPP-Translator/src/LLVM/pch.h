#ifndef PCH_H
#define PCH_H

#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Rewrite/Core/Rewriter.h"

#include <filesystem>
namespace fs = std::filesystem;

namespace settings {
	inline const fs::path output_dir{ "output" };
	inline const std::string clang_format_path{ "clang\\clang-format.exe" };
}

#endif //PCH_H