#pragma once

#include "format_string.h"
#include "object.h"

#include "clang/AST/RecursiveASTVisitor.h"
#include <clang/AST/Decl.h>
#include <functional>
#include <list>
#include <unordered_map>

class Method : public SyntaxObject {
private:
  std::string method_name_;
  std::string class_name_;

protected:
  const std::string const_qualifier = "const";
  const std::string getter_prefix = "get";

public:
  using ArgList = std::list<FormatString>;

  explicit Method(const std::string &class_name, const std::string &method_name,
                  const clang::Decl *decl);

  const std::string &getClassName() const;
  const std::string &getMethodName() const;
};

class AbstractMethod : public Method {
  std::string ret_type;
  ArgList args;

  const std::string prefix = "virtual";
  const std::string suffix = "= 0";

public:
  AbstractMethod(const std::string &class_name, const std::string &method_name,
                 clang::FieldDecl *type);

  std::string toString() const override;
};

class NonAbstractMethod : public Method {
  std::string ret_type;
  ArgList args;

public:
  NonAbstractMethod(const std::string &class_name,
                    const std::string &method_name,
                    const clang::FunctionDecl *decl);

  const ArgList &getArgs() const;
  const std::string &getReturnType() const;

  std::string toString() const override;
};

class DefaultConstructor : public NonAbstractMethod {
public:
  DefaultConstructor(const std::string &class_name,
                     const clang::FunctionDecl *decl);

  std::string toString() const override;
};

class ArgsConstructor : public NonAbstractMethod {
public:
  ArgsConstructor(const std::string &class_name,
                  const clang::FunctionDecl *decl);

  std::string toString() const override;
};

class CopyConstructor : public NonAbstractMethod {
public:
  CopyConstructor(const std::string &class_name,
                  const clang::FunctionDecl *decl);

  std::string toString() const override;
};

class Destructor : public NonAbstractMethod {
  const char prefix = '~';

public:
  Destructor(const std::string &class_name, const clang::FunctionDecl *decl);

  std::string toString() const override;
};

class GetVirtualFunctionList
    : public clang::RecursiveASTVisitor<GetVirtualFunctionList> {
  using FunctionList = std::list<clang::FieldDecl *>;
  FunctionList funcs;

public:
  virtual bool VisitFieldDecl(clang::FieldDecl *d);

  const FunctionList &getList() const;
};

class MethodFactory {
  const std::string default_constructor = "createDefault";
  const std::string args_constructor = "create";
  const std::string copy_constructor = "copy";
  const std::string destructor = "destroy";

  const char delimiter = '_';

  using creator_t = std::function<std::unique_ptr<NonAbstractMethod>(
      const std::string &class_name, clang::FunctionDecl *)>;

  std::unordered_map<std::string, creator_t> specific_methods;

public:
  MethodFactory();

  std::unique_ptr<NonAbstractMethod> createMethod(clang::FunctionDecl *decl);
  std::unique_ptr<AbstractMethod> createMethod(const std::string &class_name,
                                               clang::FieldDecl *decl);
};