#include "namespace.h"
#include "format_string.h"

Namespace::Namespace() = default;

std::string Namespace::toString() const {
  FormatString format(prefix);

  FormatString body;
  for (auto &o : objects) {
    body.append("\n");
    body.append(o->toString());
    body.append(";\n");
  }
  body.addBrackets('{', '}');
  format.append(body);
  return format;
}

void Namespace::addObject(std::unique_ptr<PrintableObject> &&object) {
  objects.push_back(std::move(object));
}
