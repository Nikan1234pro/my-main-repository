#include "method.h"
#include "format_string.h"

#include <iostream>

#include <clang/AST/PrettyPrinter.h>
#include <clang/AST/RecursiveASTVisitor.h>

namespace {
template <class Method_t>
auto creator(const std::string &class_name, clang::FunctionDecl *decl) {
  return std::make_unique<Method_t>(class_name, decl);
}

bool beginsWith(const std::string &str, const std::string &templ) {
  if (templ.size() > str.size())
    return false;

  auto t_begin = templ.begin();
  auto s_begin = str.begin();
  while (t_begin != templ.end()) {
    if (*t_begin++ != *s_begin++)
      return false;
  }
  return true;
}
} // namespace

Method::Method(const std::string &class_name, const std::string &method_name,
               const clang::Decl *decl)
    : SyntaxObject(decl), class_name_(class_name), method_name_(method_name) {}

const std::string &Method::getClassName() const { return class_name_; }

const std::string &Method::getMethodName() const { return method_name_; }

AbstractMethod::AbstractMethod(const std::string &class_name,
                               const std::string &method_name,
                               clang::FieldDecl *decl)
    : Method(class_name, method_name, decl) {
  auto type = decl->getFunctionType();
  if (!type)
    return;
  ret_type = type->getReturnType().getAsString();

  class GetFunctionArgs : public clang::RecursiveASTVisitor<GetFunctionArgs> {
  public:
    bool VisitParmVarDecl(clang::ParmVarDecl *d) {
      if (d->getFunctionScopeDepth() != 0) {
        return true;
      }
      auto param_type = clang::QualType::getAsString(d->getType().split(),
                                                     clang::PrintingPolicy{{}});
      auto param_name = d->getNameAsString();
      args.emplace_back(
          FormatString{}.appendSequence(" ", param_type, param_name));
      return true;
    }
    ArgList args;
  };
  GetFunctionArgs visitor;
  visitor.TraverseDecl(decl);

  auto res = visitor.args;
  args.insert(args.begin(), res.begin(), res.end());
}

std::string AbstractMethod::toString() const {
  const auto &name_ref = getMethodName();
  FormatString format(getComment());
  format.appendSequence(" ", prefix, ret_type, name_ref);

  FormatString arg_list;
  if (args.size() > 0)
    arg_list.appendRange(", ", ++args.begin(), args.end());
  arg_list.addBrackets();

  /* Add const qualifier if it needed */
  if (beginsWith(name_ref, getter_prefix))
    format.appendSequence(" ", arg_list, const_qualifier, suffix);
  else
    format.appendSequence(" ", arg_list, suffix);
  return format;
}

NonAbstractMethod::NonAbstractMethod(const std::string &class_name,
                                     const std::string &method_name,
                                     const clang::FunctionDecl *decl)
    : Method(class_name, method_name, decl) {
  ret_type = decl->getReturnType().getAsString();

  for (auto &param : decl->parameters()) {
    auto param_type = clang::QualType::getAsString(param->getType().split(),
                                                   clang::PrintingPolicy{{}});
    auto param_name = param->getQualifiedNameAsString();
    args.emplace_back(
        FormatString{}.appendSequence(" ", param_type, param_name));
  }
}

std::string NonAbstractMethod::toString() const {
  const auto &name_ref = getMethodName();

  FormatString format(getComment());
  format.appendSequence(" ", ret_type, name_ref);

  FormatString arg_list;
  if (args.size() > 0)
    arg_list.appendRange(", ", ++args.begin(), args.end());
  arg_list.addBrackets();

  format.append(arg_list);

  /* Add const qualifier if it needed */
  if (beginsWith(name_ref, getter_prefix)) {
    format.append(const_qualifier);
  }
  return format;
}

const NonAbstractMethod::ArgList &NonAbstractMethod::getArgs() const {
  return args;
}

const std::string &NonAbstractMethod::getReturnType() const { return ret_type; }

DefaultConstructor::DefaultConstructor(const std::string &class_name,
                                       const clang::FunctionDecl *decl)
    : NonAbstractMethod(class_name, class_name, decl) {}

std::string DefaultConstructor::toString() const {
  FormatString format(getComment());
  format.append(getClassName()).append(FormatString{}.addBrackets());
  return format;
}

ArgsConstructor::ArgsConstructor(const std::string &class_name,
                                 const clang::FunctionDecl *decl)
    : NonAbstractMethod(class_name, class_name, decl) {}

std::string ArgsConstructor::toString() const {
  const auto &args = getArgs();

  FormatString format(getComment());
  format.append(getClassName());

  FormatString arg_list;
  arg_list.appendRange(", ", args.begin(), args.end());
  arg_list.addBrackets();

  format.append(arg_list);
  return format;
}

Destructor::Destructor(const std::string &class_name,
                       const clang::FunctionDecl *decl)
    : NonAbstractMethod(class_name, class_name, decl) {}

CopyConstructor::CopyConstructor(const std::string &class_name,
                                 const clang::FunctionDecl *decl)
    : NonAbstractMethod(class_name, class_name, decl) {}

std::string CopyConstructor::toString() const {
  FormatString format(getComment());

  auto args = FormatString{}
                  .appendSequence(" ", const_qualifier, getClassName(), "&src")
                  .addBrackets();

  FormatString copy_costructor(getClassName());
  copy_costructor.append(args);

  FormatString assign_operator(getClassName() + "&");
  assign_operator.append("operator=").append(args);

  format.appendSequence(";\n", copy_costructor, assign_operator);
  return format;
}

std::string Destructor::toString() const {
  FormatString format(getComment());
  format.append(prefix)
      .append(getClassName())
      .append(FormatString{}.addBrackets());
  return format;
}

bool GetVirtualFunctionList::VisitFieldDecl(clang::FieldDecl *d) {
  if (!d->getFunctionType())
    return true;
  funcs.push_back(d);
  return true;
}

const GetVirtualFunctionList::FunctionList &
GetVirtualFunctionList::getList() const {
  return funcs;
}

MethodFactory::MethodFactory() {
  specific_methods[default_constructor] = creator<DefaultConstructor>;
  specific_methods[args_constructor] = creator<ArgsConstructor>;
  specific_methods[copy_constructor] = creator<CopyConstructor>;
  specific_methods[destructor] = creator<Destructor>;
}

std::unique_ptr<NonAbstractMethod>
MethodFactory::createMethod(clang::FunctionDecl *decl) {
  auto method = decl->getNameInfo().getName().getAsString();
  auto method_it = method.find(delimiter);
  if (method_it == std::string::npos) {
    throw std::runtime_error("Wrong method name: " + method);
  }

  auto class_name = method.substr(0, method_it);
  auto method_name = method.substr(method_it + 1);

  auto it = specific_methods.find(method_name);
  if (it == specific_methods.end()) {
    return std::make_unique<NonAbstractMethod>(class_name, method_name, decl);
  }

  return it->second(class_name, decl);
}

std::unique_ptr<AbstractMethod>
MethodFactory::createMethod(const std::string &class_name,
                            clang::FieldDecl *decl) {
  return std::make_unique<AbstractMethod>(class_name, decl->getNameAsString(),
                                          decl);
}