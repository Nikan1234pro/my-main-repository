#pragma once

#include <sstream>
#include <string>

class FormatString {
  std::string text;

  friend std::ostream &operator<<(std::ostream &os, const FormatString &s);

  template <class Arg> std::string concat(Arg &&arg) {
    std::ostringstream s;
    s << std::forward<Arg>(arg);
    return s.str();
  }

  template <class Arg> std::string concat(const std::string &, Arg &&arg) {
    return concat(std::forward<Arg>(arg));
  }

  template <class Arg, class... Args>
  std::string concat(const std::string &sep, Arg &&arg, Args &&... args) {
    std::ostringstream s;
    s << std::forward<Arg>(arg) << sep;
    s << concat(sep, std::forward<Args>(args)...);
    return s.str();
  }

public:
  using size_type = std::string::size_type;
  using iterator = std::string::iterator;
  using const_iterator = std::string::const_iterator;

  FormatString();
  FormatString(char ch);
  FormatString(const char *ptr, size_t length);
  FormatString(std::string str);

  template <class... Args>
  FormatString &appendSequence(const std::string &sep, Args &&... args) {
    text += concat(sep, std::forward<Args>(args)...);
    return *this;
  }

  template <class Arg> FormatString &append(Arg &&arg) {
    text += concat(std::forward<Arg>(arg));
    return *this;
  }

  template <class InputIt>
  FormatString &appendRange(const std::string &sep, InputIt begin,
                            InputIt end) {
    if (begin == end)
      return *this;

    while (begin != end) {
      append(*begin++);
      append(sep);
    }
	/* Remove excess separator */
    text.erase(text.size() - sep.size());
    return *this;
  }

  FormatString &removeFirst(const std::string &str);
  FormatString &insertAt(const_iterator pos, const std::string &str);
  FormatString &removeBefore(char ch);
  FormatString &removeAfter(char ch);

  FormatString &addBrackets(char left = '(', char right = ')');
  FormatString &addSeparator(const char sep = ' ');

  operator std::string() const;
  size_type size() const;

  iterator begin();
  iterator end();

  const_iterator begin() const;
  const_iterator end() const;
};
