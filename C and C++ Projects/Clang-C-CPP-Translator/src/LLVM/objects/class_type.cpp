#include "class_type.h"
#include "format_string.h"

Class::Class(const clang::TypedefDecl *decl)
    : SyntaxObject(decl), class_name(decl->getNameAsString()) {}

std::string Class::toString() const {
  FormatString format(getComment());
  format.appendSequence(" ", type_name, class_name);

  FormatString body;
  for (auto &method : methods) {
    body.append("\n");
    body.append(method->toString());
    body.append(";\n");
  }
  body.addBrackets('{', '}');
  format.append(body).append(';');
  return format;
}

void Class::addMethod(std::unique_ptr<Method> &&method) {
  methods.push_back(std::move(method));
}

const std::string &Class::getName() const { return class_name; }

void Class::setName(std::string &name) { this->class_name = name; }
