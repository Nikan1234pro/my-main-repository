#pragma once

#include <clang/AST/Decl.h>
#include <clang/AST/RawCommentList.h>
#include <ostream>
#include <sstream>

class PrintableObject {
public:
  virtual std::string toString() const = 0;
  virtual ~PrintableObject() = default;
};

class SyntaxObject : public PrintableObject {
public:
  class Comment {
    const clang::RawComment *raw = nullptr;
    const clang::Decl *decl = nullptr;

  public:
    Comment(const clang::Decl *d);

    const clang::RawComment *getRaw() const;
    operator std::string() const;
  };

  SyntaxObject(const clang::Decl *d);

  const Comment &getComment() const;
  clang::SourceLocation getDeclStart() const;
  clang::SourceLocation getDeclEnd() const;

private:
  const clang::Decl *decl = nullptr;
  Comment comment;
};

class UnspecifiedObject : public PrintableObject {
  clang::SourceLocation B;
  clang::SourceLocation E;

  const clang::SourceManager &manager;

public:
  UnspecifiedObject(const clang::SourceLocation &begin,
                    const clang::SourceLocation &end,
                    const clang::SourceManager &sm);

  std::string toString() const override;
};