#include "enum.h"
#include "format_string.h"

#include <iostream>
#include <clang/Lex/Lexer.h>

Enum::Enum(const clang::TypedefDecl *d) : SyntaxObject(d), decl(d) {}

std::string Enum::toString() const {
  /* Get enum name */
  std::string enum_name = decl->getNameAsString();

  /* Get enum source from input file */
  auto &context = decl->getASTContext();
  auto &sm = context.getSourceManager();
  auto &lopt = context.getLangOpts();

  clang::SourceLocation begin(decl->getBeginLoc());
  clang::SourceLocation end(
      clang::Lexer::getLocForEndOfToken(decl->getEndLoc(), 0, sm, lopt));

  FormatString source(sm.getCharacterData(begin),
                      sm.getCharacterData(end) - sm.getCharacterData(begin));

  source.removeBefore('{').removeAfter('}');
  source.insertAt(source.begin(),
                  FormatString{}.appendSequence(" ", type_name, enum_name));
  source.insertAt(source.begin(), getComment()).append(';');
  return source;
}
