#include "format_string.h"

FormatString::FormatString() {}

FormatString::FormatString(char ch) { text += ch; }

FormatString::FormatString(const char *ptr, size_t length)
    : text(ptr, length) {}

FormatString::FormatString(std::string str) : text(std::move(str)) {}

FormatString &FormatString::removeFirst(const std::string &str) {
  auto pos = text.find(str);
  if (pos == std::string::npos)
    return *this;
  text.erase(pos, str.size());
  return *this;
}

FormatString &FormatString::insertAt(const_iterator pos,
                                     const std::string &str) {
  if (str.empty())
    return *this;

  text.insert(pos, str.begin(), str.end());
  return *this;
}

FormatString &FormatString::removeBefore(char ch) {
  auto pos = text.find(ch);

  if (pos == std::string::npos) {
    text.clear();
    return *this;
  }
  text.erase(0, pos);
  return *this;
}

FormatString &FormatString::removeAfter(char ch) {
  auto pos = text.find(ch);
  if (ch != std::string::npos)
    text.erase(++pos);
  return *this;
}

FormatString &FormatString::addBrackets(char left, char right) {
  text.insert(text.begin(), left);
  text.push_back(right);
  return *this;
}

FormatString &FormatString::addSeparator(const char sep) {
  text.push_back(sep);
  return *this;
}

FormatString::size_type FormatString::size() const { return text.size(); }

FormatString::operator std::string() const { return text; }

FormatString::iterator FormatString::begin() { return text.begin(); }

FormatString::iterator FormatString::end() { return text.end(); }

FormatString::const_iterator FormatString::begin() const {
  return text.begin();
}

FormatString::const_iterator FormatString::end() const { return text.end(); }

std::ostream &operator<<(std::ostream &os, const FormatString &s) {
  os << s.text;
  return os;
}
