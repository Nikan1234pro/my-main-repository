#pragma once

#include "object.h"
#include "method.h"

#include <list>

class Class : public SyntaxObject {
  std::string class_name;
  std::list<std::unique_ptr<Method>> methods;

  const std::string type_name = "class";
public:
  Class(const clang::TypedefDecl *decl);

  std::string toString() const override;

  void addMethod(std::unique_ptr<Method>&& method);

  const std::string &getName() const;
  void setName(std::string &name);
};