#pragma once

#include "object.h"

#include <clang/AST/Decl.h>
#include <clang/AST/ASTContext.h>
#include <string>

class Enum : public SyntaxObject {
public:
  Enum(const clang::TypedefDecl *d);

  std::string toString() const;

private:
  const clang::TypedefDecl *decl;

  const std::string type_name = "enum class";
};