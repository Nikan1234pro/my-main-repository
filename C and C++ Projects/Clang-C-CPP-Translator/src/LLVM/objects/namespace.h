#pragma once

#include "object.h"

class Namespace : public PrintableObject {
	std::vector<std::unique_ptr<PrintableObject>> objects;
	const std::string prefix = "namespace YSK";
public:
	static const inline std::string name = "YSK";

	Namespace();
	std::string toString() const override;

	void addObject(std::unique_ptr<PrintableObject>&& object);
};