#include "object.h"

#include <clang/AST/ASTContext.h>
#include <clang/Lex/Lexer.h>

SyntaxObject::SyntaxObject(const clang::Decl *d) : decl(d), comment(d) {}

const SyntaxObject::Comment &SyntaxObject::getComment() const {
  return comment;
}

clang::SourceLocation SyntaxObject::getDeclStart() const {
  auto _A = decl->getSourceRange().getBegin();
  
  const auto *comment = getComment().getRaw();

  if (!comment)
    return _A;

  auto _B = comment->getBeginLoc();

  clang::BeforeThanCompare<clang::SourceLocation> isBefore(
      decl->getASTContext().getSourceManager());
  return (isBefore(_A, _B)) ? (_A) : (_B);
}

clang::SourceLocation SyntaxObject::getDeclEnd() const {
  auto &context = decl->getASTContext();
  auto &sm = context.getSourceManager();
  auto &lopt = context.getLangOpts();

  auto _A = clang::Lexer::getLocForEndOfToken(decl->getEndLoc(), 0, sm, lopt);

  const auto *comment = getComment().getRaw();
  if (!comment)
    return _A;

  auto _B =
      clang::Lexer::getLocForEndOfToken(comment->getEndLoc(), 0, sm, lopt);

  clang::BeforeThanCompare<clang::SourceLocation> isBefore(
      decl->getASTContext().getSourceManager());
  return (!isBefore(_A, _B)) ? (_A) : (_B);
}

SyntaxObject::Comment::Comment(const clang::Decl *d) : decl(d) {
  clang::ASTContext &ctx = decl->getASTContext();
  raw = ctx.getRawCommentForDeclNoCache(decl);
}

SyntaxObject::Comment::operator std::string() const {
  if (nullptr == raw)
    return {};

  auto &mgr = decl->getASTContext().getSourceManager();
  std::string comment_text = raw->getRawText(mgr);
  comment_text.push_back('\n');
  return comment_text;
}

const clang::RawComment *SyntaxObject::Comment::getRaw() const { return raw; }

UnspecifiedObject::UnspecifiedObject(const clang::SourceLocation &begin,
                                     const clang::SourceLocation &end,
                                     const clang::SourceManager &sm)
    : B(begin), E(end), manager(sm) {}

std::string UnspecifiedObject::toString() const {
  std::string source(manager.getCharacterData(B),
                     manager.getCharacterData(E) - manager.getCharacterData(B));
  return source;
}