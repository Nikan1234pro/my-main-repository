#pragma once
#include "pch.h"
#include <string>
#include <vector>

class Parser {
  int arg_count;

  clang::tooling::CommonOptionsParser op;
  static llvm::cl::OptionCategory MyToolCategory;
public:
  Parser(int argc, const char **argv);
  void parse(const std::string &filename);
};