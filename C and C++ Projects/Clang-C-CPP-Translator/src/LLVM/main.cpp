#include "parser.h"
#include <iostream>

int main(int argc, const char **argv) {
  setlocale(LC_ALL, "rus");
  const std::string input_catalogue = "input";
  const std::array<const char *, 3> opts = {"--", "-x", "c++"};

  if (!fs::exists(input_catalogue)) {
    std::cerr << "Create input dir" << std::endl;
    return EXIT_FAILURE;
  }

  try {
    std::list<std::string> compilation_list;
    std::transform(fs::directory_iterator(input_catalogue), {},
                   std::back_inserter(compilation_list),
                   [](const fs::directory_iterator::value_type &entry) {
                     return fs::canonical(entry.path()).u8string();
                   });

    if (!compilation_list.size()) {
      std::cerr << "Input directory is empty";
      return EXIT_FAILURE;
    }

    std::vector<const char *> args;
    args.reserve(compilation_list.size());
    std::transform(compilation_list.begin(), compilation_list.end(),
                   std::back_inserter(args),
                   [](const std::string &s) { return s.c_str(); });
    args.insert(args.end(), opts.begin(), opts.end());
    if (argc > 1) {
	  /* Args like --include_path etc. */
      args.insert(args.end(), argv + 1, argv + argc);
    }

    Parser parser(args.size(), args.data());
    for (auto &file : compilation_list) {
      std::cout << "Parsing: " << file << std::endl;
      parser.parse(file);
    }
  } catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
  std::cout << "Press any key to exit...";
  std::getchar();
  return EXIT_SUCCESS;
}