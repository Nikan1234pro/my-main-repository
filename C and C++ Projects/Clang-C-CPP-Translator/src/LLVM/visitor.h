#pragma once
#include "pch.h"

#include <list>
#include <string>
#include <unordered_map>

#include "objects/class_type.h"
#include "objects/namespace.h"

using ObjectList = std::list<std::unique_ptr<PrintableObject>>;

class SyntaxVisitor : public clang::RecursiveASTVisitor<SyntaxVisitor> {
private:
  clang::ASTContext *astContext;
  ObjectList objects;

  std::unordered_map<std::string, Class *> classes;
  Namespace *namespace_ = nullptr;

  MethodFactory method_factory;

  clang::SourceLocation begin;
  clang::SourceLocation end;

  void updateNamespace(std::unique_ptr<SyntaxObject> &&o);

  void handleUnspecifiedCode(const SyntaxObject *o);

  void handleEnumTypedef(clang::TypedefDecl *td);
  void handleStructTypedef(clang::TypedefDecl *td);

public:
  SyntaxVisitor(clang::CompilerInstance *CI);

  virtual bool VisitTypedefDecl(clang::TypedefDecl *td);

  virtual bool VisitFunctionDecl(clang::FunctionDecl *func);

  void Finalize();

  const ObjectList& getResult() const;
};

class SyntaxASTConsumer : public clang::ASTConsumer {
private:
  SyntaxVisitor visitor;
  std::string input_filename;
public:
  SyntaxASTConsumer(clang::CompilerInstance *CI, const std::string& file_path);

  virtual void HandleTranslationUnit(clang::ASTContext &Context);
};

class SyntaxFrontendAction : public clang::ASTFrontendAction {
public:
  SyntaxFrontendAction();

  virtual std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance &CI, llvm::StringRef file_path);
};