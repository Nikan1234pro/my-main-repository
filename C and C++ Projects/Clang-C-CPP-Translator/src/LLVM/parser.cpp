﻿#include "parser.h"
#include "visitor.h"

Parser::Parser(int argc, const char **argv)
    : arg_count(argc), op(arg_count, argv, MyToolCategory) {
  if (!fs::exists(settings::output_dir)) {
    if (!fs::create_directory(settings::output_dir))
      throw std::runtime_error("Couldn't create output dir");
  }
}

void Parser::parse(const std::string &filename) {
  clang::tooling::ClangTool tool(op.getCompilations(), filename);
  tool.run(
      clang::tooling::newFrontendActionFactory<SyntaxFrontendAction>().get());
}

llvm::cl::OptionCategory Parser::MyToolCategory("My tool options");